@extends('layouts.default')
@section('content')


<style>
    .grid-item
    {
        width: 280px; 
        float: left;
    }
    .grid {
        margin: 0 auto;
    }
    /* clearfix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }
    blockquote{
        border-left:none
    }

    .quote-badge{
        background-color: rgba(0, 0, 0, 0.2);   
    }

    .quote-box{

        overflow: hidden;
        margin-top: -50px;
        padding-top: -100px;
        border-radius: 17px;
        background-color: #DAE7F0;
        margin-top: 25px;
        color:#6B5B6B;
        box-shadow: 3px 5px 14px 1px #E0E0E0;
        margin:5px 5px;

    }

    .quotation-mark{

        margin-top: -10px;
        font-weight: bold;
        font-size:100px;
        color:white;
        font-family: "Times New Roman", Georgia, Serif;
        color: #6B5B6B;

    }

    .quote-text{

        font-size: 19px;
        margin-top: -65px;
        color: #6B5B6B;
    }
</style>
<div class="review-breadcrumbs">
    <img src="images/reviews.png" class="img-responsive" alt="">
</div>
<div class="review-bg">
    <div class="container-fluid mt20">
        <div class="grid js-masonry" id="blog-width">
            
           
            @if($reviewCollection->isNotEmpty())
            @foreach($reviewCollection as $reviewObj) 
            <div class="grid-item">
                <blockquote class="quote-box">
                    <p class="quotation-mark">
                        “
                    </p>
                    <p class="quote-text">
                       {{$reviewObj->descrption}} 
                    </p>
                    <hr>
                    <div class="blog-post-actions">
                        <p class="blog-post-bottom pull-left"> 
                        <!--- activity title, destnation title -->
                        {{ $reviewObj->activity ? $reviewObj->activity->title : "N/A"}}, {{ $reviewObj->activity && $reviewObj->activity->destination ? $reviewObj->activity->destination->title : "N/A"}}
                            <br>
                            @for($i = 0;$i < $reviewObj->rating; $i++)
                                <i class="fa fa-star" aria-hidden="true"></i>
                            @endfor
                        </p>
                        <p class="blog-post-bottom pull-right">
                            <span class="badge quote-badge"></span>  ❤
                        </p>
                    </div>
                </blockquote>
            </div>
            @endforeach
            @endif
           


        </div>
    </div>
</div>
<script>
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 280,
        isFitWidth: true,
        "gutter": 10
    });
</script>

@stop
