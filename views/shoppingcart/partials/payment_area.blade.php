<div class="row">
    <div class="col-md-12">
        <div class="title-all-dest">
            <h2 class="text-uppercase">Payment Type </h2>
        </div>                    
    </div>                
</div>

<!-- Credit Card Payment -->
<?php
/*
<div class="well well-shadow">
    <div class="demo-container">
        <div class="card-wrapper"></div>
        <br>
        <div class="form-container active" id="payment_form">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="cardNumber">Card Number</label>
                        <div class="input-group">
                            <input type="tel" size="16" name="card[card_number]" id="number" class="form-control user_card_number" data-stripe="number" value="" placeholder="Card number" required="">

                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        </div>
                    </div>                            
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="cardNumber" class="text-capitalize">Cardholder Name</label>
                        <input type="text" value="" name="card[card_holder_name]" id="name" class="form-control user_card_name" size="20" data-stripe="name" placeholder="Cardholder Name">
                    </div>                            
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="cardNumber" class="text-capitalize">Expiration</label>
                        <input type="tel" value="" name="card[card_expiry]" id="expiry" class="form-control user_card_expiry" size="20" data-stripe="expiry" placeholder="MM/YY">
                    </div>                            
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="cardNumber" class="text-capitalize">CVC</label>
                        <input type="text" value="" name="card[card_cvc]" id="cvc" class="form-control user_card_cvc" size="20" data-stripe="cvc" placeholder="CVC">
                    </div>                            
                </div>
            </div>
        </div>
    </div>
</div>
*/
?>
<!-- Credit Card Payment - END -->

<div class="">
    <div id="collapseTwo" aria-expanded="false" class="collapse">
        <div class="well well-shadow">
            <div class="row">                        
                <div class="col-md-9 col-sm-6">
                    <img src="images/PayPal.png" class="img-responsive">
                </div>
                <div class="col-md-3 col-sm-6 text-center mt10">
                    <div class="paypal-button">
                        <button type="button" class="text-center pink_btn_checkout  text-uppercase">Pay Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                
<div class="well well-shadow">
    <div class="row">
        <div class="col-md-6 col-sm-4">
            <div class="promo-code">
                <p>Use Promo Code</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-8 text-right">
                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                    <div class="input-group">

                        <input type="text" class="form-control form-control-custom" id="promo_code" placeholder="Coupon">
                        <div style="cursor:pointer" onclick="return redeemCoupen()" class="input-group-addon pink_btn_checkout">Redeem</div>

                    </div>
                </div>

        </div>
    </div>
</div>
<div class="well well-shadow">
    <div class="row">
        <div class="col-md-8">
            <p>By clicking Pay Now, you agree that you have read and understood our Terms & Conditions and Cancelation Policy</p>
        </div>
        <div class="col-md-4 text-right">
            <input type="submit" class="text-center pink_btn_checkout text-uppercase" name="Submit" value="Book Now" >
        </div>
    </div>
</div>