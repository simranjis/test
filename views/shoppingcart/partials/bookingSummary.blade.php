<div class="col-sm-4">
    <div class="">            
        <div class="row">
            <div class="col-md-12">
                @foreach($cart_contents as $key => $value)

                <?php $activity_id = $value['activities_id']; ?>
                <div class="add-to-cart-box @if($count > 0) mt10 @endif">
                    <h2>
                        Booking Summary
                    </h2>
                    <p>{{ $value['destinations_id'] }} - {{ $value['title'] }}</p>
                    <p>{{ $value['date'] }}</p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive checkout-table">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><strong>User</strong></td>
                                            <td class="text-center"><strong>Quantity</strong></td>
                                            <td class="text-center"><strong>Price</strong></td>
                                            <td class="text-right"><strong>Totals</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total = $persons = $service_fee = 0; ?>    
                                        <?php $activities_sub_total = 0 ?>
                                        <?php $activities_service_fee = 0 ?>
                                        <?php $activities_total = 0; ?>

                                        @foreach($value['items'] as $subkey => $subvalue)
                                        <tr>
                                            @if($subvalue['pricefor'] == 'all')
                                            <td>{{ ucwords($subvalue['title']) }}</td>
                                            @else
                                            <td>{{ ucwords($subvalue['pricefor']) }}</td>
                                            @endif

                                            <td class="text-center">
                                                {{ $subvalue['qty'] }}
                                            </td>
                                            <td class="text-center">
                                                {{ Pricing::getPrice(Pricing::getActivityPrice2($subvalue['price'], $value['activities_id'])) }}
                                            </td>
                                            <td class="text-right">
                                                {{ Pricing::getPrice(Pricing::getActivityPrice2(($subvalue['qty']*$subvalue['price']), $value['activities_id'])) }}
                                            </td>
                                        </tr>
                                        <?php
                                        $total += Pricing::getActivityPrice2(($subvalue['price'] * $subvalue['qty']), $value['activities_id']);
                                        $activities_sub_total += Pricing::getActivityPrice2(($subvalue['price'] * $subvalue['qty']), $value['activities_id']);
                                        if ($subvalue['type'] === 'person'):
                                            $persons += $subvalue['qty'];
                                        endif;
                                        ?>

                                        @endforeach
                                        <?php $final_total += $total; ?>
                                        <?php $final_subtotal += $total; ?>


                                        @if($value['servicefee_status'] === 'yes')
                                        @if($value['servicefee_type'] === 'person')
                                        <!-- Add the Service Fee servicefee_type service_fee -->
                                        <?php $service_fee = (Pricing::getActivityPrice2($value['service_fee'], $value['activities_id']) * $persons); ?>
                                        <?php if ($persons != 0) { ?>
                                            <tr>
                                                <td class="no-line">Service Fee</td>
                                                <td class="no-line text-center">
                                                    {{ $persons }}
                                                </td>
                                                <td class="no-line text-center">
                                                    {{ Pricing::getPrice(Pricing::getActivityPrice2($value['service_fee'] , $value['activities_id'])) }}
                                                </td>
                                                <td class="no-line text-right">
                                                    {{ Pricing::getPrice($service_fee) }}
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        @else
                                        <?php $service_fee = Pricing::getActivityPrice2($value['service_fee'], $value['activities_id']); ?>
                                        <?php if ($persons != 0) { ?>
                                            <tr>
                                                <td class="no-line">Service Fee</td>
                                                <td class="no-line text-center">1</td>
                                                <td class="no-line text-center">{{ Pricing::getPrice(Pricing::getActivityPrice2($value['service_fee'] , $value['activities_id'])) }}</td>
                                                <td class="no-line text-right">{{ Pricing::getPrice($service_fee) }}</td>
                                            </tr>
                                        <?php } ?>
                                        @endif
                                        <!-- Add the Service Fee -->
                                        <?php $total += $service_fee; ?>
                                        <?php $final_servicefee += $service_fee; ?>
                                        <?php $final_total += $service_fee; ?>
                                        <?php $activities_service_fee = $service_fee; ?>

                                        @endif

                                        <!-- Add the Service Fee -->

                                        <tr>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line text-center"><strong>Total</strong></td>
                                            <td class="no-line text-right">
                                                {{ Pricing::getPrice( $total ) }}
                                            </td>
                                        </tr>
                                    <input type="hidden" name="activitySubTotal[{{$activity_id}}]" id="activitySubTotal{{$activity_id}}" value="{{$total}}">
                                    <!-- promo code row -->
                                    <tr class="hide promocode_section" id="promocode_<?php echo $value['activities_id']; ?>">
                                        <td colspan="4">
                                            <table style="width:95%">
                                                <tr>
                                                    <td class="text-left">
                                                        <strong>Promo Code</strong>
                                                    </td>
                                                    <td class="text-right">
                                                        <strong>Discount</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left">
                                                        <span id="promotion_code_<?php echo $value['activities_id']; ?>"></span>
                                                    </td>
                                                    <td class="text-right">
                                                        <span id="discount_value_<?php echo $value['activities_id']; ?>"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left">
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo Pricing::getSymbol(); ?>

                                                        <span id="discounted_value_<?php echo $value['activities_id']; ?>">

                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                        
                </div>
                <?php $activities_total = ($activities_sub_total + $activities_service_fee); ?>
                <input type="hidden" class="activity_ids" name="activityIdsArr[{{ $value['activities_id'] }}]" id="activity_id_{{ $value['activities_id'] }}" value="{{ $value['activities_id'] }}" >
                <input type="hidden" name="activity_subtotal[{{ $value['activities_id'] }}]" value="{{ $activities_sub_total }}"> 
                <input type="hidden" name="activity_servicefee[{{ $value['activities_id'] }}]" value="{{ $activities_service_fee }}"> 
                <input type="hidden" name="activity_total[{{ $value['activities_id'] }}]" value="{{ $activities_total }}"> 
                <?php $count++; ?>


                <!-- Localguide Area -->
                <?php
                $commision = 0;
                $commision_amount = 0;
                $localguide_amount = 0;
                $activity = $value['activity'];
                if ($activity->addedbytype == 'localguide') {
                    $commision = $activity->localguide->commision;
                    $commision_amount = ($activities_total - $service_fee) * ($commision / 100) + $service_fee;
                    $localguide_amount = $activities_total - $commision_amount;
                    ?>

                    <!-- Commision -->
                    <input type="hidden" name="commision[{{ $value['activities_id'] }}]" id="commision_value{{ $value['activities_id'] }}" value="{{ number_format($commision , 2) }}">
                    <input type="hidden" name="commision_amount[{{ $value['activities_id'] }}]" id="commision_amount_value{{ $value['activities_id'] }}" value="{{ number_format($commision_amount , 2) }}">

                    <input type="hidden" name="localguide_amount[{{ $value['activities_id'] }}]" id="localguide_amount_value{{ $value['activities_id'] }}" value="{{ number_format($localguide_amount , 2) }}">

                    <input type="hidden" name="localguide_id[{{ $value['activities_id'] }}]" id="localguide_id{{ $value['activities_id'] }}"  value="{{ $activity->localguide_id }}" />
                    <!-- Commision - END -->

                    <div class="add-to-cart-box mt10 add-to-cart-box-amount" id="localguide_area{{$value['activities_id']}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive checkout-table">
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="text-left">
                                                    <strong>
                                                        <span id="commision{{$value['activities_id']}}">
                                                            <?php echo $commision; ?>%
                                                        </span>
                                                    </strong>
                                                </td>
                                                <td class="text-right pay-ammount">
                                                    <span class="text-center pink_btn_checkout pink_btn_checkout_copy text-uppercase fixed-at-bottom">
                                                        Pay -
                                                        <?php echo Pricing::getSymbol(); ?>

                                                        <span id="commision_amount{{$value['activities_id']}}">
                                                            <?php
                                                            $commision_amount = ($activities_total - $service_fee) * ($commision / 100) + $service_fee;

                                                            echo number_format($commision_amount, 2);
                                                            ?>                              
                                                        </span>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-condensed">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left">Amount Due</td>
                                                        <td class="text-right">

                                                            <div class="pay-ammount">
                                                                <?php echo Pricing::getSymbol(); ?>

                                                                <span id="amount_due{{$value['activities_id']}}">
                                                                    <?php
                                                                    $localguide_amount = $activities_total - $commision_amount;

                                                                    echo number_format($localguide_amount, 2);
                                                                    ?>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p>Balance to be Paid Directly to the Local Guide (in cash)</p>
                                        </div>
                                    </div>
                                </div>                                        
                            </div>
                        </div>
                    </div>
                <?php } ?>



                @endforeach
            </div>
        </div>
    </div>
    <div class="destory-sticky-1">
        <div class="sticky-wrapper">
            <div class="basic-dist-fix">            
                <div class="row">
                    <div class="col-md-12">
                        <div class="add-to-cart-box mt10 add-to-cart-box-amount">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive checkout-table">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <td class="text-left"><strong>Total</strong></td>
                                                    <td class="text-right"><strong>Payment Amount</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                <tr>
                                                    <td>
                                                        <?php echo Pricing::getSymbol(); ?>

                                                        <span class="grand_total">
                                                            <?php
                                                            echo number_format($final_total, 2);
                                                            ?>
                                                        </span>
                                                    </td>
                                                    <td class="text-right pay-ammount">
                                                        <?php echo Pricing::getSymbol(); ?>

                                                        <span class="grand_total">
                                                            <?php
                                                            echo number_format($final_total, 2);
                                                            ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<input type="hidden" name="promo_code_applied" id="promo_code_applied" value="" >
<input type="hidden" name="subtotal" value="{{ $final_subtotal }}">
<input type="hidden" name="tax" value="{{ $final_servicefee }}">
<input type="hidden" name="total" value="{{ $final_total }}">
<input type="hidden" name="currencies_id" value="{{ Pricing::getCurrenciesId() }}">
