@extends('layouts.default')
@section('content')
<!--- Comes Here --> 
<?php
//echo '<pre>';
//print_r($cart_contents);
//echo '</pre>'
?>

<script type="text/javascript">
    function substract(button) {

        var input = $(button).parent().parent().find("input");
        var initial = input.val();

        if (initial > 0) {
            input.val(parseInt(parseInt(initial) - parseInt(1)));
        } else {
            $(button).attr("disabled", "disabled");
        }

    }

    function add(button) {

        var input = $(button).parent().parent().find("input");
        var initial = input.val();

        input.val(parseInt(parseInt(initial) + parseInt(1)));

        input.val(parseInt(parseInt(initial) + parseInt(1)));
        $(".change-qty").removeAttr("disabled");
    }

    $(function () {
        $('#checkout-button').click(function (e) {




            @if (Session::has('traveller'))
            window.location = "cart/checkout";
            @else
            if ($('input[name=feel]') == 'free') {
            } else {
                $('#travellerloginForm').append('<input type="hidden" name="checkout" value="2">');
                $('#myModal3').modal("show");
                e.preventDefault();
            }


            @endif

            }
            );


            $('.cart-delete').click(function (e) {

                e.preventDefault();
                //console.log($(this).data('locations'));
                var dataObj = {'rowid': $(this).data('locations')[0].rowid};
                //console.log(dataObj.activities_id);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })

                $.ajax({
                    url: 'cart/delete',
                    type: 'post',
                    data: dataObj,
                    async: false,
                    beforeSend: function () {
                        //Can we add anything here.
                    },
                    cache: true,
                    dataType: 'json',
                    crossDomain: true,
                    success: function (data) {
                        if (data.code == 1) {
                            swal({
                                title: "Success",
                                text: data.message,
                                type: "success"
                            }, function () {
                                window.location = "cart";
                            });
                        } else {
                            bootbox.alert(data.message);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });


            });

        });
</script>
<style>
    .quantity-width {
        min-width: 131px;
        float:none;
    }
</style>

<div class="">
    <img src="images/shopcar_tbanner.png" class="img-responsive center-block" alt="who">
</div>   
<!-- shoping cart html start here -->
<div class="container">
    <div class="row">
        <div class="cart-table">
            <div class="panel margin-top-40">
                <div class="panel-heading pink_bg">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart </h4>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($cart_contents) > 0)
                {!! Form::open(['url' => 'cart/update', 'name' => 'cartUpdateForm' , 'class'=>'cartUpdateForm', 'id'=>'cartUpdateForm']) !!}                                
                <div class="panel-body">
                    <div class="over-scroll">
                        <div class="table-responsive">
                            <table id="cart" border='0' class="table table-bordered">
                                <thead class="margin-bottom-40">
                                    <tr>
                                        <!--<th class='col-xs-1 col-sm-1 col-md-1'><input name="select[]" type="checkbox" value="">Delete</th>-->
                                        <th class=''>Activity Description</th>
                                        <th class=''>Date</th>
                                        <th class=''>Units</th>
                                        <th class=''>Subtotal</th>
                                        <th class=''>Service Fee</th>
                                        <th class=''></th>
                                    </tr>
                                </thead>
                                <?php
                                $total_final = 0;
                                $totalServiceFee = 0;
                                ?>

                                @foreach($cart_contents as $key => $value)
                                <input type="hidden" name="activities_id[]" value="{{ $value['activities_id'] }}">
                                <tbody>
                                    <tr>
                                        <!--<td class='col-xs-12 col-sm-1 col-md-1' data-th="input"><input name="delete[{{ $value['activities_id'] }}]" type="checkbox" value="1"></td>-->
                                        <td class='' data-th="Activity Description">
                                            <div class="row">
                                                <div class="col-sm-12 hidden-xs"><a href="{{ url('destinations2/') }}/{{ $value['dest_id'] }}/{{ $value['activities_id'] }}"><img src="uploads/{{ $value['image'] }}" style="width:180px;" alt="..."/></a></div>

                                                <div class="col-sm-12">
                                                    <h4 class="nomargin">{{ $value['destinations_id'] }}</h4>
                                                    <p class="text-muted">{{ $value['title'] }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class='' data-th="Date">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    {{ $value['date'] }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class='' data-th="Units">

                                            <?php
                                            $total = 0;
                                            $serviceFee = 0;
                                            ?>

                                            @foreach($value['items'] as $subkey => $subvalue)
                                            <input type="hidden" name="rowid[{{ $value['activities_id'] }}][{{ $subkey }}]" value="{{ $subvalue['rowid'] }}">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    @if($subvalue['pricefor'] === 'all')
                                                    <span class="pull-left">{{ ucwords($subvalue['title']) }}</span>
                                                    @else
                                                    <span class="pull-left">{{ ucwords($subvalue['pricefor']) }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="quantity-width">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-black btn-number change-qty" data-type="minus" onclick="substract(this)" data-field="quantity[]">
                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                </button>    
                                                            </span>
                                                            <input type="text" name="qty[{{ $value['activities_id'] }}][{{ $subkey }}]"  class="form-control input-number text-center qty-box"  value="{{ $subvalue['qty'] }}" min="1" max="10">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-pink btn-number change-qty" data-type="plus" onclick="add(this)" data-field="quantity[]">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>    
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $total += $subvalue['qty'] * $subvalue['price'];
                                            if ($value['servicefee_status'] == 'yes' && ($subvalue['type'] == $value['servicefee_type'])) {
                                                $serviceFee += $subvalue['qty'] * Pricing::getActivityPrice2($value['service_fee'], $value['activities_id']);
                                            }
                                            ?>
                                            @endforeach
                                            <?php
                                            if($value['servicefee_status'] == 'yes' && $value['servicefee_type'] == 'booking'){
                                               $serviceFee = Pricing::getActivityPrice2($value['service_fee'], $value['activities_id']); 
                                            }
                                            $totalServiceFee += $serviceFee;
                                            $total_final += Pricing::getActivityPrice($total, $value['activities_id']);
                                            ?>

                                        </td>
                                        <td class='' data-th="Subtotal" class="text-center"> <?php echo Pricing::getSymbol(); ?> {{ Pricing::getActivityPrice($total , $value['activities_id']) }}
                                        </td>
                                        <td class='' data-th="service-fee" class="text-center"> <?php echo number_format($serviceFee, 2); ?>
                                        </td>
                                        <td class="actions" data-th="">
                                            <!--<button class="btn btn-info btn-sm"><i class="fa fa-calendar"></i></button>-->
                                            <button class="btn btn-danger btn-sm cart-delete" data-locations='[{"rowid":"{{ $subvalue['rowid'] }}"}]'><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach

                            </table>
                        </div>
                        <div class="tfooter">
                            <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="cost">
                                                <h4>
                                                    <span class="">Activity Total:</span>
                                                    <span class="t_main t22"> <label id="totalPcice" class="color-pink">
<?php echo Pricing::getSymbol(); ?> <?php echo number_format(($total_final + $totalServiceFee), 2); ?>
                                                        </label>
                                                    </span>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="submit" value="Update" id="update" class="pink_btn_checkout text-center text-uppercase">

                                            <input type="button" value="Checkout" id="checkout-button" class="pink_btn_checkout text-center text-uppercase">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5"></div>             
                    <div class="col-xs-2 col-sm-2 col-md-2">

                    </div>
                    <div class="col-xs-5"></div>
                </div>
                {!! Form::close() !!}
                @else
                <div class="row">
                    <div class="col-xs-5"></div>             
                    <div class="col-xs-2 col-sm-2 col-md-2 margin-top-40">
                        No Items Found
                    </div>
                    <div class="col-xs-5"></div>
                </div>
                @endif
            </div>
        </div>
    </div> 
</div>


<!-- shoping cart html end here -->
<style>


    button.pink_btn_login.m_btn_main {
        padding: 11px 33px;
    }
    .g_left span {
        margin-right: 13px;
    }
    .pink_bg{
        background: #ffc6d7 ;
    }

    @media only screen and (max-width: 767px) {
        .over-scroll {
            overflow-x: scroll;
        }
        .g_left, .g_left.pull-right, .g_left .pull-right, .g_rel{
            text-align: center;
        }
        .g_left.pull-right, .g_left .pull-right{
            float: none!important;
        }
        .cart-table #cart th, #cart td{
            font-size: 11px
        }
        .cart-table h4{
            font-size: 13px
        }
    }

</style>

<!-- Comes Here -->
@stop
