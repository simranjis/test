@extends('admin.layouts.defaultplain')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <img src="adminpanel/dist/img/logo-footer.png" alt="" class="img-responsive center-block">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        @if(Request::get('error'))
        <div style="color:red; padding:5px 0;">{{ Request::get('error')  }}</div>
        @endif
        {!! Form::open(['url' => 'admin/signin', 'name' => 'adminSignInForm' , 'class'=>'adminSignInForm', 'id'=>'adminSignInForm']) !!}                                
        <div class="form-group has-feedback">
            <input type="email" class="form-control" name="adminEmailAddress" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" name="adminPassword" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-12 text-center">
                <button type="submit" class="btn bg-navy btn-flat margin">Sign In</button>
            </div>
            {!! Form::close() !!}
            <!-- /.col -->
        </div>
        </form>

        <!-- /.social-auth-links -->

        <!--<a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>-->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@stop

