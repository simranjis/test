@extends('admin.layouts.defaultsidebar')
@section('content')
<script type="text/javascript">
  $(function() {
      
    $(".delete-pop").click(function(e) {
       var url = $(this).attr('href');
       bootbox.confirm("Confirm! Delete Selected Language", function(result){ 
           if(result) { 
              window.location = url;
           }
       });
       e.preventDefault();
    });
    
    //Let's Check the checkbox
    //set_default
    $('input[name=set_default]').change(function(){
        
        var dataObj = {'id':$(this).val()};

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            url: 'admin/others/language/setdefault',
            type: 'post',
            data: dataObj,
            async: false,
            beforeSend: function () {
                //Can we add anything here.
            },
            cache: true,
            dataType: 'json',
            crossDomain: true,
            success: function (data) {
                swal({
                    title: "Success",
                    text: data.message,
                    type: "success"
                }, function() {
                    window.location = "admin/others/language?message=Default+Language+Changed+Successfully";
                });
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


    }); 
    
  });
</script>

<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Languages
            <small>Languages</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Languages</li>
        </ol>
    </section>

    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif    
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <!--<i class="fa fa-flag-checkered" aria-hidden="true"></i>-->
                            <!--<h3>You haven’t Enteres any Destination yet.</h3>-->
                            <a href="{{ url('admin/others/language/add') }}" type="button" class="btn bg-maroon btn-flat margin">New Language</a>
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Languages</h3>
                                        <p class=""></p>
                                    </div>
                                    <!-- /.box-header -->
                                    @if($languages)
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Code</th>
                                                    <th>Default</th>
                                                    <th>Action</th>
                                                </tr>
                                                @foreach($languages as $list)
                                                <tr>
                                                    
                                                    <td>{{ $list['language'] }}</td>
                                                    <td>{{ $list['language_code'] }}</td>
                                                    <td><input type="radio" @if($list['set_default']) checked="checked" @endif name="set_default" value="{{ $list['id'] }}" ></td>
                                                    <td>
                                                        <a class="btn bg-maroon btn-flat margin" href="{{ 'admin/others/language/edit/' }}{{ $list['id'] }}" role="button">
                                                            Edit
                                                        </a>
                                                        <a class="btn bg-navy btn-flat margin delete-pop" href="{{ 'admin/others/language/remove/' }}{{ $list['id'] }}" role="button">
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                                <!-- Trigger/Open The Modal -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    @else
                                        <h3 style="padding: 5px;">You Don't have any Enteries on Destination yet.</h3>
                                    @endif
                                </div>
                                <!-- /.box -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- /.content -->
</div>






@stop