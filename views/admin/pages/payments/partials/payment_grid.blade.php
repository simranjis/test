<!-- general form elements -->
<div class="box box-primary">
    <!-- /.box-header -->
    <!-- form start -->
    <div class="">
        <div class="row">
            <div class="col-xs-12">                                
                <div class="">
                    <div class="box-header with-border box-header-color">
                        <h3 class="box-title sbold">Payments</h3>
                        <p class=""></p>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" id="manageAllPaymentsDiv">
                        <table id="manageAllPayments" class="table table-hover table-bordered"></table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</div>
