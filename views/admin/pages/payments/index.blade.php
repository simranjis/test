@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .input-group-addon{
        cursor: pointer !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payments
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Payments</li>
        </ol>
    </section>    

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>From Date</label>
                            <div class='input-group date' id='from_date1'>                                
                                <input type="text" name="from_date" class="form-control" id="from_date" placeholder="From Date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>To Date</label>
                            <div class='input-group date' id='to_date1'>                                
                                <input type="text" name="to_date" class="form-control" id="to_date" placeholder="To Date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>&nbsp;</label>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="search_button" onclick="return searchActivities()" type="button" class="btn bg-maroon btn-flat">Search</a>
                        </div>
                    </div>
                </div>
                @include('admin.pages.payments.partials.payment_grid')
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@stop
@section('page_scripts')
<script src="{{url('/js/admin/payments/payments.js')}}"></script>
@stop
