@extends('admin.layouts.defaultsidebar')
@section('content')


<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Destinations
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Destinations</li>
        </ol>
    </section>
    
                @if(Request::get('message'))
                    <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
                @endif    

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                            <!--<h3>You haven’t Enteres any Destination yet.</h3>-->
                            <a href="{{ url('admin/destinations/add') }}" type="button" class="btn bg-maroon btn-flat margin">New Destination</a>
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Destinations</h3>
                                        <p class=""></p>
                                    </div>
                                    <!-- /.box-header -->
                                    @if($destinations)
                                    
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Cover Photo</th>
                                                    <th>Continent</th>
                                                    <th>Country</th>
                                                    <th>City</th>
                                                    <!--<th>City</th>-->
                                                    <th>Action</th>
                                                </tr>
                                                
                                                @foreach($destinations as $destination)
                                                    
                                                <tr>
                                                    <td>
                                                        @if($destination['image'])
                                                            <img class="img-responsive" src="uploads/{{ $destination['image'] }}" alt="User profile picture" style="height: 80px"></td>
                                                        @else
                                                            <img class="img-responsive" src="images/missing-image-640x360-1-360x180.png" alt="User profile picture" style="height: 80px"></td>
                                                        @endif
                                                    <td>{{ $destination['regions_id'] }}</td>
                                                    <td>{{ $destination['countries_id'] }}</td>
                                                    <!--<td>Thailand</td>-->
                                                    <td>{{ $destination['title'] }}</td> 
                                                    <td>
                                                        <a class="btn bg-maroon btn-flat margin" href="{{ 'admin/destinations/edit/' }}{{ $destination['id'] }}" role="button">
                                                            Edit
                                                        </a>
                                                        <a class="btn bg-navy btn-flat margin" href="{{ 'admin/destinations/remove/' }}{{ $destination['id'] }}" role="button">
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                                    
                                                    
                                                
                                                
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <!-- /.box-body -->
                                    @else
                                    <h3 style="padding: 5px;">You Don't have any Enteries on Destination yet.</h3>
                                    @endif
                                </div>
                                <!-- /.box -->
                                
                            </div>
                        </div>
                    </div>
                </div>{{ $dest->links() }}
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


@stop