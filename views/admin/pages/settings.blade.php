@extends('admin.layouts.defaultsidebar')
@section('content')

<script src="//code.jquery.com/jquery.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Settings
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Settings</h3>
            </div>
            <div class="box-body">
                <!-- all local guides -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('title', 'Additional Conversion %', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('additional_conversion', $allSettingsArr[0]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'additional_conversion', 'placeholder'=>'Additional Conversion %')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::button('Save', array('id'=>'saveSettings', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveSettings()')) }}

                        <a href="{{ url('admin/dashboard') }}" class="btn bg-gray btn-flat collapsed" role="button">
                            Cancel
                        </a>
                    </div>                                      
                </div>
            </div>
        </div>
    </section>
</div>

@stop

@section('page_scripts')
<script src="js/admin/settings.js"></script>
@stop