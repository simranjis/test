@extends('admin.layouts.defaultsidebar')
@section('content')

<script
    src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
crossorigin="anonymous"></script>

<script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css" type="text/css" />

<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }


</style>
<script type="text/javascript">


    $(function () {

        if ($('#InputImageFile').val() != '') {
            $('#InputImageFile').val('');
        }


        $('#InputImageFile').on('change', function () {

            var ext = $(this).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {

                if ($('#preview').data('Jcrop')) {
                    // if need destroy jcrop or its created dom element here
                    $('#preview').data('Jcrop').destroy();
                    $('#preview').attr('src', 'http://placehold.it/360x180');
                    $("#preview").removeAttr("style");
                }
                $(this).val('');
                alert('invalid extension!');
            } else {
                if (this.files[0].size >= 2 * 1024 * 1024) {
                    alert("Image size can not exceeds 2 MB");
                    return false;
                } else {
                    var file_name = $(this).val();
                    if (file_name.length > 0) {
                        addJcrop(this);
                    }
                }
            }
        });


        var addJcrop = function (input) {
            if ($('#preview').data('Jcrop')) {
                // if need destroy jcrop or its created dom element here
                $('#preview').data('Jcrop').destroy();
            }

            file = input.files && input.files[0];
            var img = new Image();
            img.src = window.URL.createObjectURL(file);
            img.onload = function () {
                var width = img.naturalWidth,
                        height = img.naturalHeight;

//                if(width < 360 || width > 600 || height < 180) {
//                    $('#InputImageFile').val('');
//                    $("#preview").removeAttr("style");
//                    alert("Image size should be between 360x180 and 600px");
//                    $('#preview').attr('src', 'http://placehold.it/360x180');
//                } else {

                // this will add the src in image_prev as uploaded
                //if (input.files && input.files[0]) {
                if (file) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#preview").attr('src', e.target.result);
                        var box_width = $('#image_prev_container').width();
                        $("#preview").removeAttr("style");
//                        $('#preview').Jcrop({
//                            setSelect: [0, 0, 360, 180],
//                            minSize: [360, 180],
//                            maxSize: [360, 180],
//                            aspectRatio: 0,
//                            allowResize: true,
//                            onSelect: getCoordinates,
//                            onChange: getCoordinates,
//                        });
//                        image
                    }
                    reader.readAsDataURL(input.files[0]);
                }

                var getCoordinates = function (c) {
                    $('#cropx').val(c.x);
                    $('#cropy').val(c.y);
                    $('#cropw').val(c.w);
                    $('#croph').val(c.h);
                };

//                }
            }
        }


    });


</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Destination 
            <small>Destinations</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Destination</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/destinations/insert', 'name' => 'addDestination' , 'class'=>'addDestination', 'id'=>'addDestination', 'files'=>'true']  ) !!}                                
            <div class="col-md-12">
                <!-- general form elements -->
                <div  class="box box-primary" style="border-color:#3c8dbc;">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div  class="">
                        <div  class="row">
                            <div class="col-xs-12">                                
                                <div  class="">
                                    <div class="box-header with-border box-header-color" style="background:#3c8dbc; border-color:blue;">
                                        <h3 class="box-title sbold">My Destinations</h3>
                                        <!--<p class=""> Date 7 Jan 2018</p>-->
                                    </div>

                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Continent</label>
                                            <select class="form-control" name="regions_id">
                                                @foreach ($regions as $value)
                                                <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" name="countries_id">
                                                @foreach ($countries as $value)
                                                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>City</label><br>
                                            <input type="text" name="title" value="" style="width:100%;" />
                                        </div>
					  <div class="form-group">
						
						    <label class="control-label font-md margin-bottom-10">Meta Title</label>
						    <input name="meta_title" id="meta_title"  class="form-control" placeholder="Title" type="text">
						
					    </div>
					  <div class="form-group">
						
						    <label class="control-label font-md margin-bottom-10">Meta Keyword</label>
						    <textarea name="meta_keyword" id="meta_keyword"  class="form-control" placeholder="First Name" rows="3"></textarea>
						
					    </div>
					 <div class="form-group">
						
						    <label class="control-label font-md margin-bottom-10">Meta Description</label>
						    <textarea name="meta_description" id="meta_description"  class="form-control" placeholder="Description" rows="3"></textarea>
						
					    </div>

<!--<img src="http://placehold.it/150x100" alt="..." class=" img-responsive center-block margin-bottom">
<div class="row">
<div class="col-sm-4 col-sm-offset-4">
<div class="form-group">
<label for="exampleInputFile">File input</label>
<input id="exampleInputFile" type="file">
<p class="help-block">Attach Image.</p>
</div>
</div>
</div>-->
                                        <div class="col-xs-4">&nbsp;</div>
                                        <div class="col-xs-4">
                                            <strong>City Cover Photo</strong>
                                            <input type="hidden" name="cropx" id="cropx" value="0" />
                                            <input type="hidden" name="cropy" id="cropy" value="0" />
                                            <input type="hidden" name="cropw" id="cropw" value="0" />
                                            <input type="hidden" name="croph" id="croph" value="0" />

                                            <img id="preview" src="http://placehold.it/360x180" alt="your image" width="360px" height="180px"/>
                                            <div class="form-group margin">
                                                <label for="exampleInputFile">File input</label>
                                                <input type='file' id="InputImageFile" name="image" />

                                                <p class="help-block">Attach Image.</p>
                                            </div>


                                            <input type="hidden" name="previous_image" value="">
                                        </div>
                                        <div class="col-xs-4">&nbsp;</div>

                                    </div>
                                    <div class="box-footer text-center">
                                        <input type="submit" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
                                        <a href="{{ url('admin/destinations') }}"><input type="button" name="Submit" value="Back" class="btn bg-maroon btn-flat margin"></a>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <!--<div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Popular Destinations</h3>
                                        <p class=""></p>
                                    </div>
                                    
                                    <div class="box-body">
                                        <div class="">                                            
                                            <div class="radio">
                                                <label>
                                                    <input name="optionsRadios2" id="optionsRadios2" value="option2" role="button" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample" class="collapsed" type="radio">
                                                    Show Popular Destination
                                                </label>
                                            </div>
                                            <div class="collapse" id="collapseExample2" aria-expanded="false" style="height: 0px;">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Country</label>
                                                            <input class="form-control" id="exampleInputEmail1" placeholder="Country" type="email">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">City</label>
                                                            <input class="form-control" id="exampleInputEmail1" placeholder="City" type="email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4 col-sm-offset-4">
                                                        <img src="dist/img/popular-destination.png" class="img-reponsive">
                                                        <div class="form-group margin">
                                                            <label for="exampleInputFile">File input</label>
                                                            <input id="exampleInputFile" type="file">

                                                            <p class="help-block">Attach Image.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </section>
    <!-- /.content -->
</div>

<!-- ./wrapper -->

@stop
