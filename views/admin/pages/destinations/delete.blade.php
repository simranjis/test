@extends('admin.layouts.defaultsidebar')
@section('content')


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
       
    
</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Remove Destination 
            <small>Destinations</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Destination</li>
        </ol>
    </section>
    
    
    
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/destinations/delete', 'name' => 'deleteDestination' , 'class'=>'deleteDestination', 'id'=>'deleteDestination', 'files'=>'false']  ) !!}                                
                <input type="hidden" name="id" value="{{ $destination['id'] }}">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"><input type="submit" value="Delete" name="submit"></div>
                    <div class="col-md-3"><input type="button" value="Cancel" name="Cancel"></div>
                    <div class="col-md-3"></div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>    
</div>    


@stop
