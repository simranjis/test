@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    

    .form-horizontal .form-group{
	margin-left:0 !important;
	margin-right:0 !important;
    }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Meta
            <small>Add Meta</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
	    @if(Session::has('message'))
	    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	    @endif
	    <form id="" method="post" action="" class="form-horizontal">
		{{csrf_field()}}                           

                <div class="col-md-12">
                    <div class="box box-primary">
			    <div class="box-header with-border box-header-color">
				<h3 class="box-title sbold">Meta Pages Information</h3>
				<!--<p class=""> Date 7 Jan 2018</p>-->
			    </div>                                    

			    <div class="box-body">
				<div class="form-group">

				    <label>Page Name:</label>
				    <!--<input class="form-control" type="text" name="page_name" id="page_name" value="">-->
				    <select  class="form-control" id="page_name" name="page_name">
					<!--<option value="" disabled="disabled" selected="">SelectPage Name</option>-->
					@foreach($metaName as $metaData)
					<option  value="{{$metaData->id}}">{{$metaData->page_name}}</option>
					@endforeach
				    </select>

				</div>

				<div class="form-group">

				    <label>Meta Title:</label>
				    <input class="form-control" type="text" name="meta_title" id="meta_title" value="">        
				</div>
				<div class="form-group">
				    <label>Meta Keyword:</label>
				    <textarea class="form-control" type="text" rows="3" name="meta_keyword" id="meta_keyword" value=""></textarea>
				</div>

				<div class="form-group">

				    <label>Meta Description:</label>
				    <textarea class="form-control" rows="3" type="text" name="meta_description" id="meta_description" value=""> </textarea>       
				</div>

				<button class="btn green_btn text-uppercase margin-bottom-20 btn-pad" type="submit"  id="" >Submit</button>
			    </div>
                    </div>
                </div>

	    </form>
        </div>
    </section>

</div>
<script>

    $(document).ready(function () {
        getMeta($("#page_name").val());
        $('#page_name').on('change', function () {
            var id = $(this).val();
            getMeta(id);
        });
    });
    $(document).ready(function () {
        setTimeout(function () {
            $("p.alert").remove();
        }, 3000); // 3 secs

    });



    function getMeta(id) {
        if (id == '') {

        } else {
//                $('#state_id').prop('disable', false);
            $.ajax({
                url: base_url + '/get-meta',

                type: "GET",
                data: {'id': id},
                dataType: 'json',
                success: function (data) {
                    var html = '';
                    $("#meta_title").val(data.meta_title);
                    $("#meta_keyword").val(data.meta_keyword);
                    $("#meta_description").val(data.meta_description);

                    $('#').html(html);
                },
                error: function () {
                }

            });
        }
    }


</script>

@stop

