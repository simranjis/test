@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Language
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Language</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/others/language/update', 'name' => 'languageUpdateForm' , 'class'=>'languageUpdateForm', 'id'=>'languageUpdateForm', 'files' => true]) !!}  
            
            <!-- all local guides -->
            @include('admin.pages.languages.partials.language_form')

            {!! Form::close() !!}
        </div>
    </section>
</div>




@stop

@section('page_scripts')
<script src="js/admin/languages/edit.js"></script>
@stop
