@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Language
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Language</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/others/language/insert', 'name' => 'languageInsertForm' , 'class'=>'languageInsertForm', 'id'=>'languageInsertForm', 'files' => true]) !!}  
           <!-- all local guides -->
            @include('admin.pages.languages.partials.language_form')

            {!! Form::close() !!}
        </div>
    </section>
</div>




@stop

@section('page_scripts')
<script src="js/admin/languages/add.js"></script>
@stop