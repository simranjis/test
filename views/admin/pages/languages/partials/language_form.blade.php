<?php

    $id = '';
    if(!empty($language->id)){
        $id = $language->id;
    }
    
?>

<input type="hidden" name="id" value="{{ $id }}">
<div class="col-md-12">
    <div class="box box-primary">
        <div  class="">
            <div  class="row">
                <div class="col-xs-12"> 
        
                    <div class="box-header with-border box-header-color">
                        <h3 class="box-title sbold">Add/Edit Language</h3>
                    </div>
                    <div class="box-body">
                        <!-- all local guides -->
                        
                        <div class="col-sm-12">
                            
                            <?php
                            
                                
                                $language_title = '';
                                if(!empty($language->language)){
                                    
                                    $language_title = $language->language;
                                }
                                
                                
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('language', 'Language', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>
                                        {{ Form::text('language', $language_title, array('class' => 'form-control form-control-custom', 'id'=>'language', 'placeholder'=>'Language')) }}
                                    </div>
                                </div>
                            </div>
                            
                             <?php
                            
                                
                                $language_code = '';
                                if(!empty($language->language_code)){
                                    
                                    $language_code = $language->language_code;
                                }
                                
                                
                            ?>
                            

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('language_code', 'Language Code', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>
                                        {{ Form::text('language_code', $language_code, array('class' => 'form-control form-control-custom', 'id'=>'language_code', 'placeholder'=>'Language Code')) }}
                                    </div>
                                </div>
                            </div>
                            
                        
                            <div class="row">
                                <div class="col-sm-12">
                                    {{ Form::button('Save', array('id'=>'savePromotion', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return validateLanguagesForm()')) }}

                                    <a href="{{ url('admin/others/language') }}" class="btn bg-gray btn-flat margin">
                                         Cancel
                                    </a>

                                </div>
                            </div>
                        
                       </div>
                        
                        
                        
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>