@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Newsletter
            <small>Newsletter</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Newsletter</li>
        </ol>
    </section>

    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif    
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <!--<i class="fa fa-flag-checkered" aria-hidden="true"></i>-->
                            <!--<h3>You haven’t Enteres any Destination yet.</h3>-->
                            <!--<a href="{{ url('admin/users/traveller/add') }}" type="button" class="btn bg-maroon btn-flat margin">New Traveler</a>-->
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Newsletter</h3>
                                        <p class=""></p>
                                    </div>
                                    <!-- /.box-header -->
                                    @if($newsletter)
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <!--<th>City</th>-->
                                                    <th>Action</th>
                                                </tr>
                                                @foreach($newsletter as $list)
                                                <tr>
                                                    
                                                    <td>{{ $list['email'] }}</td>
                                                    <td>{{ $list['status'] }}</td>
                                                    
                                                    <td>
                                                        <!--<a class="btn bg-maroon btn-flat margin" href="{{ 'admin/user/traveller/edit/' }}{{ $list['id'] }}" role="button">
                                                            Edit
                                                        </a>
                                                        <a class="btn bg-navy btn-flat margin" href="{{ 'admin/user/traveller/remove/' }}{{ $list['id'] }}" role="button">
                                                            Delete
                                                        </a>-->
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    @else
                                        <h3 style="padding: 5px;">You Don't have any Enteries on Destination yet.</h3>
                                    @endif
                                </div>
                                <!-- /.box -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <!-- /.content -->
</div>






@stop