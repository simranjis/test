@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Hide/Show Pages
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hide/Show Pages</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="">

                {!! Form::open(['url' => 'admin/pages/page-settings', 'class'=>'pageSettings', 'id'=>'pageSettings']) !!} 

                @if($settingsCollection->isNotEmpty())
                    @foreach($settingsCollection as $index => $settingsObj)
                        <div class="row">
                            <div class="col-xs-12">                              
                                <div class="col-xs-4 text-right">
                                   {{ucwords(str_replace('_',' ',$settingsObj->setting_name))}}
                                </div> 
                                <div class="col-xs-6">
                                    {{ Form::checkbox($settingsObj->setting_name, 1, !empty($settingsObj) && !empty($settingsObj->setting_value) ? true : false) }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- Section 1 Ends -->
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::submit('Save', array('class'=>'btn btn-success btn-flat margin')) }}
            
                    </div>
                </div>
                {!! Form::close() !!} 
            </div>
            
        </div>
    </section>
</div>
@stop