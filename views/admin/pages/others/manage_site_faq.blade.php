@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manage FAQ
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Site Faqs</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="">
                <div class="row">
                    <div class="col-xs-12">   
                        @if(!empty($travellerFaqArr))                             
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold"> Traveller FAQ</h3>
                                    <p class=""></p>
                                </div>
                                <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <td> Sr. No. </td> 
                                                <td> Question </td> 
                                                <td> Action </td>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($travellerFaqArr as $key => $travellerFaq)
                                                <tr>
                                                    <td> {{ $key + 1 }}</td> 
                                                    <td> {{ $travellerFaq['question']}} </td> 
                                                    <td>
                                                        <a href="{{url('/admin/pages/site-faqs/'.$travellerFaq['id'])}}">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                         |
                                                         <i class="fa fa-trash text-danger" onclick="deleteFAQ({{$travellerFaq['id']}})"></i>
                                                    </td>    
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        @endif

                        <!-- local gide FAQ : starts -->

                        @if(!empty($localGuideBadgeFaqArr))                             
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold"> Local Guide FAQ</h3>
                                    <p class=""></p>
                                </div>
                                <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <td> Sr. No. </td> 
                                                <td> Question </td> 
                                                <td> Action </td>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($localGuideBadgeFaqArr as $key => $localGuideBadgeFaq)
                                                <tr>
                                                    <td> {{ $key + 1 }}</td> 
                                                    <td> {{ $localGuideBadgeFaq['question']}} </td> 
                                                    <td>
                                                        <a href="{{url('/admin/pages/site-faqs/'.$localGuideBadgeFaq['id'])}}">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                         |
                                                        <i class="fa fa-trash text-danger" onclick="deleteFAQ({{$localGuideBadgeFaq['id']}})"></i>
                                                    </td>    
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        @endif
                        <!-- /.box -->
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/site_faq.js"></script>
@stop