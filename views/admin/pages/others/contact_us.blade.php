@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Contact Us
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Contact Us</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::model($contactObj,['url' => 'admin/pages/contact-us', 'name' => 'contactUs' , 'class'=>'contactUs', 'id'=>'contactUs']) !!} 
            
              
            <input type="hidden" name="id" value="{{ !empty($contactObj) ? ($contactObj->id) : ('') }}">
            <div class="">
                <div class="row">
                    <div class="col-xs-12">                                
                        <div class="">
                            <div class="box-header with-border box-header-color">
                                <h3 class="box-title sbold">Contact Us</h3>
                                <p class=""></p>
                            </div>
                            <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                        Contact Number 1
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::text('contact_number_1', null, array('class' => 'form-control  form-control-custom', 'id'=>'contact_number_1', 'placeholder'=>'Contact Number 1')) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                        Contact Number 2
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::text('contact_number_2', null, array('class' => 'form-control  form-control-custom', 'id'=>'contact_number_2', 'placeholder'=>'Contact Number 2')) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                        Contact Number 3
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::text('contact_number_3', null, array('class' => 'form-control  form-control-custom', 'id'=>'contact_number_3', 'placeholder'=>'Contact Number 3')) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                        Email
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::text('email', null, array('class' => 'form-control  form-control-custom', 'id'=>'email', 'placeholder'=>'Email')) }}
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::button('Save', array('class'=>'btn bg-maroon btn-flat margin','onclick'=>'saveContactDetails()')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/contact_us.js"></script>
@stop