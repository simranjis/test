@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            How It Works
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">How It Works</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="">

                {!! Form::open(['url' => 'admin/pages/how-it-works', 'class'=>'howItWorks', 'id'=>'howItWorks','files'=>'true']) !!} 

                @if($subModuleSectionCollection->isNotEmpty())
                    @foreach($subModuleSectionCollection as $index => $subModuleSection)
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">                                    
                                                Section {{$index + 1}}                            
                                        </h3>
                                        <p class=""></p>
                                    </div>
                                    <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                        <div class="row">
                                            <div class="col-xs-4 text-right">
                                              Title {{$index + 1}}
                                            </div> 
                                            <div class="col-xs-6">
                                                {{ Form::text("sub_module[$subModuleSection->id][title]", $subModuleSection->title, array('class' => 'form-control  form-control-custom', 'id'=>"heading_".($index + 1), 'placeholder'=>'Heading')) }}
                                            </div>
                                        </div>

                                       
                                        <div class="row">
                                            <div class="col-xs-4 text-right">
                                               Upload Image
                                            </div> 
                                            @php
                                                $sequence_no = $index + 1;
                                            @endphp
                                            <div class="col-xs-6">
                                                {{ Form::file("sub_module[$subModuleSection->id][image_path]",['class'=>'file','onchange'=>"fileUpload('sub_module[$subModuleSection->id][image_path]',$sequence_no)"]) }}                                              
                                                @if(!empty($subModuleSection->image_path))
                                                    <div>
                                                        <a href="{{url('admin/pages/download-image/'.$subModuleSection->id)}}" >Download</a>
                                                    </div>
                                                @endif    
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- Section 1 Ends -->
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::button('Save', array('class'=>'btn btn-success btn-flat margin','onclick'=>'saveHowItWorks()')) }}
            
                    </div>
                </div>
                {!! Form::close() !!} 
            </div>
            
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/how_it_works.js"></script>
@stop