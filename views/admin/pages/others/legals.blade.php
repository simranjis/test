@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Legals
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Legals</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="well well-sm">

                {!! Form::open(['url' => 'admin/pages/legals', 'class'=>'legals', 'id'=>'legals']) !!} 

                @if($legalsCollection->isNotEmpty())
                    <div class="row ">
                        <div class="col-xs-12 ">                                                            
                            <div class="pull-right text-danger well well-sm">
                               Last Updated : {{$legalsCollection[0]->updated_at}}
                            </div>
                        </div>
                    </div>
                    @foreach($legalsCollection as $index => $legalObj)
                        <div class="row">
                            <div class="col-xs-12">                                                                                        
                                    <div class="box-body table-responsive"> 
                                    <div class="col-xs-12 h4">                                   
                                           {{$legalObj->type}} 
                                       </div>    
                                        <div class="col-xs-12">
                                            {{ Form::textarea("legal[$legalObj->id][content]", $legalObj->content, array('class' => 'form-control ckeditor  form-control-custom', 'id'=>"content_".$legalObj->id)) }}
                                        </div>                            
                                    </div>                       
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- Section 1 Ends -->
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::submit('Save', array('class'=>'btn btn-success btn-flat margin')) }}            
                    </div>
                </div>
                {!! Form::close() !!} 
            </div>            
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/legals.js"></script>
@stop