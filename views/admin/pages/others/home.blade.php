@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Home Page
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Home Page</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Tab links -->
                <?php
                $bannerActive = $featuredExperienceActive = $featuredDestinationActive = $whyDestinationSeekerActive = $beInspiredActive = $howItWorksActive = '';
                $bannerHidden = $featuredExperienceHidden = $featuredDestinationHidden = $whyDestinationSeekerHidden = $beInspiredHidden = $howItWorksHidden = 'hide';

                $bannerActive = 'active';
                $bannerHidden = '';
                
                if(!empty($active_tab)){
                    $bannerActive = '';
                    $bannerHidden = 'hide';

                    if($active_tab == 'bannerInfo'){
                        $bannerActive = 'active';
                        $bannerHidden = '';
                    }
                    if($active_tab == 'featuredExperienceInfo'){
                        $featuredExperienceActive = 'active';
                        $featuredExperienceHidden = '';
                    }
                    if($active_tab == 'whyDestinationSeekerInfo'){
                        $whyDestinationSeekerActive = 'active';
                        $whyDestinationSeekerHidden = '';
                    }
                    if($active_tab == 'featuredDestinationInfo'){
                        $featuredDestinationActive = 'active';
                        $featuredDestinationHidden = '';
                    }
                    if($active_tab == 'beInspiredInfo'){
                        $beInspiredActive = 'active';
                        $beInspiredHidden = '';
                    }
                    if($active_tab == 'howItWorksInfo'){
                        $howItWorksActive = 'active';
                        $howItWorksHidden = '';
                    }
                }
                ?>
                <div class="tab">
                    <button class="tablinks <?php echo $bannerActive; ?>" onclick="activeTab2('Banner')" id="BannerTab">Banner Info
                    </button>
                    <button class="tablinks <?php echo $featuredExperienceActive; ?>" onclick="activeTab2('FeaturedExperience')" id="FeaturedExperienceTab">
                        Featured Experiences
                    </button>
                    <button class="tablinks <?php echo $featuredDestinationActive; ?>" onclick="activeTab2('FeaturedDestination')" id="FeaturedDestinationTab">
                        Featured Destination
                    </button>
                    <button class="tablinks <?php echo $whyDestinationSeekerActive; ?>" onclick="activeTab2('WhyDestinationSeeker')" id="WhyDestinationSeekerTab"> Why Destination Seeker?
                    </button>
                    <!-- <button class="tablinks <?php echo $beInspiredActive; ?>" onclick="activeTab2('BeInspired')" id="BeInspiredTab">
                        Be Inspred
                    </button>
                    <button class="tablinks <?php echo $howItWorksActive; ?>" onclick="activeTab2('HowItWorks')" id="HowItWorksTab">
                        How It Works
                    </button> -->
                </div>

                <!-- Tab content -->

                <!-- Banner Tab -->
                @include('admin.pages.others.partials.banner')

                <!-- Featured Experience Tab -->
                @include('admin.pages.others.partials.featuredExperience')

                <!-- Featured Destination Tab -->
                @include('admin.pages.others.partials.featuredDestination')

                <!-- Why Destination Seeker Tab -->
                @include('admin.pages.others.partials.whyDestinationSeeker')

                <!-- Be Inspired Tab -->
                @include('admin.pages.others.partials.beInspired')

                <!-- How It Works Tab -->
                @include('admin.pages.others.partials.howItWorks')

            </div>
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/home.js"></script>
@stop