@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            FAQ
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Site Faqs</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::model($siteFaqObj,['url' => 'admin/pages/site-faqs', 'name' => 'siteFaq' , 'class'=>'siteFaq', 'id'=>'siteFaq']) !!} 
              
              <input type="hidden" name="id" value="{{ !empty($siteFaqObj) ? ($siteFaqObj->id) : ('') }}">
            <div class="">
                <div class="row">
                    <div class="col-xs-12">                                
                        <div class="">
                            <div class="box-header with-border box-header-color">
                                <h3 class="box-title sbold">                                    
                                        Site FAQ                            
                                </h3>
                                <span class="pull-right">
                                      <b> <a class="btn btn-primary" style="color:white;" href="{{url('admin/pages/site-faqs/manage')}}">  Manage FAQ </a></b>
                                    </span>
                                <p class=""></p>
                            </div>
                            <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                      Question
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::text('question', null, array('class' => 'form-control  form-control-custom', 'id'=>'question', 'placeholder'=>'Question')) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                       Answer
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::textarea('answer', null, array('class' => 'form-control  form-control-custom', 'id'=>'answer', 'placeholder'=>'Answer')) }}
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                       For Local Guide ?
                                    </div> 
                                    <div class="col-xs-6">
                                        {{ Form::checkbox('is_local_guide_badge', 1, !empty($siteFaqObj) && !empty($siteFaqObj->is_local_guide_badge) ? true :false ) }}
                                    </div>
                                </div> 

                                {{Form::hidden('action','save_exit',['id'=>'action'])}}
                                                
                                
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::button('Save & New', array('class'=>'btn btn-success btn-flat margin','onclick'=>'saveSiteFaq("save_new")')) }}
                        {{ Form::button('Save & Exit', array('class'=>'btn bg-maroon btn-flat margin','onclick'=>'saveSiteFaq("save_exit")')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/site_faq.js"></script>
@stop