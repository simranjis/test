<div id="FeaturedDestinationInfo" class="tabcontent <?php echo $featuredDestinationHidden; ?>">
    <br><br><br>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>Keywords</label>
                <input type="text" name="search" class="form-control" id="search" placeholder="Search">
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Continent</label>
                <select id="region_id" name="region_id" class="form-control">
                    <option value="">All Continent</option>
                    <?php foreach ($regionsArr as $key => $value) { ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label>Country</label>
                <select id="countries_id" name="countries_id" class="form-control">
                    <option value="">All Countries</option>
                    <?php foreach ($countriesArr as $key => $value) { ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <label>&nbsp;</label>
            <div class="form-group">
                <a href="javascript:void(0)" id="search_button" onclick="return searchActivities()" type="button" class="btn bg-maroon btn-flat">Search</a>
            </div>
        </div>
    </div>
    <hr>
    <table id="manageAllDestinations" class="table table-hover table-bordered col-sm-12" style="width: 100%"></table>
</div>