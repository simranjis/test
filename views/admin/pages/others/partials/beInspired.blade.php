<div id="BeInspiredInfo" class="tabcontent <?php echo $beInspiredHidden; ?>">
      {!! Form::open(['url' => 'admin/pages/home', 'name' => 'saveHomeForm' , 'class'=>'saveHomeForm', 'id'=>'saveHomeForm']) !!} 
        <div class="row">
            <div class="col-sm-12">
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('banner_text', 'Be Inspired Text: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>

                        {{ Form::text('banner_text', $pageInfoObj->page_details[0]->content, array('class' => 'form-control form-control-custom', 'rows'=>3, 'id'=>'banner_text', 'placeholder'=>'Banner Text')) }}
                    </div>
                </div>
                <br>
                <div class="row">
                    &nbsp;
                    {{ Form::button('Save', array('id'=>'saveHomePage', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveHomePageDetails()')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!} 
</div>