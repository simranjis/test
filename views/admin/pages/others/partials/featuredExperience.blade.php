<div id="FeaturedExperienceInfo" class="tabcontent <?php echo $featuredExperienceHidden; ?>">
    <br><br><br>
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <label>Keywords</label>
                <input type="text" name="search" class="form-control" id="search" placeholder="Search">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Activity Type</label>
                <select name="addedbytype" id="addedbytype" class="form-control">
                    <option selected="selected" value="">ALL</option>
                    <option value="admin">Admin</option>
                    <option value="localguide">Local Guide</option>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Continent</label>
                <select id="region_id" name="region_id" class="form-control">
                    <option value="">All Continent</option>
                    <?php foreach ($regionsArr as $key => $value) { ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Country</label>
                <select id="countries_id" name="countries_id" class="form-control">
                    <option value="">All Countries</option>
                    <?php foreach ($countriesArr as $key => $value) { ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>City</label>
                <select id="destinations_id" name="destinations_id" class="form-control">
                    <option value="">All Destinations</option>
                    <?php foreach ($destinationsArr as $key => $value) { ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <label>&nbsp;</label>
            <div class="form-group">
                <a href="javascript:void(0)" id="search_button" onclick="return searchActivities()" type="button" class="btn bg-maroon btn-flat">Search</a>
            </div>
        </div>
    </div>
    <hr>
    <table id="manageAllActivities" class="table table-hover table-bordered col-sm-12" style="width: 100%"></table>
</div>