<div id="WhyDestinationSeekerInfo" class="tabcontent <?php echo $whyDestinationSeekerHidden; ?>">
    <?php
        $whyDestinationSeekerBlockObj1 = $whyDestinationSeekerBlockObj[0];
        $whyDestinationSeekerBlockObj1 = json_decode($whyDestinationSeekerBlockObj1['content']);
    ?>
      {!! Form::open(['url' => 'admin/pages/saveWhyDestinationSeeker', 'name' => 'saveWhyDestinationSeeker' , 'class'=>'saveWhyDestinationSeeker', 'id'=>'saveWhyDestinationSeeker', 'files' => true]) !!} 
        <div class="row">
            <div class="col-sm-12">
            <br>
                <h4>Block 1</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('title1', 'Title: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>

                        {{ Form::text('title1', $whyDestinationSeekerBlockObj1->title, array('class' => 'form-control form-control-custom', 'id'=>'title1', 'placeholder'=>'Title')) }}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('image1', 'Image: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>
                        <?php 
                        $image1_block_hide = '';
                        if(!empty($whyDestinationSeekerBlockObj1->image)){ 
                            $image1_block_hide = 'hide';
                            ?>
                        <div id="image_src1_block">
                            <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj1->image ?>" style="width: 100; height: 100px" />
                            <a href="javascript:void(0)" onclick="return editImage(1)" ><i class="fa fa-edit"></i> EDIT</a>
                        </div>
                        <?php } ?>
                        <div id="image1_block" class="<?php echo $image1_block_hide; ?>">
                            <input type="file" name="image1" id="image1" />
                        </div>
                    </div>
                </div>
                <hr>

                <?php
                    $whyDestinationSeekerBlockObj2 = $whyDestinationSeekerBlockObj[1];
                    $whyDestinationSeekerBlockObj2 = json_decode($whyDestinationSeekerBlockObj2['content']);
                ?>
                <h4>Block 2</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('title2', 'Title: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>

                        {{ Form::text('title2', $whyDestinationSeekerBlockObj2->title, array('class' => 'form-control form-control-custom', 'id'=>'title1', 'placeholder'=>'Title')) }}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('image2', 'Image: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>
                        <?php 
                        $image2_block_hide = '';
                        if(!empty($whyDestinationSeekerBlockObj2->image)){ 
                            $image2_block_hide = 'hide';
                            ?>
                        <div id="image_src2_block">
                            <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj2->image ?>" style="width: 100; height: 100px" />
                            <a href="javascript:void(0)" onclick="return editImage(2)" ><i class="fa fa-edit"></i> EDIT</a>
                        </div>
                        <?php } ?>
                        <div id="image2_block" class="<?php echo $image2_block_hide; ?>">
                            <input type="file" name="image2" id="image2" />
                        </div>
                    </div>
                </div>
                <hr>
                <?php
                    $whyDestinationSeekerBlockObj3 = $whyDestinationSeekerBlockObj[2];
                    $whyDestinationSeekerBlockObj3 = json_decode($whyDestinationSeekerBlockObj3['content']);
                ?>
                <h4>Block 3</h4>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('title3', 'Title: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>

                        {{ Form::text('title3', $whyDestinationSeekerBlockObj3->title, array('class' => 'form-control form-control-custom', 'id'=>'title3', 'placeholder'=>'Title')) }}
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::label('image3', 'Image: ', array('class'=>'label-font')) }}
                        <span class="text-danger">*</span>
                        <?php 
                        $image3_block_hide = '';
                        if(!empty($whyDestinationSeekerBlockObj3->image)){ 
                            $image3_block_hide = 'hide';
                            ?>
                        <div id="image_src3_block">
                            <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj3->image ?>" style="width: 100; height: 100px" />
                            <a href="javascript:void(0)" onclick="return editImage(3)" ><i class="fa fa-edit"></i> EDIT</a>
                        </div>
                        <?php } ?>
                        <div id="image3_block" class="<?php echo $image3_block_hide; ?>">
                            <input type="file" name="image3" id="image3" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    &nbsp;
                    {{ Form::button('Save', array('id'=>'saveHomePage', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveWhyDestinationSeekerDetails()')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!} 
</div>