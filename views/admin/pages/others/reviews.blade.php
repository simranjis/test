@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 

    .quote-box{
        overflow: hidden;
        margin-top: -50px;
        padding-top: -100px;
        border-radius: 17px;
        background-color: #DAE7F0;
        margin-top: 25px;
        color: #6B5B6B;
        box-shadow: 3px 5px 14px 1px #E0E0E0;
        margin: 5px 5px;
    }
    .grid-item {
        width: 280px;
        float: left;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Legals
            <small></small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Legals</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="well well-sm">

                {!! Form::open(['url' => 'admin/pages/reviews', 'class'=>'reviews', 'id'=>'reviews']) !!} 

                @if($reviewCollection->isNotEmpty())
                    <div class="review-bg">
                        <div class="container-fluid mt20">
                            <div class="grid js-masonry" id="blog-width">
                                @foreach($reviewCollection as $index => $reviewObj)
                                    <div class="grid-item">
                                        <blockquote class="quote-box">
                                            <p class="quotation-mark">
                                                “
                                            </p>
                                            <p class="quote-text">
                                               {{$reviewObj->descrption}} 
                                            </p>
                                            <hr>
                                            <div class="blog-post-actions">
                                                <p class="blog-post-bottom pull-left">
                                                <!--- activity title, destnation title -->
                                                {{ $reviewObj->activity ? $reviewObj->activity->title : "N/A"}}, {{ $reviewObj->activity && $reviewObj->activity->destination ? $reviewObj->activity->destination->title : "N/A"}}
                                                    <br>
                                                    @for($i = 0;$i < $reviewObj->rating; $i++)
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    @endfor
                                                </p>
                                                <p class="blog-post-bottom pull-right">
                                                    <span class="badge quote-badge"></span>  ❤
                                                </p>
                                            </div>
                                        </blockquote>
                                        <div class="col-xs-12">
                                            {{Form::checkbox("review[".$reviewObj->id."][is_show]",1,!empty($reviewObj->is_show) ? true :false,['id'=>$reviewObj->id])}}
                                            <label for="{{$reviewObj->id}}">Is Show ?</label>
                                            {{Form::hidden("showed_review[]",$reviewObj->id)}}
                                        </div>
                                    </div>
                                    
                                @endforeach
                            </div>
                        </div>
                    </div>
                    {{ $reviewCollection->links() }}
                @endif
                <!-- Section 1 Ends -->
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::submit('Save', array('class'=>'btn btn-success btn-flat margin')) }}            
                    </div>
                </div>
                {!! Form::close() !!} 
            </div>            
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/legals.js"></script>
@stop