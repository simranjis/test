@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Who We Are
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Who We Are</li>
        </ol>
    </section>
    <section class="content">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['url' => 'admin/pages/whoWeAre', 'name' => 'whoWeAre' , 'class'=>'whoWeAre', 'id'=>'whoWeAre']) !!} 
            <div class="">
                <div class="row">
                    <div class="col-xs-12">                                
                        <div class="">
                            <div class="box-header with-border box-header-color">
                                <h3 class="box-title sbold">Who We Are</h3>
                                <p class=""></p>
                            </div>
                            <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                                {{ Form::textarea('content', $whoWeAre->content, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'content', 'placeholder'=>'Description')) }}
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::button('Save', array('id'=>'saveWhoWeArePage', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return validateCharactersLength()')) }}
                    </div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
    </section>
</div>
@stop

@section('page_scripts')
<script type="text/javascript" src="js/admin/others/whoWeAre.js"></script>
@stop