@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Accounts
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Accounts</li>
        </ol>
    </section>
    {!! Form::open(['url' => 'admin/others/accounts', 'name' => 'accountsForm' , 'class'=>'accountsForm', 'id'=>'accountsForm']) !!}  
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Accounts</h3>
            </div>
            <div class="box-body">
                <!-- all local guides -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('settingValue2', 'Facebook URL: ', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('setting_value[2]', $allSettingsArr[1]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'settingValue2', 'placeholder'=>'Facebook URL')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('settingValue3', 'Twitter URL: ', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('setting_value[3]', $allSettingsArr[2]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'settingValue3', 'placeholder'=>'Twitter URL')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('settingValue4', 'Instagram URL: ', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('setting_value[4]', $allSettingsArr[3]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'settingValue4', 'placeholder'=>'Instagram URL')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('settingValue5', 'Youtube URL: ', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('setting_value[5]', $allSettingsArr[4]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'settingValue5', 'placeholder'=>'Youtube URL')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('settingValue6', 'Pinterest URL: ', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('setting_value[6]', $allSettingsArr[5]->setting_value, array('class' => 'form-control form-control-custom', 'id'=>'settingValue6', 'placeholder'=>'Pinterest URL')) }}
                            </div>                                      
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        {{ Form::submit('Save', array('id'=>'saveAccounts', 'class'=>'btn bg-maroon btn-flat margin')) }}

                        <a href="{{ url('admin/dashboard') }}" class="btn bg-gray btn-flat collapsed" role="button">
                            Cancel
                        </a>
                    </div>                                      
                </div>
            </div>
        </div>
    </section>
    <input type="hidden" name="csrf_token" value="{!! csrf_token() !!}" >
    {!! Form::close() !!}  
</div>

@stop

@section('page_scripts')
<script src="js/admin/settings.js"></script>
@stop