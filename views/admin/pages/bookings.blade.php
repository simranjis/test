@extends('admin.layouts.defaultsidebar')
@section('content')

<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bookings
            <small>My Bookings</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>


    @if(Request::get('message'))
    <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif  
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                        </div>
                    </div>
                </div>

                <!--Filter form-->
                <div class="row">
                    <div class="col-sm-3 col-md-6"></div>
                    <div class="col-sm-5 col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search by Booking Reference Number" id="booking_reference_number" value="<?php echo isset($keyword) ? $keyword : ''; ?>"/>
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-1">
                        <button type="button" id="search" class="btn bg-maroon btn-flat">Search</button>
                    </div>
                    <div class="col-sm-2 col-md-1">
                        <button type="button" id="clear" class="btn bg-maroon btn-flat">Clear</button>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Bookings</h3>
                                        <p class=""></p>
                                    </div>
                                    <!-- /.box-header -->
                                    @if($bookings_list)
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>#ID</th>
                                                    <th>Traveler</th>
                                                    <th>Subtotal</th>
                                                    <th>Fee</th>
                                                    <th>Total</th>
                                                    <th>Currency</th>
                                                    <th>Promocode</th>
                                                    <th>Date Created</th>
                                                    <th>Action</th>
                                                </tr>

                                                @foreach($bookings_list as $bookings)
                                                <tr>
                                                    <td>{{ $bookings['bookings_reference'] }}</td> 
                                                    <td>{{ $bookings['traveller']['firstname'] }} {{ $bookings['traveller']['lastname'] }}</td>
                                                    <td>{{ $bookings['subtotal'] }}</td>
                                                    <td>{{ $bookings['tax'] }}</td> 
                                                    <td>{{ $bookings['total'] }}</td> 
                                                    <td>{{ $bookings['currency'] }}</td>
                                                    <td>{{ $bookings['promotion']?$bookings['promotion']:'-NA-' }}</td>
                                                    <td>{{ Carbon\Carbon::parse($bookings['created_at'])->format('d/m/Y') }}</td> 
                                                    <td>
                                                        @if($bookings['status'] == 0)
                                                        <!--<a href="{{ 'admin/bookings/accept/' }}{{ $bookings['id'] }}" class="btn bg-green btn-flat margin" role="button">
                                                            Accept
                                                        </a>
                                                        <a href="{{ 'admin/bookings/decline/' }}{{ $bookings['id'] }}" class="btn bg-red btn-flat margin" role="button">
                                                            Decline
                                                        </a>-->
                                                        @endif
                                                        <a href="{{ 'admin/bookings/view/' }}{{ $bookings['id'] }}" class="btn bg-maroon btn-flat margin" role="button">
                                                            View
                                                        </a>
                                                        <a href="{{ 'admin/bookings/remove/' }}{{ $bookings['id'] }}" class="btn bg-black btn-flat margin" role="button">
                                                            Delete
                                                        </a>
                                                    </td> 

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    @else
                                    <h3 style="padding: 5px;">You Don't have any Enteries on Bookings yet.</h3>
                                    @endif
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ $booking->links() }}

    </section>
</div>   
<script>
    $("#search").on('click', function () {
        if ($("#booking_reference_number").val() != '') {
            window.location.href = base_url + '/admin/bookings/' + $("#booking_reference_number").val()
        } else {
            window.location.href = base_url + '/admin/bookings';
        }
    });
    $("#clear").on('click', function () {
        window.location.href = base_url + '/admin/bookings';
    });
</script>
@stop