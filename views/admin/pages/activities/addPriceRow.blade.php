<?php
$row_count = $requestArr['row_count'] + 1;
?>

<div id="pricing_row_<?php echo $row_count; ?>" class="pricing_row" >
    <hr>
    <div class="bg-gray" style="padding:10px 15px;">
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <?php
                    $pricingTypeArr['person'] = 'Per Person';
                    $pricingTypeArr['package'] = 'Per Package';
                    ?>

                    {{ Form::select("pricing[$row_count][type]", $pricingTypeArr,  null, 
            array('class'=>'form-control pricing-type', 'id'=>"pricingType$row_count", 'row_index_attr' => "$row_count", 'onchange'=>'changePackType(this)')) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <?php
                    $pricingForArr['adult'] = 'Adult';
                    $pricingForArr['child'] = 'Child';
                    $pricingForArr['all'] = 'All';
                    ?>

                    {{ Form::select("pricing[$row_count][pricefor]", $pricingForArr,  null, array('class'=>'form-control pricefor', 'id'=>"pricingPricefor$row_count")) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {{ Form::text("pricing[$row_count][min_traveller]", '0', array('class' => 'form-control', 'id'=>"minTraveller$row_count", 'placeholder'=>'Minimum Traveller')) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {{ Form::text("pricing[$row_count][title]", null, array('class' => 'form-control', 'id'=>"pricingTitle$row_count", 'placeholder'=>'Title')) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {{ Form::text("pricing[$row_count][original_price]", null, array('class' => 'form-control', 'id'=>"originalPrice$row_count", 'placeholder'=>'Original Price')) }}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {{ Form::text("pricing[$row_count][price]", null, array('class' => 'form-control', 'id'=>"pricingPrice$row_count", 'placeholder'=>'Price')) }}
                </div>
            </div>
        </div>
        <div class="row hide" id="packageDetailsBlock{{$row_count}}">
            <div class="col-sm-12">
                <div class="form-group">
                    {{ Form::textarea("pricing[$row_count][description]", null, array('class' => 'form-control', 'rows'=>3, 'id'=>"pricingDescription$row_count", 'placeholder'=>'Package Details ...')) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="deletePrcingRow({{$row_count}})">
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </div>
    </div>
</div>