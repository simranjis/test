<!-- general form elements -->
<div class="box box-primary">
    <!-- /.box-header -->
    <!-- form start -->
    <div class="">
        <div class="row">
            <div class="col-xs-12">                                
                <div class="">
                    <div class="box-header with-border box-header-color">
                        <h3 class="box-title sbold">My Activities</h3>
                        <p class=""></p>
                    </div>
                    <!-- /.box-header -->
                    @if($activities_list)
                    <div class="box-body table-responsive" id="manageAllActivitiesDiv">
                        <table id="manageAllActivities" class="table table-hover table-bordered"></table>
                    </div>
                    <!-- /.box-body -->
                    @else
                        <h3 style="padding: 5px;">You Don't have any Enteries on Activities yet.</h3>
                    @endif
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
</div>