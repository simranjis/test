<div id="PricingInfo" class="tabcontent <?php echo $pricingHidden; ?>">
    {!! Form::open(['url' => 'admin/activities/savePricing', 'name' => 'insertPricingForm' , 'class'=>'insertPricingForm', 'id'=>'insertPricingForm', 'files' => false]) !!}                                
    <input type="hidden" name="id" value="{{ $id }}">
    <div class="">
        <div class="row">
            <div class="col-xs-12">                                
                <div class="">
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            All prices will be quoted in our base currency (USD)
                            In case you need to quote it in your Local Currency, <br>it will 
                            be converted in USD on the activity listings.
                        </div>
                    </div>

                    <h3 class="margin-top-0 text-black">Currency</h3>
                    <hr>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Currency</label>
                                <?php
                                $currencies_id = null;
                                if (!empty($activity->currencies_id)) {
                                    $currencies_id = $activity->currencies_id;
                                }
                                ?>
                                {{ Form::select('currencies_id', $currenciesListArr,  $currencies_id, array('class'=>'form-control', 'id'=>'currencies_id')) }}
                            </div>
                        </div>
                    </div>
                    <br>
                    <h3 class="margin-top-0 text-black">Availability</h3><hr/>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Availability Notice</label>
                                <select id="activity_notice_period" name="activity_notice_period" class="form-control">
                                    @foreach($noticePeriodArray as $key => $notice)
                                    <option {{($key == $activity->activity_notice_period) ? 'selected="selected"':'' }} value="{{$key}}">{{$notice}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Start Date</label>
                                <div class="input-group date ">
                                    {{ Form::text('activity_start_date', $activity->activity_start_date!=null?date('m/d/Y',strtotime($activity->activity_start_date)):null, array('class' => 'form-control datepicker', 'id'=>'activity_start_date', 'placeholder'=>'Start Date')) }}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>End Date</label>
                                <div class="input-group date">
                                    {{ Form::text('activity_end_date', $activity->activity_end_date!=null?date('m/d/Y',strtotime($activity->activity_end_date)):null, array('class' => 'form-control datepicker', 'id'=>'activity_end_date', 'placeholder'=>'End Date')) }}
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Days Availability</label><br/>
                            <div class="form-group">
                                @foreach($days as $key => $day)
                                <?php
                                $selected = false;
                                if (count($activity->activity_avialability) > 0) {
                                    if ($activity->activity_avialability->$key == '1') {
                                        $selected = true;
                                    }
                                }
                                ?>
                                <input <?php echo $selected == true ? 'checked="checked"' : ''; ?> type="checkbox" name="{{$key}}" id="{{$key}}" value="1"/> {{$day}} &emsp;
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <br/>
                    <h3 class="margin-top-0 text-black">Price</h3>
                    <hr>
                    <!-- /.box-header -->
                    <div class="row content-header">
                        <div class="col-sm-2">
                            <strong>Package</strong>
                        </div>
                        <div class="col-sm-2">
                            <strong>User</strong>
                        </div>
                        <div class="col-sm-2">
                            <strong>Minimum Traveller</strong>
                        </div>
                        <div class="col-sm-2">
                            <strong>Title</strong>
                        </div>
                        <div class="col-sm-2">
                            <strong>Original Price</strong>
                        </div>
                        <div class="col-sm-2">
                            <strong>Selling Price</strong>
                        </div>
                    </div>
                    <div id="pricing_rows">
                        <?php
                        if (!empty($activity->activitity_pricings) && count($activity->activitity_pricings) > 0) {
                            $pricing_row_count = 0;
                            foreach ($activity->activitity_pricings as $key => $activitityPricingArr) {
                                $pricing_row_count = $pricing_row_count + 1;
                                ?>
                                <div id="pricing_row_{{$pricing_row_count}}" class="pricing_row" >
                                    <hr>

                                    {{ Form::hidden("pricing[$pricing_row_count][id]", $activitityPricingArr->id) }}

                                    <div class="bg-gray" style="padding:10px 15px;">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?php
                                                    $pricingTypeArr['person'] = 'Per Person';
                                                    $pricingTypeArr['package'] = 'Per Package';
                                                    ?>

                                                    {{ Form::select("pricing[$pricing_row_count][type]", $pricingTypeArr,  $activitityPricingArr->type, array('class'=>'form-control pricing-type', 'id'=>"pricingType$pricing_row_count", 'row_index_attr'=>$pricing_row_count, 'onchange'=>'changePackType(this)')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?php
                                                    $pricingForArr['adult'] = 'Adult';
                                                    $pricingForArr['child'] = 'Child';
                                                    $pricingForArr['all'] = 'All';

                                                    $disabled = false;
                                                    if (!empty($activitityPricingArr->type) && ($activitityPricingArr->type == 'package')) {
                                                        $disabled = true;
                                                    }
                                                    ?>

                                                    {{ Form::select("pricing[$pricing_row_count][pricefor]", $pricingForArr,  $activitityPricingArr->pricefor, array('disabled'=>$disabled, 'class'=>'form-control pricefor', 'id'=>"pricingPricefor$pricing_row_count")) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {{ Form::text('pricing['.$pricing_row_count.'][min_traveller]', $activitityPricingArr->min_traveller, array('class' => 'form-control', 'id'=>'minTraveller'.$pricing_row_count, 'placeholder'=>'Minimum Traveller')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {{ Form::text("pricing[$pricing_row_count][title]", $activitityPricingArr->title, array('class' => 'form-control', 'id'=>"pricingTitle$pricing_row_count", 'placeholder'=>'Title')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {{ Form::text("pricing[$pricing_row_count][original_price]", $activitityPricingArr->original_price, array('class' => 'form-control', 'id'=>"originalPrice$pricing_row_count", 'placeholder'=>'Original Price')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {{ Form::text("pricing[".$pricing_row_count."][price]", $activitityPricingArr->price, array('class' => 'form-control', 'id'=>"pricingPrice$pricing_row_count", 'placeholder'=>'Selling Price')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row <?php echo $activitityPricingArr->type == 'package' ? '' : 'hide'; ?>" id="packageDetailsBlock{{$pricing_row_count}}">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    {{ Form::textarea("pricing[$pricing_row_count][description]", $activitityPricingArr->description, array('class' => 'form-control', 'rows'=>3, 'id'=>"pricingDescription$pricing_row_count", 'placeholder'=>'Package Details ...')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 text-right">
                                                <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="deletePrcingRow({{$pricing_row_count}})">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            <?php
                        } else {
                            $pricing_row_count = 1;
                            ?>
                            <div id="pricing_row_1" class="pricing_row" >
                                <hr>
                                <input type="hidden" value="" name="pricing[1][id]" id="pricingId1" />
                                <div class="bg-gray" style="padding:10px 15px;">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?php
                                                $pricingTypeArr['person'] = 'Per Person';
                                                $pricingTypeArr['package'] = 'Per Package';
                                                ?>

                                                {{ Form::select('pricing[1][type]', $pricingTypeArr,  null, array('class'=>'form-control pricing-type', 'id'=>'pricingType1', 'row_index_attr'=>1, 'onchange'=>'changePackType(this)')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?php
                                                $pricingForArr['adult'] = 'Adult';
                                                $pricingForArr['child'] = 'Child';
                                                $pricingForArr['all'] = 'All';
                                                ?>

                                                {{ Form::select('pricing[1][pricefor]', $pricingForArr,  null, array('class'=>'form-control pricefor', 'id'=>'pricingPricefor1')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{ Form::text('pricing[1][min_traveller]', '0', array('class' => 'form-control', 'id'=>'minTraveller', 'placeholder'=>'Minimum Traveller')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{ Form::text('pricing[1][title]', null, array('class' => 'form-control', 'id'=>'pricingTitle1', 'placeholder'=>'Title')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{ Form::text('pricing[1][original_price]', null, array('class' => 'form-control', 'id'=>'originalPrice', 'placeholder'=>'Original Price')) }}
                                            </div>
                                        </div>                                
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {{ Form::text('pricing[1][price]', null, array('class' => 'form-control', 'id'=>'pricingPrice1', 'placeholder'=>'Price')) }}
                                            </div>
                                        </div>                                
                                    </div>
                                    <div class="row hide" id="packageDetailsBlock1">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                {{ Form::textarea('pricing[1][description]', null, array('class' => 'form-control', 'rows'=>3, 'id'=>'pricingDescription1', 'placeholder'=>'Package Details ...')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="deletePrcingRow(1)">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <div class="row text-right">
                        <div class="col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="addPrcingRow(1)">
                                <i class="fa fa-plus"></i> Add Price
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

                <h3 class="margin-top-0 text-black">Service Fee</h3>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">

                            {{ Form::label('servicefee_status', 'Service Fee', array('class'=>'label-font')) }}
                            &nbsp;
                            <?php
                            $servicefee_status_checked = '';
                            if (!empty($activity->servicefee_status) && ($activity->servicefee_status == 'yes')) {
                                $servicefee_status_checked = 'checked="checked"';
                            }
                            ?>

                            <input type="checkbox" name="servicefee_status" value="yes" {{$servicefee_status_checked}}>

                            <?php
                            $servicefeeTypeArr['person'] = 'Per Person';
                            $servicefeeTypeArr['booking'] = 'Per Booking';

                            $servicefee_type = null;
                            if (!empty($activity->servicefee_type)) {
                                $servicefee_type = $activity->servicefee_type;
                            }
                            ?>

                            {{ Form::select('servicefee_type', $servicefeeTypeArr,  $servicefee_type, array('class'=>'form-control', 'id'=>'servicefee_type')) }}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {{ Form::label('service_fee', 'Amount', array('class'=>'label-font')) }}

                            <?php
                            $service_fee = null;
                            if (!empty($activity->service_fee)) {
                                $service_fee = $activity->service_fee;
                            }
                            ?>

                            {{ Form::text('service_fee', $service_fee, array('class' => 'form-control', 'id'=>'service_fee', 'placeholder'=>'Amount')) }}
                        </div>
                    </div>
                </div>

                <h3 class="margin-top-0 text-black">Extra Details</h3>
                <hr>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            {{ Form::label('extra_details', 'Extra Details', array('class'=>'label-font')) }}
                            &nbsp;

                            <?php
                            $extra_details_checked = '';
                            if (!empty($activity->extra_details) && ($activity->extra_details == 'yes')) {
                                $extra_details_checked = 'checked="checked"';
                            }
                            ?>


                            <input type="checkbox" name="extra_details" value="yes" {{$extra_details_checked}}>
                        </div>
                    </div>
                </div>

                <div class="">
                    <div id="options_rows">
                        <?php
                        if (!empty($activity->activitity_options) && count($activity->activitity_options) > 0) {
                            $option_row_count = 0;
                            foreach ($activity->activitity_options as $activitityOptionArr) {
                                $option_row_count = $option_row_count + 1;
                                ?>
                                <div id="options_row_{{$option_row_count}}" class="options_row" >
                                    {{ Form::hidden("option[$option_row_count][id]", $activitityOptionArr->id) }}
                                    <div class="bg-gray" style="padding:10px 15px;">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <?php
                                                    $title = '';
                                                    if (!empty($activitityOptionArr->title)) {
                                                        $title = $activitityOptionArr->title;
                                                    }
                                                    ?>
                                                    {{ Form::text("option[$option_row_count][title]", $title, array('class' => 'form-control', 'id'=>"optionTitle$option_row_count", 'placeholder'=>'Option')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <?php
                                                    $description = '';
                                                    if (!empty($activitityOptionArr->description)) {
                                                        $description = $activitityOptionArr->description;
                                                    }
                                                    ?>

                                                    {{ Form::text("option[$option_row_count][description]", $description, array('class' => 'form-control', 'id'=>"optionDescription$option_row_count", 'placeholder'=>'Description')) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-2 text-right">
                                                <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="deleteOptionRow({{$option_row_count}})">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                        } else {
                            $option_row_count = 1;
                            ?>
                            <input type="hidden" value="" name="option[1][id]" id="optionId1" />
                            <div id="options_row_1" class="options_row" >
                                <div class="bg-gray" style="padding:10px 15px;">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                {{ Form::text('option[1][title]', null, array('class' => 'form-control', 'id'=>'option1Title', 'placeholder'=>'Option')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                {{ Form::text('option[1][description]', null, array('class' => 'form-control', 'id'=>'option1Description', 'placeholder'=>'Description')) }}
                                            </div>
                                        </div>
                                        <div class="col-sm-2 text-right">
                                            <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="deleteOptionRow(1)">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                    <div class="row text-right">
                        <div class="col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-sm bg-maroon btn-flat margin" onclick="addOptionRow(1)">
                                <i class="fa fa-plus"></i> Add More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <hr>  
        <input type="hidden" value="{{$pricing_row_count}}" name="row_count" id="row_count" />
        <input type="hidden" value="{{$option_row_count}}" name="option_row_count" id="option_row_count" />
        <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
        {!! Form::close() !!}
    </div>
</div>