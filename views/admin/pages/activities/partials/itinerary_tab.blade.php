<link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<style>
    .clone-components a{
        cursor: pointer !important;
    }
</style>
<div id="ItineraryInfo" class="tabcontent <?php echo $itineraryInfoHidden; ?>">
    {!! Form::open(['url' => 'admin/activities/saveItineraryInfo', 'name' => 'insertItineraryInfo' , 'class'=>'insertItineraryInfo', 'id'=>'insertItineraryInfo']) !!}   
    <div class="">
        <div class="row">
            <div class="col-xs-12">                                
                <div class="">
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!--                        <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            {{ Form::checkbox('show[3]', 1, '', array('id' => 'show3')) }}
                                                            <label for="show3">Show</label>
                                                        </div>
                                                    </div>-->
                    </div> 
                    <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                    if (isset($activityItineryArr) && count($activityItineryArr) > 0) {
                        foreach ($activityItineryArr as $key => $itinery) {
                            ?>
                            <div class="clone_component_1" id="cloneCom_0">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            {{ Form::label('title3', 'Title: ', array('class'=>'label-font')) }}

                                            {{ Form::text('title[]', $itinery->title,  array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            {{ Form::label('description3', 'Description: ', array('class'=>'label-font')) }}
                                            <textarea class="form-control summernote form-control-custom" id="description<?php echo $key; ?>" name="description[]">{!! $itinery->description !!}</textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group clone-components margin-top-20 pull-right">
                                    <?php if ((count($activityItineryArr) - 1) == $key) { ?>
                                        <a class="clr-green-custom  margin-right-10 remove_component_button_1 {{$key == 0?'hidden':'' }}" onclick="remove_component(this, 1)"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove </a>
                                        &nbsp;<a class="clr-green-custom clone_component_button_1" onclick="clone_component(this, 1);"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</a>
                                    <?php } else { ?>
                                        <a class="clr-green-custom  margin-right-10 remove_component_button_1 hidden" onclick="remove_component(this, 1)"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove </a>
                                        &nbsp;<a class="clr-green-custom clone_component_button_1 hidden" onclick="clone_component(this, 1);"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</a>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?> 
                        <div class="clone_component_1" id="cloneCom_0">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title3', 'Title: ', array('class'=>'label-font')) }}

                                        {{ Form::text('title[]', '',  array('class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description3', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[]', '', array('class' => 'form-control summernote form-control-custom', 'rows'=>12, 'id'=>'description0', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>
                            <div class="btn-group clone-components margin-top-20 pull-right">
                                <a class="clr-green-custom  margin-right-10 remove_component_button_1 hidden" onclick="remove_component(this, 1)"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove </a>
                                &nbsp;<a class="clr-green-custom clone_component_button_1 " onclick="clone_component(this, 1);"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <input type="button" onclick="return validateItineraryInfo()" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
    {!! Form::close() !!}
</div>
<script>
    $(function () {
        $(".summernote").summernote({
            height: 150
        });
    });

    function clone_component(t, n) {
        var tr = $(t).closest('.clone_component_' + n);
        var clone = tr.clone();
        clone.find('input').val('');
        clone.find('textarea').html('');
        $('.summernote').summernote('destroy');
//        clone.find(".summernote").summernote({
//            height: 150
//        });
        tr.after(clone);
//        $('.clone_component_' + n).each(function (i, v) {
//            $(v).find('textarea').attr('id', 'description' + i);
//        });
//        $('.clone_component_' + n).each(function (i, v) {
//            $('#description'+i).summernote({height: 150})
//        });
        $(".summernote").summernote({
            height: 150
        });
        clone.find('.note-frame:not(:first)').hide();
        clone.find('.remove_component_button_' + n).removeClass("hidden");
        tr.find('.remove_component_button_' + n).addClass("hidden");
        $(t).addClass("hidden");
    }

    function remove_component(t, n) {
        if ($('.clone_component_' + n).length !== 1) {
            $(t).closest('.clone_component_' + n).remove();
            if ($('.clone_component_' + n).length === 1) {
                $('.remove_component_button_' + n).addClass("hidden");
            } else {
                $('.remove_component_button_' + n).eq(($('.clone_component_' + n).length - 1)).removeClass("hidden");
            }
        } else {
            $('.remove_component_button_' + n).addClass("hidden");
        }
        $('.clone_component_button_' + n).eq(($('.clone_component_' + n).length - 1)).removeClass("hidden");
    }


</script>