<div id="PaymentProtectionInfo" class="tabcontent <?php echo $paymentProtectionInfoHidden; ?>">
     {!! Form::open(['url' => 'admin/activities/savePaymentProtectionInfo', 'name' => 'insertPaymentProtectionInfo' , 'class'=>'insertPaymentProtectionInfo', 'id'=>'insertPaymentProtectionInfo']) !!}   
     
     <?php
        $title = $description = $payment_protection_activity_faq_id = '';
        $is_paymentProtection_info = 0;
        if(!empty($activity->activitity_faqs)){
            foreach ($activity->activitity_faqs as $key => $value) {
                if($value->faq_id==6){
                    $title = $value->title;
                    $description = $value->description;
                    $payment_protection_activity_faq_id = $value->id;
                    $is_paymentProtection_info = 1;
                }
            }
        }

        if(empty($is_paymentProtection_info)){
            $title = $FaqsArr[6]['title'];
            $description = $FaqsArr[6]['description'];
        }
    ?> 

 	<input type="hidden" name="id" value="{{ $id }}">
 	<input type="hidden" name="faq_id[6]" value="6">
    <input type="hidden" name="activity_faq_id[6]" value="{{$payment_protection_activity_faq_id}}">
 		<div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title6', 'Title: ', array('class'=>'label-font')) }}

                                        {{ Form::text('title[6]', $title,  array('class'=>'form-control', 'id'=>'title6', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description6', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[6]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description6', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
     {!! Form::close() !!}
</div>