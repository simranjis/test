<div id="InclusionExclusionInfo" class="tabcontent <?php echo $inclusionExclusionInfoHidden; ?>">
     {!! Form::open(['url' => 'admin/activities/saveInclusionExclusionInfo', 'name' => 'insertInclusionExclusionInfo' , 'class'=>'insertInclusionExclusionInfo', 'id'=>'insertInclusionExclusionInfo']) !!}   
     <?php
        $title = $description = $inclusion_show_checkbox = $inclusion_activity_faq_id = '';

        $is_inclusion_info = 0;
        if(!empty($activity->activitity_faqs)){
            foreach ($activity->activitity_faqs as $key => $value) {
                if($value->faq_id==2){
                    $title = $value->title;
                    $description = $value->description;
                    $inclusion_activity_faq_id = $value->id;
                    if(!empty($value->show)){
                        $inclusion_show_checkbox = "checked='checked'";
                    }
                    $is_inclusion_info = 1;
                }
            }
        }

        if(empty($is_inclusion_info)){
            $title = $FaqsArr[2]['title'];
            $description = $FaqsArr[2]['description'];
        }
    ?> 
 	<input type="hidden" name="id" value="{{ $id }}">
 	<input type="hidden" name="faq_id[2]" value="2">
    <input type="hidden" name="activity_faq_id[2]" value="{{$inclusion_activity_faq_id}}">
 		<div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::checkbox('show[2]', 1, $inclusion_show_checkbox, array('id' => 'show2')) }}
                                        <label for="show2">Show</label>
                                    </div>
                                </div>
                            </div>

                            <h4>Inclusion</h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title2', 'Title: ', array('class'=>'label-font')) }}

                                        {{ Form::text('title[2]', $title,  array('class'=>'form-control', 'id'=>'title2', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description2', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[2]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description2', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <br>

                           <?php
                                $title = $description = $exclusion_activity_faq_id = '';
                                
                                $is_exclusion_info = 0;
                                if(!empty($activity->activitity_faqs)){
                                    foreach ($activity->activitity_faqs as $key => $value) {
                                        if($value->faq_id==7){
                                            $title = $value->title;
                                            $description = $value->description;
                                            $exclusion_activity_faq_id = $value->id;
                                            $is_exclusion_info = 1;
                                        }
                                    }
                                }

                                if(empty($is_exclusion_info)){
                                    $title = $FaqsArr[7]['title'];
                                    $description = $FaqsArr[7]['description'];
                                }
                            ?> 
                            
                        <input type="hidden" name="faq_id[7]" value="7">
                        <input type="hidden" name="activity_faq_id[7]" value="{{$exclusion_activity_faq_id}}">

                            <h4>Exclusion</h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title7', 'Title: ', array('class'=>'label-font')) }}

                                        {{ Form::text('title[7]', $title,  array('class'=>'form-control', 'id'=>'title7', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description7', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[7]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description7', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="button" onclick="return validateInclusionExclutionInfo()" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
     {!! Form::close() !!}
</div>