<div id="QuickInfo" class="tabcontent <?php echo $quickInfoHidden; ?>">
      {!! Form::open(['url' => 'admin/activities/saveQuickInfo', 'name' => 'saveQuickInfoForm' , 'class'=>'saveQuickInfoForm', 'id'=>'saveQuickInfoForm']) !!}                                
        <div class="">
          <div class="row">
              <div class="col-xs-12">                                
                  <div class="">
                      <!-- /.box-header -->
                      <div class="box-body">
                          <div class="">
                              <div class="row">
                                  <div class="col-sm-4 ovrflw-hidden">
                                      <div class="well-text">
                                          <div class="row">
                                              <div class="col-xs-3 col-sm-5 col-md-4">
                                                  <div class="marine_park-box">
                                                      <div class="marin-box">
                                                          <div class="text-center">
                                                              <i class="fa fa-user-o" aria-hidden="true"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-xs-9 col-sm-7 col-md-8">
                                                  <div class="row">
                                                      <div class="col-md-12">
                                                        {{ Form::text('quickinfo1', $activity->quickinfo1, array('class' => 'form-control', 'id'=>'quickinfo1', 'placeholder'=>'Minimum Travelers To Book')) }}
                                                      </div>
                                                      <div class="col-md-8">Minimum Travelers To Book</div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 ovrflw-hidden">
                                      <div class="well-text">
                                          <div class="row">
                                              <div class="col-xs-3 col-sm-5 col-md-4">
                                                  <div class="marine_park-box">
                                                      <div class="marin-box">
                                                          <div class="text-center">
                                                              <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-xs-9 col-sm-7 col-md-8">
                                                  <?php 
                                                    $quickinfo2 = array(
                                                                      'Instant Confirmation'=>'Instant Confirmation',
                                                                      '24 Hours'=>'24 Hours',
                                                                      '48 Hours'=>'48 Hours'
                                                                    );
                                                  ?>
                                                  {{ Form::select('quickinfo2', $quickinfo2,  $activity->quickinfo2, array('class'=>'form-control', 'id'=>'quickinfo2')) }}
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 ovrflw-hidden">
                                      <div class="well-text">
                                          <div class="row">
                                              <div class="col-xs-3 col-sm-5 col-md-4">
                                                  <div class="marine_park-box">
                                                      <div class="marin-box">
                                                          <div class="text-center">
                                                              <i class="fa fa-calendar" aria-hidden="true"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-xs-9 col-sm-7 col-md-8">
                                                  <div class="row">
                                                      <div class="col-md-4">Duration: </div>
                                                      <div class="col-md-8">
                                                        {{ Form::text('quickinfo3', $activity->quickinfo3, array('class' => 'form-control', 'id'=>'quickinfo3', 'placeholder'=>'Duration')) }}
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <br>
                              <div class="row">
                                  <div class="col-sm-4 ovrflw-hidden">
                                      <div class="well-text">
                                          <div class="row">
                                              <div class="col-xs-3 col-sm-5 col-md-4">
                                                  <div class="marine_park-box">
                                                      <div class="marin-box">
                                                          <div class="text-center">
                                                              <i class="fa fa-user" aria-hidden="true"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-xs-9 col-sm-7 col-md-8">
                                                  <?php 
                                                    $quickinfo4 = array(
                                                                    'Private Tour'=>'Private Tour',
                                                                    'Join in Group Tour'=>'Join in Group Tour',
                                                                  );
                                                  ?>
                                                  {{ Form::select('quickinfo4', $quickinfo4,  $activity->quickinfo4, array('class'=>'form-control', 'id'=>'quickinfo4')) }}
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-8 ovrflw-hidden">
                                      <div class="well-text">
                                          <div class="row">
                                              <div class="col-xs-3 col-sm-5 col-md-2">
                                                  <div class="marine_park-box">
                                                      <div class="marin-box">
                                                          <div class="text-center">
                                                              <i class="fa fa-mobile" aria-hidden="true"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-xs-9 col-sm-7 col-md-10">
                                                  <?php 
                                                    $quickinfo5 = array(
                                                                    'Present either a printed or mobile e-voucher to redeem this activity'=>'Present either a printed or mobile e-voucher to redeem this activity',
                                                                    'You Must present a printed voucher to redeem this activity'=>'You Must present a printed voucher to redeem this activity',
                                                                  );
                                                  ?>
                                                  {{ Form::select('quickinfo5', $quickinfo5,  $activity->quickinfo5, array('class'=>'form-control', 'id'=>'quickinfo5')) }}
                                              </div>
                                          </div>
                                      </div>
                                  </div>                                  
                              </div>
                              <br>
                              <div class="row">
                                <div class="col-sm-12 ovrflw-hidden">
                                    <div class="well-text">
                                      <div class="row">
                                          <div class="col-xs-9 col-sm-7 col-md-12 ">
                                              <?php if($activity->addedbytype == 'localguide'){ ?>
                                                  <div>
                                                      <img src="adminpanel/dist/img/local-guide.png" class="img-responsive" width="50px">
                                                  </div>
                                              <?php } ?>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                              </div>

                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <h3 style="color:#000" class="margin-top-0">Highlights</h3>
                              </div>
                          </div>
                          <div class="form-group">
                            {{ Form::textarea('hightlights', $activity->hightlights, array('class' => 'form-control form-control-custom', 'rows'=>4, 'id'=>'hightlights', 'placeholder'=>'Highlights')) }}
                          </div>
                      </div>
                      <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
              </div>
          </div>
      </div> 
        <input type="hidden" name="id" id="id" value="{{$id}}" />
        <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
      {!! Form::close() !!}
</div>