<div id="AdditionalInfo" class="tabcontent <?php echo $additionalInfoHidden; ?>">
     {!! Form::open(['url' => 'admin/activities/saveAdditionalInfo', 'name' => 'insertAdditionalInfo' , 'class'=>'insertAdditionalInfo', 'id'=>'insertAdditionalInfo']) !!}  
    <?php
        $title = $description = $additional_info_activity_faq_id = '';
        $is_additional_info = 0;
        if(!empty($activity->activitity_faqs)){
            foreach ($activity->activitity_faqs as $key => $value) {
                if($value->faq_id==1){
                    $title = $value->title;
                    $description = $value->description;
                    $additional_info_activity_faq_id = $value->id;
                    $is_additional_info = 1;
                }
            }
        }
        if(empty($is_additional_info)){
            $title = $FaqsArr[1]['title'];
            
            $additionalInfoDescription = "<ul>";
            if($activity->quickinfo2 == '24 Hours'){
                $additionalInfoDescription .= "<li>You will receive a confirmation email instantly and voucher within 1 business day after booking.</li>";
            }elseif($activity->quickinfo2 == '48 Hours'){
                $additionalInfoDescription .= "<li>You will receive a confirmation email instantly and voucher within 2 business days after booking.</li>";
            }else{
                $additionalInfoDescription .= "<li>You will receive a confirmation email and voucher instantly.</li>";
            }
            $currency_value = '';
            if(!empty($activity->localguide->currency)){
                $currency_value = $activity->localguide->currency->title;
            }

            $additionalInfoDescription .= "<li>In the event that you do not receive an email from us, please check your Spam folder or notify us via email.</li>";
            if(!empty($activity->localguide_id)){ 
                $additionalInfoDescription .= "<li>Should have localguide prefer to be paid in their local currency";
                if(!empty($currency_value)){
                    $additionalInfoDescription .= " ({$currency_value})";
                }

                $additionalInfoDescription .= ".</li>";
            }
            $additionalInfoDescription .= "</ul>";
            $description = $additionalInfoDescription;
        }
    ?> 
 	<input type="hidden" name="id" value="{{ $id }}">
 	<input type="hidden" name="faq_id[1]" value="1">
    <input type="hidden" name="activity_faq_id[1]" value="{{$additional_info_activity_faq_id}}">
 		<div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title1', 'Title: ', array('class'=>'label-font')) }}

                                        {{ Form::text('title[1]', $title,  array('class'=>'form-control', 'id'=>'title1', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description1', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[1]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description1', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="button" onclick="return validateAdditionalInfo()" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
     {!! Form::close() !!}
</div>