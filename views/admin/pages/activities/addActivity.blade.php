@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }   

    .marin-box {
        background: #35d5e7 none repeat scroll 0 0;
        border: medium none #e9004c;
        color: #fff;
        font-size: 16px;
        padding: 6px 12px;
    } 
</style>
<?php
$id = '';
if(!empty($activity->id)){
    $id = $activity->id;
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Activities
            <small>Add Activity</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Activity</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Tab links -->
                <?php
                $quickInfoActive = $pricingActive = $galleryActive = $additionalInfoActive = $inclusionExclusionInfoActive = $itineraryInfoActive = $howToUseHowToGetInfoActive = $cancellationInfoActive = $paymentProtectionInfoActive = '';

                $quickInfoHidden = $pricingHidden = $galleryHidden = $additionalInfoHidden = $inclusionExclusionInfoHidden = $itineraryInfoHidden = $howToUseHowToGetInfoHidden = $cancellationInfoHidden = $paymentProtectionInfoHidden = 'hide';

                $activityActive = 'active';
                $activityHidden = '';
                
                if(!empty($active_tab) && !empty($activity->id)){
                    $activityActive = '';
                    $activityHidden = 'hide';
                    if($active_tab == 'activityInfo'){
                        $activityActive = 'active';
                        $activityHidden = '';
                    }
                    if($active_tab == 'quickInfo'){
                        $quickInfoActive = 'active';
                        $quickInfoHidden = '';
                    }
                    if($active_tab == 'pricingInfo'){
                        $pricingActive = 'active';
                        $pricingHidden = '';
                    }
                    if($active_tab == 'galleryInfo'){
                        $galleryActive = 'active';
                        $galleryHidden = '';
                    }
                    if($active_tab == 'additionalInfo'){
                        $additionalInfoActive = 'active';
                        $additionalInfoHidden = '';
                    }
                    if($active_tab == 'inclusionExclusionInfo'){
                        $inclusionExclusionInfoActive = 'active';
                        $inclusionExclusionInfoHidden = '';
                    }
                    if($active_tab == 'itineraryInfo'){
                        $itineraryInfoActive = 'active';
                        $itineraryInfoHidden = '';
                    }
                    if($active_tab == 'howToUseHowToGetInfo'){
                        $howToUseHowToGetInfoActive = 'active';
                        $howToUseHowToGetInfoHidden = '';
                    }
                    if($active_tab == 'cancellationInfo'){
                        $cancellationInfoActive = 'active';
                        $cancellationInfoHidden = '';
                    }
                    if($active_tab == 'paymentProtectionInfo'){
                        $paymentProtectionInfoActive = 'active';
                        $paymentProtectionInfoHidden = '';
                    }
                }
                ?>
                <?php
                $links_disabled = 'disabled="disabled"';
                if(!empty($activity->id)){
                    $links_disabled = '';
                }
                ?>
                <div class="tab">
                    <button class="tablinks <?php echo $activityActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('Activity')" id="ActivityTab">Activity
                    </button>
                    <button class="tablinks <?php echo $quickInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('QuickInfo')" id="QuickInfoTab">Quick Info
                    </button>
                    <button class="tablinks <?php echo $pricingActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('PricingInfo')" id="PricingInfoTab">Pricing
                    </button>
                    <button class="tablinks <?php echo $galleryActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('GalleryInfo')" id="GalleryInfoTab">Gallery
                    </button>
                    <button class="tablinks <?php echo $additionalInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('AdditionalInfo')" id="AdditionalInfoTab">Additional Info.</button>
                    <button class="tablinks <?php echo $inclusionExclusionInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('InclusionExclusionInfo')" id="InclusionExclusionInfoTab">Inclusion/Exclusion
                    </button>
                    <button class="tablinks <?php echo $itineraryInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('ItineraryInfo')" id="ItineraryInfoTab">Itinerary
                    </button>
                    <button class="tablinks <?php echo $howToUseHowToGetInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('HowToUseHowToGetInfo')" id="HowToUseHowToGetInfoTab">How to Use and How to Get There
                    </button>
                    <button class="tablinks <?php echo $cancellationInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('CancellationInfo')" id="CancellationInfoTab">Cancellation </button>
                    <button class="tablinks <?php echo $paymentProtectionInfoActive; ?>" <?php echo $links_disabled; ?> onclick="activeTab2('PaymentProtectionInfo')" id="PaymentProtectionInfoTab">Payment Protection 
                    </button>
                </div>

                <!-- Tab content -->

                <!-- Activity Tab -->
                @include('admin.pages.activities.partials.activity_tab')

                <!-- Quick Info Tab -->
                @include('admin.pages.activities.partials.quickInfo_tab')

                <!-- Pricing Tab -->
                @include('admin.pages.activities.partials.pricing_tab')

                <!-- Gallery Tab -->
                @include('admin.pages.activities.partials.gallery_tab')

                <!-- Additional Info Tab -->
                @include('admin.pages.activities.partials.additional_info_tab')

                <!-- Inclusion/Exclusion Info Tab -->
                @include('admin.pages.activities.partials.inclusion_exclusion_info_tab')

                <!-- Inclusion/Exclusion Info Tab -->
                @include('admin.pages.activities.partials.itinerary_tab')

                <!-- Inclusion/Exclusion Info Tab -->
                @include('admin.pages.activities.partials.howToUseHowToGet')

                <!-- Inclusion/Exclusion Info Tab -->
                @include('admin.pages.activities.partials.cancellation')

                <!-- Inclusion/Exclusion Info Tab -->
                @include('admin.pages.activities.partials.paymentProtectionInfo')
            </div>
        </div>
    </section>
</div>  
@stop
@section('page_scripts')
<script src="js/admin/activities/addActivity.js"></script>
@stop