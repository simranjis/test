@extends('admin.layouts.defaultsidebar')
@section('content')

<script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css" type="text/css" />


<script src="{{asset('adminpanel/tinymce/js/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('adminpanel/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<script>
    /*tinymce.init({
      selector: 'textarea.editor'
    });*/
</script>
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<script type="text/javascript">



$(function(){

     // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    $( ".pickerdate" ).datepicker();
    
    if ($('#activity-image').val() != '') {
        $('#activity-image').val('');
    }

    if ($('#gallery-image').val() != '') {
        $('#gallery-image').val('');
    }
    
        
    changeCurrency();
    
   
    $('input[name=price]').bind('change' , function(e){
        changeCurrency();
    });
   
   
    
    $('#activity-image').on('change', function() {

        var ext = $(this).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            if ($('#preview').data('Jcrop')) {
                // if need destroy jcrop or its created dom element here
                $('#preview').data('Jcrop').destroy();
                $('#preview').attr('src', 'http://placehold.it/360x180');
                $("#preview").removeAttr("style");
            }
            $(this).val('');
            alert('invalid extension!');
        } else {
            var file_name = $(this).val();
            if(file_name.length > 0) {
              addJcrop(this);
            }
        }
    });
    

    $('#gallery-image').on('change', function() {

        var ext = $(this).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            if ($('#gallery-preview').data('Jcrop')) {
                // if need destroy jcrop or its created dom element here
                $('#gallery-preview').data('Jcrop').destroy();
                $('#gallery-preview').attr('src', 'http://placehold.it/304x350');
                $("#gallery-preview").removeAttr("style");
            }
            $(this).val('');
            alert('invalid extension!');
        } else {
            var file_name = $(this).val();
            if(file_name.length > 0) {
              addJcropGallery(this);
            }
        }
    });
    
    var addJcrop = function(input) {
        if ($('#preview').data('Jcrop')) {
          // if need destroy jcrop or its created dom element here
          $('#preview').data('Jcrop').destroy();
        }

        file = input.files && input.files[0];
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = function () {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            if(width < 360 || width > 600 || height < 180) {
                $('#activity-image').val('');
                $("#preview").removeAttr("style");
                alert("Image size should be between 360x180 and 600px ");
                $('#preview').attr('src', 'http://placehold.it/360x180');
            } else {

                // this will add the src in image_prev as uploaded
                //if (input.files && input.files[0]) {
                if(file) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                    $("#preview").attr('src', e.target.result);
                    var box_width = $('#image_prev_container').width();
                    $("#preview").removeAttr("style");
                    $('#preview').Jcrop({
                      setSelect: [0, 0, 360, 180],
                      minSize: [360, 180],
                      maxSize: [360, 180],
                      aspectRatio: 0,
                      allowResize: true,
                      onSelect: getCoordinates,
                      onChange: getCoordinates,
                     });image
                    }
                    reader.readAsDataURL(input.files[0]);
                }

                var getCoordinates = function(c){
                 $('#cropx').val(c.x);
                 $('#cropy').val(c.y);
                 $('#cropw').val(c.w);
                 $('#croph').val(c.h);
                };
   
            }
        }
    }


    var addJcropGallery = function(input) {
        if ($('#gallery-preview').data('Jcrop')) {
          // if need destroy jcrop or its created dom element here
          $('#gallery-preview').data('Jcrop').destroy();
        }

        file = input.files && input.files[0];
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = function () {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            if(width < 304 || width > 600 || height < 350) {
                $('#gallery-image').val('');
                $("#gallery-preview").removeAttr("style");
                alert("Image size should be between 304x350 and 600px ");
                $('#gallery-preview').attr('src', 'http://placehold.it/304x350');
            } else {

                // this will add the src in image_prev as uploaded
                //if (input.files && input.files[0]) {
                if(file) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                    $("#gallery-preview").attr('src', e.target.result);
                    var box_width = $('#gallery-image_prev_container').width();
                    $("#gallery-preview").removeAttr("style");
                    $('#gallery-preview').Jcrop({
                      setSelect: [0, 0, 304, 350],
                      minSize: [304, 350],
                      maxSize: [304, 350],
                      aspectRatio: 0,
                      allowResize: true,
                      onSelect: getCoordinates,
                      onChange: getCoordinates,
                     });image
                    }
                    reader.readAsDataURL(input.files[0]);
                }

                var getCoordinates = function(c){
                 $('#gallery-cropx').val(c.x);
                 $('#gallery-cropy').val(c.y);
                 $('#gallery-cropw').val(c.w);
                 $('#gallery-croph').val(c.h);
                };
   
            }
        }
    }
});



function openTab(evt, TabName) {
    // Declare all variables
    var i, tabcontent, tablinks;
    //alert('Please Add Activity Information');
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(TabName).style.display = "block";
    evt.currentTarget.className += " active";
}


function addNewRow(button) {
    if($('.pricing-container').length > 0) {
        $('.pricing-container').last().after('<div class="box-body table-responsive pricing-container">' + $('.pricing-container').html() + '</div>');
        //$('.pricing-container').next().find('select').removeAttr('disabled');
        $('.pricing-container').next().find('select').removeAttr('disabled');
        $( ".pickerdate" ).datepicker();
        $('.pricing-container').last().find('input:text').val('');
        $('.pricing-container').last().find('.delete').show();
        $('.pricing-container').last().find('input').val('');
        
        //var next_container = $(button).parent().parent().parent().parent().parent().next();
        //if(next_container.attr('class') == 'pricing-container') {
        //    next_container.find('input:text').val('');
        //}
        
    }
}

function addRemoveThisRow(button) {
    if($('.pricing-container').length > 1) {
        $(button).parent().parent().parent().parent().parent().remove();
        /*$('.pricing-container').each(function(){
            $(this).click(function(){
                $(this).remove();
            });
        });*/
        $( ".pickerdate" ).datepicker();
    }    
}

function changeCurrency() {
    
    var selected_currency = $('#currencies_id').val();
    var price = ($('input[name=price]').val());
    //var conversion = [1 => 1, 2 => 4.5];
    var conversion = {
    @foreach($currencies as $currency)    
           {{ $currency['id'] }}:"{{ $currency['conversion'] }}",
    @endforeach
    };
    
    $("input[name=conversion]").val((conversion[selected_currency] * price).toFixed(2));
    console.log(selected_currency);
    console.log(conversion[selected_currency]);
} 

function changePackType(select) {
    //alert(select.value);
   if(select.value == 'package') {
        $(select).parent().parent().next().find('select').val('all').attr('readonly' , 'readonly');
        //$(select).parent().parent().next().find('select').val('all');
        $(select).parent().parent().parent().next().find('textarea').show();
    } else {
        $(select).parent().parent().next().find('select').val('adult').removeAttr('readonly');
        //$(select).parent().parent().next().find('select').val('adult');
        $(select).parent().parent().parent().next().find('textarea').hide();
    }
   //.find('input').val('all');
   //console.log($(select).parent().parent().parent().next().find('textarea').hide());
}    


function addFaqRow(button) {
    
    if($('.faq-container').length >= 1) {
        //tinymce.remove('textarea');
        tinymce.remove();
        $('.faq-container').last().after('<div class="faq-container">' + $('.faq-container').html() + '</div>');
        /*tinymce.init({
            selector: 'textarea.editor'
        });*/
    }
}


function deleteFaqRow(button) {
    if($('.faq-container').length > 1) {
        //tinymce.remove('textarea');
        tinymce.remove();
        $('.faq-container').last().remove();
        /*tinymce.init({
                selector: 'textarea.editor'
        });*/
    }
}


function addExtraRow(button) {
    if($(".extra-options-container").length > 0) {
          $('.extra-options-container').last().after('<div class="row margin extra-options-container">' + $('.extra-options-container').html() + '</div>');
          $('.extra-options-container').last().find('input:text').val('');
          $('.extra-options-container').last().find('input').val('');
          $('.extra-options-container').last().find('.delete').show();

    }
}

function addRemoveExtraRow(button) {
    if($(".extra-options-container").length > 1) {
          //$('.extra-options-container').last().remove();
          $(button).parent().parent().remove();
          console.log($(button).parent().parent().html());
    }    
}

</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->                        <!-- /.box -->

    <section class="content-header">
        <h1>
            Activities
            <small>Add {{ $activities['title'] }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activities</li>
        </ol>
    </section>
    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif

    
        
           

    
    <section class="content">
        <!-- Tab links -->
        <div class="tab">
          <button class="tablinks" onclick="openTab(event, 'Activity')" @if(Request::get('default') === 'Activity') id="defaultOpen" @endif >Activity</button>
          <button class="tablinks" onclick="openTab(event, 'Quick-info')" @if(Request::get('default') === 'Quick-info') id="defaultOpen" @endif >Quick Info</button>
          <button class="tablinks" onclick="openTab(event, 'Pricing')" @if(Request::get('default') === 'Pricing') id="defaultOpen" @endif >Pricing</button>
          <button class="tablinks" onclick="openTab(event, 'Gallery')" @if(Request::get('default') === 'Gallery') id="defaultOpen" @endif >Gallery</button>
          <button class="tablinks" onclick="openTab(event, 'Faq')" @if(Request::get('default') === 'Faq') id="defaultOpen" @endif >Faq</button>
        </div>

        <!-- Tab content -->
        <div id="Activity" class="tabcontent">
              {!! Form::open(['url' => 'admin/activities/insert', 'name' => 'insertActivitiesForm' , 'class'=>'insertActivitiesForm', 'id'=>'insertActivitiesForm', 'files' => true]) !!}                                
                    <input type="hidden" name="id" value="{{ $activities['id'] }}">
                
                
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">                                
                                    <div class="">
                                        
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>City:</label>
                                                    <select name="destinations_id" id="destinations_id" class="form-control">                                                   
                                                        @foreach($destinations as $destination)
                                                        <option @if($activities['destinations_id'] == $destination['id']) selected="selected" @endif value="{{ $destination['id'] }}">{{ $destination['title'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <!---->
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>For:</label>
                                                    <select name="categories_id" class="form-control">
                                                        <option @if($activities['categories_id'] == 1) selected="selected" @endif value="1">AUTHENTIC EXPERIENCES</option>
                                                        <option @if($activities['categories_id'] == 2) selected="selected" @endif value="2">POPULAR ACTIVITIES</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <input type="hidden" name="cropx" id="cropx" value="0" />
                                                <input type="hidden" name="cropy" id="cropy" value="0" />
                                                <input type="hidden" name="cropw" id="cropw" value="0" />
                                                <input type="hidden" name="croph" id="croph" value="0" />
                                                <div class="col-sm-4">&nbsp;</div>
                                                <div class="col-sm-4">
                                                   <strong>Activity Cover Photo</strong><br>
                                                   @if($activities['image'])
                                                        <img src="uploads/{{ $activities['image'] }}" class="img-reponsive">
                                                        <br />
                                                        {{ $activities['image'] }}
                                                    @else
                                                    @endif
                                                    <hr />
                                                    <div id="image_prev_container">
                                                        <img id="preview" src="http://placehold.it/360x180" alt="your image" />
                                                    </div>
                                                    
                                                    <div class="form-group margin">
                                                        <label for="exampleInputFile">File input</label>
                                                        <input type='file' id="activity-image" name="image" />
                                                        <input type="hidden" name="previous_image" value="{{ $activities['image'] }}">
                                                        <p class="help-block">Attach Image.</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">&nbsp;</div>
                                            </div>    
                                            <div class="row">    
                                                <!--<div class="col-sm-6">
                                                    <img src="adminpanel/dist/img/360.png" class="img-reponsive">
                                                    <div class="form-group margin">
                                                        <label for="exampleInputFile">File input</label>
                                                        <input id="exampleInputFile" name="image" type="file">
    
                                                        <p class="help-block">Attach Image.</p>
                                                    </div>
                                                </div>-->

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Activity Title</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="title" placeholder="Title" value="{{ $activities['title'] }}" type="text">
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <label for="exampleInputEmail1">Actual Price</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="amount" placeholder="Amount" value="{{ $activities['amount'] }}" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Final Price</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="finalprice" placeholder="Final Amount" value="{{ $activities['finalprice'] }}" type="text">
                                                    </div>-->
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="addedbytype" id="addedbytype" value="admin" @if($activities['addedbytype'] == 'admin') checked="checked" @endif  type="radio">
                                                                    Admin
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="addedbytype" id="addedbytype" value="localguide" @if($activities['addedbytype'] == 'localguide') checked="checked" @endif type="radio">
                                                                    Local Guide
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $local_guide_block = 'hide';
                                                        $localguide_disabled = 'disabled="disabled"';
                                                        if($activities['addedbytype'] == 'localguide'){
                                                            $local_guide_block = '';
                                                            $localguide_disabled = '';
                                                        }
                                                        ?>
                                                        <div class="row {{$local_guide_block}}" id="local_guide_block">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Local Guide:</label>
                                                                    <select name="localguide_id" id="localguide_id" class="form-control" {{$localguide_disabled}}>
                                                                        <option value="0">Select Local Guide</option>
                                                                        <?php foreach($localGuidesArr as $id => $localguide_name){ ?>
                                                                            <?php if($id == $activities['localguide_id']){ ?>
                                                                                <option value="{{$id}}" selected>{{$localguide_name}}</option>
                                                                            <?php }else{ ?>
                                                                                <option value="{{$id}}">{{$localguide_name}}</option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div>    
        
             
 
                <input type="submit" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
              {!! Form::close() !!}
        </div>

        <div id="Quick-info" class="tabcontent">
              {!! Form::open(['url' => 'admin/activities/insert/quickinfo', 'name' => 'insertQuickInfoForm' , 'class'=>'insertQuickInfoForm', 'id'=>'insertQuickInfoForm', 'files' => false]) !!}                                
              <input type="hidden" name="id" value="{{ $activities['id'] }}">
                     
                <div class="">
                  <div class="row">
                      <div class="col-xs-12">                                
                          <div class="">

                              <!-- /.box-header -->
                              <div class="box-body">
                                  <div class="">
                                      
                                      <div class="row">
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              <div class="marin-box">
                                                                  <div class="text-center">
                                                                      <i class="fa fa-user-o" aria-hidden="true"></i>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          
                                                          <div class="row">
                                                              <div class="col-md-4"><input class="form-control" name="quickinfo1" placeholder="Minimum Travelers To Book" value="{{ $activities['quickinfo1'] }}" type="text"></div>
                                                              <div class="col-md-8">Minimum Travelers To Book</div>
                                                              
                                                          </div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              <div class="marin-box">
                                                                  <div class="text-center">
                                                                      <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          <!--<input class="form-control" name="quickinfo2" id="exampleInputEmail1" placeholder="Confirmation" value="{{ $activities['quickinfo2'] }}" type="text">-->
                                                          <select name="quickinfo2">
                                                              <option @if($activities['quickinfo2'] === 'Instant Confirmation') selected="selected" @endif value="Instant Confirmation">Instant Confirmation</option>
                                                              <option @if($activities['quickinfo2'] === '24 Hours') selected="selected" @endif value="24 Hours">24 Hours</option>
                                                              <option @if($activities['quickinfo2'] === '48 Hours') selected="selected" @endif value="48 Hours">48 Hours</option>
                                                          </select>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              <div class="marin-box">
                                                                  <div class="text-center">
                                                                      <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          <div class="row">
                                                              <div class="col-md-4">Duration:</div>
                                                              <div class="col-md-8"><input class="form-control" name="quickinfo3" id="exampleInputEmail1" placeholder="Duration" value="{{ $activities['quickinfo3'] }}" type="text"></div>
                                                              
                                                          </div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              <div class="marin-box">
                                                                  <div class="text-center">
                                                                      <i class="fa fa-user" aria-hidden="true"></i>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          <!--<input class="form-control" name="quickinfo4" id="exampleInputEmail1" placeholder="Tour Type" value="{{ $activities['quickinfo4'] }}" type="text">-->
                                                          <select name="quickinfo4">
                                                              <option  @if($activities['quickinfo4'] === 'Private Tour') selected="selected" @endif value="Private Tour">Private Tour</option>
                                                              <option @if($activities['quickinfo4'] === 'Join in Group Tour') selected="selected" @endif value="Join in Group Tour">Join in Group Tour</option>
                                                          </select>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              <div class="marin-box">
                                                                  <div class="text-center">
                                                                      <i class="fa fa-mobile" aria-hidden="true"></i>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          <!--<input class="form-control" name="quickinfo5" id="exampleInputEmail1" placeholder="Redemption" value="{{ $activities['quickinfo5'] }}" type="text">-->
                                                          <select name="quickinfo5">
                                                              <option @if($activities['quickinfo5'] === 'Present either a printed or mobile e-voucher to redeem this activity') selected="selected"  @endif value="Present either a printed or mobile e-voucher to redeem this activity">Present either a printed or mobile e-voucher to redeem this activity</option>
                                                              @if($activities['addedbytype'] === 'admin') <option @if($activities['quickinfo5'] === 'You Must present a printed voucher to redeem this activity') selected="selected"  @endif value="You Must present a printed voucher to redeem this activity">You Must present a printed voucher to redeem this activity</option>@endif
                                                          </select>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-4 ovrflw-hidden">
                                              <div class="well-text">
                                                  <div class="row">
                                                      <div class="col-xs-3 col-sm-5 col-md-4">
                                                          <div class="marine_park-box">
                                                              
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-9 col-sm-7 col-md-8">
                                                          <input type="hidden" name="quickinfo6" value="{{ $activities['quickinfo6'] }}">
                                                          <!--<input class="form-control" name="quickinfo6" id="exampleInputEmail1" placeholder="Local Guide Hosts" value="{{ $activities['quickinfo6'] }}" type="text">-->
                                                          @if($activities['addedbytype'] == 'localguide')
                                                          <div class="">
                                                            <div class="text-center">
                                                                <img src="adminpanel/dist/img/local-guide.png" class="img-responsive">
                                                            </div>
                                                          </div>
                                                          @endif
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-12">
                                          <h3 style="color:#000" class="margin-top-0">Highlights</h3>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <textarea class="form-control" name="hightlights" rows="3" placeholder="Enter ...">{{ $activities['hightlights'] }}</textarea>
                                  </div>

                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                      </div>
                  </div>
              </div> 
                <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
              {!! Form::close() !!}
        </div>
        
        
        <div id="Pricing" class="tabcontent">
            {!! Form::open(['url' => 'admin/activities/insert/pricing', 'name' => 'insertPricingForm' , 'class'=>'insertPricingForm', 'id'=>'insertPricingForm', 'files' => false]) !!}                                
                <input type="hidden" name="id" value="{{ $activities['id'] }}">
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                
                                <div class="row">
                                    <div class="col-xs-4"></div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-xs-4">All prices will be quoted in our base currency (USD). <br />
                                        In case you need to quote it in your Local Currency, it will <br /> 
                                        be converted in USD on the activity listings. <br /> </div>
                                </div>
                                
                                
                                  <div class="row">
                                      <div class="col-xs-4">
                                          <div class="form-group">
                                              <label>Currency</label>
                                              <select onChange="changeCurrency();" name="currencies_id" id="currencies_id" class="form-control">
                                                  @foreach($currencies as $currency)
                                                    @if($currency['setdefault'] == 1 || $activities['currencies_id'] == $currency['id'])
                                                      <option selected="selected" value="{{ $currency['id'] }}">{{ $currency['title'] }}</option>
                                                    @else
                                                      <option value="{{ $currency['id'] }}">{{ $currency['title'] }}</option>
                                                    @endif
                                                  @endforeach
                                              </select>
                                          </div>
                                      </div>
                                      <div class="col-xs-4">
                                          <div class="form-group">
                                              <!--<label>Price</label>-->
                                              <!--<input class="form-control" name="price" value="{{ $activities['price'] }}"  placeholder="USD" type="text">-->
                                              <input type="hidden" name="price" value="{{ $activities['price'] }}">
                                          </div>
                                      </div>
                                      <div class="col-xs-4">
                                          <div class="form-group">
                                              <!--<label>Base Currency</label>-->
                                              <input class="form-control" name="conversion" id="conversion-price" placeholder="" type="hidden" readonly="readonly">
                                          </div>
                                      </div>
                                  </div>
                                
                                
                                @if(count($activities['pricing']) > 0)
                                @foreach($activities['pricing'] as $pricing)
                                <!-- /.box-header -->
                                <div class="box-body table-responsive pricing-container">
                                <input type="hidden" name="pricing[id][]" value="{{ $pricing['id'] }}">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Package</th>
                                                <th>User</th>
                                                <th>Title</th>
                                                <th>Price</th>
                                                <th>Date</th>
                                                <th>Call To Action</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="pricing[type][]" onchange="changePackType(this);" class="pricing-type form-control">
                                                            <option value="person" @if($pricing['type'] == 'person') selected="selected" @endif>Per Person</option>
                                                            <option value="package" @if($pricing['type'] == 'package') selected="selected" @endif>Per Package</option>
                                                            
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="pricing[pricefor][]" class="form-control" @if($pricing['pricefor'] == 'all') readonly="readonly" @endif>
                                                            
                                                            <option value="adult" @if ($pricing['pricefor'] == 'adult') selected="selected" @endif >Adult</option>
                                                            <option value="child" @if ($pricing['pricefor'] == 'child') selected="selected" @endif >Child</option>
                                                            <option value="all" @if ($pricing['pricefor'] == 'all') selected="selected" @endif >All</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input class="form-control" id="exampleInputEmail1" name="pricing[title][]" value="{{ $pricing['title'] }}" placeholder="Title" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input class="form-control" id="exampleInputEmail1" name="pricing[price][]" value="{{ $pricing['price'] }}" placeholder="Price" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input style="width:90px;" type="text"  name="pricing[startdate][]" value="{{ $pricing['startdate'] }}" placeholder="Start Date" class="pickerdate form-control">
                                                            &nbsp;
                                                            <input style="width:90px;" type="text" name="pricing[enddate][]" value="{{ $pricing['enddate'] }}" placeholder="End Date" class="pickerdate form-control">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn bg-maroon btn-flat margin add-more" onclick="addNewRow(this);" ><i class="fa fa-plus" aria-hidden="true"></i> Add more</button>
                                                    <button type="button" class="btn bg-navy btn-flat margin delete" style="display:none;" onclick="addRemoveThisRow(this);" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><textarea name="pricing[description][]" class="form-control" rows="3" @if($pricing['pricefor'] == 'all') style="display:block;" @else style="display:none;" @endif placeholder="Package Details ...">{{ $pricing['description'] }}</textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                                @endforeach
                                @else
                                <!-- /.box-header -->
                                <div class="box-body table-responsive pricing-container">
                                    <input type="hidden" name="pricing[id][]" value="">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Package</th>
                                                <th>User</th>
                                                <th>Title</th>
                                                <th>Price</th>
                                                <th>Date</th>
                                                <th>Call To Action</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="pricing[type][]" onchange="changePackType(this);" class="pricing-type form-control">
                                                            <option value="person">Per Person</option>
                                                            <option value="package">Per Package</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select name="pricing[pricefor][]" class="form-control">
                                                            <option value="adult" >Adult</option>
                                                            <option value="child" >Child</option>
                                                            <option value="all" >All</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input class="form-control" id="exampleInputEmail1" name="pricing[title][]" value="" placeholder="Title" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input class="form-control" id="exampleInputEmail1" name="pricing[price][]" value="" placeholder="Price" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input style="width:90px;" type="text" name="pricing[startdate][]" value="" placeholder="Start Date" class="pickerdate form-control">
                                                            &nbsp;
                                                            <input style="width:90px;" type="text" name="pricing[enddate][]" value="" placeholder="End Date" class="pickerdate form-control">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn bg-maroon btn-flat margin add-more" onclick="addNewRow(this);" ><i class="fa fa-plus" aria-hidden="true"></i> Add more</button>
                                                    <button type="button" class="btn bg-navy btn-flat margin delete" onclick="addRemoveThisRow(this);" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6"><textarea name="pricing[description][]" class="form-control" rows="3" style="display:none;" placeholder="Package Details ..."></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                @endif
                                
                                 <div class="row">
                                      <div class="col-sm-12">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <h3 class="margin-top-0 text-black">Service fee?</h3>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-2">
                                                  <div class="form-group">
                                                      <label>Service Fee</label>
                                                      <input type="checkbox" name="servicefee_status" @if($activities['servicefee_status'] == 'yes') checked="checked" @endif value="yes">
                                                      <select name="servicefee_type" class="form-control">
                                                            <option @if($activities['servicefee_type'] == 'person') selected="selected" @endif value="person">Per Person</option>
                                                            <option @if($activities['servicefee_type'] == 'booking') selected="selected" @endif value="booking">Per Booking</option>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="col-sm-4">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Amount</label>
                                                      <input class="form-control" id="exampleInputEmail1" placeholder="Amount" name="service_fee" value="{{ $activities['service_fee'] }}" type="text">
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                              </div>
                                          </div>
                                          
                                          <div class="row">
                                              <div class="col-sm-12">
                                                  
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="margin-top-0 text-black">Extra Details?</h3>
                                                    </div>
                                                  </div>
                                                  
                                                  <div class="row">
                                                    <div class="col-sm-4">
                                                      <div class="form-group">
                                                          <label>Extra Details:</label>
                                                          <input type="checkbox" name="extra_details" @if($activities['extra_details'] == 'yes') checked="checked" @endif value="yes" >
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-8"></div>
                                                  </div>
                                                  
                                                  @if(count($activities['options']) > 0)
                                                    @foreach($activities['options'] as $options)
                                                        <div class="row margin extra-options-container">
                                                            <input type="hidden" name="options[id][]" value="{{ $options['id'] }}">
                                                            <div class="col-sm-4"><input type="text" class="form-control" name="options[title][]" placeholder="Option...." value="{{ $options['title'] }}"></div> 
                                                            <div class="col-sm-4"><input type="text" class="form-control" name="options[description][]" placeholder="Description...." value="{{ $options['description'] }}"></div>
                                                            <div class="col-sm-4">

                                                              <button type="button" class="btn bg-maroon btn-flat add-more" onclick="addExtraRow(this);" ><i class="fa fa-plus" aria-hidden="true"></i> Add more</button>
                                                              <button type="button" class="btn bg-navy btn-flat delete" style="display:none;" onclick="addRemoveExtraRow(this);" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>


                                                            </div>
                                                        </div>
                                                    @endforeach
                                                  
                                                  @else

                                                  <div class="row margin extra-options-container">
                                                      <input type="hidden" name="options[id][]" value="">
                                                      <div class="col-sm-4"><input type="text" class="form-control" name="options[title][]" placeholder="Option...." value=""></div> 
                                                      <div class="col-sm-4"><input type="text" class="form-control" name="options[description][]" placeholder="Description...." value=""></div>
                                                      <div class="col-sm-4">
                                                          
                                                        <button type="button" class="btn bg-maroon btn-flat add-more" onclick="addExtraRow(this);" ><i class="fa fa-plus" aria-hidden="true"></i> Add more</button>
                                                        <button type="button" class="btn bg-navy btn-flat delete" onclick="addRemoveExtraRow(this);" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>

                                                          
                                                      </div>
                                                  </div>
                                                  
                                                  @endif
                                                  
                                              </div>
                                          </div>
                                          
                                      </div>
                                  </div>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>              
                <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
            {!! Form::close() !!}
        </div>

        <div id="Gallery" class="tabcontent">
            {!! Form::open(['url' => 'admin/activities/insert/gallery', 'name' => 'insertGalleryForm' , 'class'=>'insertGalleryForm', 'id'=>'insertGalleryForm', 'files' => true]) !!}                                
            <input type="hidden" name="id" value="{{ $activities['id'] }}">    
            <div class="box-body">
                <div>
                    
                    @if(count($activities['gallery']) > 0)
                        <div class="row">
                        @foreach($activities['gallery'] as $items)
                            <div class="col-sm-3">
                                Title: {{ $items['title'] }}
                                <img src="uploads/{{ $items['image'] }}" class="img-reponsive" >
                                {{ $items['image'] }}
                                
                            </div>
                        @endforeach
                        </div>
                    @endif
                    <hr />
                    @if(count($activities['gallery']) < 10)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input class="form-control" id="exampleInputEmail1" name="title" placeholder="Title" type="text">
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="row">

                        <input type="hidden" name="cropx" id="gallery-cropx" value="0" />
                        <input type="hidden" name="cropy" id="gallery-cropy" value="0" />
                        <input type="hidden" name="cropw" id="gallery-cropw" value="0" />
                        <input type="hidden" name="croph" id="gallery-croph" value="0" />

                        <div class="col-sm-12">
                          
                            <div id="gallery-image_prev_container">
                                <img id="gallery-preview" src="http://placehold.it/304x350" alt="your image" />
                            </div>
                            <div class="form-group margin">
                                <label for="exampleInputFile">File input</label>
                                <input type='file' id="gallery-image" name="image" />
                                <p class="help-block">Attach Image.</p>
                            </div>
                        </div>
                        
                    </div> 
                    <input type="submit" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
                    <a href="{{ url('admin/activities/add') }}/{{ $activities['id'] }}?default=Faq"><input type="button" name="Next" value="Next" class="btn bg-maroon btn-flat margin"></a>
                    @endif
                    
                    
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div id="Faq" class="tabcontent">
            {!! Form::open(['url' => 'admin/activities/insert/faq', 'name' => 'insertFaqForm' , 'class'=>'insertFaqForm', 'id'=>'insertFaqForm', 'files' => true]) !!}                                
            <input type="hidden" name="id" value="{{ $activities['id'] }}">    
            <div class="">
                <div class="row">
                    <div class="col-xs-10">
                        <div class="faq-container">
                                <div class="form-group margin">
                                    <input class="form-control" id="exampleInputEmail1" name="title[]" placeholder="Title" type="text">
                                        <br />
                                    <textarea name="description[]" cols="140" rows="15" class="editor"></textarea>
                                </div>
                                <hr />
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <button type="button" class="btn bg-maroon btn-flat margin add-more" onclick="addFaqRow(this)" ><i class="fa fa-plus" aria-hidden="true"></i> Add more</button>
                        <button type="button" class="btn bg-navy btn-flat margin delete" onclick="deleteFaqRow(this)" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                    </div>
                </div>
            </div>            
            <input type="submit" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
            {!! Form::close() !!}
        </div>    
    </section>
</div>
@stop
@section('page_scripts')
<script src="js/admin/activities/add.js"></script>
@stop