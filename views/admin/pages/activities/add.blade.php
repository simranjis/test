@extends('admin.layouts.defaultsidebar')
@section('content')

<!--<script
    src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
crossorigin="anonymous"></script>-->

<script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css" type="text/css" />


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<script type="text/javascript">

$(function(){

    if ($('#activity-image').val() != '') {
        $('#activity-image').val('');
    }
    
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    $('#activity-image').on('change', function() {

        var ext = $(this).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            if ($('#preview').data('Jcrop')) {
                // if need destroy jcrop or its created dom element here
                $('#preview').data('Jcrop').destroy();
                $('#preview').attr('src', 'http://placehold.it/360x180');
                $("#preview").removeAttr("style");
            }
            $(this).val('');
            alert('invalid extension!');
        } else {
            var file_name = $(this).val();
            if(file_name.length > 0) {
              addJcrop(this);
            }
        }
    });
    
    
    var addJcrop = function(input) {
        if ($('#preview').data('Jcrop')) {
          // if need destroy jcrop or its created dom element here
          $('#preview').data('Jcrop').destroy();
        }

        file = input.files && input.files[0];
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = function () {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            if(width < 360 || width > 600 || height < 180) {
                $('#activity-image').val('');
                $("#preview").removeAttr("style");
                alert("Image size should be between 360x180 and 600px ");
                $('#preview').attr('src', 'http://placehold.it/360x180');
            } else {

                // this will add the src in image_prev as uploaded
                //if (input.files && input.files[0]) {
                if(file) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                    $("#preview").attr('src', e.target.result);
                    var box_width = $('#image_prev_container').width();
                    $("#preview").removeAttr("style");
                    $('#preview').Jcrop({
                      setSelect: [0, 0, 360, 180],
                      minSize: [360, 180],
                      maxSize: [360, 180],
                      aspectRatio: 0,
                      allowResize: true,
                      onSelect: getCoordinates,
                      onChange: getCoordinates,
                     });image
                    }
                    reader.readAsDataURL(input.files[0]);
                }

                var getCoordinates = function(c){
                 $('#cropx').val(c.x);
                 $('#cropy').val(c.y);
                 $('#cropw').val(c.w);
                 $('#croph').val(c.h);
                };
   
            }
        }
    }
});



function openTab(evt, TabName) {
    // Declare all variables
    var i, tabcontent, tablinks;
    //alert('Please Add Activity Information');
    // Get all elements with class="tabcontent" and hide them
    /*tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }*/

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById('Activity').style.display = "block";
    document.getElementById('Quick-info').style.display = "none";
    document.getElementById('Pricing').style.display = "none";
    document.getElementById('Gallery').style.display = "none";
    document.getElementById('Faq').style.display = "none";
    //evt.currentTarget.className += " active";
}



</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Activities
            <small>Add Activity</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activities</li>
        </ol>
    </section>
    <section class="content">
        <!-- Tab links -->
        <div class="tab">
          <button class="tablinks" onclick="openTab(event, 'Activity')" id="defaultOpen">Activity</button>
          <button class="tablinks" onclick="openTab(event, 'Quick-info')">Quick Info</button>
          <button class="tablinks" onclick="openTab(event, 'Pricing')">Pricing</button>
          <button class="tablinks" onclick="openTab(event, 'Gallery')">Gallery</button>
          <button class="tablinks" onclick="openTab(event, 'Faq')">Faq</button>
        </div>

        <!-- Tab content -->
        <div id="Activity" class="tabcontent">
              {!! Form::open(['url' => 'admin/activities/insert', 'name' => 'insertActivitiesForm' , 'class'=>'insertActivitiesForm', 'id'=>'insertActivitiesForm', 'files' => true]) !!}                                
                    
                
                
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">                                
                                    <div class="">
                                        
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>City:</label>
                                                    <select name="destinations_id" id="destinations_id" class="form-control">                                                   
                                                        @foreach($destinations as $destination)
                                                        <option value="{{ $destination['id'] }}">{{ $destination['title'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <!---->
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>For:</label>
                                                    <select name="categories_id" class="form-control">
                                                        <option value="1">AUTHENTIC EXPERIENCES</option>
                                                        <option value="2">POPULAR ACTIVITIES</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <input type="hidden" name="cropx" id="cropx" value="0" />
                                                <input type="hidden" name="cropy" id="cropy" value="0" />
                                                <input type="hidden" name="cropw" id="cropw" value="0" />
                                                <input type="hidden" name="croph" id="croph" value="0" />
                                                <div class="col-sm-4">&nbsp;</div>
                                                <div class="col-sm-4">
                                                    <strong>Activity Cover Photo</strong><br>
                                                    <!--<img src="adminpanel/dist/img/360.png" class="img-reponsive">-->
                                                    <hr />
                                                    <div id="image_prev_container">
                                                        <img id="preview" src="http://placehold.it/360x180" alt="your image" />
                                                    </div>
                                                    <div class="form-group margin">
                                                        <label for="exampleInputFile">File input</label>
                                                        <input type='file' id="activity-image" name="image" />

                                                        <p class="help-block">Attach Image.</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">&nbsp;</div>
                                            </div>    
                                            <div class="row">    
                                                <!--<div class="col-sm-6">
                                                    <img src="adminpanel/dist/img/360.png" class="img-reponsive">
                                                    <div class="form-group margin">
                                                        <label for="exampleInputFile">File input</label>
                                                        <input id="exampleInputFile" name="image" type="file">
    
                                                        <p class="help-block">Attach Image.</p>
                                                    </div>
                                                </div>-->

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Activity Title</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="title" placeholder="Title" type="text">
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <label for="exampleInputEmail1">Actual Price</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="amount" placeholder="Amount" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Final Price</label>
                                                        <input class="form-control" id="exampleInputEmail1" name="finalprice" placeholder="Final Amount" type="text">
                                                    </div>-->
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="addedbytype" id="addedbytype_admin" value="admin" checked="checked" type="radio">
                                                                    Admin
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="addedbytype" id="addedbytype_localguide" value="localguide" type="radio">
                                                                    Local Guide
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row hide" id="local_guide_block">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Local Guide:</label>
                                                                <select name="localguide_id" id="localguide_id" class="form-control" disabled="disabled">
                                                                    <option value="0">Select Local Guide</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div>    
        
             
 
                <input type="submit" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
              {!! Form::close() !!}
        </div>


    
    
    </section>
</div>    
@stop
@section('page_scripts')
<script src="js/admin/activities/add.js"></script>
@stop