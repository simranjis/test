@extends('admin.layouts.defaultsidebar')
@section('content')

<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Activities
            <small>Remove Activity</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activity</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/activities/delete', 'name' => 'deleteActivity' , 'class'=>'deleteActivity', 'id'=>'deleteActivity', 'files'=>'false']  ) !!}                                
                <input type="hidden" name="id" value="{{ $activity['id'] }}">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"><input type="submit" value="Delete" name="submit"></div>
                    <div class="col-md-3"><input type="button" value="Cancel" name="Cancel"></div>
                    <div class="col-md-3"></div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>  
</div>    
@stop