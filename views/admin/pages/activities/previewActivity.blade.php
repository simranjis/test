<style>
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .submenu li {
        color: #000;
        padding: 9px 19px;
    }

    .submenu li {
        border-bottom: 1px solid #35D5E7;
    }
    .li-heading {
        background: #35D5E7 none repeat scroll 0 0;
        color: #fff !important;
    }
    .submenu li:last-child {
        border-bottom: 0 none;
    }

    .submenu {
        display: none;
        background: #fff;
        font-size: 14px;
    }

    .list-unstyled {
        padding-left: 0;
        list-style: none;
    }
</style>
<?php if(!empty($activity->banner_image)){ ?>
    <img src="uploads/banner/<?php echo $activity->banner_image ?>" style="width: 875px; height: 380px;">
<?php }else{ ?>
    <img src="<?php echo URL::to('/'); ?>/images/bali.png" style="width: 875px; height: 380px;">
<?php } ?>
<div>
    <h3><?php echo $activity->title ?></h3>
</div>
<br>
<div class="">
    <div class="row">
        <?php if(!empty($activity->currency)){ ?>
        <div class="col-sm-6">
            <b>Currency:</b>&nbsp;&nbsp;
            <?php echo $activity->currency->title; ?>
        </div>
        <?php } ?>
        <div class="col-sm-6">
            <b>From:</b>&nbsp;&nbsp;
            <?php echo date('d F Y', strtotime($activity->activity_start_date)); ?>
            &nbsp;&nbsp;
            <b> - To - </b>&nbsp;&nbsp;
            <?php echo date('d F Y', strtotime($activity->activity_end_date)); ?>
        </div>
    </div>

    <?php if($activity->servicefee_status == 'yes'){ ?>
        <div class="row">
            <div class="col-sm-6">
                <b>Service Fee Type: </b> &nbsp;&nbsp;<?php echo ucwords($activity->servicefee_type); ?><br>
            </div>
            <div class="col-sm-6">
                <b>Service Fee: </b> &nbsp;&nbsp; <?php echo $activity->service_fee; ?>
            </div>
        </div>
    <?php } ?>

    <?php if(!empty($activity->activity_avialability) && (count($activity->activity_avialability)>0)){ ?>
    <div class="row">
        <div class="col-sm-6">
            <b>Availability: </b>&nbsp;&nbsp;
            <?php echo $activity->activity_notice_period; ?> Notice | 
            <?php 
                $activityAvailablity =  $activity->activity_avialability;
                $days = '';
                if($activityAvailablity->activity_avail_monday){
                    $days .= 'Mon';
                }
                if($activityAvailablity->activity_avail_tuesday){
                    $days .= ', Tue';
                }
                if($activityAvailablity->activity_avail_wednesday){
                    $days .= ', Wed';
                }
                if($activityAvailablity->activity_avail_thursday){
                    $days .= ', Thu';
                }
                if($activityAvailablity->activity_avail_friday){
                    $days .= ', Fri';
                }
                if($activityAvailablity->activity_avail_saturday){
                    $days .= ', Sat';
                }
                if($activityAvailablity->activity_avail_sunday){
                    $days .= ', Sun';
                }
                echo $days;
            ?>
        </div>
    </div>
    <?php } ?>
</div>
<?php if(!empty($activity->activitity_pricings) && (count($activity->activitity_pricings)>0)){ ?>
<hr>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 style="color:#000" class="margin-top-0">Prices</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <b>Type</b>
        </div>
        <div class="col-md-2">
            <b>User</b>
        </div>
        <div class="col-md-2">
            <b>Title</b>
        </div>
        <div class="col-md-2">
            <b>Original Price</b>
        </div>
        <div class="col-md-2">
            <b>Selling Price</b>
        </div>
    </div>
        <?php foreach ($activity->activitity_pricings as $key => $value) { 
            $type = $value->type;
            $pricefor = $value->pricefor;
            $title = $value->title;
            $original_price = $value->original_price;
            $price = $value->price;
            ?>
            <div class="row">
                <div class="col-md-2">
                    <?php echo ($type=='person')?'Person':'Package'; ?>
                </div>
                <div class="col-md-2">
                    <?php 
                        echo ucwords($pricefor);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php 
                        echo ucwords($pricefor);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                        echo number_format($original_price,2);
                    ?>
                </div>
                <div class="col-md-2">
                    <?php
                        echo $price;
                    ?>
                </div>
            </div>
        <?php } ?>
</div>
<?php } ?>
<hr>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 style="color:#000" class="margin-top-0">QUICK INFO</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-user-o"  aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Minimum {{ $activity->quickinfo1 }} Travelers To Book</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Confirmation: {{ $activity->quickinfo2 }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Duration: {{ $activity->quickinfo3 }} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Tour Type: {{ $activity->quickinfo4 }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Redemption: {{ $activity->quickinfo5 }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <?php if($activity->addedbytype === 'localguide'){ ?>
                        <div class="col-xs-3 col-sm-5 col-md-4">
                            <div class="marine_park-box">
                                
                                <div class="">
                                    <div class="text-center">
                                        <img src="images/castle-3.png" style="height:35px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-7 col-md-8">
                            <p class="margin-top-0">Local Guide Hosts</p>
                        </div>
                    <?php } ?>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <!--<p class="margin-top-0">{{ $activity['quickinfo6'] }}</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($activity->activitity_faqs) && (count($activity->activitity_faqs)>0)){ 
    $faqsArr = array();
    foreach ($activity->activitity_faqs as $key => $value) {
        $faqsArr[$value->faq_id] = $value->getAttributes();
    }
    ?>
    <br>
    <hr>
    <!-- FAQ accordion -->
    <div class="accordion-well-background">
        <div class="container">
            <div class="row">            
                <div class="col-sm-9">
                    <div class="accordion-well ">
                        <ul id="accordion" class="accordion list-unstyled">
                        <!-- Additional Info -->
                            <?php 
                            if(!empty($faqsArr[1])){
                                $additionalInfoArr = $faqsArr[1];
                                $title = $additionalInfoArr['title']; 
                                $description = $additionalInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-info-circle" aria-hidden="true"></i>{{ strtoupper($title) }}<i class="fa fa-chevron-down"></i></div>
                                    
                                    <?php
                                    $description = str_replace('<ul>', '<ul class="submenu list-unstyled">', $description);
                                    ?>

                                    {!! $description !!}                                    
                                
                            </li>
                            <?php } ?>
                        <!-- Additional Info - END -->


                        <!-- Inclusion / Exclusion Info -->
                            <?php 

                            if(!empty($faqsArr[2]) && !empty($faqsArr[7]) && ($faqsArr[2]['show']==1)){
                                $inclusionInfoArr = $faqsArr[2];
                                $inclusion_title = $inclusionInfoArr['title']; 
                                $inclusion_description = $inclusionInfoArr['description'];

                                $exclusionInfoArr = $faqsArr[7];
                                $exclusion_title = $exclusionInfoArr['title']; 
                                $exclusion_description = $exclusionInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-database" aria-hidden="true"></i>INCLUSIONS / EXCLUSIONS<i class="fa fa-chevron-down"></i></div>
                                    
                                    <?php
                                    $inclusion_exclusion_description = str_replace('<ul>', '<ul class="submenu list-unstyled">
                                        <li class="li-heading">
                                        '.$inclusion_title.'
                                        </li>', $inclusion_description);

                                    $inclusion_exclusion_description = str_replace('</ul>', '', $inclusion_exclusion_description);

                                    $inclusion_exclusion_description .= '<li class="li-heading">'.$exclusion_title.'</li>';

                                    $exclusion_description = str_replace('<ul>', '', $exclusion_description);
                                    
                                    $exclusion_description = str_replace('</ul>', '', $exclusion_description);

                                    $inclusion_exclusion_description .= $exclusion_description;
                                    $inclusion_exclusion_description .= '</ul>';
                                    ?>

                                    {!! $inclusion_exclusion_description !!}                                    
                                
                            </li>
                            <?php } ?>
                        <!-- Inclusion / Exclusion Info - END -->


                        <!-- Itinerary Info -->
                            <!-- Itinerary Info -->
                        <?php if(count($activity->activity_itineries) > 0){
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-check-square-o" aria-hidden="true"></i>ITINERARY<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu list-unstyled" style="display: none;">
                                @foreach($activity->activity_itineries as $itinery)
                                    <li> <div class="li-heading">{{$itinery->title}}
                                        </div>
                                        <div>{!! $itinery->description !!}</div>  
                                    </li>
                                    @endforeach
                                </ul>                                
                            </li>
                        <?php } ?>
                         <!-- Itinerary Info - END -->

                         <!-- How to use and How to get there -->
                            <?php 
                            if(!empty($faqsArr[4])){
                                $howToUseHowToGetInfoArr = $faqsArr[4];
                                $title = $howToUseHowToGetInfoArr['title']; 
                                $description = $howToUseHowToGetInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-binoculars" aria-hidden="true"></i>HOW TO USE AND HOW TO GET THERE  
                                <i class="fa fa-chevron-down"></i></div>
                                    
                                    <ul class="submenu list-unstyled">
                                        <li class="li-heading">HOW TO USE</li>
                                        <li>{{$title}}</li>
                                        <li class="li-heading">HOW TO GET THERE
                                        </li>
                                        <?php 
                                            $description = str_replace('<ul>', '', $description);
                                            $description = str_replace('</ul>', '', $description);
                                        ?>
                                        {!! $description !!}
                                    </ul>                            
                            </li>
                            <?php } ?>
                         <!-- How to use and How to get there - END -->

                         <!-- Cancellation -->
                            <?php 
                            if(!empty($faqsArr[5])){
                                $cancellationInfoArr = $faqsArr[5];
                                $title = $cancellationInfoArr['title']; 
                                $title = str_replace('{{FOLLOWING_DAYS}}', $cancellationInfoArr['days'], $title);
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-ban" aria-hidden="true"></i>CANCELLATION<i class="fa fa-chevron-down"></i></div>

                                <ul class="submenu list-unstyled">
                                    <li>{{$title}}</li>
                                </ul>
                            </li>
                            <?php } ?>
                         <!-- Cancellation - END -->


                         <!-- Payment Protection -->
                            <?php 
                            if(!empty($faqsArr[6])){
                                $paymentProtectionInfoArr = $faqsArr[6];
                                $title = $paymentProtectionInfoArr['title']; 
                                $description = $paymentProtectionInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-money" aria-hidden="true"></i>{{ strtoupper($title) }}<i class="fa fa-chevron-down"></i></div>

                                <ul class="submenu list-unstyled">
                                    <?php
                                    $description = str_replace('<ul>', '', $description);
                                    $description = str_replace('</ul>', '', $description);
                                    $description = str_replace('<p>', '<li class="li-heading">', $description);
                                    $description = str_replace('</p>', '</li>', $description);
                                    ?>

                                    {!! $description !!}    
                                </ul>                                
                            </li>
                            <?php } ?>
                         <!-- Payment Protection - END -->

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FAQ accordion - END -->
<?php } ?>
<?php if($activity->status==0){ ?>
<br><hr>
        <div class="text-center">
            <a href="javascript:void(0)" class="btn bg-green btn-flat margin" role="button" onclick="return activateActivity(<?php echo $activity->id; ?>)">
                <i class="fa fa-check"></i> Accept
            </a>
            <a href="javascript:void(0)" class="btn bg-maroon btn-flat margin" role="button" onclick="return declineActivity(<?php echo $activity->id; ?>)">
                <i class="fa fa-times"></i> Decline
            </a>
        </div>
    <?php } ?>
<script>
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
             $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            };
        }
        //$("#accordion").accordion({ active: false });
        var accordion = new Accordion($('#accordion'), false);
    });
</script>