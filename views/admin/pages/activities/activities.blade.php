@extends('admin.layouts.defaultsidebar')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Activities
            <small>My Activities</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activity</li>
        </ol>
    </section>

    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif    
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                            <h3> Enter an activity</h3>
                            <a href="{{ url('admin/activities/addActivity') }}" type="button" class="btn bg-maroon btn-flat margin">New Activity</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Keywords</label>
                            <input type="text" name="search" class="form-control" id="search" placeholder="Search">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Activity Type</label>
                            <select name="addedbytype" id="addedbytype" class="form-control">
                                <option selected="selected" value="">ALL</option>
                                <option value="admin">Admin</option>
                                <option value="localguide">Local Guide</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Continent</label>
                            <select id="region_id" name="region_id" class="form-control">
                                <option value="">All Continent</option>
                                <?php foreach ($regionsArr as $key => $value) { ?>
                                    <option value="{{$key}}">{{$value}}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Country</label>
                            <select id="countries_id" name="countries_id" class="form-control">
                                <option value="">All Countries</option>
                                <?php foreach ($countriesArr as $key => $value) { ?>
                                    <option value="{{$key}}">{{$value}}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>City</label>
                            <select id="destinations_id" name="destinations_id" class="form-control">
                                <option value="">All Destinations</option>
                                <?php foreach ($destinationsArr as $key => $value) { ?>
                                    <option value="{{$key}}">{{$value}}</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>&nbsp;</label>
                        <div class="form-group">
                            <a href="javascript:void(0)" id="search_button" onclick="return searchActivities()" type="button" class="btn bg-maroon btn-flat">Search</a>
                        </div>
                    </div>
                </div>
                @include('admin.pages.activities.partials.activities_grid')
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('admin.pages.activities.partials.viewActivity')
@stop
@section('page_scripts')
<script src="js/admin/activities/activities.js"></script>
@stop