<div class="col-md-12">
    <div class="box box-primary">
        <div  class="">
            <div  class="row">
                <div class="col-xs-12"> 
        
                    <div class="box-header with-border box-header-color">
                        <h3 class="box-title sbold">Add New Promotion</h3>
                    </div>
                    <div class="box-body">
                        <!-- all local guides -->
                        <div class="col-sm-6">
                            

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title', 'Title', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $title = '';
                                            if(!empty($promotions->title)){
                                                $title = $promotions->title;
                                            }
                                        ?>
                                        
                                        {{ Form::text('title', $title, array('class' => 'form-control form-control-custom', 'id'=>'title', 'placeholder'=>'Title')) }}

                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('promocode', 'Promo Code', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $promocode = '';
                                            if(!empty($promotions->promocode)){
                                                $promocode = $promotions->promocode;
                                            }
                                        ?>

                                        {{ Form::text('promocode', $promocode, array('class' => 'form-control form-control-custom', 'id'=>'promocode', 'placeholder'=>'Promo Code')) }}
                                    </div>
                                </div>
                            </div>
                            <!-- Start Date -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                        $startdate = '';
                                        if(!empty($promotions->startdate)){
                                            $startdate = date('m/d/Y',strtotime($promotions->startdate));
                                        }
                                        ?>
                                        {{ Form::label('startdate', 'Start Date', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        {{ Form::text('startdate', $startdate, array('class' => 'form-control form-control-custom pickerdate', 'id'=>'startdate', 'placeholder'=>'Start Date')) }}
                                    </div>
                                </div>
                            </div>

                            <!-- End Date -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">

                                        <?php 
                                        $enddate = '';
                                        if(!empty($promotions->enddate)){
                                            $enddate = date('m/d/Y',strtotime($promotions->enddate));
                                        }
                                        ?>

                                        {{ Form::label('enddate', 'End Date', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        {{ Form::text('enddate', $enddate, array('class' => 'form-control form-control-custom pickerdate', 'id'=>'enddate', 'placeholder'=>'End Date')) }}
                                    </div>
                                </div>
                            </div>

                            <!-- Promotion Type -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                    <?php
                                        $promoTypesArr = array(
                                                            'public'=>'Public',
                                                            'destination'=>'Destination',
                                                            'user'=>'By User'
                                                        );
                                    ?>

                                        {{ Form::label('type', 'Where dows this promotion apply to ?', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $type = '';
                                            if(!empty($promotions->type)){
                                                $type = $promotions->type;
                                            }
                                        ?>

                                        {{ Form::select('type', $promoTypesArr,  $type, array('class'=>'form-control', 'id'=>'type', 'onchange'=>'toggleType(this);')) }}

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                        $quantityTypesArr = [
                                                            'total' => 'Total',
                                                            'day' => 'Per Day'
                                                        ];
                                        ?>
                                        {{ Form::label('qty_type', 'Quantity Type', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $qty_type = '';
                                            if(!empty($promotions->qty_type)){
                                                $qty_type = $promotions->qty_type;
                                            }
                                        ?>

                                        {{ Form::select('qty_type', $quantityTypesArr, $qty_type, array('class'=>'form-control', 'id'=>'qty_type')) }}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('qty', 'Quantity', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $qty = '';
                                            if(!empty($promotions->qty)){
                                                $qty = $promotions->qty;
                                            }
                                        ?>

                                        {{ Form::text('qty', $qty, array('class' => 'form-control form-control-custom', 'id'=>'qty', 'placeholder'=>'Quantity')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                        $discountTypesArr = [
                                                            '0' => 'Fixed',
                                                            '1' => 'Percent'
                                                        ];
                                        ?>
                                        {{ Form::label('discount_type', 'Discount Type', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $discount_type = '';
                                            if(!empty($promotions->discount_type)){
                                                $discount_type = $promotions->discount_type;
                                            }
                                        ?>

                                        {{ Form::select('discount_type', $discountTypesArr, $discount_type, array('class'=>'form-control', 'id'=>'discount_type')) }}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('discount', 'Discount', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        <?php 
                                            $discount = '';
                                            if(!empty($promotions->discount)){
                                                $discount = $promotions->discount;
                                            }
                                        ?>

                                        {{ Form::text('discount', $discount, array('class' => 'form-control form-control-custom', 'id'=>'discount', 'placeholder'=>'Discount')) }}
                                    </div>
                                </div>
                            </div>

                            <?php

                            $destination_hide_class = $traveller_hide_class = 'hide';
                            $selectedDestinationsArr = [];
                            $selectedTravellersArr = [];
                            if(!empty($promotions->type) && ($promotions->type == 'destination')){
                                $destination_hide_class = '';

                                foreach ($promotions->promo_relations as $key => $promo_relation) {
                                    $selectedDestinationsArr[] = $promo_relation->destination_id;
                                }

                            }

                            if(!empty($promotions->type) && ($promotions->type == 'user')){
                                $traveller_hide_class = '';

                                foreach ($promotions->promo_relations as $key => $promo_relation) {
                                    $selectedTravellersArr[] = $promo_relation->traveller_id;
                                }
                            }

                            ?>

                            <div class="row destination {{$destination_hide_class}}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('destination_id', 'Select Destination', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>
                                        <br>
                                        {{ Form::select('destination_id[]', $destinationsListArr,  $selectedDestinationsArr, array('class'=>'form-control select2', 'id'=>'destination_id', 'multiple'=>true)) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row traveller {{$traveller_hide_class}}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('traveller_id', 'Select Traveller', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>
                                        <br>
                                        {{ Form::select('traveller_id[]', $travellersListArr,  $selectedTravellersArr, array('class'=>'form-control select2', 'id'=>'traveller_id', 'multiple'=>true)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form::button('Save', array('id'=>'savePromotion', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return validatePromoForm()')) }}

                                <a href="{{ url('admin/promotions') }}" class="btn bg-gray btn-flat margin">
                                     Cancel
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>