@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Promotion
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Promotion</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            {!! Form::open(['url' => 'admin/promotions/insert', 'name' => 'promotionsInsertForm' , 'class'=>'promotionsInsertForm', 'id'=>'promotionsInsertForm', 'files' => true]) !!}  
           <!-- all local guides -->
            @include('admin.pages.promotions.partials.promotion_form')

            {!! Form::close() !!}
        </div>
    </section>
</div>

@stop

@section('page_scripts')
<script src="js/admin/promotions/add.js"></script>
@stop