@extends('admin.layouts.defaultsidebar')
@section('content')
<script type="text/javascript">
$(function(){
    $( ".pickerdate" ).datepicker();
})
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit {{ $promotions->id }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit {{ $promotions->id }}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">

           {!! Form::open(['url' => 'admin/promotions/update', 'name' => 'promotionsUpdateForm' , 'class'=>'promotionsUpdateForm', 'id'=>'promotionsUpdateForm', 'files' => true]) !!}                                
            <input type="hidden" name="id" value="{{ $promotions->id }}">
            <!-- all local guides -->
            @include('admin.pages.promotions.partials.promotion_form')

            {!! Form::close() !!}

        </div>
    </section>
</div>

@stop

@section('page_scripts')
<script src="js/admin/promotions/edit.js"></script>
@stop