@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Delete {{ $promotions->promocode }}
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit {{ $promotions->id }}</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
           {!! Form::open(['url' => 'admin/promotions/delete', 'name' => 'promotionsDeleteForm' , 'class'=>'promotionsDeleteForm', 'id'=>'promotionsDeleteForm', 'files' => true]) !!}                                
           <input type="hidden" name="id" value="{{ $promotions->id }}">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div  class="">
                            <div  class="row">
                                <div class="col-xs-12"> 
                        
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Delete {{ $promotions->promocode }}</h3>
                                    </div>
                                    
                                    <input type="submit" name="delete" class="btn bg-maroon btn-flat margin" value="Delete">
                                    <a href="{{ url('admin/promotions') }}"><input type="button" class="btn bg-maroon btn-flat margin" name="cancel" value="Cancel"></a>
                                    
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
</div>





@stop

