@extends('admin.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Admin Users
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Admin Users</li>
        </ol>
    </section>
    
    <!-- All Admin Users Grid -->
    @include('admin.pages.user.partials.all_admins_grid')
            
</div>    
@stop
@section('page_scripts')
<script src="js/admin/user/administrators.js"></script>
@stop