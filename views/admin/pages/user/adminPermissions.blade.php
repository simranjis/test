@extends('admin.layouts.defaultsidebar')
@section('content')

<script src="//code.jquery.com/jquery.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Admin Permissions 
            <small>{{$admin->firstname}} {{$admin->lastname}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Admin Permissions</li>
        </ol>
    </section>
    {!! Form::open(['url' => 'admin/user/adminPermissions/'.$admin->id, 'name' => 'insertActivitiesForm' , 'class'=>'insertActivitiesForm', 'id'=>'insertActivitiesForm', 'files' => true]) !!} 
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <!-- admin permissions -->                
                <?php
                foreach ($modulesDataObj as $moduleObj) {
                    $module_checked = '';
                    $parentModuleClass = 'parentModuleClass';

                    if ((count($moduleObj->submodules) == 0) && !empty($userPermissionDataObj[$moduleObj->id][0])) {
                        $module_checked = 'checked';
                    }
                    if (count($moduleObj->submodules) == 0) {
                        $parentModuleClass = '';
                    }
                    ?>
                <hr>
                    <div class="row">
                        <div class="col-sm-12 content-header">

                            <input type="checkbox" module_index="<?php echo $moduleObj->id ?>" onclick="checkeAllSubmodules(<?php echo $moduleObj->id ?>);" id="module_<?php echo $moduleObj->id ?>" name="moduleArr[<?php echo $moduleObj->id ?>]" <?php echo $module_checked; ?>  class="<?php echo $parentModuleClass; ?>" > 

                            <label for="module_<?php echo $moduleObj->id ?>">{{ $moduleObj->module_title }}</label>

                        </div>
                    </div>
                    <?php
                    if (!empty($moduleObj->submodules)) {
                        $submodulesObj = $moduleObj->submodules;
                        ?>
                        <div class="row">
                            <?php
                            foreach ($submodulesObj as $submoduleObj) {

                                $sub_module_checked = '';
                                if (!empty($userPermissionDataObj[$moduleObj->id][$submoduleObj->id])) {
                                    $sub_module_checked = 'checked';
                                }
                                ?>
                                <div class="col-sm-3">

                                    <input type="checkbox" onclick="checkModule(<?php echo $moduleObj->id ?>);" class="module_<?php echo $moduleObj->id ?>" id="submodule_<?php echo $submoduleObj->id ?>" name="moduleArr[<?php echo $moduleObj->id ?>][<?php echo $submoduleObj->id ?>]" <?php echo $sub_module_checked; ?> > 

                                    <span for="submodule_<?php echo $submoduleObj->id ?>">{{ $submoduleObj->sub_module_title }}</span>
                                </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                    <br>
<?php } ?>
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn bg-maroon btn-flat collapsed">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}
</div>

@stop

@section('page_scripts')
<script src="js/admin/user/adminPermissions.js"></script>
@stop