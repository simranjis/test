@extends('admin.layouts.defaultsidebar')
@section('content')


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Traveler
            <small>Edit Traveler</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activities</li>
        </ol>
    </section>
    
    
    
    <section class="content">
        <div class="row">

        {!! Form::open(['url' => 'admin/user/traveller/insert', 'name' => 'insertTravelerForm' , 'class'=>'insertTravelerForm', 'id'=>'insertTravelerForm', 'files' => true]) !!}                                
            
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="">
                                    
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Traveler Information</h3>
                                        <!--<p class=""> Date 7 Jan 2018</p>-->
                                    </div>                                    
                                    
                                    <div class="box-body">
                                        <!--<div class="form-group">
                                            
                                                <img class="img-responsive" src="http://via.placeholder.com/250x250" alt="User profile picture" style="height: 160px">
                                                <input  type="hidden" name="previous_image" value="">        
                                        </div>-->
                                    
                                    
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Title:</label>
                                            <select class="form-control" name="title">
                                                <option value="0">Mr</option>
                                                <option value="1">Mrs</option>
                                                <option value="2">Miss</option>
                                            </select>
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Firstname:</label>
                                            <input class="form-control" type="text" name="firstname" value="">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Lastname:</label>
                                            <input class="form-control" type="text" name="lastname" value="">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            <label>Mobile Number:</label>
                                            <input class="form-control" type="text" name="mobilenumber" id="mobilenumber" value="">        
                                        </div>
                                        <input type="hidden" name="country_code" id="country_code">
                                    
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Passport:</label>
                                            <input class="form-control" type="text" name="passport" value="">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Email:</label>
                                            <input class="form-control" type="text" name="email" value="">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Country:</label>
                                            <select class="form-control" name="countries_id">
                                                @foreach ($countries as $countries_id => $countries_name)
                                                    <option value="{{ $countries_id }}">{{ $countries_name }}</option>
                                                @endforeach
                                            </select>        
                                        </div>
                                    


                                    </div>
                                    </div>
                                    <a href="{{ url('admin/user/traveller') }}"><input type="button" name="Submit" value="Cancel" class="btn bg-maroon btn-flat margin"></a>
                                    <input type="submit" name="Submit" value="Add" class="btn bg-maroon btn-flat margin">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
        {!! Form::close() !!}
        </div>
    </section>
    
</div>
@stop
@section('page_scripts')
<script src="js/admin/user/traveller/add.js"></script>
@stop