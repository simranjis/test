@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Traveler
            <small>Edit Traveler</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activities</li>
        </ol>
    </section>
    
    
    
    <section class="content">
        <div class="row">

        {!! Form::open(['url' => 'admin/user/traveller/update', 'name' => 'updateTravelerForm' , 'class'=>'updateTravelerForm', 'id'=>'updateTravelerForm', 'files' => true]) !!}                                
            <input type="hidden" name="id" value="{{ $traveller['id'] }}">
            
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="">
                                    
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Traveler Information</h3>
                                        <!--<p class=""> Date 7 Jan 2018</p>-->
                                    </div>                                    
                                    
                                    <div class="box-body">
                                        <div class="form-group">
                                            @if($traveller['image'])
                                                <img src="uploads/{{ $traveller['image'] }}" alt="{{ $traveller['firstname'] }}" style="width:160px;" >
                                            @else
                                                <img class="img-responsive" src="http://via.placeholder.com/250x250" alt="User profile picture" style="height: 160px">
                                            @endif
                                            <input  type="hidden" name="previous_image" value="{{ $traveller['image'] }}">        
                                        </div>
                                    
                                    
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Title:</label>
                                            <select class="form-control" name="title">
                                                <option @if($traveller['title'] == '0') selected="selected" @endif value="0">Mr</option>
                                                <option @if($traveller['title'] == '1') selected="selected" @endif value="1">Mrs</option>
                                                <option @if($traveller['title'] == '2') selected="selected" @endif value="2">Miss</option>
                                            </select>
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Firstname:</label>
                                            <input class="form-control" type="text" name="firstname" value="{{ $traveller['firstname'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Lastname:</label>
                                            <input class="form-control" type="text" name="lastname" value="{{ $traveller['lastname'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            <label>Mobile Number:</label>
                                            <input class="form-control" type="text" name="mobilenumber" id="mobilenumber" value="{{ $traveller['mobilenumber'] }}">        
                                        </div>
                                        <input type="hidden" name="country_code" id="country_code" value="{{ $traveller['country_code'] }}">
                                    
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Passport:</label>
                                            <input class="form-control" type="text" name="passport" value="{{ $traveller['passport'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Email:</label>
                                            <input class="form-control" type="text" name="email" value="{{ $traveller['email'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Country:</label>
                                            <select class="form-control" name="countries_id">
                                                @foreach ($countries as $countries_id => $countries_name)
                                                    @if($countries_id == $traveller['countries_id'])
                                                        <option selected='selected' value="{{ $countries_id }}">{{ $countries_name }}</option>
                                                    @else
                                                        <option value="{{ $countries_id }}">{{ $countries_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>        
                                        </div>
                                    


                                    </div>
                                    </div>
                                    <a href="{{ url('admin/user/traveller') }}"><input type="button" name="Submit" value="Cancel" class="btn bg-maroon btn-flat margin"></a>
                                    <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
        {!! Form::close() !!}
        </div>
    </section>
    
</div>
@stop
@section('page_scripts')
<script src="js/admin/user/traveller/edit.js"></script>
@stop