{!! Form::open(['url' => 'admin/user/addAdmin', 'name' => 'saveAdminForm' , 'class'=>'saveAdminForm', 'id'=>'saveAdminForm']) !!} 
<div class="row">
    <div class="col-sm-12">
    	<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('firstname', 'First Name', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('firstname', $admin->firstname, array('class' => 'form-control form-control-custom', 'id'=>'firstname', 'placeholder'=>'First Name')) }}
            </div>                                      
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('lastname', 'Last Name', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('lastname', $admin->lastname, array('class' => 'form-control form-control-custom', 'id'=>'lastname', 'placeholder'=>'Last Name')) }}
            </div>                                      
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('email', 'Email', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('email', $admin->email, array('class' => 'form-control form-control-custom', 'id'=>'email', 'placeholder'=>'Email')) }}
            </div>                                      
        </div>
        <?php  
        $password_fields_hide = '';
        if(!empty($admin->id)){
            $password_fields_hide = 'hide';
        }
        ?>
        <div class="row password_fields {{$password_fields_hide}}">
        <br>
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('password', 'Password', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>


                {{ Form::password('password', array('class' => 'form-control form-control-custom', 'id'=>'password', 'placeholder'=>'Password')) }}

            </div>                                      
        </div>
        <div class="row password_fields {{$password_fields_hide}}">
        <br>
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('confirm_password', 'Confirm Password', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>
                
                {{ Form::password('confirm_password', array('class' => 'form-control form-control-custom', 'id'=>'confirm_password', 'placeholder'=>'Confirm Password')) }}
            </div>                                      
        </div>
        <?php if(!empty($admin->id)){ ?>
            <br>
            <div class="text-right">
                <a href="javascript:void(0)" id="showPasswordLink" onclick="showPasswordFields()">Change Password</a>
                <a href="javascript:void(0)" id="hidePasswordLink" class="hide" onclick="hidePasswordFields()">Hide Password</a>
            </div>
        <?php } ?>

        <div class="row">
            &nbsp;
            {{ Form::button('Save', array('id'=>'saveAdminUser', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveAdmin()')) }}

            {{ Form::hidden('id', $admin->id, array('id'=>'id')) }}

            <a href="{{ url('admin/user/admin') }}" class="btn bg-gray btn-flat collapsed" role="button">
            	Cancel
            </a>
        </div>
    </div>
</div>
{!! Form::close() !!} 