<!-- /.tab-pane -->
<div class="tab-pane" id="tab_2">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold">LocalGuide</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                   <table id="manageAllNewLocalGuides" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Guide</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Call To Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($newLocalGuideObj)>0){ ?>
                                                <?php foreach ($newLocalGuideObj as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-success">{{$value->fullname}}</span>
                                                        </td>
                                                        <td>
                                                            {{$value->email}}
                                                        </td>
                                                        <td>
                                                            {{$value->mobilenumber}}
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-success bg-success btn-flat margin collapsed" onclick="displayNewLocalguideDetails({{ $value->id }})" role="button" data-toggle="modal" data-target="#myModal" >
                                                                <i class="fa fa-eye" aria-hidden="true"></i> View
                                                            </a>
                                                            <a class="btn bg-purple btn-flat margin collapsed" onclick="openActiveLocalPopup(<?php echo $value->id ?>)" role="button" data-toggle="modal" data-target="#modal-default-4">
                                                                <i class="fa fa-check" aria-hidden="true"></i> Accept
                                                            </a>
                                                            <a class="btn bg-maroon btn-flat margin collapsed" role="button" onclick="deleteNewLocalGuide(<?php echo $value->id ?>)">
                                                                <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan=4 style="text-align:center">No Records found</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>