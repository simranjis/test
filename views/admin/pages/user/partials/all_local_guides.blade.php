<div class="tab-pane active active_tab" id="tab_1">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold">LocalGuide</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="manageAllActiveLocalGuides" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Guide</th>
                                                <!--<th>Activities</th>-->
                                                <th>% Commission Rate</th>
                                                <th>Call To Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($localGuideResultObj)>0){ ?>
                                                <?php foreach ($localGuideResultObj as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <span class="label label-success">{{$value->fullname}}</span>
                                                        </td>
                                                        <!--<td>
                                                            Disney land
                                                        </td>-->
                                                        <td>
                                                            {{$value->commision}}
                                                        </td>
                                                        <!--<td>
                                                            <a class="btn bg-olive btn-flat margin collapsed" role="button" role="button" data-toggle="modal" data-target="#modal-default-1">
                                                                <i class="fa fa-plus" aria-hidden="true"></i>  Add Activity 
                                                            </a>
                                                        </td>-->
                                                        <td>
                                                            <a class="btn bg-purple btn-flat margin collapsed" onclick="displayLocalguideDetails({{ $value->id }})" role="button" data-toggle="modal" data-target="#myModal" >
                                                                <i class="fa fa-eye" aria-hidden="true"></i> View
                                                            </a>
                                                            <a href="{{ url('admin/user/localguide/edit') }}/{{ $value->id }}" class="btn bg-navy btn-flat margin collapsed" role="button">
                                                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                                            </a>
                                                            <a class="btn bg-maroon btn-flat margin collapsed delete-pop" role="button" href="{{ url('admin/user/localguide/remove') }}/{{ $value->id }}" >
                                                                <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                                            </a>
                                                            @if($value->status == 1)
                                                            <a class="btn bg-green btn-flat margin collapsed delete-pop2" title="Lock" role="button" href="{{ url('admin/user/localguide/lock') }}/{{ $value->id }}" >
                                                                <i class="fa fa-unlock" aria-hidden="true"></i> 
                                                            </a>
                                                            @else
                                                            <a class="btn bg-red btn-flat margin collapsed delete-pop3" title="Unlock" role="button" href="{{ url('admin/user/localguide/unlock') }}/{{ $value->id }}" >
                                                                <i class="fa fa-lock" aria-hidden="true"></i> 
                                                            </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan=5 style="text-align:center">No Records found</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>