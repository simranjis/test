 <section class="content">
    <div class="text-right">
        <a href="{{ url('admin/user/addAdmin/') }}" class="btn bg-maroon btn-flat collapsed" role="button">
        <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Create Admin
        </a>
    </div>
    <br>
    <div class="box box-primary">
        <div class="box-header with-border box-header-color">
            <h3 class="box-title sbold">Admin Users</h3>
        </div>
        <div class="box-body">
             <table id="manageAllAdminUsers" class="table table-hover table-bordered"></table>
        </div>
    </div>
</section>