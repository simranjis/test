<div class="modal fade" id="modal-default-4">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">% Commission Rate</h4>
                </div>
                <form role="form" id="activeLocalGuide">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">                                        
                                    <div class="col-sm-12">
                                        <label for="inputHelpBlock" class="label-font">Add Commission Rate</label>
                                        <input class="form-control" name="commision" id="commision">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <a class="btn bg-olive btn-flat margin collapsed" onclick="activeLocalGuide()" role="button" >
                            <i class="fa fa-plus" aria-hidden="true"></i> Add
                        </a>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>