@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Travelers
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Travelers</li>
        </ol>
    </section>
    <section class="content">
    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif    
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                            <!--<h3>You haven’t Enteres any Destination yet.</h3>-->
                            <a href="{{ url('admin/user/traveller/add') }}" type="button" class="btn bg-maroon btn-flat margin">New Traveler</a>
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Travelers</h3>
                                        <p class=""></p>
                                    </div>
                                    <!-- /.box-header -->
                                    @if($travellerResultObj)
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Photo</th>
                                                    <th>Firstname</th>
                                                    <th>Lastname</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                @foreach($travellerResultObj as $traveller)
                                                <tr>
                                                    <td>
                                                        @if($traveller['image'])
                                                            <img class="img-responsive" src="uploads/{{ $traveller['image'] }}" alt="User profile picture" style="height: 80px"></td>
                                                        @else
                                                            <img class="img-responsive" src="http://via.placeholder.com/250x250" alt="User profile picture" style="height: 80px"></td>
                                                        @endif
                                                    <td>{{ $traveller['firstname'] }}</td>
                                                    <td>{{ $traveller['lastname'] }}</td>
                                                    <td>{{ $traveller['email'] }}</td> 
                                                    <td>{{ $status[$traveller['status']] }}</td>
                                                    <td>
                                                        <a class="btn bg-maroon btn-flat margin" href="{{ 'admin/user/traveller/edit/' }}{{ $traveller['id'] }}" role="button">
                                                            Edit
                                                        </a>
                                                        <a class="btn bg-navy btn-flat margin" href="{{ 'admin/user/traveller/remove/' }}{{ $traveller['id'] }}" role="button">
                                                            Delete
                                                        </a>
                                                        @if($traveller['status'] == 0)
                                                        <a class="btn bg-black btn-flat margin" href="{{ 'admin/user/traveller/block/' }}{{ $traveller['id'] }}" role="button">
                                                            Block
                                                        </a>
                                                        @else
                                                        <a class="btn bg-green btn-flat margin" href="{{ 'admin/user/traveller/activate/' }}{{ $traveller['id'] }}" role="button">
                                                            Activate
                                                        </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                    @else
                                        <h3 style="padding: 5px;">You Don't have any Enteries on Destination yet.</h3>
                                    @endif
                                </div>
                                <!-- /.box -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- /.tab-content -->
    </section>
</div>    
@stop
@section('page_scripts')
<script src="js/admin/user/traveller.js"></script>
@stop