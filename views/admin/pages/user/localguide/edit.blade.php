@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
    .ovrflw-hidden {
        margin: 6px 0;
    }


    /****/

    img{
    }

    /*input[type=file]{
        padding:10px;
        background:#2d2d2d;
    } */   


    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons that are used to open the tab content */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style thepreview tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background: #FFFFFF;
    }    
    
   
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Localguide
            <small>Edit Localguide</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    
    
    
    <section class="content">
        <div class="row">

        {!! Form::open(['url' => 'admin/user/localguide/update', 'name' => 'updateLocalguideForm' , 'class'=>'updateLocalguideForm', 'id'=>'updateLocalguideForm', 'files' => true]) !!}                                
            <input type="hidden" name="id" value="{{ $localguide['id'] }}">
            
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="">
                                    
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">LocalGuide Information</h3>
                                        <!--<p class=""> Date 7 Jan 2018</p>-->
                                    </div>                                    
                                    
                                    <div class="box-body">
                                        <div class="form-group">
                                            @if($localguide['profile_image'])
                                                <img src="uploads/{{ $localguide['profile_image'] }}" alt="{{ $localguide['profile_image'] }}" style="width:160px;" >
                                            @else
                                                <img class="img-responsive" src="http://via.placeholder.com/250x250" alt="User profile picture" style="height: 160px">
                                            @endif
                                            <input  type="hidden" name="previous_image" value="{{ $localguide['profile_image'] }}">        
                                        </div>
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Fullname:</label>
                                            <input class="form-control" type="text" name="fullname" value="{{ $localguide['fullname'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Email:</label>
                                            <input class="form-control" type="text" name="email" value="{{ $localguide['email'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Mobile Number:</label>
                                            <input class="form-control" type="text" name="mobilenumber" id="mobilenumber" value="{{ $localguide['mobilenumber'] }}">        
                                        </div>
                                        <input type="hidden" name="country_code" id="country_code" value="{{ $localguide['country_code'] }}">
                                    
                                    
                                    
                                        <div class="form-group">
                                            
                                            <label>Business Name:</label>
                                            <input class="form-control" type="text" name="business_name" value="{{ $localguide['business_name'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Business Address:</label>
                                            <input class="form-control" type="text" name="business_address" value="{{ $localguide['business_address'] }}">        
                                        </div>
                                    

                                    
                                        <div class="form-group">
                                            
                                            <label>Business Nature:</label>
                                            <input class="form-control" type="text" name="business_nature" value="{{ $localguide['business_nature'] }}">        
                                        </div>
                                        
                                        <div class="form-group">
                                            
                                            <label>Designation:</label>
                                            <input class="form-control" type="text" name="designation" value="{{ $localguide['designation'] }}">        
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Preferred Mobile Contact:</label>
                                            <input class="form-control" type="text" name="preferred_mobile_contact" id="preferred_mobile_contact" value="{{ $localguide['preferred_mobile_contact'] }}">        
                                        </div>
                                        <input type="hidden" name="preferred_mobile_contact_country_code" id="preferred_mobile_contact_country_code" value="{{ $localguide['preferred_mobile_contact_country_code'] }}">
                                        
                                    
                                        <div class="form-group">
                                            <label>Country:</label>
                                            <select class="form-control" name="country_id">
                                                @foreach ($countries as $countries_id => $countries_name)
                                                    @if($countries_id == $localguide['country_id'])
                                                        <option selected='selected' value="{{ $countries_id }}">{{ $countries_name }}</option>
                                                    @else
                                                        <option value="{{ $countries_id }}">{{ $countries_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>        
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>City:</label>
                                            <input class="form-control" type="text" name="city" value="{{ $localguide['city'] }}">        
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>About:</label>
                                            <input class="form-control" type="text" name="about" value="{{ $localguide['about'] }}">        
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Website Link:</label>
                                            <input class="form-control" type="text" name="websitelink" value="{{ $localguide['websitelink'] }}">        
                                        </div>

                                        <div class="form-group">
                                            <label>Currency:</label>
                                            <select class="form-control" name="currency_id">
                                                @foreach ($currencies as $id => $code)
                                                    @if($id == $localguide['country_id'])
                                                        <option selected='selected' value="{{ $id }}">{{ $code }}</option>
                                                    @else
                                                        <option value="{{ $id }}">{{ $code }}</option>
                                                    @endif
                                                @endforeach
                                            </select>        
                                        </div>

                                        
                                        <div class="form-group">
                                            <label>Commision:</label>
                                            <input class="form-control" type="text" name="commision" value="{{ $localguide['commision'] }}">        
                                        </div>
                                        
                                    


                                    </div>
                                    </div>
                                    <a href="{{ url('admin/user/localguide') }}"><input type="button" name="Submit" value="Cancel" class="btn bg-maroon btn-flat margin"></a>
                                    <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
        {!! Form::close() !!}
        </div>
    </section>
    
</div>
@stop

@section('page_scripts')
<script src="js/admin/user/localguide/edit.js"></script>
@stop