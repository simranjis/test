@extends('admin.layouts.defaultsidebar')
@section('content')
<script type="text/javascript">
    
$(function() {
      
    $(".delete-pop").click(function(e) {
       var url = $(this).attr('href');
       bootbox.confirm("Confirm! Delete Selected Localguide", function(result){ 
           if(result) { 
              window.location = url;
           }
       });
       e.preventDefault();
    });
    $(".delete-pop2").click(function(e) {
       var url = $(this).attr('href');
       bootbox.confirm("Confirm! Lock Selected Localguide", function(result){ 
           if(result) { 
              window.location = url;
           }
       });
       e.preventDefault();
    });
    
    $(".delete-pop3").click(function(e) {
       var url = $(this).attr('href');
       bootbox.confirm("Confirm! Unlock Selected Localguide", function(result){ 
           if(result) { 
              window.location = url;
           }
       });
       e.preventDefault();
    });
    
  });
    
</script>    
<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }



</style>
<!-- jQuery 2.2.3  -->
<script src="adminpanel/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Local Guide
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Local Guide</li>
        </ol>
    </section>  
    @if(Request::get('message'))
        <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif    

    <section class="content">
       
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <!--<i class="fa fa-flag-checkered" aria-hidden="true"></i>-->
                            <!--<h3>You haven’t Enteres any Destination yet.</h3>-->
                            <a href="{{ url('admin/user/localguide/add') }}" type="button" class="btn bg-maroon btn-flat margin">New Localguide</a>
                        </div>
                    </div>
                </div>
        
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">All LocalGuide</a></li>
                <li><a href="#tab_2" data-toggle="tab">New Applicant</a></li>
            </ul>
            <div class="tab-content">
                <!-- all local guides -->
                @include('admin.pages.user.partials.all_local_guides')

                <!-- new local guides -->
                @include('admin.pages.user.partials.new_local_guides')

                <!-- all local guides -->
                @include('admin.pages.user.partials.commision_form_popup')
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </section>
    @include('admin.pages.user.localguide.partials.modal')
</div>    
@stop


@section('page_scripts')
<script src="js/admin/user/localguide/default.js"></script>
@stop