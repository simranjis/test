@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .margin-top-58{
        margin-top: 58px !important;
    }
    .input-group-addon{
        cursor: pointer !important;
    }
    .portlet{
        background-color: #e9004c !important
    }
    .margin-top-30{
        margin-top:30px;    
    }
    .margin-top-26{
        margin-top:26px;    
    }
    .reports .portlet > .portlet-title > .caption {
        font-weight: 400;
        color: #fff !important;
        font-size: 18px;
    }
    .portlet-title, .Make-business-decisi, .uppercase_heading, .reports .widget-thumb-body {
        border-bottom: 0;
        padding: 10px 14px;
    }
    .portlet.box > .portlet-body {
        background-color: #fff;
        padding: 15px;
    }
    .reports .static-info {
        margin-bottom: 20px;
    }
    .reports .static-info .name, .form-box .value {

        font-weight: 600;
    }
    .reports .static-info .total, .form-box .name {

        font-weight: 600;
    }
    .reports .form-filter {

        padding: 0;
        font-size: 16px;
        font-weight: 700;
        border-color: transparent;
        background: #fff;
        border: none;
    }
    .animationload {
/*        background-color: #fff;*/
        height: 100%;
        left: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 10000;
    }

    .osahanloading {
        animation: 1.5s linear 0s normal none infinite running osahanloading;
        background: #7dd0dd none repeat scroll 0 0;
        border-radius: 50px;
        height: 50px;
        left: 50%;
        margin-left: -25px;
        margin-top: -25px;
        position: absolute;
        top: 50%;
        width: 50px;
    }
    .osahanloading::after {
        animation: 1.5s linear 0s normal none infinite running osahanloading_after;
        border-color: #E9004E transparent;
        border-radius: 80px;
        border-style: solid;
        border-width: 10px;
        content: "";
        height: 80px;
        left: -15px;
        position: absolute;
        top: -15px;
        width: 80px;
    }
    @keyframes osahanloading {
        0% {
            transform: rotate(0deg);
        }
        50% {
            background: #FF6 none repeat scroll 0 0;
            transform: rotate(180deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Financial Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Financial Report</li>
        </ol>
    </section>    

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="reports">
                    <div class="container">
                        <div class="row">
                            {{csrf_field()}}
                            <div class="reports">
                                <div class="col-xs-12 col-sm-6 col-md-6 margin-top-30">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select id="filter_duration" class="form-control form-filter input-sm" onchange="filterReport(1)">
                                                <option value="0" selected="selected">Select</option>
                                                <option value="1">Daily</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Monthly</option> 
                                                <option value="4">Yearly</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="portlet blue-hoki box  margin-top-30">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                Total Sales</div>
                                            <div class="actions">
                                                <!--<a href="javascript:;" class="btn btn-default btn-sm">View Report </a>-->
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 name"> Gross sales </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="gross_sales">0</span> </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 name"> Discounts </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="discount">0</span> </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 name"> Refund </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="returns">0</span> </div>
                                            </div>
                                            <hr>

                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 name"> Net sales </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="net_sales">0</span> </div>
                                            </div>
                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 name"> Taxes </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="taxes">0</span> </div>
                                            </div>

                                            <hr>
                                            <div class="row static-info">
                                                <div class="col-xs-6 col-sm-5 col-md-5 total"> Total sales </div>
                                                <div class="col-xs-6 col-sm-7 col-md-7 value"> $<span id="total_sales">0</span>  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 margin-top-30">
                                    <form class="month-date">

                                        <button type="button" class="form-control" data-toggle="modal" data-target="#monthFilterModal" id="monthFilterButton"><span class="glyphicon glyphicon-calendar"></span> Month to date</button>
                                    </form>
                                    <div class="clearfix"></div>
                                    <div class="portlet blue-hoki box  margin-top-30">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                Sales</div>
                                            <div class="actions">
                                                <!--<a href="javascript:;" class="btn btn-default btn-sm">View Report </a>-->
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="payment_reports">

                                                <div class="row static-info">
                                                    <div class="col-xs-6 col-sm-5 col-md-7 name">Total Payments</div>
                                                    <div class="col-xs-6 col-sm-7 col-md-5 value"> $<span id="payments_total">0</span>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div id="payment_reports">

                                                <div class="row static-info">
                                                    <div class="col-xs-6 col-sm-5 col-md-7 name">Payments Completed</div>
                                                    <div class="col-xs-6 col-sm-7 col-md-5 value"> $<span id="payment_completed">0</span>
                                                    </div>
                                                </div>
                                            </div> 
                                            <hr>
                                            <div id="payment_reports">

                                                <div class="row static-info">
                                                    <div class="col-xs-6 col-sm-5 col-md-7 name">Payments Pending</div>
                                                    <div class="col-xs-6 col-sm-7 col-md-5 value"> $<span id="payment_pending">0</span>
                                                    </div>
                                                </div>
                                            </div> 
                                            <hr>
                                            <div id="payment_reports">

                                                <div class="row static-info">
                                                    <div class="col-xs-6 col-sm-5 col-md-7 name">Local Guide Payments </div>
                                                    <div class="col-xs-6 col-sm-7 col-md-5 value"> $<span id="localguidepayments">0</span>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </body>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- Month Filter Modal -->
<div id="monthFilterModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filter By Month</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <div id="date-picker-from" data-date-format="mm/dd/yyyy"> </div>
                        <input type="hidden" id="date-from" value="">
                    </div>                    
                    <div class="col-md-1">
                        To
                    </div>
                    <div class="col-md-5">
                        <div id="date-picker-to" data-date-format="mm/dd/yyyy"> </div>
                        <input type="hidden" id="date-to" value="">
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green_btn border-radius-btn btn-lg" onclick="filterReport(2)">Filter</button>
            </div>
        </div>

    </div>
</div>
<script src="{{url('/js/admin/reports/financial_report.js')}}"></script>
@stop

