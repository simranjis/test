@extends('admin.layouts.defaultsidebar')
@section('content')

<script src="//code.jquery.com/jquery.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Currencies
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Currencies</li>
        </ol>
    </section>
    <section class="content">
        <div class="text-right">
            <a href="{{ url('admin/currency/add/') }}" class="btn bg-maroon btn-flat collapsed" role="button">
            <i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp;Add Curency
            </a>
        </div>
        <br>
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Currencies</h3>
            </div>
            <div class="box-body">
                 <table id="manageAllCurrencies" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Currency Code</th>
                            <th>Set as Default</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($currenciesResultArr)>0){ ?>
                            <?php foreach ($currenciesResultArr as $key => $valueObj) { ?>
                                <tr>
                                    <td>
                                        {{$valueObj->title}}
                                    </td>
                                    <td>
                                        {{$valueObj->code}}
                                    </td>
                                    <td class="text-center">
                                        <?php 
                                        $setdefault = null;
                                        $setdefault_disabled = '';
                                        $setdefault_action = 'setDefault('.$valueObj->id.')';
                                        if($valueObj->setdefault){
                                            $setdefault = true;
                                            $setdefault_disabled = 'disabled';
                                            $setdefault_action = false;
                                        }
                                        ?>

                                        {{ Form::radio('setdefault', 1, $setdefault, ['class' => 'field', $setdefault_disabled, 'onclick'=>'return '.$setdefault_action ]) }}
                                    </td>
                                    <td class="col-md-4">
                                        <a href="{{ url('admin/currency/add/'.$valueObj->id) }}" class="btn bg-navy btn-flat margin collapsed" role="button">
                                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                        </a>

                                        <a href="javascript:void()" onclick="deleteCurrency({{$valueObj->id}})" class="btn bg-maroon btn-flat margin collapsed" role="button">
                                            <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan=5 style="text-align:center">No Records found</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@stop

@section('page_scripts')
<script src="js/admin/currencies/index.js"></script>
@stop