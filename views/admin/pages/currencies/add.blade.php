@extends('admin.layouts.defaultsidebar')
@section('content')

<script src="//code.jquery.com/jquery.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Currencies
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Currencies</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Currencies</h3>
            </div>
            <div class="box-body">
                <!-- all local guides -->
                @include('admin.pages.currencies.partials.currency_form')
            </div>
        </div>
    </section>
</div>

@stop

@section('page_scripts')
<script src="js/admin/currencies/add.js"></script>
@stop