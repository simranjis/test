<div class="row">
    <div class="col-sm-12">
    	<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('title', 'Title', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('title', $currency->title, array('class' => 'form-control form-control-custom', 'id'=>'title', 'placeholder'=>'Title')) }}
            </div>                                      
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('code', 'Code', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('code', $currency->code, array('class' => 'form-control form-control-custom', 'id'=>'code', 'placeholder'=>'Code')) }}
            </div>                                      
        </div>
        <!-- <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('symbol_left', 'Symbol', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('symbol_left', $currency->symbol_left, array('class' => 'form-control form-control-custom', 'id'=>'symbol_left', 'placeholder'=>'Symbol')) }}
            </div>                                      
        </div> -->
       <!--  <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::label('conversion', 'Conversion', array('class'=>'label-font')) }}
                <span class="text-danger">*</span>

                {{ Form::text('conversion', $currency->conversion, array('class' => 'form-control form-control-custom', 'id'=>'conversion', 'placeholder'=>'Symbol')) }}
            </div>                                      
        </div> -->
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{ Form::button('Save', array('id'=>'saveCurrency', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveCurrency()')) }}

                {{ Form::hidden('id', $currency->id, array('id'=>'id')) }}

                <a href="{{ url('admin/others/currencies') }}" class="btn bg-gray btn-flat collapsed" role="button">
	            	Cancel
	            </a>
            </div>                                      
        </div>
    </div>
</div>