@extends('admin.layouts.defaultsidebar')
@section('content')


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bookings
            <small>Accept</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>
    
    <section class="content">
        
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            {!! Form::open(['url' => 'admin/bookings/deleteProcess', 'name' => 'deleteBookingsForm' , 'class'=>'deleteBookingsForm', 'id'=>'deleteBookingsForm', 'files' => false]) !!}                                
                            <input type="hidden" name="id" value="{{ $booking_detail['id'] }}">
                                <a href="{{ url('admin/bookings') }}"><input type="button" name="Submit" value="Cancel" class="btn bg-maroon btn-flat margin"></a>

                                <input type="submit" name="Submit" value="Delete" class="btn bg-black btn-flat margin">
                                

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    
    
</div>



@stop
