@extends('admin.layouts.defaultsidebar')
@section('content')
<script type="text/javascript">

    $(function () {

        $(".accept-pop").click(function (e) {
            var url = $(this).attr('href');
            bootbox.confirm("Confirm! Accept Booked Activity", function (result) {
                if (result) {
                    window.location = url;
                }
            });
            e.preventDefault();
        });
        $(".decline-pop").click(function (e) {
            var url = $(this).attr('href');
            bootbox.confirm("Confirm! Decline Booked Activity", function (result) {
                if (result) {
                    window.location = url;
                }
            });
            e.preventDefault();
        });
    });</script> 
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }


</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Booking #{{ $booking['bookings_reference'] }} 
            <small>Information</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>
    @if(Request::get('message'))
    <span style="width:98%; background: green; color:white; float: left; margin:15px 15px 10px 10px; padding: 5px 4px;">{{ Request::get('message') }}</span>
    @endif  

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">#{{ $booking['bookings_reference'] }}</h3>

                                    </div>
                                </div>    
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <h3>Booking Information</h3>
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <tr>
                                                        <td>Reference Number</td>
                                                        <td>#{{ $booking['bookings_reference'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Date Created:</td>
                                                        <td>{{ Carbon\Carbon::parse($booking['created_at'])->format('d/m/Y') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Currency:</td>
                                                        <td>{{ $booking['currency'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>SubTotal:</td>
                                                        <td>{{ $booking['subtotal'] }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <h3>Traveler Booking Information</h3>
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>{{ $traveler_title[$booking['title']] }} {{ $booking['firstname'] }} {{ $booking['lastname'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email:</td>
                                                        <td>{{ $booking['email'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile:</td>
                                                        <td>{{ $booking['mobilenumber'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Country:</td>
                                                        <td>{{ $booking['country'] }}</td>
                                                    </tr>
                                                </table>
                                            </div>    
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <h3>Traveler Information</h3>
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <tr>
                                                        <td>Name</td>
                                                        <td>{{ $traveler_title[$booking['traveller']['title']] }} {{ $booking['traveller']['firstname'] }} {{ $booking['traveller']['lastname'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email:</td>
                                                        <td>{{ $booking['traveller']['email'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile:</td>
                                                        <td>{{ $booking['traveller']['mobilenumber'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Country:</td>
                                                        <td>{{ $booking['traveller_country']?$booking['traveller_country']:'-NA-' }}</td>
                                                    </tr>
                                                </table>
                                            </div>    
                                        </div>

                                    </div>


                                    <!-- Activity Information -->


                                    <h3>Activities</h3>
                                    @if(count($booking['bookings_activities']) > 0)
                                    @foreach($booking['bookings_activities'] as $key => $value)
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <tr>
                                                        <th colspan="2">Activity ID</th>
                                                        <th>Destination</th>
                                                        <th>Activity</th>
                                                        <th>Currency</th>
                                                        <th>Date</th>
                                                        <th colspan="2">Status</th>
                                                    </tr>
                                                    <tr>
                                                        <td><img width="125" src="uploads/{{ $value['activities_data']['image'] }}" ></td>
                                                        <td><!-- #{{ $value['activities_id'] }} --></td>
                                                        <td>{{ $value['destination'] }} </td>
                                                        <td>{{ $value['activities_data']['title'] }} </td>
                                                        <td>{{ $value['currency'] }} </td>
                                                        <td>{{ date('d/m/Y' , strtotime($value['date'])) }} </td>
                                                        <td>
                                                            @if($value['status'] === 0)
                                                            <a href="{{ 'admin/bookings/activities/accept/' }}{{ $value['bookings_activities_id'] }}" class="btn bg-green btn-flat margin accept-pop" role="button">
                                                                Accept
                                                            </a>

                                                            <a href="{{ 'admin/bookings/activities/decline/' }}{{ $value['bookings_activities_id'] }}" class="btn bg-black btn-flat margin decline-pop" role="button">
                                                                Decline
                                                            </a>
                                                            @else

                                                            @if($value['status'] == '1')
                                                            Accepted
                                                            @else
                                                            Declined
                                                            @endif

                                                            @endif
                                                        </td>

                                                        <td>
                                                            @if($value['payment_status'] == 1)
                                                            @if($value['refund_status'] == 1)
                                                            Refunded
                                                            @else
                                                            <a href="{{url('/admin/refund-detail/'.$value['bookings_activities_id'].'/'.$booking['id'])}}" class="btn bg-green btn-flat margin" role="button">
                                                                Refund
                                                            </a>
                                                            @endif
                                                            @endif
                                                        </td>
                                                    </tr>    
                                                    <tr>
                                                        <td colspan="6">    
                                                            @if(count($value['items']) > 0)
                                                            <h4>Pricing</h4>
                                                            <div class="box-body table-responsive">
                                                                <table class="table table-hover table-bordered">
                                                                    <tr>
                                                                        <th>Type</th>
                                                                        <th>Price For</th>
                                                                        <th>Title</th>
                                                                        <th>Price</th>
                                                                        <th>Qty</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                    @foreach($value['items'] as $items)
                                                                    <tr>
                                                                        <td>{{ ucwords($items['type']) }}</td>
                                                                        <td>{{ ucwords($items['pricefor']) }}</td>
                                                                        <td>{{ $items['title'] }}</td>
                                                                        <td>{{ $items['price'] }}</td>
                                                                        <td>{{ $items['quantity'] }}</td>
                                                                        <td>{{ $items['quantity']*$items['price'] }}</td>
                                                                    </tr>                                                        
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                            @endif

                                                            @if(count($value['options']) > 0)
                                                            <h4>Options</h4>
                                                            <div class="box-body table-responsive">
                                                                <table class="table table-hover table-bordered">
                                                                    <tr>
                                                                        <th>Title</th>
                                                                        <th>Description</th>
                                                                        <th>Value</th>
                                                                    </tr>
                                                                    @foreach($value['options'] as $options)
                                                                    <tr>
                                                                        <td>{{ ucwords($options['title']) }}</td>
                                                                        <td>{{ ucwords($options['description']) }}</td>
                                                                        <td>{{ $options['value'] }}</td>
                                                                    </tr>                                                        
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                            @endif
                                                        </td>
                                                        <td style="vertical-align:bottom;">
                                                            <!-- Activity Sub Total -->
                                                            <div class="box-body table-responsive">
                                                                <table class="table table-hover table-bordered">
                                                                    <tr>
                                                                        <td>Sub Total:</td>
                                                                        <td>{{ $value['subtotal'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Fee:</td>
                                                                        <td>{{ $value['tax'] }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total:</td>
                                                                        <td><strong>{{ $value['total'] }}</strong></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach
                                    @endif          







                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-4">

                                        </div>                       
                                        <div class="col-xs-4 col-sm-4 col-md-4">

                                        </div>                       
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <table class="table table-hover table-bordered">
                                                <tr>
                                                    <td>SubTotal:</td>
                                                    <td>{{ $booking['subtotal'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tax/Fee:</td>
                                                    <td>{{ $booking['tax'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Total:</strong></td>
                                                    <td><strong>{{ $booking['total'] }}</strong></td>
                                                </tr>
                                            </table>                                            
                                        </div>                       
                                    </div>

                                </div>
                                <a href="{{ url('admin/bookings') }}"><input type="button" name="Back" class="btn bg-maroon btn-flat margin" value="Back"></a>

                            </div>
                        </div>
                    </div>   

                </div>

            </div>
        </div>
    </section>
</div>
@stop