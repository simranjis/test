@extends('admin.layouts.defaultsidebar')
@section('content')

<style>
    .animationload {
        /*        background-color: #fff;*/
        height: 100%;
        left: 0;
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 10000;
        color:#E9004E !important;
    }
    .processing{
        position: absolute;
        top:58%;
        left: 0;
        right: 0;
        font-size: 18px;
    }
    .osahanloading {
        animation: 1.5s linear 0s normal none infinite running osahanloading;
        background: #7dd0dd none repeat scroll 0 0;
        border-radius: 50px;
        height: 50px;
        left: 50%;
        margin-left: -25px;
        margin-top: -25px;
        position: absolute;
        top: 50%;
        width: 50px;
    }
    .osahanloading::after {
        animation: 1.5s linear 0s normal none infinite running osahanloading_after;
        border-color: #E9004E transparent;
        border-radius: 80px;
        border-style: solid;
        border-width: 10px;
        content: "";
        height: 80px;
        left: -15px;
        position: absolute;
        top: -15px;
        width: 80px;
    }
    @keyframes osahanloading {
        0% {
            transform: rotate(0deg);
        }
        50% {
            background: #FF6 none repeat scroll 0 0;
            transform: rotate(180deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border bg-maroon">
                        <h3 class="box-title text-bold text-white"><?php echo $bookingActivityDetailsArr->activity->title ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="text-center">
                            <img src="uploads/{{ $bookingActivityDetailsArr->activity->image }}" style="width: 360px; height: 130px;"><br><br>
                        </div>
                        <h4><strong>Pricing: </strong></h4>
                        <table border="1" width="100%">
                            <tr>
                                <th class="col-sm-2">Type</th>
                                <th class="col-sm-2">Price For</th>
                                <th class="col-sm-2">Title</th>
                                <th class="col-sm-2">Price</th>
                                <th class="col-sm-2">Quantity</th>
                                <th class="col-sm-2">Total</th>
                            </tr>
                            <?php
                            $total = 0;
                            foreach ($bookingActivityDetailsArr->bookings_items as $key => $value) {
                                $total += ($value->price * $value->quantity);
                                ?>
                                <tr>
                                    <td class="col-sm-2"><?php echo $value->type ?></td>
                                    <td class="col-sm-2"><?php echo ucwords($value->pricefor); ?></td>
                                    <td class="col-sm-2"><?php echo $value->title ?></td>
                                    <td class="col-sm-2"><?php echo $value->price ?></td>
                                    <td class="col-sm-2"><?php echo $value->quantity ?></td>
                                    <td class="col-sm-2"><?php echo number_format($value->price * $value->quantity, 2) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"><strong>Subtotal: </strong></td>
                                <td class="col-sm-2"><?php echo number_format($total, 2) ?></td>
                            </tr>
                            <?php if ($bookingActivityDetailsArr->tax != 0.00) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4">
                                    <td class="col-sm-2">
                                        <strong>Tax / Fee: </strong>
                                    </td>
                                    <td class="col-sm-2"><?php echo number_format($bookingActivityDetailsArr->tax, 2) ?>
                                        <?php $total += $bookingActivityDetailsArr->tax; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if (!empty($bookingActivityDetailsArr->promotion_id)) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Promo (<?php echo $bookingActivityDetailsArr->promotion->promocode ?>): 
                                        </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php echo number_format($bookingActivityDetailsArr->promotion->discount, 2); ?>
                                        <?php
                                        $discount = $bookingActivityDetailsArr->promotion->discount;
                                        if ($bookingActivityDetailsArr->promotion->discount_type == 2) {
                                            echo ' %';
                                            $discount = ($total * ($bookingActivityDetailsArr->promotion->discount / 100));
                                        }
                                        $total = $total - $discount;
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                <td class="col-sm-2">
                                    <strong>Grand Total: </strong>
                                </td>
                                <td class="col-sm-2">
                                    <?php echo number_format($total, 2); ?>
                                </td>
                            </tr>
                            <?php if (!empty($bookingActivityDetailsArr->localguide_id)) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Payable Amount: </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php
                                        if (!empty($bookingActivityDetailsArr->localguide)) {
                                            $commision_amount = $total * ($bookingActivityDetailsArr->localguide->commision / 100);
                                            echo number_format($commision_amount, 2) . '&nbsp;';
                                            echo '( ' . $bookingActivityDetailsArr->localguide->commision . ' % )';
                                            $total = $total - $commision_amount;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Refundable Amount </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php echo $bookingActivityDetailsArr->code . ' ' . number_format($bookingActivityDetailsArr->amount, 2); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="text-center">
                        <button type="button" id="doRefund" href="javascript:void(0)" class="btn bg-maroon btn-flat margin" role="button">
                            Refund
                        </button>
                    </div>
                </div>
                <input type="hidden" name="activity_voucher_hash" id="activity_voucher_hash" value="{{ $bookingActivityDetailsArr->voucher_hash }}" />
            </div>
        </div>
    </section>
</div>

<script>

    $("#doRefund").on('click', function () {
        swal({
            title: "Confirm Refund",
            text: "Are you sure to refund the transaction. Remember : Refund will be made as full returns of transaction amount.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Proceed",
            closeOnConfirm: true
        }
        , function (isConfirm) {
            if (isConfirm) {
                $(window).block({
                    'message': '<div class="animationload"><div class="osahanloading"></div><span class="processing">Processing</span></div>',
                    'css': {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none',
                        marginTop: '5%',
                        zIndex: '100000'
                    },
                    overlayCSS: {backgroundColor: '#555', opacity: 0.5, cursor: 'wait', zIndex: '100000'},
                });
                var bookingID = '<?php echo $bookingActivityDetailsArr->bookings_id; ?>';
                var bookingActivityID = '<?php echo $bookingActivityDetailsArr->id; ?>';
                $.post(base_url + '/admin/refundBookingActivity', {'_token': '{{csrf_token()}}', bookingID: bookingID, bookingActivityID: bookingActivityID}, function (data) {
                    if (data.code == '1') {
                        swal({
                            title: "Refund Processed",
                            text: "",
                            type: "success",
                        }, function () {
                            window.location.href = base_url + '/admin/bookings/view/' + bookingID;
                        });
                    } else if (data.code == '0') {
                        bootbox.alert(data.message);
                    }
                    $(window).unblock();
                });
            }
        });
    });
</script>

@stop
