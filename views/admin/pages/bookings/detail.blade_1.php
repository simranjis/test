@extends('admin.layouts.defaultsidebar')
@section('content')
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
        
    
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Booking #{{ $booking['id'] }} 
            <small>Information</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                
                <div class="box box-primary">
                   <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">#{{ $booking['id'] }}</h3>
                                        
                                    </div>
                                </div>    
                                <div class="box-body">
                                    
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3>Traveler Information</h3>
                                            #{{ $booking['traveller']['id'] }} <br />
                                            Name: {{ $traveler_title[$booking['traveller']['title']] }}  
                                            {{ $booking['traveller']['firstname'] }} 
                                            {{ $booking['traveller']['lastname'] }} <br />
                                            Email: {{ $booking['traveller']['email'] }} <br />
                                            Mobile: {{ $booking['traveller']['mobilenumber'] }} <br />
                                            Country: {{ $booking['traveller_country'] }} <br />
                                            
                                            
                                            <h3>Traveler Booking Information</h3>
                                            Name: {{ $traveler_title[$booking['title']] }}  
                                            {{ $booking['firstname'] }} 
                                            {{ $booking['lastname'] }} <br />
                                            Email: {{ $booking['email'] }} <br />
                                            Mobile: {{ $booking['mobilenumber'] }} <br />
                                            Country: {{ $booking['country'] }} <br />
                                        </div>
                                        <div class="col-xs-8">
                                            <h3>Booking Information</h3>
                                            #{{ $booking['id'] }} <br />
                                             SubTotal: {{ $booking['subtotal'] }} <br />
                                             Tax/Fee: {{ $booking['tax'] }} <br />
                                             Total: {{ $booking['total'] }} <br />
                                             Status: {{ $booking_status[$booking['status']] }} <br />
                                             Create Date: {{ date('d/m/Y' , strtotime($booking['created_at'])) }} <br />
                                             Currency: {{ $booking['currencies_id'] }} <br />
                                             Booking Reference: {{ $booking['bookings_reference'] }} 

                                             <hr />
                                             
                                             <h4>Activities</h4>
                                             @if(count($booking['bookings_activities']) > 0)
                                                @foreach($booking['bookings_activities'] as $key => $value)
                                                    
                                                    #{{ $value['activities_id'] }} <br />
                                                    Activity: {{ $value['activities_data']['title'] }} <br /><br />
                                                    <img src="uploads/{{ $value['activities_data']['image'] }}" > <br /><br />
                                                    Destination: {{ $value['destination'] }} <br />
                                                    Currency: {{ $value['currencies'] }} <br />
                                                    Date: {{ date('d/m/Y' , strtotime($value['date'])) }} <br /><br /><br />
                                                    
                                                    Pricing:
                                                    <hr />
                                                    
                                                    @if(count($value['items']) > 0)
                                                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Price For</th>
                                                            <th>Title</th>
                                                            <th>Price</th>
                                                            <th>Qty</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        @foreach($value['items'] as $items)
                                                        <tr>
                                                            <td>{{ ucwords($items['type']) }}</td>
                                                            <td>{{ ucwords($items['pricefor']) }}</td>
                                                            <td>{{ $items['title'] }}</td>
                                                            <td>{{ $items['price'] }}</td>
                                                            <td>{{ $items['quantity'] }}</td>
                                                            <td>{{ $items['quantity']*$items['price'] }}</td>
                                                        </tr>                                                        
                                                        @endforeach
                                                    </table>    
                                                    @endif
                                                    <br /><br />
                                                    Options:
                                                    <hr />
                                                    
                                                    @if(count($value['options']) > 0)
                                                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                            <th>Value</th>
                                                        </tr>
                                                        @foreach($value['options'] as $options)
                                                        <tr>
                                                            <td>{{ ucwords($options['title']) }}</td>
                                                            <td>{{ ucwords($options['description']) }}</td>
                                                            <td>{{ $options['value'] }}</td>
                                                        </tr>                                                        
                                                        @endforeach
                                                    </table>    
                                                    @endif
                                                    
                                                    
                                                    <hr />
                                                    
                                                @endforeach
                                             @endif
                                             
                                             
                                             
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                <a href="{{ url('admin/bookings') }}"><input type="button" name="Back" class="btn bg-maroon btn-flat margin" value="Back"></a>

                            </div>
                        </div>
                   </div>   
                    
                </div>
                
            </div>
        </div>
    </section>
</div>





@stop