@extends('admin.layouts.defaultsidebar')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Templates
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Templates</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Templates</h3>
            </div>
            <div class="box-body">
                <div class="text-right">
                    <a href="{{ url('admin/leads/addTemplate/') }}" class="btn bg-maroon btn-sm margin collapsed" role="button">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add Template
                    </a>
                </div>

                 <table id="manageAllCurrencies" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="col-sm-4">Title</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($templatesResultObj)>0){ ?>
                            <?php foreach ($templatesResultObj as $key => $templateResultObj) { ?>
                                <tr>
                                    <td>
                                        {{$templateResultObj->title}}
                                    </td>
                                    <td>
                                        {{strip_tags($templateResultObj->description)}}
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/leads/addTemplate/'.$templateResultObj->id) }}" class="btn bg-navy btn-sm btn-flat margin collapsed" role="button">
                                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan=5 style="text-align:center">No Records found</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>    
@stop