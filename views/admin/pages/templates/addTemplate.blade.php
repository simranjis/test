@extends('admin.layouts.defaultsidebar')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Template
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Template</li>
        </ol>
    </section>
    {!! Form::open(['url' => 'admin/leads/addTemplate', 'name' => 'addTemplateForm' , 'class'=>'addTemplateForm', 'id'=>'addTemplateForm']) !!}  
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border box-header-color">
                <h3 class="box-title sbold">Add Template</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('title', 'Title', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::text('title', $template->title, array('class' => 'form-control form-control-custom', 'id'=>'title', 'placeholder'=>'Title')) }}
                            </div>                                      
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::label('description', 'Description', array('class'=>'label-font')) }}
                                <span class="text-danger">*</span>

                                {{ Form::textarea("description", $template->description, array('class' => 'form-control ckeditor', 'id'=>"description", 'placeholder'=>'Description')) }}
                            </div>                                      
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                {{ Form::submit('Save', array('id'=>'saveTemplate', 'class'=>'btn bg-maroon btn-flat margin')) }}
                                
                                {{ Form::hidden('id', $template->id, array('id'=>'id')) }}

                                <a href="{{ url('admin/leads/templates') }}" class="btn bg-gray btn-flat collapsed" role="button">
                                    Cancel
                                </a>
                            </div>                                      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>    
@stop