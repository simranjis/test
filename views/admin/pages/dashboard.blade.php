@extends('admin.layouts.defaultsidebar')
@section('content')

<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $todayBooking }}</h3>

                        <p>Today's Booking</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $todayPayments }}</h3>

                        <p>Today's Transactions</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $topDestinations->title }}</h3>
                        <p>Top Destination</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $newTravellerRegistered }}</h3>

                        <p>New Traveller Registered</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <h2>Activity Feed</h2>
        <div class="activity-feed">
            <div class="feed-item">
                <div class="date">Sep 25</div>
                <div class="text">Responded to need <a href="single-need.php">“Volunteer opportunity”</a></div>
            </div>
            <div class="feed-item">
                <div class="date">Sep 24</div>
                <div class="text">Added an interest “Volunteer Activities”</div>
            </div>
            <div class="feed-item">
                <div class="date">Sep 23</div>
                <div class="text">Joined the group <a href="single-group.php">“Boardsmanship Forum”</a></div>
            </div>
            <div class="feed-item">
                <div class="date">Sep 21</div>
                <div class="text">Responded to need <a href="single-need.php">“In-Kind Opportunity”</a></div>
            </div>
            <div class="feed-item">
                <div class="date">Sep 18</div>
                <div class="text">Created need <a href="single-need.php">“Volunteer Opportunity”</a></div>
            </div>
            <div class="feed-item">
                <div class="date">Sep 17</div>
                <div class="text">Attending the event <a href="single-event.php">“Some New Event”</a></div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
</div>
<!-- ./wrapper -->

@stop
