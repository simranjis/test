<div class="tab-pane" id="tab_3">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold">News Letters</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="manageAllNewsLettes" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?php if(count($newsLetterResultObj)>0){ ?>
                                                        <input type="checkbox" class="allNewsletters" id="allNewsletters" name="allNewsletters">
                                                    <?php } ?>
                                                </th>
                                                <th>Email</th>
                                                <th>Subscribed at</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($newsLetterResultObj)>0){ ?>
                                                <?php 
                                                $i = 1;
                                                foreach ($newsLetterResultObj as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="newsletters" id="Newsletter<?php echo $value->id ?>" name="newsletters[<?php echo $value->id ?>]" value="<?php echo $value->email ?>">
                                                        </td>
                                                        <td>
                                                            {{$value->email}}
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo date('d F Y,  H:i a',strtotime($value->created_at));
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan=3 style="text-align:center">No Records found</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>