<div class="tab-pane active active_tab" id="tab_1">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold">LocalGuides</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="manageAllActiveLocalGuides" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-1">
                                                    <?php if(count($localguidesResultObj)>0){ ?>
                                                        <input type="checkbox" class="allLocalGuides" id="allLocalGuides" name="allLocalGuides">
                                                    <?php } ?>
                                                </th>
                                                <th class="col-sm-5">Name</th>
                                                <th class="col-sm-6">Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($localguidesResultObj)>0){ ?>
                                                <?php 
                                                foreach ($localguidesResultObj as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="localguides" id="LocalGuide<?php echo $value->id ?>" name="localguides[<?php echo $value->id ?>]" value="<?php echo $value->email ?>" >
                                                        </td>
                                                        <td>
                                                            {{$value->fullname}}
                                                        </td>
                                                        <td>
                                                            {{$value->email}}
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan=3 style="text-align:center">No Records found</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>