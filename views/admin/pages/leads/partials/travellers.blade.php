<div class="tab-pane" id="tab_2">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="">
                    <div class="row">
                        <div class="col-xs-12">                                
                            <div class="">
                                <div class="box-header with-border box-header-color">
                                    <h3 class="box-title sbold">Travellers</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="manageAllActiveLocalGuides" class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?php if(count($travellersResultObj)>0){ ?>
                                                        <input type="checkbox" class="allTravellers" id="allTravellers" name="allTravellers">
                                                    <?php } ?>
                                                </th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($travellersResultObj)>0){ ?>
                                                <?php 
                                                $i = 1;
                                                foreach ($travellersResultObj as $key => $value) { ?>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="travellers" id="Traveller<?php echo $value->id ?>" name="travellers[<?php echo $value->id ?>]" value="<?php echo $value->email ?>">
                                                        </td>
                                                        <td>
                                                            {{$value->firstname}} {{$value->lastname}}
                                                        </td>
                                                        <td>
                                                            {{$value->email}}
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan=3 style="text-align:center">No Records found</td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>