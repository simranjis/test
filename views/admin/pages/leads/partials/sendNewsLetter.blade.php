<!-- Modal -->
<div id="sendNewsLetterModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send NewsLetter</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                  {{ Form::label('template', 'Template', array('class'=>'label-font')) }}
                  <span class="text-danger">*</span>

                  {{ Form::select("template_id", $templatesResultArr,  $defaultTemplateObj->id, array('class'=>'form-control template', 'id'=>"template_id", 'onchange'=>'changeTemmplate()')) }}
              </div>                                      
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                  {{ Form::label('description', 'Description', array('class'=>'label-font')) }}
                  <span class="text-danger">*</span>

                  {{ Form::textarea("description", $defaultTemplateObj->description, array('class' => 'form-control ckeditor', 'id'=>"description", 'placeholder'=>'Description')) }}
              </div>                                      
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-default btn-primary" value="Send " />
        </div>
      </div>
    </div>
  </div>
</div>