@extends('admin.layouts.defaultsidebar')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Leads
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leads</li>
        </ol>
    </section>
    {!! Form::open(['url' => 'admin/leads/sendNewsLetter', 'name' => 'sendNewsLetterForm' , 'class'=>'sendNewsLetterForm', 'id'=>'sendNewsLetterForm']) !!}  
        <section class="content">
            
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Localguides</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Travellers</a></li>
                    <li><a href="#tab_3" data-toggle="tab">News Letters</a></li>
                </ul>
                <div class="tab-content">
                    <!-- all local guides -->
                    @include('admin.pages.leads.partials.localguides')

                    <!-- new local guides -->
                    @include('admin.pages.leads.partials.travellers')

                    <!-- new local guides -->
                    @include('admin.pages.leads.partials.news_letters')
                    
                    <!-- new local guides -->
                    @include('admin.pages.leads.partials.sendNewsLetter')

                    <!-- /.tab-pane -->
                    <div class="text-center">
                        <a href="javascript:void(0)" onclick="return sendNewsLetter()" class="btn bg-maroon btn-sm margin collapsed" role="button">
                            <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp; Send News Letter
                        </a>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </section>
    {!! Form::close() !!}
</div>    
@stop


@section('page_scripts')
<script src="js/admin/leads/index.js"></script>
@stop