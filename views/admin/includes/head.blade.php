<?php
        header("Cache-Control: no-store, must-revalidate, max-age=0");
        header("Pragma: no-cache");
?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Destination Seeker</title>
        <base href="{{ url("/")}}/">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="adminpanel/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="adminpanel/dist/css/custom.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="adminpanel/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="adminpanel/dist/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="adminpanel/dist/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="adminpanel/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="adminpanel/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="adminpanel/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="adminpanel/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="adminpanel/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="adminpanel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


        <link rel="stylesheet" href="adminpanel/plugins/select2/select2.min.css" />

        <!--fine uploader -->
        <link rel="stylesheet" href="js/fine-uploader/fine-uploader.min.css" />
        <link rel="stylesheet" href="js/fine-uploader/fine-uploader-gallery.min.css" />

        <link href="js/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>
            var base_url = '{{url("/")}}';
        </script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>