<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="adminpanel/dist/img/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a href="dashboard"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <?php 
                $dashboard_active_class = '';
                if(Request::path() == 'admin/dashboard'){
                    $dashboard_active_class = 'active';
                }
            ?>
            <li class="treeview {{$dashboard_active_class}}">
                <a href="{{ url('admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php 
                $selected_module_id = $selected_sub_module_id = '';
                $modulesArr = Session::get('modulesArr'); 
                $userPermissionsArr = Session::get('userPermissionsArr'); 
                $administratorArr = Session::get('administrator'); 
            ?>
            
            <?php 
            foreach ($modulesArr as $key => $value) { 
                if(!empty($userPermissionsArr[$value['id']]) || !empty($administratorArr['role_id'])){
                ?>
                <?php if(!empty($value['all_submodules'])){ 
                    $allSubmodulesArr = $value['all_submodules'];
                    ?>
                    <li class="treeview" id="module_link_{{$value['id']}}">
                        <a href="javascript:void(0)">
                            <i class="fa fa-pie-chart"></i>
                            <span>{{$value['module_title']}}</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" id="submodule_links_{{$value['id']}}"  style="display: none;">
                            <?php foreach ($allSubmodulesArr as $key => $subModuleArr) { 
                                if(!empty($userPermissionsArr[$value['id']][$subModuleArr['id']]) || !empty($administratorArr['role_id'])){
                                    if((Request::path() == $subModuleArr['url']) || (in_array(Request::segment(3),explode(',',$subModuleArr['additional_links'])))){
                                        $selected_sub_module_id = $subModuleArr['id'];
                                        $selected_module_id = $subModuleArr['module_id'];
                                    }
                                ?>
                                    <li class="treeview" id="sub_module_link_{{$subModuleArr['id']}}">
                                        <a href="{{$subModuleArr['url']}}"><i class="fa fa-circle-o"></i> {{$subModuleArr['sub_module_title']}}</a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } else { 
                    if(Request::path() == $value['url']){
                        $selected_module_id = $value['id'];
                    }
                    ?>
                    <li class="treeview" id="module_link_{{$value['id']}}" >
                        <a href="<?php echo url($value['url']) ?>">
                            <i class="fa fa-list"></i>
                            <span>{{$value['module_title']}}</span>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
            <?php } ?>
		      <li class="treeview" id="module_link_0" >
                        <a href="{{url('/admin/pages/meta/add-meta')}}">
                            <i class="fa fa-list"></i>
                            <span>Meta </span>
                        </a>
                    </li>
        </ul>
        <input type="hidden" name="selected_module_id" id="selected_module_id" value="<?php echo $selected_module_id; ?>">
        <input type="hidden" name="selected_sub_module_id" id="selected_sub_module_id" value="<?php echo $selected_sub_module_id; ?>">
    </section>
    <!-- /.sidebar -->
</aside>