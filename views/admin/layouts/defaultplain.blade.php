<!DOCTYPE html>
<html>
    <head>
        @include('admin.includes.head')
    </head>
        <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">    
            
                 @yield('content')

        </div>
    </body>
</html>

