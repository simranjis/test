<!DOCTYPE html>
<html>
    <head>
        @include('admin.includes.head')
    </head>
        <body class="hold-transition skin-blue-light sidebar-mini">
        <div class="wrapper">    
            @include('admin.includes.header')
            @include('admin.includes.sidebar')
            
                 @yield('content')

            @include('admin.includes.footer')
            @yield('page_scripts')
        </div>
    </body>
</html>
