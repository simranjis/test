<!DOCTYPE html>
<html>
    <head>
        @include('admin.includes.head')
    </head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('admin.includes.header')
        
            @yield('content')

        
    </div>
</body>
</html>
