<div class="footer margin-top-40" id="footer-destroy">
    <div class="container">
        <div class="margin-top-search">
            <div class="news-heading">
                <h4>NEWS LETTER</h4>
            </div>
            {!! Form::open(['url' => 'newsletter', 'name' => 'newsletterForm' , 'class'=>'newsletterForm', 'id'=>'newsletterForm']) !!}            
            <div class="input-group" id="adv-search">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle search-custom-icon search-custom-icon-bottom" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </button>
                </div>
                <input type="email" name="emailAddress" required class="form-control search-custom" placeholder="Your Email Address" />
                <div class="input-group-btn">
                    <button type="button" id="newsletterForm-submit-button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Subscribe</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="row margin-top-180">
            <div class="col-sm-7 col-md-5">
                <h3>LET’S GET SOCIAL</h3>
                <?php
                    $accountLinksArr = Common::getAccountLinks();
                ?>
                <ul class="socials-list list-unstyled list-inline">                            
                    <li class="socials-item-facebook"><a href="{{$accountLinksArr['2']}}"><i class="fa fa-facebook "></i></a></li>
                    <li class="socials-item-linkedin"><a href="{{$accountLinksArr['3']}}"><i class="fa fa-twitter"></i></a></li>
                    <li class="socials-item-linkedin"><a href="{{$accountLinksArr['4']}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li class="socials-item-linkedin"><a href="{{$accountLinksArr['5']}}"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                    <li class="socials-item-linkedin"><a href="{{$accountLinksArr['6']}}"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-sm-5 col-md-3">
                <img src="images/credit-logos.png" alt="John" class="img-responsive center-block logo-footer-fix margin-top-40">
            </div>
        </div>
        <div class="">
            <div class="row margin-top-80">
                <div class="col-md-12">
                    <ul class="list-inline footer-lower text-center">
                        <li> <a href="{{ url('contactus') }}">CONTACT</a></li>                            
                        <li> <a href="{{ url('faq') }}">FAQs</a></li>  

                    @php
                        if(App\Setting::isPageDisplay('display_blog_page'))
                        {
                    @endphp                    
                        <li> <a href="{{ url('blog') }}">BLOG</a></li>
                    @php
                        }
                    @endphp
                        <li> <a href="{{ url('reviews') }}">REVIEWS</a></li>                            
                        <li> <a href="{{ url('legal') }}">LEGAL</a></li>   
                        @php
                            if(App\Setting::isPageDisplay('display_press_page'))
                            {
                        @endphp    
                               <li> <a href="{{ url('press') }}">PRESS</a></li>
                        @php           
                            }
                        @endphp                          
                                               
                        <li> <a href="{{ url('localguide/signup') }}">BECOME A LOCAL GUIDE</a></li>                            
                    </ul>
                </div>
            </div>
        </div>
        <div class="row margin-top-40">
            <div class="col-md-12">
                <div class="footer-end">
                    <h5 class="text-center">© Destination Seeker.</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<meta name="_token" content="{!! csrf_token() !!}" />
<script type="text/javascript" src="js/card/card.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="js/smoothscroll.js"></script>-->
    <script src="js/bootbox.min.js" type="text/javascript"></script> 

<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="js/jquery.parallax.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 
<script type="text/javascript" src="js/login.js"></script> 

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
<script src="js/fine-uploader/fine-uploader.min.js"></script>
<script src="js/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js"></script>

<script>
$("#adv_search_txt").on('focus', function () {
    $("#adv-search-dropdown").css({'display': 'block'});
});
$("body").on('click', function (event) {
    if (event.target.id != "adv_search_txt" && event.target.id != $("#adv-search-dropdown") && $(event.target).parents("#adv-search-dropdown").length < 1) {
        $("#adv-search-dropdown").css({'display': 'none'});
    }
});
if ($(window).scrollTop() >= 50) {
    $('.navbar').css('background', '#2e2e2e');
    $('.navbar').css('padding', '1px 0px');
    $('.navbar-default .navbar-nav > li > a').css('padding', '12px 17px');
    $('.navbar-toggle').css('display', 'block');
    $("#scroll-logo").attr("src", "images/scroll-logo.png");
    $("#scroll-logo").css("height", "45px");
} else {
    $('.navbar').css('background', 'transparent');
    $('.navbar-default .navbar-nav > li > a').css('padding', '41px 15px');
    $('.navbar-toggle').css('display', 'none');
    $("#scroll-logo").attr("src", "images/logo.png");
    $("#scroll-logo").css("height", "103px");
}
$(window).scroll(function () {
    if ($(window).scrollTop() >= 50) {
        $('.navbar').css('background', '#2e2e2e');
        $('.navbar').css('padding', '1px 0px');
        $('.navbar-default .navbar-nav > li > a').css('padding', '12px 17px');
        $('.navbar-toggle').css('display', 'block');
        $("#scroll-logo").attr("src", "images/scroll-logo.png");
        $("#scroll-logo").css("height", "45px");
        $('.in').css('display', 'block');
        $(".navbar-custom > li > .dropdown-menu").css("margin-top", "5px");
    } else {
        $('.navbar').css('background', 'transparent');
        $('.navbar-default .navbar-nav > li > a').css('padding', '41px 15px');
        $('.navbar-toggle').css('display', 'none');
        $('.in').css('display', 'none');
        $("#scroll-logo").attr("src", "images/logo.png");
        $("#scroll-logo").css("height", "103px");
        $(".navbar-custom > li > .dropdown-menu").css("margin-top", "-25px");

    }
});
//    var waypoints = $('#footer-destroy').waypoint(function (direction) {
//        if (direction == "down") {
//            $(".destory-sticky ul").css("display", "none")
//            $(".destory-sticky-1").css("display", "none")
//        } else {
//            $(".destory-sticky ul").css("display", "block")
//            $(".destory-sticky-1").css("display", "block")
//        }
//    }, {
//        offset: '70%'
//    })
</script>
