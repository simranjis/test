<style>
    .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover {
        color: #fff;
        background-color: transparent;
    }

    .navbar-custom  .dropdown-menu li a {
        padding: 13px 20px;
    }

    .padding-right-25{
        padding-right: 25px !important;
    }

</style>
<script type="text/javascript">

<!--{{ url('currency/change') }}/-->

$(function(){

    $(".currency-link").click(function(e){
        e.preventDefault();
    });
});

function updateCurrency(currency_id){
    //alert(currency_id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
            
    $.ajax({
                url: 'currency/change',
                type: 'post',
                data: {id:currency_id},
                async: false,
                beforeSend: function () {
                    //Can we add anything here.
                },
                cache: true,
                dataType: 'json',
                crossDomain: true,
                success: function (data) {
                    if (data.code == 1) {
                        swal({
                            title: "Success",
                            text: data.message,
                            type: "success"
                        }, function() {
                            window.location = "{{ Request::url() }}";
                        });
                    } else {
                        bootbox.alert(data.message);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });    
}
</script>
<?php
$logo_mobile_hide_class = '';
if(!empty($is_mobile_view_logo)){
    $logo_mobile_hide_class = 'hidden-xs';
}
?>
<div class="preloader">
    <div class="preloder-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="loader">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header id="navigation"> 
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="400">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle hidden-md hidden-lg hidden-sm" data-toggle="collapse" data-target="#navbar-brand-centered">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand navbar-brand-centered padding-right-25 {{$logo_mobile_hide_class}}" id="logo_div">
                <a href="{{ url('/') }}"><img src="images/logo.png" alt="logo" class="img-responsive center-block" id="scroll-logo"></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse bg-black" id="navbar-brand-centered">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('whoweare') }}" class="pad-left-0-menu">Who We Are</a></li>
                    <li><a href="{{ url('destinations') }}">Destinations</a></li>
                    <li><a href="{{ url('howitworks') }}">HOW IT WORKS</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-custom">
                    @if(Session::has('traveller'))
                    <script type="text/javascript"> 
                       $(function() { 
                        $('.localguide-link').hide(); 
                       });                                     
                                        
                    </script>    
                    <li class="dropdown traveller-link">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="padding: 0px !important;">Hi! {{ (Session::get('traveller')['firstname']) }} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="kopie"><a href="{{ url('traveller') }}">My Account</a></li>
                            <li><a href="{{ url('traveller/mybookings') }}">My Bookings</a></li>
                            <li><a href="{{ url('traveller/myreviews') }}">My Reviews</a></li>
                            <li><a href="{{ url('traveller/myrewards') }}">My Rewards</a></li>
                            <li class=""><a href="{{ url('traveller/mysettings') }}">Settings</a></li>
                            <li class=""><a href="{{ url('traveller/logout') }}">Sign out</a></li>
                        </ul>
                    </li>


                    @else

                    <li class="traveller-link" id="prefacture">
                        <a href="" data-toggle="modal" data-target="#travellerLoginModal">Traveler Log In</a>
                    </li>
                    <li id="arefacture" class="dropdown" style="display:none;">
                        <a href="#" class="dropdown-toggle" id="arefacture-none" data-toggle="dropdown" aria-expanded="true" style="padding: 0px !important;"><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="kopie"><a href="{{ url('traveller') }}">My Account</a></li>
                            <li><a href="{{ url('traveller/mybookings') }}">My Bookings</a></li>
                            <li><a href="{{ url('traveller/myreviews') }}">My Reviews</a></li>
                            <li><a href="{{ url('traveller/myrewards') }}">My Rewards</a></li>
                            <li class=""><a href="{{ url('traveller/mysettings') }}">Settings</a></li>
                            <li class=""><a href="{{ url('traveller/logout') }}">Sign out</a></li>
                        </ul>
                    </li>
                    @endif
                    
                    
                    @if(Session::has('localguide'))
                        <script type="text/javascript"> 
                           $(function() { 
                            $('.traveller-link').hide(); 
                           });                                     
                        </script> 
                        <li class="localguide-link"><a href="{{ url('localguide/myaccount') }}">Hi! {{ (Session::get('localguide')['fullname']) }}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" id="arefacture-none" data-toggle="dropdown" aria-expanded="true" style="padding: 0px !important;"><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="kopie"><a href="{{ url('localguide/myaccount') }}">My Account</a></li>
                                <li><a href="{{ url('localguide/mybookings') }}">My Bookings</a></li>
                                <li><a href="{{ url('localguide/myactivities') }}">My Activities</a></li>
                                <li><a href="{{ url('localguide/myreviews') }}">My Reviews</a></li>
                                <li class=""><a href="{{ url('localguide/settings') }}">Settings</a></li>
                                <li class=""><a href="{{ url('localguide/logout') }}">Sign out</a></li>
                            </ul>
                        </li>
                    @else
                        <li class="localguide-link"><a href="" data-toggle="modal" data-target="#myModal2">Local Guide Log In</a></li>
                    @endif
                    @if(Request::segment(3) !== 'checkout')
                    <?php
                    
                     
                    $currencies = App\Currencies::all();
                    
                    $code = strtolower(Session::get('currencies')['code']);

                    ?>
                    
                    <li class="icon-head"><a href="{{ url('cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>

                    @if(Request::segment(2) !== 'checkout')
                    <li class="dropdown"><a href="{{ url('currency') }}" data-toggle="dropdown" aria-expanded="true" style="padding: 0px !important;">{{$code}}</a>
                        <ul class="dropdown-menu list-inline currency-width">
                            <?php foreach($currencies as $key => $value ): ?>
                            <li style="width:200px;" class="kopie"><a href="#" onclick="updateCurrency({{ $value->id }});" class="currency-link"><span class="currency-low">{{ $value->code }}</span> {{ $value->title }}</a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    @endif
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav><!--/navbar--> 
</header> <!--/#navigation-->
<!-- Modal -->
<div class="modal right fade" id="travellerLoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog modal-dialog-custom" role="document">
        <div class="modal-content modal-content-custom">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel2">Traveler Log In</h4>
            </div>
            {!! Form::open(['url' => 'traveller/login', 'name' => 'travellerloginForm' , 'class'=>'travellerloginForm', 'id'=>'travellerloginForm']) !!}            
            <div class="modal-body modal-body-custom">
                <div class="login-text">
                    <p>Login and start creating memories! </p>
                </div>
                <div class="right-side-login">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <label for="inputHelpBlock" class="label-font">Email</label>
                                            <input name="travellerEmail" class="form-control form-control-custom" placeholder="Email" type="text">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="inputHelpBlock" class="label-font">Password</label>
                                            <input name="travellerPassword" class="form-control form-control-custom" placeholder="Password" type="password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center mt20">
                                                <button id="travellerForm-submit-button" type="button" class="pink_btn_login">Login</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <a class=" black_btn_fb  black_btn_contact" href="{{url('/facebook_redirect')}}"><i class="fa fa-facebook-official" aria-hidden="true"></i> Login with facebook</a>
                                            <div class="mt10">
                                                <a href="{{ url('traveller/forgotpassword') }}"><p class="color-pink">Forgot Password?</p></a>
                                            </div>
                                            <div class="mt10">
                                                <p>Don’t have account ?<a href="{{ url('traveller/signup') }}"><span class="color-pink"> SIGN UP</span></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog modal-dialog-custom" role="document">
        <div class="modal-content modal-content-custom">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel2">Local Guide Log In</h4>
            </div>
            {!! Form::open(['url' => 'localguide/login', 'name' => 'localguideloginForm' , 'class'=>'localguideloginForm', 'id'=>'localguideloginForm']) !!}            
            <div class="modal-body modal-body-custom">
                <div class="login-text">
                    <p>Login and start sharing your unique experiences</p>
                </div>
                <div class="right-side-login">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <label for="inputHelpBlock" class="label-font">Email</label>
                                            <input class="form-control form-control-custom" name="localguideEmail" placeholder="Email" type="text">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="inputHelpBlock" class="label-font">Password</label>
                                            <input class="form-control form-control-custom" name="localguidePassword" placeholder="Password" type="password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center mt20">
                                                <button id="localguideForm-submit-button" type="button" class="pink_btn_login">Login</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <div class="mt10">
                                                <a href="{{ url('localguide/forgotpassword') }}"><p class="color-pink">Forgot Password?</p></a>
                                            </div>
                                            <div class="mt10">
                                                <p>Don’t have account ?<a href="{{ url('localguide/signup') }}"><span class="color-pink"> SIGN UP</span></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<script>
    $(document).ready(function () {
<?php if (Session::has('localguide_login_attempt')) {
    Session::forget('localguide_login_attempt')
    ?>                                                                                                                         $("#myModal2").modal('show');
<?php } ?>
    $('#Carousel').carousel({
        interval: 2500
        });
    });
            </script>