<?php
header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
?>
<meta charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<meta name="title" content="<?php echo isset($meta_title) && $meta_title!='' ? $meta_title : 'Destination Seeker'; ?>">
<meta name="description" content="<?php echo isset($meta_description) ? $meta_description : ''; ?>">
<meta name="keywords" content="<?php echo isset($meta_keyword) ? $meta_keyword : ''; ?>">
<title><?php echo isset($meta_title) && $meta_title!='' ? $meta_title : 'Destination Seeker'; ?></title>
<base href="{{ url("/")}}/">
<!--        <meta name="google-site-verification" content="b-st23Br08XfaD8I3h4I6kJwbXQvxXt7hP5uxxi4fLU" />-->
<meta name="msvalidate.01" content="43FEFD55F2A886BD71F956F324D33991" />
<meta name="google-site-verification" content="zD1f2gQGuums3fpUKpoaxxQE5xzFdhoSrwjx8FdYsCk" />
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/main.css?12345678" rel="stylesheet">
<link href="css/media-query.css?12345678" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet"> 
<link href="css/login.css" rel="stylesheet">
<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
<script src="js/respond.min.js"></script> <![endif]--> 
<link rel="shortcut icon" href="images/logo.jpg">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<!--<script src="/path/to/masonry.pkgd.min.js"></script>-->
<script src="js/bootbox.min.js" type="text/javascript"></script> 
<script src="js/destination-seeker.js" type="text/javascript"></script> 


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<link rel="stylesheet" href="int-tel/build/css/intlTelInput.css">

<script type="text/javascript" src="int-tel/build/js/intlTelInput.js"></script>

<!--fine uploader -->
<link rel="stylesheet" href="js/fine-uploader/fine-uploader.min.css" />
<link rel="stylesheet" href="js/fine-uploader/fine-uploader-gallery.min.css" />

<link href="js/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113694577-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-113694577-1');
</script>

<style>
    @media screen and (min-width:768px){
        .navbar-brand-centered {
            position: absolute;
            left: 50%;
            display: block;
            text-align: center;
            width: 160px;
        }
        .navbar>.container .navbar-brand-centered, 
        .navbar>.container-fluid .navbar-brand-centered {
            margin-left: -80px;
        }
    }

    .affix {
        -webkit-transition:padding 0.2s ease-out;
        -moz-transition:padding 0.2s ease-out;  
        -o-transition:padding 0.2s ease-out;         
        transition:padding 0.2s ease-out;

    }

    .affix-top {
        /* navbar style at top */
        background:transparent;
        border-color:transparent;
        padding: 15px;
        -webkit-transition:all 0.5s ease;
        -moz-transition:all 0.5s ease; 
        -o-transition:all 0.5s ease;         
        transition:all 0.5s ease;  
    }

    .affix-top .navbar-collapse {
        border-color:transparent;
        box-shadow:initial;
    }

    section {
        height:calc(90% - 50px);
        height: 90vh;
    }


    .iti-flag {background-image: url("int-tel/build/img/flags.png");}

    @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
        .iti-flag {background-image: url("int-tel/build/img/flags@2x.png");}
    }

</style>