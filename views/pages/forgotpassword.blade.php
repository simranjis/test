@extends('layouts.defaultplain')
@section('content')
<style>
    .navbar-default{
        display: none;
    }
    .form-control {
        width: 100%;
        padding: 10px 0;
        border: none;
        border-bottom: 1px solid;
        border-color: #a0a9b4;
        color: #868e97;
        font-size: 14px;
        margin-bottom: 30px;
        border-radius: 0 !important;
        border-top: 0px !important;
        box-shadow: none !important;
        height: 38px;
    }
</style>
<div class="user-login-5">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="row bs-reset">
                <div class="col-md-12 login-container bs-reset">
                    <a href="{{ url('/') }}">
                        <img class="login-logo login-6" src="images/logo.png" style="height:115px" class="img-responsive center-block">
                    </a>
                    <div class="login-content">
                        <h1>Forgot Password</h1>
                        <form action="javascript:;" class="login-form" method="post" novalidate="novalidate">
                            <div class="form-group form-md-line-input">
                                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="real_black_btn_login black_btn_contactpull-right">Back</button>
                                <button type="submit" class="black_btn_login black_btn_contact">Submit</button>
                            </div>
                        </form>
                        <!-- END FORGOT PASSWORD FORM -->
                    </div>
                    <!--            <div class="login-footer">
                                    <div class="row bs-reset">
                                        <div class="col-xs-5 bs-reset">
                                            <ul class="login-social">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-7 bs-reset">
                                                                    <div class="login-copyright text-right">
                                                                        <p>© Destination Seeker.</p>
                                                                    </div>
                                        </div>
                                    </div>
                                </div>-->
                </div>
                
            </div>
        </div>
    </div>

</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="js/smoothscroll.js"></script>-->

<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="js/jquery.parallax.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 
<script type="text/javascript" src="js/login.js"></script> 

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.min.js"></script>
<script type="text/javascript">
    $(function () {
        jQuery('#forgot_password_form-1').click(function () {
            jQuery('.login-form').hide();
            jQuery('.register-form').show();
        });
        jQuery('#register-back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
            jQuery('.register-form').hide();
        });
        $('select').select2();
        $('#register_success').delay(5000).fadeOut();
        $("#user_login_form").validate({
            errorElement: 'span', errorClass: 'help-block',
            rules: {
                user_login1: {
                    required: true,
                    minlength: 5
                },
                user_login_password1: {
                    required: true,
                    minlength: 5
                }
            },
            messages: {
                user_login1: {
                    required: "The User ID field is required.",
                    minlength: "The User ID field must be at least {0} characters in length."
                },
                user_login_password1: {
                    required: "The Password field is required.",
                    minlength: "The Password field must be at least {0} characters in length."
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
                $(element).closest('.form-group').children('span.help-block').remove();
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest('.form-group'));
            },
            submitHandler: function (form) {
                $(".alert-danger").remove();
                $("#user_login_button").button('loading');
                $.post('', {'user_login': btoa(btoa($.trim($("#user_login1").val()))), 'user_login_password': btoa(btoa(md5(md5($.trim($("#user_login_password1").val()).toLowerCase())))), 'user_remember': $("#user_remember:checked").val(), 'captcha_image': $("#captcha_image").val()}, function (data) {
                    if (data === '1') {
                        $("#user_login_form_div").hide();
                        $("#login_success_redirect").fadeIn('fast');
                        document.location.href = base_url + 'dashboard';
                    } else if (/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(data)) {
                        $("#user_login_form_div").hide();
                        $("#login_success_redirect").fadeIn('fast');
                        document.location.href = data;
                    } else if (data === '-1') {
                        document.location.href = '';
                    } else {
                        show_login_error();
                    }
                    $("#user_login_button").button('reset');
                });
            }
        });
        $("#forgot_password_form").validate({
            errorElement: 'span', errorClass: 'help-block',
            rules: {
                user_email: {
                    required: true,
                    minlength: 5,
                    email: true,
                }
            },
            messages: {
                user_email: {
                    required: "The User Email field is required.",
                    minlength: "The User Email field must be at least {0} characters in length.",
                    email: "The User Email must be valid"
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
                $(element).closest('.form-group').children('span.help-block').remove();
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest('.form-group'));
            },
            submitHandler: function (form) {
                $(".alert-danger").remove();
                $("#forgot_password_btn").button('loading');
                $.post(base_url + 'auth/recover', {email_address: btoa(btoa($.trim($("#email_address").val())))}, function (data) {
                    if (data === '1') {
                        $("#forgot_password_form").hide();
                        $("#forgot_msg").html('We have sent an email with new password.');
                        $("#forgot_success_redirect").fadeIn('fast');
                    } else if (data === '0') {
                        $("#forgot_msg").html('Invalid Email ID');
                    } else {
                        document.location.href = '';
                    }
                    $("#forgot_password_btn").button('reset');
                });
            }
        });
        $("#register_form").validate({
            errorElement: 'span', errorClass: 'help-block pull-right',
            rules: {
                user_name: {
                    required: true,
                    minlength: 5,
                    maxlength: 50,
                },
                user_email: {
                    required: true,
                    minlength: 5,
                    maxlength: 100,
                    email: true,
                    remote: {
                        url: base_url + 'auth/validate_email',
                        type: 'post'
                    }
                },
                user_address: {
                    required: true,
                    minlength: 5,
                    maxlength: 150,
                },
                user_city: {
                    required: true,
                    minlength: 5,
                    maxlength: 50,
                },
                country_id: {
                    required: true
                },
                user_login: {
                    required: true,
                    minlength: 5,
                    maxlength: 30,
                    remote: {
                        url: base_url + 'auth/validate_user_login',
                        type: 'post'
                    }
                },
                user_login_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 30,
                },
                confirm_login_password: {
                    required: true,
                    equalTo: "#user_login_password"
                }
            },
            messages: {
                user_name: {
                    required: "Full Name is required.",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                },
                user_email: {
                    required: "User Email field is required.",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                    email: "User Email must be valid.",
                    remote: "Email is already in use"
                },
                user_address: {
                    required: "User Address is required.",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                },
                user_city: {
                    required: "User City is required.",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                },
                country_id: {
                    required: "Select your Country."
                },
                user_login: {
                    required: "User Name is required",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                },
                user_login_password: {
                    required: "User Password is required.",
                    minlength: "User Email field must be at least {0} characters in length.",
                    maxlength: "User Email field must be at least {0} characters in length.",
                },
                confirm_login_password: {
                    required: "Confirm Password is required.",
                    equalTo: "Confirm Password doesnot match with password."
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
                $(element).closest('.form-group').children('span.help-block').remove();
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest('.form-group'));
            },
            submitHandler: function (form) {
                $("#register-submit-btn").button('loading');
                $.post(base_url + 'auth/signup', $("#register_form").serialize(), function (data) {
                    if (data === '1') {
                        document.location.href = '';
                    } else if (data === '0') {
                        $("#register_fail .alert-danger").html('Error in Signup.');
                    } else {
                        $('#register_fail').css('display', 'block');
                        $('#register_fail .alert-danger').html(data);
                    }
                    $("#register-submit-btn").button('reset');
                });
            }
        });
        $('select').change(function () {
            $("#register_form").validate().element($(this));
        });
        function show_login_error() {
            $("p.login-box-msg").after('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>You have entered an invalid username/email or password.</div>');
        }
    });
</script>
@stop