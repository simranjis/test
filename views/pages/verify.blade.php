@extends('layouts.defaultplain')
@section('content')
<style>
    .form-control {
        width: 100%;
        padding: 10px 0;
        border: none;
        border-bottom: 1px solid;
        border-color: #a0a9b4;
        color: #868e97;
        font-size: 14px;
        margin-bottom: 30px;
        border-radius: 0 !important;
        border-top: 0px !important;
        box-shadow: none !important;
        height: 38px;
    }
</style>
<div class="user-login-5">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="row bs-reset">
                <div class="col-md-12 login-container bs-reset">
                    <a href="{{ url('/') }}">
                        <img class="login-logo login-6" src="images/logo.png" style="height:115px" class="img-responsive center-block">
                    </a>
                    <div class="login-content">
                        @if($status == '1')
                        <script>
                            swal({title:'', text:'Email Verified Successfully', type:'success'}, function () {
                            window.location.href = '{{url("/")}}';
                            });
                        </script>
                        @else
                        <script>
                            swal({title:'', text:'Error in email verification', type:'error'}, function () {
                            window.location.href = '{{url("/")}}';
                            });
                        </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
