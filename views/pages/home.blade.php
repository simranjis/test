@extends('layouts.default')
@section('content')

<!-- Banner Area -->
@include('pages.partials.banner_area')

<!-- Feature Experiences -->
<?php if (count($featuredActivityObj) > 0) { ?>
    @include('pages.partials.featured_experiences')
<?php } ?>

<!-- Why Destination Seeker -->
@include('pages.partials.whyDestinationSeeker')

<!-- Featured Destinations -->
<?php if (count($featuredDestinationObj) > 0) { ?>
    @include('pages.partials.featuredDestinations')
<?php } ?>

<!-- Featured Destinations -->
@include('pages.partials.beInspired')

<!-- Featured Destinations -->
@include('pages.partials.howItWorks')

<!-- Featured Destinations -->
@include('pages.partials.whatOtherSays')



<script>
//    $(document).ready(function () {
//        var pwd = prompt('Please enter your password:', '');
//
//        if (pwd != null)
//        {
//            if (pwd == 'dest@12345#')
//            {
//                return true;
//            }
//        }
//        alert('Wrong password');
//        window.stop();
//        return false;
//        getPassword();
<?php
if (Session::has('traveller_login_attempt')) {
    Session::forget('traveller_login_attempt')
    ?>
            $("#travellerLoginModal").modal('show');
<?php } ?>
        $('#Carousel').carousel({
            interval: 2500
        });
    });


</script>
@stop