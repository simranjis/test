@extends('layouts.default')
@section('content')
<style>
    .legal{
        margin-top:180px;
    }
    .navbar-default .navbar-nav > li > a {
        color: #b9b9b9;
    }
    p {
        padding: 0 0px 0px 33px;
        font-family: 'gothamregular';
    }
    .legal h3{
        margin: 28px 0px;
        color: #000;
        font-size: 14px;
    }
    .legal h4{
        margin: 15px 0px;
    }
</style>

<div class="legal">
    <div class="container">
        <ul class="nav nav-pills nav-stacked pill-dest basic-dist-fix col-md-2">
            <li class="active"><a href="#tab_a" data-toggle="pill">{{$legalCollection[0]->type}}</a></li>
            <li><a href="#tab_b" data-toggle="pill">{{$legalCollection[1]->type}}</a></li>
        </ul>
        <div class="tab-content col-md-10">

            <div class="tab-pane active" id="tab_a">
                {!! $legalCollection[0]->content !!}
            </div>

            <div class="tab-pane" id="tab_b">
                {!! $legalCollection[1]->content !!}
            </div>
            
           
        </div><!-- tab content -->
    </div><!-- end of container -->
</div>
@stop
