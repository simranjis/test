<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
    <!-- Overlay -->
    <div class="overlay"></div>
    <!-- Indicators -->
    <!-- Wrapper for slides -->
    <div class="carousel-inner carousel-overflow-show">
        <?php
        $i = 1;
        $j = 0;
        foreach ($pageInfoObj->page_details as $pageDetail) {
            ?>
            <?php
            if ($pageDetail->block_group == 'BANNER_IMAGES') {
                $active_class = '';
                if ($i == 1) {
                    $active_class = 'active';
                }
                ?>
                <div class="item slides <?php echo $active_class; ?>">
                    <div class="slide-1" style="background-image: url(<?php echo URL::to('/'); ?>/uploads/bannerImages/<?php echo $pageDetail->content ?>);"></div>                    
                </div>     
                <?php
                $i++;
                $j++;
            }
            ?>
<?php } ?>
        <?php if ($j == 0) { ?>
            <div class="item slides active">
                <div class="slide-1" style="background-image: url(<?php echo URL::to('/'); ?>/images/banner.png);"></div>
            </div>
<?php } ?>
        <div class="hero hero-container">
            <div class="banner-heading">
                <h1 class="text-center text-uppercase"><?php echo $pageInfoObj->page_details[0]->content; ?></h1>
            </div>
            {!! Form::open(['url' => 'search', 'method' => 'get', 'name' => 'searchForm' , 'class'=>'searchForm', 'id'=>'searchForm']) !!}  
            <div class="advance-search advance-search-homepage">
                <div class="input-group margin-top-search" id="adv-search">
                    <input type="text" id="adv_search_txt" name="keyword" autocomplete="off" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs advanced-search-text" placeholder="Where's Your Next Adventure?" />
                    <input id="adv_search_txt" name="keyword" autocomplete="off" class="form-control search-custom search-custom-top search-custom-top-1 hidden-lg hidden-md hidden-sm" placeholder="Where To?" type="text">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle black_btn advanced-search-button" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <!-- Search Box -->
            @include('pages.search.default')
            <!-- Search Box -->

        </div>
    </div> 
</div>