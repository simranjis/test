<!--mobile view end-->
<div class="destination-section ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all">
                    <h2 class="text-uppercase"><span class="divider"></span>Why Destination Seeker?</h2>
                </div>                    
            </div>                
        </div>
        <?php
        $whyDestinationSeekerBlockObj1 = $whyDestinationSeekerBlockObj[0];
        $whyDestinationSeekerBlockObj1 = json_decode($whyDestinationSeekerBlockObj1['content']);
        $whyDestinationSeekerBlockObj2 = $whyDestinationSeekerBlockObj[1];
        $whyDestinationSeekerBlockObj2 = json_decode($whyDestinationSeekerBlockObj2['content']);
        $whyDestinationSeekerBlockObj3 = $whyDestinationSeekerBlockObj[2];
        $whyDestinationSeekerBlockObj3 = json_decode($whyDestinationSeekerBlockObj3['content']);
        ?>
        <div class="row margin-top-40">
            <div class="col-sm-4 col-xs-4">
                <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj1->image ?>" class="img-responsive center-block" alt="">
                <div class="dest-heading">
                    <h2 class="text-uppercase text-center"><?php echo ucwords($whyDestinationSeekerBlockObj1->title) ?></h2>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj2->image ?>" class="img-responsive center-block" alt="">
                <div class="dest-heading">
                    <h2 class="text-uppercase text-center"><?php echo ucwords($whyDestinationSeekerBlockObj2->title) ?></h2>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <img src="uploads/whyDestinationSeeker/<?php echo $whyDestinationSeekerBlockObj3->image ?>" class="img-responsive center-block" alt="">
                <div class="dest-heading">
                    <h2 class="text-uppercase text-center"><?php echo ucwords($whyDestinationSeekerBlockObj3->title) ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--mobile view-->