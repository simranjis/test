<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">BE INSPIRED</h2>
            </div>                    
        </div>                
    </div>
</div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item active inspired-img">
            <img src="images/inspire.png" alt="First slide">
            <div class="">
                <div class="hero">
                    <div class="banner-heading-inspired">
                        <h1 class="text-center text-uppercase">BE INSPIRED</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        <div class="text-center margin-top-80 margin-top-20-imp">
                            <a href="{{ url('blog') }}" button type="button" class="text-center border-button text-uppercase">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item inspired-img">
            <img src="images/inspire-2.png" alt="First slide">
            <div class="">
                <div class="hero">
                    <div class="banner-heading-inspired">
                        <h1 class="text-center text-uppercase">BE INSPIRED</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        <div class="text-center margin-top-80 margin-top-20-imp">
                            <a href="{{ url('howitworks') }}" button type="button" class="text-center border-button text-uppercase">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left color-pink"></span></a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right color-pink">
        </span>
    </a>
</div>