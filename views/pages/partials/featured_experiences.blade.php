<div class="container hidden-xs">
    <div class="padding-tb-40">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all">
                    <h2 class="text-uppercase"><span class="divider"></span>Featured Experiences</h2>
                </div>                    
            </div>                
        </div>
        <div class="row margin-top-40 pad-15">
            <?php foreach ($featuredActivityObj as $key => $value) { ?>
                <div class="col-sm-3 pad-right-0">
                    <div class="hovereffect">
                        <div class="feat-image-loacl">
                            <img src="uploads/<?php echo $value->image ?>" class="img-responsive center-block" alt="" style="width:360px; height:200px;">
                            <?php if($value->addedbytype == 'localguide'){ ?>
                            <div class="feat-on-image">
                                <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                            </div>
                            <?php } ?>
                        </div>
                        <div class="overlay">
                            <h2><?php echo $value->title ?></h2>
                            <a class="info" href="{{ url('destinations') }}/<?php echo $value->destinations_id ?>/<?php echo $value->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </div>
                    </div>                    
                </div>
            <?php } ?>
            <div class="row margin-top-40 margin-top-0">


            </div>
            <div class="row margin-top-40 margin-bottom-40">
                <div class="col-md-12">
                    <div class="text-center">
                        <a href="{{ url('destinations') }}" button type="button" class="text-center black_btn text-uppercase">Explore More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--mobile view-->
<div class="container hidden-md hidden-lg hidden-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase"><span class="divider"></span>Featured Experiences</h2>
            </div>                    
        </div>                
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="Carousel" class="carousel slide" data-ride="carousel" data-interval="4000">
                <!-- Carousel items -->
                <div class="carousel-inner">
                   <?php 
                   $i=1;
                   foreach ($featuredActivityObj as $key => $value) { 
                    $active_class = '';
                    if($i == 1){
                        $active_class = 'active';
                    }
                    ?>
                        <div class="item <?php echo $active_class; ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="feat-price">
                                        <div class="hovereffect">
                                            <img src="uploads/<?php echo $value->image ?>" class="img-responsive center-block" alt="" style="width:360px; height:200px;">
                                            <div class="overlay">
                                                <h2><?php echo $value->title ?></h2>
                                                <a class="info" href="{{ url('destinations') }}/<?php echo $value->destination_id ?>/<?php echo $value->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="feat-price-tag">
                                                <div class="col-xs-8">
                                                    <h2><?php echo $value->title ?></h2>
                                                </div>
                                                <div class="col-xs-4">
                                                    <p class="text-right"><a href="{{ url('destinations') }}/<?php echo $value->destination_id ?>/<?php echo $value->id ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--.row-->
                        </div><!--.item-->
                    <?php $i++; } ?>
                    <div class="row margin-top-40 margin-bottom-40 margin-xs">
                        <div class="col-md-12">
                            <div class="text-center">
                                <a href="{{ url('destinations') }}" button type="button" class="text-center black_btn text-uppercase">Explore Now</a>
                            </div>
                        </div>
                    </div>
                </div><!--.carousel-inner-->
            </div><!--.Carousel-->
            <a data-slide="prev" href="#Carousel" class="left carousel-control arrow-left-up">
                <span class="glyphicon glyphicon-chevron-left " aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a data-slide="next" href="#Carousel" class="right carousel-control arrow-left-up">
                <span class="glyphicon glyphicon-chevron-right " aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div><!--.container-->