<div class="feature-section hidden-md hidden-lg hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all">
                    <h2 class="text-uppercase text-right"><span class="divider"></span>FEATURED DESTINATIONS</h2>
                </div>                    
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="Carousel12" class="carousel slide" data-ride="carousel" data-interval="4000">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <?php 
                        $i=1;
                        foreach ($featuredDestinationObj as $key => $value) { 
                            $active_class = '';
                            if($i==1){
                                $active_class = 'active';
                            }
                            ?>
                            <div class="item <?php echo $active_class; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="feat-price">
                                            <div class="hovereffect">
                                                <img src="uploads/<?php echo $value->image ?>" class="img-responsive center-block" alt="" style="width: 360px; height: 200px;">
                                                <div class="overlay">
                                                    <h2 class="feat-heading"><?php echo ucwords($value->title) ?></h2>
                                                </div>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                        <?php $i++; } ?>
                        <div class="row margin-top-40 margin-bottom-40 margin-xs">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <a href="{{ url('destinations') }}" button type="button" class="text-center black_btn text-uppercase">Explore Now</a>
                                </div>
                            </div>
                        </div>
                    </div><!--.carousel-inner-->
                </div><!--.Carousel-->
                <a data-slide="prev" href="#Carousel12" class="left carousel-control arrow-left-up-1">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a data-slide="next" href="#Carousel12" class="right carousel-control arrow-left-up-1">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div><!--.container-->
</div>

<!--mobile view end-->
<div class="feature-section hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all">
                    <h2 class="text-uppercase text-right"><span class="divider"></span>FEATURED DESTINATIONS</h2>
                </div>                    
            </div>                
        </div>
        <div class="row margin-top-40 pad-15">
            <?php foreach ($featuredDestinationObj as $key => $value) {  ?>
                <div class="col-sm-3 pad-right-0">
                    <div class="hovereffect">
                        <img src="uploads/<?php echo $value->image ?>" class="img-responsive center-block" alt="" style="width: 360px; height: 200px;">
                        <div class="overlay">
                            <h2 class="feat-heading"><?php echo ucwords($value->title) ?></h2>
                        </div>
                    </div> 
                </div>
            <?php } ?>
        </div>
        <div class="row margin-top-40 margin-bottom-40 margin-xs">
            <div class="col-md-12">
                <div class="text-center">
                    <a href="{{ url('destinations') }}" button type="button" class="text-center black_btn text-uppercase">Explore Now</a>
                </div>
            </div>
        </div>
    </div>
</div>