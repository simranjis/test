
<div class="container-fluid hidden-lg hidden-md hidden-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">HOW IT WORKS</h2>
            </div>                    
        </div>                
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">

                @foreach($subModuleSectionCollection as $index => $subModuleSectionObj)
                    <div class="col-md-12">
                        <div class="works-heading">
                            <h3 class="text-center">{{$subModuleSectionObj->title}}</h3>
                        </div>
                        <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionObj->image_path)}}" alt="No Image Available" class="img-responsive center-block">
                    </div>
                @endforeach               
            </div>
            <div class="row mt20">
                <div class="col-md-12">
                    <div class="text-center">
                         <a href="{{ url('destination') }}" button type="button" class="text-center black_btn text-uppercase">Explore Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">HOW IT WORKS</h2>
            </div>                    
        </div>                
    </div>
    <div class="row margin-top-40">
        <div class="col-sm-4 col-md-5">
            <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionCollection[0]->image_path)}}" alt="No Image Available" class="img-responsive center-block">            
            <div class="works-heading">
                <h3>{{$subModuleSectionCollection[1]->title}}</h3>
            </div>
            <div class="work-heading">
                <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionCollection[2]->image_path)}}" alt="No Image Available" class="img-responsive center-block">
            </div>
            <div class="works-heading">
                <h3>{{$subModuleSectionCollection[3]->title}}</h3>
            </div>
            <div class="work-heading work-heading-margin">
                <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionCollection[4]->image_path)}}" alt="No Image Available" class="img-responsive center-block">
            </div>
        </div>
        <div class="col-sm-4 col-md-2 hidden-xs">
            <img src="images/works-line.png" alt="John" class="img-responsive center-block">
        </div>
        <div class="col-sm-4 col-md-5">
            <div class="works-heading">
                <h3>{{$subModuleSectionCollection[0]->title}}</h3>
            </div>
            <div class="work-heading">
                <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionCollection[1]->image_path)}}" alt="No Image Available" class="img-responsive">
            </div>
            <div class="works-heading">
                <h3>{{$subModuleSectionCollection[2]->title}}</h3>
            </div>
            <div class="work-heading">
                <img src="{{URL::asset('/uploads/how_it_works/'.$subModuleSectionCollection[3]->image_path)}}" alt="No Image Available" alt="John" class="img-responsive center-block">
            </div>
            <div class="works-heading work-heading-margin">
                 <h3>{{$subModuleSectionCollection[4]->title}}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <button type="button" class="text-center black_btn text-uppercase">Explore Now</button>
            </div>
        </div>
    </div>
</div>