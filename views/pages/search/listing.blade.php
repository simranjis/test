@extends('layouts.default')
@section('content')
<style>
    .grid-item
    {
        width: 280px; 
        float: left;
    }
    .grid {
        margin: 0 auto;
    }
    /* clearfix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }
</style>
<div class="destination-breadcrumbs">
</div>
<div class="">
    <div class="container">
        <div class="row mt10">
            <div class="col-md-6">
                <div class="well">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="images/BALI-ULUWATU.png" class="img-responsive">
                        </div>
                        <div class="col-md-8">
                            <h4>Vientiane Cycling Retreat</h4>
                            <p>Discover Vientiane on this fantastic cycling retreat,</p>
                            <div class="text-right mt20">
                                <a href="booking.php" button="" type="button" class="text-center pink_btn_checkout  text-uppercase fixed-at-bottom">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="images/BALI-ULUWATU.png" class="img-responsive">
                        </div>
                        <div class="col-md-8">
                            <h4>Vientiane Cycling Retreat</h4>
                            <p>Discover Vientiane on this fantastic cycling retreat,</p>
                            <div class="text-right mt20">
                                <a href="booking.php" button="" type="button" class="text-center pink_btn_checkout  text-uppercase fixed-at-bottom">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 280,
        isFitWidth: true,
        "gutter": 10
    });
</script>
@stop

