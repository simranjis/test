<div class="tab-destination-1" id="adv-search-dropdown" style="display:none;">
    <div class="container-custom container-custom-home"> 


        <div id="adv-search-results" >
           
                    
        </div>

        <div id="adv-search-suggestions">


            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-all-dest">
                                <h2 class="text-uppercase">Browse by</h2>
                            </div>                    
                        </div>                
                    </div>
                    <ul class="nav nav-pills nav-stacked pill-dest">
                        @if(count(Search::getRegions()))
                        @foreach(Search::getRegions() as $index => $region)
                        <li @if($region['id'] == 1) class="active" @endif><a  href="#tab_{{ $index }}" data-toggle="pill">{{ strtoupper($region['title']) }}</a></li>                                   
                        @endforeach                                      
                        @endif    

                    </ul>
                </div>
                <div class="col-sm-8 col-md-9">                
                    <div class="tab-content">
                        @if(count(Search::getRegions()))
                        @foreach(Search::getRegions() as $index => $region)
                        <div class="tab-pane @if($region['id'] == 1) active @endif" id="tab_{{ $index }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in {{ strtoupper($region['title']) }}</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">

                                @foreach($region->destination as $destination)

                                <div class="col-md-4 col-sm-6">
                                    <div class="city-text-heading-1">
                                        <a href="{{ url('destinations' , $destination['id']) }}">
                                            @if($destination['image'])
                                            <img src="uploads/{{ $destination['image'] }}" class="img-responsive center-block" alt="">
                                            @else
                                            <img src="images/missing-image-640x360-1-360x180.png" class="img-responsive center-block" alt="">
                                            @endif
                                            <div class="overlay">
                                                <h2 class="feat-heading">{{ $destination['title'] }}</h2>
                                                <p>
                                                    {{ $destination->country->countries_name }}
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                @endforeach

                            </div>
                        </div>    
                        @endforeach
                        @endif
                    </div><!-- tab content -->
                </div>
            </div>            



            
        </div>

        
    </div><!-- end of container -->
</div>
