@extends('layouts.default')
@section('content')
<style>

    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #E9004C;
        font-size: 14px;
    }

    .submenu li { border-bottom: 1px solid #4b4a5e; }
    .submenu li {
        color: #fff;
        padding: 20px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);;
    }
    .label-font {
        font-family: 'gothamregular' !important;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .activity-pay-breadcrumbs h2 {
            color: #fff;
            font-family: "gothambold";
            font-size: 34px;
        }

    }

    #clouds{
        padding: 197px 0;
        background: #c9dbe9;
        background: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);
        background: -linear-gradient(top, #c9dbe9 0%, #fff 100%);
        background: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);
    }

    /*Time to finalise the cloud shape*/
    .cloud {
        width: 200px; 
        height: 60px;
        background: #fff;

        border-radius: 200px;
        -moz-border-radius: 200px;
        -webkit-border-radius: 200px;

        position: relative; 
        top:-367px;
    }

    .cloud:before, .cloud:after {
        content: '';
        position: absolute; 
        background: #fff;
        width: 100px; height: 80px;
        position: absolute; top: -15px; left: 10px;

        border-radius: 100px;
        -moz-border-radius: 100px;
        -webkit-border-radius: 100px;

        -webkit-transform: rotate(30deg);
        transform: rotate(30deg);
        -moz-transform: rotate(30deg);
    }

    .cloud:after {
        width: 120px; height: 120px;
        top: -55px; left: auto; right: 15px;
    }

    /*Time to animate*/
    .x1 {
        -webkit-animation: moveclouds 15s linear infinite;
        -moz-animation: moveclouds 15s linear infinite;
        -o-animation: moveclouds 15s linear infinite;
    }

    /*variable speed, opacity, and position of clouds for realistic effect*/
    .x2 {
        left: 200px;

        -webkit-transform: scale(0.6);
        -moz-transform: scale(0.6);
        transform: scale(0.6);
        opacity: 0.6; /*opacity proportional to the size*/

        /*Speed will also be proportional to the size and opacity*/
        /*More the speed. Less the time in 's' = seconds*/
        -webkit-animation: moveclouds 25s linear infinite;
        -moz-animation: moveclouds 25s linear infinite;
        -o-animation: moveclouds 25s linear infinite;
    }

    .x3 {
        left: -250px; top: -200px;

        -webkit-transform: scale(0.8);
        -moz-transform: scale(0.8);
        transform: scale(0.8);
        opacity: 0.8; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 20s linear infinite;
        -moz-animation: moveclouds 20s linear infinite;
        -o-animation: moveclouds 20s linear infinite;
    }

    .x4 {
        left: 470px; top: -250px;

        -webkit-transform: scale(0.75);
        -moz-transform: scale(0.75);
        transform: scale(0.75);
        opacity: 0.75; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 18s linear infinite;
        -moz-animation: moveclouds 18s linear infinite;
        -o-animation: moveclouds 18s linear infinite;
    }

    .x5 {
        left: -150px; top: -150px;

        -webkit-transform: scale(0.8);
        -moz-transform: scale(0.8);
        transform: scale(0.8);
        opacity: 0.8; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 20s linear infinite;
        -moz-animation: moveclouds 20s linear infinite;
        -o-animation: moveclouds 20s linear infinite;
    }

    @-webkit-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    @-moz-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    @-o-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    .test {
        min-height: 540px;
        position: relative;
    }    
</style>
<div class="who-are-breadcrumbs">
    <div class="container">
        <img src="images/who-are-we.png" class="img-responsive center-block" alt="who">
    </div>
</div>
<div class="who-background hidden-lg hidden-md hidden-sm" style="min-height: 735px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="who-are-text text-center">
                    <?php if(empty($whoWeAre->content)){ ?>
                        <p class="text-center">We live in a world that despite the differences, people are made to seek, explore & reach out. </p>
                        <p class="text-center">With everything made available within our reach through mobile phones and laptops, we want independence to find our own sense adventure. From planned to unplanned spontaneous trips, we want our experience to be as authentic and genuine as possible and there are  still a lot of destinations that have so much more to offer; local businesses that deserve more exposure,and even local individuals themselves have something unique to share.  </p>
                        <p class="text-center">We created this platform to make finding a whole lot easier for travelers and whether you are an established business, a local business or a local, YOU are welcome to share your experience.  </p>
                        <p class="text-center">“ Adventure ”is subjective. Whether you want to sip a nice cup of espresso with a spectacular view of the Eiffel Tower where only locals know or facing your fear of heights in Lex Luthor:  Drop of Doom at Six Flags, learn how to cook local Vietnamese dishes, swim wit the whale sharks, trek the Everest, discover an off-beaten paradise alone just a few minutes boat ride away from Bali, or simply meet every child’s bucket list: Mickey Mouse. You define your adventure.  </p>
                        <p class="text-center">You’ll be amazed by how refreshingly beautiful every destination is in their own way, how rich every culture is across the globe, and how kindness is still evidently  universal.Just go and you’ll be surprised by how brave and capable you are in letting yourself EXPERIENCE. </p>
                    <?php }else{
                        echo $whoWeAre->content;
                    } ?>
                </div>
            </div>
        </div>
        <div clasa="row">
            <div class="col-md-12 text-center mt20">
                <div class="destiny-button">
                    <a href="destination.php" type="button" class="text-center black_btn text-uppercase">WHAT DO YOU WANT TO DO?</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hidden-xs">
    <div class="test">
        <div id="clouds">
            <div class="cloud x1"></div>
            <!-- Time for multiple clouds to dance around -->
            <div class="cloud x2"></div>
            <div class="cloud x3"></div>
            <div class="cloud x4"></div>
            <div class="cloud x5"></div>
        </div>
        <div class="who-background"  style="min-height: 735px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="who-are-text text-center">
                            <?php if(empty($whoWeAre->content)){ ?>
                                <p>We live in a world that despite the differences, people are made to seek, explore & reach out. </p>
                                <p>With everything made available within our reach through mobile phones and laptops, we want independence to find our own sense adventure. From planned to unplanned spontaneous trips, we want our experience to be as authentic and genuine as possible and there are  still a lot of destinations that have so much more to offer; local businesses that deserve more exposure,and even local individuals themselves have something unique to share.  </p>
                                <p>We created this platform to make finding a whole lot easier for travelers and whether you are an established business, a local business or a local, YOU are welcome to share your experience.  </p>
                                <p>“ Adventure ”is subjective. Whether you want to sip a nice cup of espresso with a spectacular view of the Eiffel Tower where only locals know or facing your fear of heights in Lex Luthor:  Drop of Doom at Six Flags, learn how to cook local Vietnamese dishes, swim wit the whale sharks, trek the Everest, discover an off-beaten paradise alone just a few minutes boat ride away from Bali, or simply meet every child’s bucket list: Mickey Mouse. You define your adventure.  </p>
                                <p>You’ll be amazed by how refreshingly beautiful every destination is in their own way, how rich every culture is across the globe, and how kindness is still evidently  universal.Just go and you’ll be surprised by how brave and capable you are in letting yourself EXPERIENCE. </p>
                            <?php }else{
                                echo $whoWeAre->content;
                            } ?>
                        </div>
                    </div>
                </div>
                <div clasa="row">
                    <div class="col-md-12 text-center mt20">
                        <div class="destiny-button">
                            <a href="{{ url('destinations') }}" type="button" class="text-center black_btn text-uppercase">WHAT DO YOU WANT TO DO?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    $(function () {
        var card = new Card({
            // a selector or DOM element for the form where users will
            // be entering their information
            form: '#payment-form', // *required*
            // a selector or DOM element for the container
            // where you want the card to appear
            container: '.card-wrapper', // *required*

            formSelectors: {
                numberInput: 'input.user_card_number', // optional â€” default input[name="number"]
                cvcInput: 'input.user_card_cvv', // optional â€” default input[name="cvc"]
                nameInput: 'input.user_card_name', // optional - defaults input[name="name"],
                expiryInput: 'input.user_expiry_month'
            },
            width: 200, // optional â€” default 350px
            formatting: true, // optional - default true

            // Strings for translation - optional
            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },
            // Default placeholders for rendered fields - optional
            placeholders: {
                user_card_number: 'â€¢â€¢â€¢â€¢ â€¢â€¢â€¢â€¢ â€¢â€¢â€¢â€¢ â€¢â€¢â€¢â€¢',
            },
            // if true, will log helpful messages for setting up Card
            debug: false // optional - default false
        });
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                    $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            }
            ;
        }

        var accordion = new Accordion($('#accordion'), false);
    });

</script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })
</script>
<script>
    var waypoints = $('#destroy-gallery-upperbox').waypoint(function (direction) {
        if (direction == "down") {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    }, {
        offset: '40%'
    })
</script>
<script>

    //    $(function () {
    //        $('.carousel-custom').carousel-custom({
    //            pause: true, // init without autoplay (optional)
    //            interval: false, // do not autoplay after sliding (optional)
    //            wrap: false // do not loop
    //        });
    //        // left control hide
    //        //$('.carousel').children('.left.carousel-control').hide();
    //    });
    //    $('.carousel-custom').on('slid.bs.carousel', function () {
    //        var carouselData = $(this).data('bs.carousel');
    //        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
    //        $(this).children('.carousel-control-custom').show();
    //        if (currentIndex == 0) {
    //            $(this).children('.left.carousel-control-custom').fadeOut();
    //        } else if (currentIndex + 1 == carouselData.$items.length) {
    //            $(this).children('.right.carousel-control-custom').fadeOut();
    //        }
    //    });

    $('.carousel-custom .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>
@stop
