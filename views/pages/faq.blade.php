@extends('layouts.default')
@section('content')

@if(!empty($travellerFaqArr) || !empty($localGuideBadgeFaqArr)) 



<style>

    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #fff;
        font-size: 14px;
    }

    .submenu li {
        border-bottom: 1px solid #E9004C;
    }
    .submenu li {
        color: #000;
        padding: 9px 40px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);;
    }
    .label-font {
        font-family: 'gothamregular' !important;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .activity-pay-breadcrumbs h2 {
            color: #fff;
            font-family: "gothambold";
            font-size: 34px;
        }

    }

    #clouds{
        padding: 197px 0;
        background: #c9dbe9;
        background: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);
        background: -linear-gradient(top, #c9dbe9 0%, #fff 100%);
        background: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);
    }

    /*Time to finalise the cloud shape*/
    .cloud {
        width: 200px; height: 60px;
        background: #fff;

        border-radius: 200px;
        -moz-border-radius: 200px;
        -webkit-border-radius: 200px;

        position: relative; 
    }

    .cloud:before, .cloud:after {
        content: '';
        position: absolute; 
        background: #fff;
        width: 100px; height: 80px;
        position: absolute; top: -15px; left: 10px;

        border-radius: 100px;
        -moz-border-radius: 100px;
        -webkit-border-radius: 100px;

        -webkit-transform: rotate(30deg);
        transform: rotate(30deg);
        -moz-transform: rotate(30deg);
    }

    .cloud:after {
        width: 120px; height: 120px;
        top: -55px; left: auto; right: 15px;
    }

    /*Time to animate*/
    .x1 {
        -webkit-animation: moveclouds 15s linear infinite;
        -moz-animation: moveclouds 15s linear infinite;
        -o-animation: moveclouds 15s linear infinite;
    }

    /*variable speed, opacity, and position of clouds for realistic effect*/
    .x2 {
        left: 200px;

        -webkit-transform: scale(0.6);
        -moz-transform: scale(0.6);
        transform: scale(0.6);
        opacity: 0.6; /*opacity proportional to the size*/

        /*Speed will also be proportional to the size and opacity*/
        /*More the speed. Less the time in 's' = seconds*/
        -webkit-animation: moveclouds 25s linear infinite;
        -moz-animation: moveclouds 25s linear infinite;
        -o-animation: moveclouds 25s linear infinite;
    }

    .x3 {
        left: -250px; top: -200px;

        -webkit-transform: scale(0.8);
        -moz-transform: scale(0.8);
        transform: scale(0.8);
        opacity: 0.8; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 20s linear infinite;
        -moz-animation: moveclouds 20s linear infinite;
        -o-animation: moveclouds 20s linear infinite;
    }

    .x4 {
        left: 470px; top: -250px;

        -webkit-transform: scale(0.75);
        -moz-transform: scale(0.75);
        transform: scale(0.75);
        opacity: 0.75; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 18s linear infinite;
        -moz-animation: moveclouds 18s linear infinite;
        -o-animation: moveclouds 18s linear infinite;
    }

    .x5 {
        left: -150px; top: -150px;

        -webkit-transform: scale(0.8);
        -moz-transform: scale(0.8);
        transform: scale(0.8);
        opacity: 0.8; /*opacity proportional to the size*/

        -webkit-animation: moveclouds 20s linear infinite;
        -moz-animation: moveclouds 20s linear infinite;
        -o-animation: moveclouds 20s linear infinite;
    }

    @-webkit-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    @-moz-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    @-o-keyframes moveclouds {
        0% {margin-left: 1000px;}
        100% {margin-left: -1000px;}
    }
    .test {
        min-height: 540px;
        position: relative;
    }
    body{
        background-color: #F5F5F5;
    }
</style>
<div class="contact-breadcrumbs">
    <img src="images/faq.png" class="img-responsive" alt="">
</div>
<div class="accordion-well-background">
    <div class="container">
        <div class="row">            
            <div class="col-sm-12">
                <div class="accordion-well ">
                    <ul id="accordion" class="accordion list-unstyled">
                        @if(!empty($travellerFaqArr))    
                            @foreach($travellerFaqArr as $index => $travellerFaqDetailArr)
                            
                                <li>
                                    <div class="link"><i class="fa fa-hand-o-right" aria-hidden="true"></i>{!!$travellerFaqDetailArr['question']!!}<i class="fa fa-chevron-down"></i></div>
                                    <ul class="submenu list-unstyled">
                                        <li>
                                            {!!$travellerFaqDetailArr['answer']!!}
                                        </li>
                                    </ul>
                                </li>
                            @endforeach
                        @endif

                        @if(!empty($localGuideBadgeFaqArr))  
                            <div class="mt20">
                                <h4>LOCAL GUIDE BADGE  </h4>
                            </div>  
                            @foreach($localGuideBadgeFaqArr as $index => $localGuideBadgeDetailFaqArr)
                                <li>
                                    <div class="link"><i class="fa fa-hand-o-right" aria-hidden="true"></i>{!!$localGuideBadgeDetailFaqArr['question']!!}<i class="fa fa-chevron-down"></i></div>
                                    <ul class="submenu list-unstyled">
                                        <li>
                                        {!!$localGuideBadgeDetailFaqArr['answer']!!}
                                         </li>
                                    </ul>
                                </li>
                            @endforeach
                        @endif
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                    $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            }
            ;
        }

        var accordion = new Accordion($('#accordion'), false);
    });

</script>
<script>

    //    $(function () {
    //        $('.carousel-custom').carousel-custom({
    //            pause: true, // init without autoplay (optional)
    //            interval: false, // do not autoplay after sliding (optional)
    //            wrap: false // do not loop
    //        });
    //        // left control hide
    //        //$('.carousel').children('.left.carousel-control').hide();
    //    });
    //    $('.carousel-custom').on('slid.bs.carousel', function () {
    //        var carouselData = $(this).data('bs.carousel');
    //        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
    //        $(this).children('.carousel-control-custom').show();
    //        if (currentIndex == 0) {
    //            $(this).children('.left.carousel-control-custom').fadeOut();
    //        } else if (currentIndex + 1 == carouselData.$items.length) {
    //            $(this).children('.right.carousel-control-custom').fadeOut();
    //        }
    //    });

    $('.carousel-custom .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>

@endif

@stop
