<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>[SUBJECT]</title>
        <style type="text/css">
            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            p, h2{
                margin:0;
            }

            div,p,ul,h2,h2{
                margin:0;
            }

            h2.bigger,h2.bigger{
                font-size: 32px;
                font-weight: normal;
            }

            h2.big,h2.big{
                font-size: 21px;
                font-weight: normal;
            }

            a.link1{
                color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
            }

            a.link2{
                padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
            }

            a.link3{
                background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
            }
            .bgBody{
                background: #F6F6F6;
            }
            .bgItem{
                background: #ffffff;
            }

            @media only screen and (max-width:480px)

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }   
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

            }

            @media only screen and (max-width:540px) 

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }       
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

                .font{
                    font-size:15px !important;
                    line-height:19px !important;

                }
            }


        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"F6F6F6",
            "link":"62A9D2",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"555555"
            }
        </script>

    </head>
    <body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" style="margin-left:5px; margin-right:5px; margin-top:0px; margin-bottom:0px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:helvetica, sans-serif;'>
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td height='25' bgcolor='#333e48' colspan='3'></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" width="600"><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer" bgcolor="#ffffff">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class='movableContentContainer'>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td bgcolor='#333e48' valign='top'>
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                            <tr>
                                                                                <td align='center' valign='middle' >
                                                                                    <div class="contentEditableContainer contentImageEditable">
                                                                                        <div class="contentEditable" >
                                                                                            <img src="{{ url('images/logo-email.png') }}" alt='Compagnie logo' data-default="placeholder" data-max-width="300" width='350'>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='25' bgcolor='#333e48'></td></tr>

                                                                <tr><td height='5' bgcolor='#333e48'></td></tr>

                                                                <tr><td height=12' bgcolor="#333e48"></td></tr>
                                                                <tr><td height=20' bgcolor="#fff"></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p>WELCOME TO DESTINATION SEEKER! We’re excited to have you as one of our Local Guide Hosts! </p>
                                                                                                    <p>Password: <b><?php echo $new_password; ?></b>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <p style="font-size:20px; color:#565656">View your Account and post your first activity!</p>

                                                                                        </td>
                                                                                        <td style="padding-bottom:8px;" valign="middle" align="right">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable">
                                                                                                    <a target="_blank" class="link2" href="{{ url('/faq') }}" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">View</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='20'></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td>
                                                                        <div class='contentEditableContainer contentImageEditable'>
                                                                            <div class="contentEditable">
                                                                                <img class="banner" src="{{ url('images/ab.jpg') }}" alt='What we do' data-default="placeholder" data-max-width="600" width='600' height='139' >
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top' class='' style="background:#333e48;" >

                                                                <tr><td height='28'></td></tr>

                                                                <tr>
                                                                    <td valign='top' align='center'>
                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                            <div class="contentEditable" style='color:#A8B0B6; font-size:13px;line-height: 16px;'>
                                                                                <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr><td height='28'></td></tr>
                                                            </table>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>      
    </body>
</html>