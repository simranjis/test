 @extends('emails.layout.email_layout')
 @section('email_content')
 
        
      <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody><tr>

                    <td class="specbundle2" width="18" valign="top">&nbsp;</td>
                </tr>

            </tbody>
        </table>
    </div>
        

        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                    <tr>
                        <td class="spechide" width="16">&nbsp;</td>
                        <td class="specbundle2" width="568">
                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                <tbody>

                                    <tr>
                                        <td valign="top" align="left">
                                            <div class="contentEditableContainer contentTextEditable">
                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                    <p>We received your submission! Your activity will be subject for review and we will send you within 72 hours a confirmation once your activity is live!  </p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr><td height="20">&nbsp;</td></tr>
                                </tbody>
                            </table>
                        </td>
                        <td class="spechide" width="16">&nbsp;</td>
                    </tr>
                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                </tbody>
            </table>
        </div>
       
        
       <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                    <tr>
                        <td class="spechide" width="16">&nbsp;</td>
                        <td class="specbundle2" width="568">
                            <table valign="top" style="padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                <tbody>
                                    <tr><td height="16">&nbsp;</td></tr>
                                    <tr>
                                        <td style="padding-bottom:8px;" valign="middle" align="center">
                                            <div class="contentEditableContainer contentTextEditable">
                                                <div class="contentEditable">
                                                    <a target="_blank" class="link2" href="{{url('/localguide/myactivities')}}" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">View My Activities</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td class="spechide" width="16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" height="16">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>

    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
            <tbody><tr><td height="20"></td></tr>
        </tbody></table>
    </div>

@endSection