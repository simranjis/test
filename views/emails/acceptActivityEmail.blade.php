<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            p, h2{
                margin:0;
            }

            div,p,ul,h2,h2{
                margin:0;
            }

            h2.bigger,h2.bigger{
                font-size: 32px;
                font-weight: normal;
            }

            h2.big,h2.big{
                font-size: 21px;
                font-weight: normal;
            }

            a.link1{
                color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
            }

            a.link2{
                padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
            }

            a.link3{
                background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
            }
            .bgBody{
                background: #F6F6F6;
            }
            .bgItem{
                background: #ffffff;
            }

            @media only screen and (max-width:480px)

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }	
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

            }

            @media only screen and (max-width:540px) 

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }		
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

                .font{
                    font-size:15px !important;
                    line-height:19px !important;

                }
            }


        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"F6F6F6",
            "link":"62A9D2",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"555555"
            }
        </script>

    </head>
    <body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" cz-shortcut-listen="true">
        <table class="tableContent bgBody" style="font-family:helvetica, sans-serif;" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td colspan="3" bgcolor="#333e48" height="25"></td>
                </tr>
                <tr>
                    <td><table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="spechide" valign="top">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="600" valign="top"><table class="MainContainer" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="600" align="center">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class="movableContentContainer">
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                    <td bgcolor="#333e48" valign="top">
                                                                        <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="middle" align="center">
                                                                                        <div class="contentEditableContainer contentImageEditable">
                                                                                            <div class="contentEditable">
                                                                                                <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        
                                                        <!-- middle content -->
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#333e48" height="25"></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td bgcolor="#333e48" height="5"></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td bgcolor="#333e48" height="12'"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td bgcolor="#fff" height="20'"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3" height="15"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>

                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3" height="15"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="251" align="center">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <!-- Add Content Here -->
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p style="font-weight:bold; font-size:24px;">Hi {{ $data_array->booking->traveller->firstname }} {{ $data_array->booking->traveller->lastname }}, </p>
                                                                                                    <p style="">Your booking {{ $data_array->activity->title }} is <span style="font-weight:bold;">CONFIRMED</span></p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>                                                                                    
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                                                                                    <h2 class="big" style="font-weight:bold;">Your Booking Summary:</h2>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p>{{ $data_array->activity->title }}</p>
                                                                                                    
                                                                                                    @if(count($data_array->bookings_items))
                                                                                                        @foreach($data_array->bookings_items as $items => $pricing)
                                                                                                            <p>{{ ucwords($pricing['pricefor']) }} ({{ $data_array->booking->currency->code }} {{ $pricing['price'] }} per {{ $pricing['type'] }}) x {{ $pricing['quantity'] }} = {{ $data_array->booking->currency->code }} {{ $pricing['price']*$pricing['quantity'] }}</p>
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                                    
                                                                                                    <div>
                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Subtotal: {{ $data_array->booking->currency->code }} {{ number_format($data_array->subtotal,2) }}</p>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Service Fee: {{ $data_array->booking->currency->code }} {{ number_format($data_array->tax,2) }}</p>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Amount to be paid to Local Guide: {{ $data_array->booking->currency->code }} {{ number_format($data_array->localguide_amount,2) }}</p>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Total Tour Price: {{ $data_array->booking->currency->code }} {{ number_format($data_array->total,2) }}</p>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Amount to be paid today: {{ $data_array->booking->currency->code }} {{ number_format($data_array->commision_amount,2) }}</p>
                                                                                                    </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-bottom:8px;" valign="center" align="center">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable">
                                                                                                    <a target="_blank" class="link2" href="{{ url('traveller/pay', [$hash]) }}" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">Pay Now</a>
                                                                                                </div>
                                                                                                <div>
                                                                                                    <strong>Note</strong>: The link will be expired after 48 hours
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="border:2px solid #9b9b9b; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;">
                                                                                                    <p style="font-weight:bold; margin-top:15px">For bookings with INSTANT &amp; 24 HOURS CONFIRMATION: You can expect your Mobile E - voucher within 1 business day </p>

                                                                                                    <p style="font-weight:bold; margin-top:15px"> For bookings with 48 HOURS CONFIRMATION: You can expect your Mobile E-voucher within 2 business days </p>
                                                                                                    <p style="font-weight:bold; margin-top:15px">Rest assured your credit card will not be charged unless the booking is 100% CONFIRMED and is AFTER the FREE cancelation period of your activity.  </p>
                                                                                                    <p style="font-weight:bold; margin-top:15px">In the event you don’t receive any response from us, please check your Spam folder or you may contact us at support@seeglobalexperiencelocal.com </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid red; padding: 0 15px;">
                                                                                                    <p>Note: If your activity has a FREE CANCELATION period, you will only be charged AFTER that specified period and will receive your INVOICE accordingly.  </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p style="font-weight:bold; font-size:24px;">We look forward to having you back with your travels! Have loads of fun!</p>
                                                                                                    <p style="">Cheers, <br>
                                                                                                            Destination Seeker Team
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr><td height="20"></td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentImageEditable">
                                                                                <div class="contentEditable">
                                                                                    <img class="banner" src="{{ url('images/ab.jpg') }}" alt="What we do" data-default="placeholder" data-max-width="600" width="600" height="139">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- Bottom Part -->
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" class="" style="background:#333e48;" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="28"></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td valign="top" align="center">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable" style="color:#A8B0B6; font-size:13px;line-height: 16px;">
                                                                                    <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td height="28"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="spechide" valign="top"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>      
    

</body></html>