 @extends('emails.layout.email_layout')
 @section('email_content')
 

 <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
<table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
    <td class="specbundle2" width="291" valign="top">
        <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody><tr><td colspan="3" height="15"></td></tr>

                <tr>
                    <td width="20"></td>
                    <td width="251">
                        <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                    <td>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                <p class="big">
                                                    Booking Date: {{ date('Y-m-d', strtotime($bookingActivityObj->bookingdate)) }}   
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td height="16"></td></tr>
                                <tr><td height="16"></td></tr>
                            </tbody></table>
                    </td>
                    <td width="20"></td>
                </tr>

                <tr><td colspan="3" height="15"></td></tr>
            </tbody></table>
    </td>

    <td class="specbundle2" width="18" valign="top">&nbsp;</td>

    <td class="specbundle2" width="291" valign="top">
        <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody><tr><td colspan="3" height="15"></td></tr>

                <tr>
                    <td width="20"></td>
                    <td width="251">
                        <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                    <td>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                <p class="big">
                                                Reference Number: {{$bookingActivityObj->booking->bookings_reference}}
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="20"></td>
                </tr>

                <tr><td colspan="3" height="15"></td></tr>
            </tbody>
        </table>
    </td>
</tr>

</tbody>
</table>
</div>


<div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
    <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
            <tr>
                <td class="spechide" width="16">&nbsp;</td>
                <td class="specbundle2" width="568">
                    <table valign="top" style="border:2px solid #9b9b9b; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                            <tr>
                                <td valign="top" align="left">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                            <h2 class="big" style="font-weight:bold;">Your Booking Summary:</h2>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr><td height="16">&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="left">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                            <p>Activity Name: {{ $bookingActivityObj->activity->title }}</p>

                                            <p>Activity Date {{ date('Y-m-d', strtotime($bookingActivityObj->bookingdate)) }}  </p>

                                            <p>Quantity </p>

                                            <p> Price Breakdown 
                                            
                                            <br>
                                            <!-- Sub Total :  -->
                                            <br>
                                            <!-- Tax: -->
                                            
                                            <?php if(!empty($dataArr['promo_code_applied'])){ ?>
                                            <br>
                                            Discount : TO DO
                                            <?php } ?>
                                            </p>

                                            <p>Total Price: </p>
                                            <p>Traveler Details</p>
                                            <p>
                                               <!--  First Name :  <br>
                                                Last Name :  <br>
                                                Phone :  <br>
                                                Email :  <br> -->
                                                
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr><td height="20">&nbsp;</td></tr>
                            <tr>
                                <td style="padding-bottom:8px;" valign="center" align="center">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable">
                                            <a href="{{url('/localguide/changeBookingActivtyStatus?booking_activity_id='.$bookingActivityObj->id.'?status=1')}}" class="link2" href="#" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">Accept - NOT WORKING</a>
                                            <a href="{{url('/localguide/changeBookingActivtyStatus?booking_activity_id='.$bookingActivityObj->id.'?status=0')}}" class="link2" href="#" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">Decline - NOT WORKING</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td class="spechide" width="16">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" height="16">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>        
      

      <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                                                                <tbody><tr><td height="20"></td></tr>
                                                            </tbody></table>
                                                        </div>

@endSection