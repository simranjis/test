

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>[SUBJECT]</title>
        <style type="text/css">

            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            p, h2{
                margin:0;
            }

            div,p,ul,h2,h2{
                margin:0;
            }

            h2.bigger,h2.bigger{
                font-size: 32px;
                font-weight: normal;
            }

            h2.big,h2.big{
                font-size: 21px;
                font-weight: normal;
            }

            a.link1{
                color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
            }

            a.link2{
                padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
            }

            a.link3{
                background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
            }
            .bgBody{
                background: #F6F6F6;
            }
            .bgItem{
                background: #ffffff;
            }
            .table-border tr {
                margin: 0 0 16px 0 !important;
                display: block;
                width: 100%;
                border-bottom: 1px solid #e3e3e3;
                padding: 0px 0 16px 0px;
            }
            .table-border tr td{
                padding: 0 10px;
            }
            @media only screen and (max-width:480px)

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }	
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

            }

            @media only screen and (max-width:540px) 

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }		
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

                .font{
                    font-size:15px !important;
                    line-height:19px !important;

                }
            }


        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"F6F6F6",
            "link":"62A9D2",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"555555"
            }
        </script>

    </head>
    <body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" style="margin-left:5px; margin-right:5px; margin-top:0px; margin-bottom:0px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:helvetica, sans-serif;'>
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td height='25' bgcolor='#333e48' colspan='3'></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" width="600"><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer" bgcolor="#ffffff">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class='movableContentContainer'>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td bgcolor='#333e48' valign='top'>
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                            <tr>
                                                                                <td align='center' valign='middle' >
                                                                                    <div class="contentEditableContainer contentImageEditable">
                                                                                        <div class="contentEditable" >
                                                                                            <img src="{{url('/images/logo-email.png')}}" alt='Compagnie logo' data-default="placeholder" data-max-width="300" width='350'>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='25' bgcolor='#333e48'></td></tr>

                                                                <tr><td height='5' bgcolor='#333e48'></td></tr>

                                                                <tr><td height=12' bgcolor="#333e48"></td></tr>
                                                                <tr><td height=20' bgcolor="#fff"></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p style="font-weight:bold;">Your Booking Summary</p>
                                                                                                    <p style="font-weight:bold; font-size: 24px;">Hi, <?php echo $data_array['users']['firstname'] . ' ' . $data_array['users']['lastname']; ?></p>
                                                                                                    <p style="">Thank you for booking with Destination Seeker! We have received your booking. Your voucher will be sent in a separate e-mail once your booking is confirmed. </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" class="table-actual table-border" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <thead>                                                                    
                                                                    <tr>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" align="center">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                                                                    <h2 class="big" style="font-weight:bold;">Booking Summary</h2>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <?php foreach ($data_array['activities'] as $activity) { ?>
                                                                        <tr>
                                                                            <td class="spechide" width="16">&nbsp;</td>
                                                                            <td><img src="{{url('/uploads/'.$activity->image)}}" alt="" style="width: 140px;"/></td>
                                                                            <td>
                                                                                <p style="color:#000; font-weight: bold; font-size: 19px; margin:0 0 10px 0;">Activity Name: {{$activity->title}}</p>
                                                                                @foreach($activity['activityPricing'] as $pricing)
                                                                                <p style="color:#989898; font-size: 18px;">User : {{$pricing->pricefor}}</p>
                                                                                <p style="color:#989898; font-size: 18px;">Date : {{date('m/d/Y',strtotime($activity->date))}}</p>
                                                                                <p style="color:#989898; font-size: 18px;">Units : {{$pricing->quantity}}</p>
                                                                                @endforeach
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" class="bgItem" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>

                                                                    <tr><td colspan="4" height="15"></td></tr>
                                                                    <tr>
                                                                        <td style="padding:0 0 0 20px;" width="238" valign="top">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable">

                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="padding:0 0 0 20px;" width="238" valign="top">
                                                                            <table valign="top" style="color:#6f777c; font-size: 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr align="right">
                                                                                        <td>Order Subtotal </td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format(($data_array['totalAmount'] + $data_array['discountAmount'] - $data_array['serviceFee']),2)}} </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" height="8"></td>
                                                                                    </tr>
                                                                                    <tr align="right">
                                                                                        <td>Promo Code Discount </td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format($data_array['discountAmount'],2)}} </td>
                                                                                    </tr>
                                                                                    <tr align="right">
                                                                                        <td>Service Fee </td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format($data_array['serviceFee'],2)}} </td>
                                                                                    </tr>
                                                                                    @if($data_array['localGuideAmount']> 0)
                                                                                    <tr align="right">
                                                                                        <td>Local Guide Amount </td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format($data_array['localGuideAmount'],2)}} </td>
                                                                                    </tr>
                                                                                    @endif
                                                                                    <tr align="right"><td colspan="4" height="20"></td>
                                                                                    </tr>
                                                                                    <tr align="right">
                                                                                        <td>Total Tour Price</td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format($data_array['total'],2)}} </td>
                                                                                    </tr>
                                                                                    <tr align="right">
                                                                                        <td>Amount to be Paid </td>
                                                                                        <td>{{$data_array['currencyCode']}} {{number_format($data_array['commissionAmount'],2)}} </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="4" height="15"></td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="border:2px solid #9b9b9b; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                                                                                    <h2 class="big" style="font-weight:bold;">Note:</h2>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;">
                                                                                                    <p style="font-weight:bold; margin-top:15px">For bookings with INSTANT & 24 HOURS CONFIRMATION: You can expect your Mobile E - voucher within 1 business day </p>

                                                                                                    <p style="font-weight:bold; margin-top:15px"> For bookings with 48 HOURS CONFIRMATION: You can expect your Mobile E-voucher within 2 business days </p>
                                                                                                    <p style="font-weight:bold; margin-top:15px">Rest assured your credit card will not be charged unless the booking is 100% CONFIRMED and is AFTER the FREE cancelation period of your activity.  </p>
                                                                                                    <p style="font-weight:bold; margin-top:15px">In the event you don’t receive any response from us, please check your Spam folder or you may contact us at support@seeglobalexperiencelocal.com </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p style="font-weight:bold; font-size:24px;">We look forward to having you back with your travels! Have loads of fun!</p>
                                                                                                    <p style="">Cheers, </br>
                                                                                                        Destination Seeker Team
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='20'></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td>
                                                                        <div class='contentEditableContainer contentImageEditable'>
                                                                            <div class="contentEditable">
                                                                                <img class="banner" src="{{url('/images/ab.jpg')}}" alt='What we do' data-default="placeholder" data-max-width="600" width='600' height='139' >
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top' class='' style="background:#333e48;" >

                                                                <tr><td height='28'></td></tr>

                                                                <tr>
                                                                    <td valign='top' align='center'>
                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                            <div class="contentEditable" style='color:#A8B0B6; font-size:13px;line-height: 16px;'>
                                                                                <img src="{{url('/images/logo-email.png')}}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr><td height='28'></td></tr>
                                                            </table>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>      
    </body>
</html>
