<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[SUBJECT]</title>
        <style type="text/css">
            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            p, h2{
                margin:0;
            }

            div,p,ul,h2,h2{
                margin:0;
            }

            h2.bigger,h2.bigger{
                font-size: 32px;
                font-weight: normal;
            }

            h2.big,h2.big{
                font-size: 21px;
                font-weight: normal;
            }

            a.link1{
                color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
            }

            a.link2{
                padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
            }

            a.link3{
                background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
            }
            .bgBody{
                background: #F6F6F6;
            }
            .bgItem{
                background: #ffffff;
            }

            @media only screen and (max-width:480px)

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }	
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

            }

            @media only screen and (max-width:540px) 

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }		
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

                .font{
                    font-size:15px !important;
                    line-height:19px !important;

                }
            }


        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"F6F6F6",
            "link":"62A9D2",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"555555"
            }
        </script>

    </head>
    <body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" cz-shortcut-listen="true">
        <table class="tableContent bgBody" style="font-family:helvetica, sans-serif;" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td colspan="3" bgcolor="#333e48" height="25"></td>
                </tr>
                <tr>
                    <td><table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="spechide" valign="top"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="600" valign="top"><table class="MainContainer" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="600" align="center">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class="movableContentContainer">
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody><tr>
                                                                    <td bgcolor="#333e48" valign="top">
                                                                        <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                            <tbody><tr>
                                                                                <td valign="middle" align="center">
                                                                                    <div class="contentEditableContainer contentImageEditable">
                                                                                        <div class="contentEditable">
                                                                                            <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody><tr><td bgcolor="#333e48" height="25"></td></tr>

                                                                <tr><td bgcolor="#333e48" height="5"></td></tr>

                                                                <tr><td bgcolor="#333e48" height="12'"></td></tr>
                                                                <tr><td bgcolor="#fff" height="20'"></td></tr>
                                                            </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody><tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="251" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                                                                                    <p class="big">
                                                                                                                    
                                                                                                                    </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>
                                                                                </tbody></table>
                                                                        </td>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>

                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="251" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                                                                                    <p class="big">{{ $data_array['fullname'] }}
                                                                                                                    </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            <table valign="center" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable" style="margin:0px auto; text-align: center">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold; text-align: center;">
                                                                                                                    
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p>Your Account has been Created! </p>
                                                                                                    
                                                                                                    Email: {{ $data_array['email'] }} 
                                                                                                    <br>
                                                                                                    Password: {{ $password }} 
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody><tr><td height="20"></td></tr>
                                                            </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody><tr>
                                                                    <td>
                                                                        <div class="contentEditableContainer contentImageEditable">
                                                                            <div class="contentEditable">
                                                                                <img class="banner" src="{{ url('images/ab.jpg') }}" alt="What we do" data-default="placeholder" data-max-width="600" width="600" height="139">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" class="" style="background:#333e48;" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                
                                                                <tbody><tr><td height="28"></td></tr>

                                                                <tr>
                                                                    <td valign="top" align="center">
                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                            <div class="contentEditable" style="color:#A8B0B6; font-size:13px;line-height: 16px;">
                                                                                <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr><td height="28"></td></tr>
                                                            </tbody></table>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="spechide" valign="top"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>      
    

</body></html>


