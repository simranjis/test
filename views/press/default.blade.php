@extends('layouts.default')
@section('content')
@php
    if(App\Setting::isPageDisplay('display_press_page'))
    {
@endphp  

<style>
    .grid-item
    {
        width: 280px; 
        float: left;
    }
    .grid {
        margin: 0 auto;
    }
    /* clearfix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }
</style>
<div class="contact-breadcrumbs">
    <img src="images/press.png" class="img-responsive" alt="">
</div>
<div class="container-fluid">
    <div class="grid js-masonry" id="blog-width">
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
    </div>
</div>
<script>
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 280,
        isFitWidth: true,
        "gutter": 10
    });
</script>


@php
    }
@endphp


@stop