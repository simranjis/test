<!DOCTYPE html>
<html lang="en">
    <head> 
        @include('includes.head')
    </head><!--/head-->
    <body>
    @include('includes.header')
 <div class="">
    <img src="images/it-work.png" class="img-responsive center-block" alt="who">
</div>   
         @yield('content')
    
<meta name="_token" content="{!! csrf_token() !!}" />
<script type="text/javascript" src="js/card/card.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="js/smoothscroll.js"></script>-->

<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="js/jquery.parallax.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 
<script type="text/javascript" src="js/login.js"></script> 

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.min.js"></script>
<script>
    
    
    $("#adv_search_txt").on('focus', function () {
        $("#adv-search-dropdown").css({'display': 'block'});
    });
    $("body").on('click', function (event) {
        if (event.target.id != "adv_search_txt" && event.target.id != $("#adv-search-dropdown") && $(event.target).parents("#adv-search-dropdown").length < 1) {
            $("#adv-search-dropdown").css({'display': 'none'});
        }
    });
    if ($(window).scrollTop() >= 50) {
        $('.navbar').css('background', '#2e2e2e');
        $('.navbar').css('padding', '1px 0px');
        $('.navbar-default .navbar-nav > li > a').css('padding', '12px 17px');
        $('.navbar-toggle').css('display', 'block');
        $("#scroll-logo").attr("src", "images/scroll-logo.png");
        $("#scroll-logo").css("height", "45px");
    } else {
        $('.navbar').css('background', 'transparent');
        $('.navbar-default .navbar-nav > li > a').css('padding', '41px 17px');
        $('.navbar-toggle').css('display', 'none');
        $("#scroll-logo").attr("src", "images/logo.png");
        $("#scroll-logo").css("height", "103px");
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 50) {
            $('.navbar').css('background', '#2e2e2e');
            $('.navbar').css('padding', '1px 0px');
            $('.navbar-default .navbar-nav > li > a').css('padding', '12px 17px');
            $('.navbar-toggle').css('display', 'block');
            $("#scroll-logo").attr("src", "images/scroll-logo.png");
            $("#scroll-logo").css("height", "45px");
            $('.in').css('display' , 'block');
        } else {
            $('.navbar').css('background', 'transparent');
            $('.navbar-default .navbar-nav > li > a').css('padding', '41px 17px');
            $('.navbar-toggle').css('display', 'none');
            $('.in').css('display' , 'none');
            $("#scroll-logo").attr("src", "images/logo.png");
            $("#scroll-logo").css("height", "103px");

        }
    });
//    var waypoints = $('#footer-destroy').waypoint(function (direction) {
//        if (direction == "down") {
//            $(".destory-sticky ul").css("display", "none")
//            $(".destory-sticky-1").css("display", "none")
//        } else {
//            $(".destory-sticky ul").css("display", "block")
//            $(".destory-sticky-1").css("display", "block")
//        }
//    }, {
//        offset: '70%'
//    })
</script>   
    </body>
</html>