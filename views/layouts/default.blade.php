<!DOCTYPE html>
<html lang="en">
    <head> 
        @include('includes.head')
    </head><!--/head-->
    <body>
    @include('includes.header')

         @yield('content')
    
    @include('includes.footer')
    @yield('page_scripts')
    </body>
</html>