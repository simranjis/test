<!DOCTYPE html>
<html lang="en">
    <head> 
        @include('includes.head')
    </head><!--/head-->
    <body>

         @yield('content')
    
    @include('includes.footer')
    </body>
</html>