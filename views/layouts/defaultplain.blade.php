<!DOCTYPE html>
<html lang="en">
    <head> 
        @include('includes.head')
    </head><!--/head-->
    <body>

         @yield('content')
    
    </body>
</html>