<?php if($activity['addedbytype'] == 'localguide'){ ?>
    <div class="local-guide-host mt20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-all">
                        <h2 class="text-center"><span class="divider"></span>LOCAL GUIDE HOSTS</h2>
                    </div>                    
                </div>                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/host-pay-cash.gif" class="img-responsive">
                        <p>Book NOW! Pay only a deposit to confirm this booking and the balance, you can pay directly to your Local Guide in cash!</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-2.gif" class="img-responsive">
                        <p>Worry free booking! You will only be charged the deposit once the booking is CONFIRMED by the Local Guide and it’s AFTER the FREE cancelation period</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-3.gif" class="img-responsive">
                        <p> Directly contact your Local Guide! Once your booking is 100% confirmed, you can access your Local Guide’s contact details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $commision = 0;
    if(!empty($activity['localguide']['commision'])){
        $commision = $activity['localguide']['commision'];
    } ?>
    <input type="hidden" name="commision_value" id="commision_value" value="{{ $commision }}">

<?php } ?>