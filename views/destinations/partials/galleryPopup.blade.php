<style>
    .modal-padding{
        padding: 10px 25px;
    }
</style>
<!-- Modal -->
<div id="galleryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body modal-padding" id="modal-body">
                <div class="carousel carousel-custom slide" id="myCarousel12" >
                    <div class="carousel-inner" >
                        @if($activity['gallery'])
                        @foreach($activity['gallery'] as $key => $value)
                        <div class="item @if($key == 0) active @endif">
                            <div class="col-md-12 col-sm-12">
                                <div class="hovereffect">
                                    <a href="javascript:void(0)" >
                                        <img  class="img-responsive" src="uploads/gallery/{{ $value['image'] }}" title="<?php echo $value['title'] ?>" alt="<?php echo $value['tags'] ?>" style="width:1200px; height:350px;">
                                    </a>
                                    <div class="overlay">
                                        <h2 class="feat-heading">{{ $value['title'] }}</h2>
                                        <p>
                                            {{ $value['title'] }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div> 
                    <a class="left carousel-control" href="#myCarousel12" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel12" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>