<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">What Others Say</h2>
            </div>                    
        </div>                
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="Carousel12" class="carousel slide" data-ride="carousel" data-interval="4000">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                </div><!--.carousel-inner-->
            </div><!--.Carousel-->
        </div>
    </div>
</div><!--.container-->