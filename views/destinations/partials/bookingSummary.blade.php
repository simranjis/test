<hr>
<div class="row">
    <div class="col-sm-12">
        <div class="city-description">
            <h2>HIGHLIGHTS</h2>
            <p>{{ $activity['hightlights'] }}</p>
        </div>
    </div>
</div>
<!-- This is where we put the pricing section -->
{!! Form::open(['url' => 'activities', 'name' => 'activitiesSelectionForm' , 'class'=>'activitiesSelectionForm', 'id'=>'activitiesSelectionForm']) !!}   
<div class="well well-shadow">
     <div class="row">
        <div class="col-md-12">
            <div class="package-options">
                <div class="btn-group">
                    <input type="text" id="datePicker" name="date" class="datepicker" style="display:none;" value="{{ date('m/d/Y') }}">
                    <button type="button" class="pink_btn_checkout dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-calendar"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <button type="button" class="pink_btn_checkout">Please select a booking date </button>
                </div>
                <div class="activity-information"></div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
</div>


{!! Form::open(['url' => '/destinations/activities/checkout', 'name' => 'activitiesOrderForm' , 'class'=>'activitiesOrderForm', 'id'=>'activitiesOrderForm']) !!}            
<input type="hidden" name="activities_id" value="{{ $activity['id'] }}">

<div class="col-sm-4">
    <div class="destory-sticky-1">
        <div class="sticky-wrapper">
            <div class="basic-dist-fix">
                <div class="add-to-cart-box">
                    <h2>
                        Booking Summary
                    </h2>
                    <p>{{ $activity['title'] }}</p>
                    <p id='booking-date'>{{ date('m/d/Y') }}</p>
                    <div class="row totlas">
                        <div class="col-md-12">
                            <div class="table-responsive checkout-table">
                                
                                <!-- NULL TYPE -->
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="checkout-button">
                            <!-- Parent After -->
                        </div>
                    </div>
                    
                </div>

                <div class="add-to-cart-box mt10 add-to-cart-box-amount hide" id="localguide_area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive checkout-table">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-left">
                                                <strong>
                                                    <span id="commision"></span>
                                                </strong>
                                            </td>
                                            <td class="text-right pay-ammount">
                                                <span class="text-center pink_btn_checkout pink_btn_checkout_copy text-uppercase fixed-at-bottom">
                                                    Pay -
                                                    <?php echo Pricing::getSymbol(); ?>

                                                    <span id="commision_amount"></span>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">Amount Due</td>
                                                    <td class="text-right">

                                                        <div class="pay-ammount">
                                                            <?php echo Pricing::getSymbol(); ?>

                                                            <span id="amount_due">
                                                                
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>Balance to be Paid Directly to the Local Guide (in cash)</p>
                                    </div>
                                </div>
                            </div>                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}