<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 style="color:#000" class="margin-top-0">QUICK INFO</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-user-o"  aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Minimum {{ $activity['quickinfo1'] }} Travelers To Book</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Confirmation: {{ $activity['quickinfo2'] }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Duration: {{ $activity['quickinfo3'] }} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Tour Type: {{ $activity['quickinfo4'] }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    <div class="col-xs-3 col-sm-5 col-md-4">
                        <div class="marine_park-box">
                            <div class="marin-box">
                                <div class="text-center">
                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <p class="margin-top-0">Redemption: {{ $activity['quickinfo5'] }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ovrflw-hidden">
            <div class="well-text">
                <div class="row">
                    @if($activity['addedbytype'] === 'localguide')
                        <div class="col-xs-3 col-sm-5 col-md-4">
                            <div class="marine_park-box">
                                
                                <div class="">
                                    <div class="text-center">
                                        <img src="images/castle-3.png" style="height:35px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-7 col-md-8">
                            <p class="margin-top-0">Local Guide Hosts</p>
                        </div>
                    @endif
                    <div class="col-xs-9 col-sm-7 col-md-8">
                        <!--<p class="margin-top-0">{{ $activity['quickinfo6'] }}</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>