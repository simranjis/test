@extends('layouts.default')
@section('content')
<style>
    .checkout-table td {
        white-space: nowrap;
    }
    .margin-top-10{
        margin-top:10px;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }
    .color-pink{
        color:#E9004C !important ;
    }
    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #E9004C;
        font-size: 14px;
    }

    .submenu li { border-bottom: 1px solid #4b4a5e; }
    .submenu li {
        color: #fff;
        padding: 20px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);;
    }
    .label-font {
        font-family: 'gothamregular' !important;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .red-tooltip + .tooltip > .tooltip-inner {
        background-color: #000;
    }
    .red-tooltip + .tooltip > .tooltip-arrow {
        border-bottom-color:#000; }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .stuck {
            width: 294px;
        }
        .label-font {
            font-size: 12px;
        }
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .destory-sticky-1{
            display: block !important;
        }
    }
</style>
{!! Form::open(['url' => 'destinations/activities/checkout/process', 'name' => 'activitiesCheckoutForm' , 'class'=>'activitiesCheckoutForm', 'id'=>'activitiesCheckoutForm']) !!}

<div class="activity-pay-breadcrumbs">
    <div class="container">
        <h2 class="text-center">CHECKOUT</h2>
    </div>
</div>
<div class="container">
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li><a href="{{url('/destinations/'.$activity['destinations_id'])}}">{{ $checkout['destinations_id'] }}</a></li>
        <li><a href="{{url('/destinations/'.$activity['destinations_id'].'/'.$activity['id'])}}">{{ $checkout['title'] }}</a></li>
        <li class="active">Checkout</li>
    </ul>
</div>
<div class="activity-pay-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all-dest">
                    <h2 class="text-uppercase">Traveler's Information</h2>
                </div>                    
            </div>                
        </div>
        <div class="row">            
            <div class="col-sm-8">
                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <label  for="inputHelpBlock" class="label-font">Title</label>
                                    <select name="title" class="form-control">
                                        <option @if($traveller_data['title'] == 0) selected="selected" @endif value="0">Mr</option>
                                        <option @if($traveller_data['title'] == 1) selected="selected" @endif value="1">Mrs</option>
                                        <option @if($traveller_data['title'] == 2) selected="selected" @endif value="2">Miss</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <label  for="inputHelpBlock" class="label-font">Passport First Name</label>
                                    <input type="text" name="firstname" class="form-control form-control-custom" value="{{ $traveller_data['firstname'] }}" placeholder="Passport First Name">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <label  for="inputHelpBlock" class="label-font">Passport Family Name</label>
                                    <input type="text" name="lastname" class="form-control form-control-custom" value="{{ $traveller_data['lastname'] }}" placeholder="Passport Family Name">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label  for="inputHelpBlock" class="label-font">Country</label>
                                    <select name="countries_id" class="form-control">
                                        @foreach($countries as $key => $value)
                                        @if($traveller_data['countries_id'] == $key)
                                        <option selected="selected" value="{{ $key }}">{{ $value }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label  for="inputHelpBlock" class="label-font">Mobile Number</label>
                                    <input type="text" name="mobilenumber" class="form-control form-control-custom phone-number" value="{{ $traveller_data['mobilenumber'] }}" placeholder="Mobile Number">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <label  for="inputHelpBlock" class="label-font">Email Address (your voucher will be sent here, make sure it is correct)</label>
                                    <input type="text" name="email" readonly="readonly" class="form-control form-control-custom " value="{{ $traveller_data['email'] }}" placeholder="Test@gmail.com">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($checkout['extra_details'] == 'yes')
                @if(count($checkout['options']))    

                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Extra Details <!--- {{ $checkout['destinations_id'] }} - {{ $checkout['title'] }}--></h2>
                        </div>                    
                    </div>                
                </div>
                <div class="well well-shadow">

                    @foreach($checkout['options'] as $options)
                    @if($options['title'])
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <label class="label-font color-pink">{{ $options['title'] }}:</label>
                            <input type="text" class="form-control form-control-custom " name="options[{{ $checkout['activities_id'] }}][{{ $options['id'] }}]" value="" placeholder="{{ $options['description'] }}">
                        </div> 
                    </div>
                    @endif
                    @endforeach
                </div>

                @endif
                @endif 

                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Payment Type </h2>
                        </div>                    
                    </div>                
                </div>

                <?php
                // Credir Card Area
                /*

                  <!-- Credit Card Payment -->
                  <div class="well well-shadow">
                  <div class="demo-container">
                  <div class="card-wrapper"></div>
                  <br>
                  <div class="form-container active" id="payment_form">
                  <div class="row">
                  <div class="col-xs-12">
                  <div class="form-group">
                  <label for="cardNumber">Card Number</label>
                  <div class="input-group">
                  <input type="tel" size="16" name="card[card_number]" id="number" class="form-control user_card_number" data-stripe="number" value="" placeholder="Card number" required="">

                  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-xs-12">
                  <div class="form-group">
                  <label for="cardNumber" class="text-capitalize">Cardholder Name</label>
                  <input type="text" value="" name="card[card_holder_name]" id="name" class="form-control user_card_name" size="20" data-stripe="name" placeholder="Cardholder Name">
                  </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-xs-6">
                  <div class="form-group">
                  <label for="cardNumber" class="text-capitalize">Expiration</label>
                  <input type="tel" value="" name="card[card_expiry]" id="expiry" class="form-control user_card_expiry" size="20" data-stripe="expiry" placeholder="MM/YY">
                  </div>
                  </div>
                  <div class="col-xs-6">
                  <div class="form-group">
                  <label for="cardNumber" class="text-capitalize">CVC</label>
                  <input type="text" value="" name="card[card_cvc]" id="cvc" class="form-control user_card_cvc" size="20" data-stripe="cvc" placeholder="CVC">
                  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  </div> */
                ?>
                <!-- Credit Card Payment - END -->

                <div class="">
                    <div id="collapseTwo" aria-expanded="false" class="collapse">
                        <div class="well well-shadow">
                            <div class="row">                        
                                <div class="col-md-9 col-sm-6">
                                    <img src="images/PayPal.png" class="img-responsive">
                                </div>
                                <div class="col-md-3 col-sm-6 text-center mt10">
                                    <div class="paypal-button">
                                        <button type="button" class="text-center pink_btn_checkout  text-uppercase">Pay Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-6 col-sm-4">
                            <div class="promo-code">
                                <p>Use Promo Code</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-8 text-right">

                            <div class="form-group">
                                <label class="sr-only" for="promo_code">Amount (in dollars)</label>
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-custom" id="promo_code" placeholder="Coupon">
                                    <div style="cursor:pointer" onclick="return redeemCoupen()" class="input-group-addon pink_btn_checkout">Redeem</div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-8">
                            <p>By clicking Pay Now, you agree that you have read and understood our Terms & Conditions and Cancelation Policy</p>
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="#" data-toggle="tooltip" data-placement="top"
                               title="" data-original-title="Book Now! No credit card needed! Once your activity is 100% CONFIRMED that's the only time you'll have to pay!"
                               class="red-tooltip"><input type="submit" class="text-center pink_btn_checkout text-uppercase" name="Submit" value="Book Now" >
                            </a>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-sm-4">
                <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="add-to-cart-box">
                                <h2>
                                    Booking Summary
                                </h2>
                                <p>{{ $checkout['destinations_id'] }} - {{ $checkout['title'] }}</p>
                                <p>{{ $checkout['date'] }}<!-- This needs to be changed --></p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left"><strong>User</strong></td>
                                                        <td class="text-center"><strong>Quantity</strong></td>
                                                        <td class="text-center"><strong>Price</strong></td>
                                                        <td class="text-right"><strong>Totals</strong></td>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <?php
                                                    $total = 0;
                                                    $tax = 0;
                                                    $persons = 0;
                                                    $final_total = 0;
                                                    $final_tax = 0;
                                                    ?>
                                                    @foreach($checkout['items'] as $id => $value)

                                                    <tr>
                                                        @if($value['pricefor'] === 'all')
                                                        <td>{{ $value['title'] }}</td>
                                                        @else
                                                        <td>{{ ucwords($value['pricefor']) }}</td>
                                                        @endif
                                                        <td class="text-center">{{ $value['qty'] }}</td>
                                                        <td class="text-center">
                                                            <?php echo Pricing::getSymbol(); ?> 
                                                            {{ Pricing::getActivityPrice($value['price'] , $checkout['activities_id']) }}
                                                        </td>
                                                        <td class="text-right">
                                                            <?php echo Pricing::getSymbol(); ?> 

                                                            {{ 
                                                                Pricing::getActivityPrice($value['price']*$value['qty'] , $checkout['activities_id']) 
                                                            }} 
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    //$checkout['activities_id']
                                                    $total += Pricing::getActivityPrice2($value['price'], $checkout['activities_id']) * $value['qty'];
                                                    $final_total += Pricing::getActivityPrice2($value['price'], $checkout['activities_id']) * $value['qty'];
                                                    if ($value['type'] === 'person') {
                                                        $persons += $value['qty'];
                                                    }
                                                    ?>

                                                    @endforeach
                                                    @if($checkout['servicefee_status'] === 'yes')
                                                    @if($checkout['servicefee_type'] === 'person')
                                                    <?php $tax = (Pricing::getActivityPrice2($checkout['service_fee'], $checkout['activities_id']) * $persons); ?>

                                                    <?php $final_tax = (Pricing::getActivityPrice2($checkout['service_fee'], $checkout['activities_id']) * $persons); ?>
                                                    <tr>
                                                        <td class="no-line">Service Fee</td>
                                                        <td class="no-line text-center">{{ $persons }}</td>
                                                        <td class="no-line text-center">{{ Pricing::getPrice(Pricing::getActivityPrice2($checkout['service_fee'] , $checkout['activities_id']))  }}</td>
                                                        <td class="no-line text-right">{{ Pricing::getPrice($tax) }}</td>
                                                    </tr>
                                                    @else
                                                    <?php $tax = Pricing::getActivityPrice2($checkout['service_fee'], $checkout['activities_id']); ?>
                                                    <?php $final_tax = (Pricing::getActivityPrice2($checkout['service_fee'], $checkout['activities_id'])); ?>
                                                    <tr>
                                                        <td class="no-line">Service Fee</td>
                                                        <td class="no-line text-center">1</td>
                                                        <td class="no-line text-center">{{ Pricing::getPrice(Pricing::getActivityPrice2($checkout['service_fee'] , $checkout['activities_id']))  }}</td>
                                                        <td class="no-line text-right">{{ Pricing::getPrice($tax) }}</td>
                                                    </tr>

                                                    @endif
                                                    @endif

                                                    <tr>
                                                        <td class="no-line"></td>
                                                        <td class="no-line"></td>
                                                        <td class="no-line text-center"><strong>Total</strong></td>
                                                        <td class="no-line text-right">
                                                            <?php $final_bookingh_total = $final_total + $final_tax; ?>
                                                            <?php echo Pricing::getSymbol(); ?>
                                                            <?php echo number_format($final_bookingh_total, 2); ?>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

                <br>
                <div class=" hide" id="promo_code_div">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="add-to-cart-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <tr>
                                                    <td class="text-left">
                                                        <strong>Promo Code</strong>
                                                    </td>
                                                    <td>
                                                        <strong>Discount</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left">
                                                        <span id="promotion_code"></span>
                                                    </td>
                                                    <td>
                                                        <span id="discount_value"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="destory-sticky-1">
                    <div class="sticky-wrapper">
                        <div class="basic-dist-fix">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="add-to-cart-box mt10 add-to-cart-box-amount">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive checkout-table">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <td class="text-left"><strong>Total</strong></td>
                                                                <td class="text-right"><strong>Payment Amount</strong></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                            <tr>
                                                                <td>
                                                                    <?php echo Pricing::getSymbol(); ?>
                                                                    <span class="activity_final_booking_total">
                                                                        <?php echo number_format($final_bookingh_total, 2); ?>
                                                                    </span>
                                                                </td>
                                                                <td class="text-right pay-ammount ">
                                                                    <?php echo Pricing::getSymbol(); ?>
                                                                    <span class="activity_final_booking_total">
                                                                        <?php echo number_format($final_bookingh_total, 2);
                                                                        ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Localguide Area -->
                                    <?php
                                    $commision = 0;
                                    $commision_amount = 0;
                                    $localguide_amount = 0;
                                    if ($activity->addedbytype == 'localguide') {
                                        $commision = $activity->localguide->commision;
                                        ?>
                                        <div class="add-to-cart-box mt10 add-to-cart-box-amount" id="localguide_area">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive checkout-table">
                                                        <table class="table table-condensed">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="text-left">
                                                                        <strong>
                                                                            <span id="commision">
                                                                                <?php echo $commision; ?>%
                                                                            </span>
                                                                        </strong>
                                                                    </td>
                                                                    <td class="text-right pay-ammount">
                                                                        <span class="text-center pink_btn_checkout pink_btn_checkout_copy text-uppercase fixed-at-bottom">
                                                                            Pay -
                                                                            <?php echo Pricing::getSymbol(); ?>

                                                                            <span id="commision_amount">
                                                                                <?php
                                                                                $commision_amount = ($final_bookingh_total - $final_tax) * ($commision / 100) + $final_tax;

                                                                                echo number_format($commision_amount, 2);
                                                                                ?>                              
                                                                            </span>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                                <table class="table table-condensed">
                                                                    <thead>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="text-left">Amount Due</td>
                                                                            <td class="text-right">

                                                                                <div class="pay-ammount">
                                                                                    <?php echo Pricing::getSymbol(); ?>

                                                                                    <span id="amount_due">
                                                                                        <?php
                                                                                        $localguide_amount = $final_bookingh_total - $commision_amount;

                                                                                        echo number_format($localguide_amount, 2);
                                                                                        ?>
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <p>Balance to be Paid Directly to the Local Guide (in cash)</p>
                                                            </div>
                                                        </div>
                                                    </div>                                        
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>






        </div>
    </div><!-- end of container -->
    <div id="sira-end"></div>
    <div class="container">

        <div class="row">
            <div class="col-md-6">
                <div class="note note-danger">
                    <h2><i class="fa fa-lock" aria-hidden="true"></i>&emsp;Data Security</h2>
                    <p>Your details are safe with us. All data is encrypted and transmitted securely with an SSL protocol. Privacy Statement</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="note note-danger">
                    <img src="images/PayPal.png" class="img-responsive center-block">
                    <p>No matter where you shop, our security is always the same. So buy with peace of mind from thousands of online stores around the globe. The world is yours.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- Lets Have Here the Hidden Inputs -->
<input type="hidden" id="traveller_id" name="traveller_id" value="{{ $traveller_data['id'] }}">
<input type="hidden" id="activity_id" name="activity_id" value="{{ $checkout['activities_id'] }}">

<input type="hidden" name="date[{{ $checkout['activities_id'] }}]" value="{{ $checkout['date'] }}" >
@foreach($checkout['items'] as $key => $value)
<input type="hidden" name="data[{{ $checkout['activities_id'] }}][{{ $key }}]" value="{{ $value['qty'] }}" >
@endforeach
<input type="hidden" name="subtotal" value="{{ $total }}" >
<input type="hidden" name="tax" value="{{ $tax }}" >
<input type="hidden" name="total" id="total" value="{{ $final_bookingh_total }}" >
<input type="hidden" name="promo_code_applied" id="promo_code_applied" value="" >
<input type="hidden" name="currencies_id" value="{{ Pricing::getCurrenciesId() }}">

<!-- Commision -->
<input type="hidden" name="commision" id="commision_value" value="{{ number_format($commision , 2) }}">
<input type="hidden" name="commision_amount" id="commision_amount_value" value="{{ number_format($commision_amount , 2) }}">
<input type="hidden" name="localguide_amount" id="localguide_amount_value" value="{{ number_format($localguide_amount , 2) }}">
<!-- Commision - END -->

<input type="hidden" id="localguide_id" name="localguide_id" value="{{ $activity->localguide_id }}" />
{!! Form::close() !!}
<script>
    $(".phone-number").intlTelInput();
</script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>


<script>
    var subtotal = '<?php echo ($final_bookingh_total - $final_tax); ?>';
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })
</script>
<script>
    $(document).ready(function () {
        $("a").tooltip();
    });
</script>
<script>

    function redeemCoupen() {
        var promo_code = $('#promo_code').val();
        var traveller_id = $('#traveller_id').val();
        var activity_id = $('#activity_id').val();
        if (promo_code == "") {
            bootbox.alert({
                title: "Error",
                message: 'Please enter coupon'
            });
        } else {
            var dataObj = {'promo_code': promo_code, 'traveller_id': traveller_id, 'activity_id': activity_id};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            $.ajax({
                url: 'destinations/getCopuenDiscount',
                type: 'post',
                data: dataObj,
                async: false,
                beforeSend: function () {
                    //Can we add anything here.
                },
                cache: true,
                dataType: 'json',
                crossDomain: true,
                success: function (data) {
                    if (data.code != 0) {
                        swal({
                            title: "Success",
                            text: 'Promo code applied.',
                            type: "success"
                        });

                        var final_total = $('#total').val();
                        $('#promo_code_applied').val(data.id);
                        $('#promotion_code').html(data.promocode);
                        var discount_value = data.discount;

                        if (data.discount_type == 1) {
                            discount_value += '%';
                            var discount = data.discount / 100;
                            // final_discounted_total = (parseFloat({{$commision_amount}}) - (subtotal * (discount))).toFixed(2);
                        } else {
                            var discount = data.discount;
                            //  final_discounted_total = (parseFloat({{$commision_amount}}) - data.discount).toFixed(2);
                        }

                        $('#discount_value').html(discount_value.toFixed(2));
//                        $('.activity_final_booking_total').html(final_discounted_total);


                        // promo on commission
                        var localguide_id = $('#localguide_id').val();
                        if (localguide_id != '' && localguide_id != 0) {

                            var commision = $('#commision_amount_value').val();
                            var commision_amount = parseFloat(commision) - discount;
                            // var amount_due = parseFloat($("#localguide_amount_value").val()) - discount;
                            var total = parseFloat($("#total").val()) - discount;

                            $('#commision_amount').html(commision_amount.toFixed(2));
                            // $('#amount_due').html(amount_due.toFixed(2));
                            $('.activity_final_booking_total').html(total.toFixed(2));

                            $('#commision_amount_value').val(commision_amount.toFixed(2));
                            // $('#localguide_amount_value').val(amount_due.toFixed(2));
                            $("#total").val(total.toFixed(2));
                        } else {
                            var total = parseFloat($("#total").val()) - discount;
                            $("#total").val(total.toFixed(2));
                            $('.activity_final_booking_total').html(total.toFixed(2));
                        }
                        // promo on commission - END


                        $('#promo_code_div').removeClass('hide');
                    } else {

                        bootbox.alert({
                            title: "Error",
                            message: data.message
                        });
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    }


    //    var waypoints = $('#destroy-gallery-upperbox').waypoint(function (direction) {
    //        if (direction == "down") {
    //            $(".destory-sticky ul").css("display", "none")
    //            $(".destory-sticky-1").css("display", "none")
    //        } else {
    //            $(".destory-sticky ul").css("display", "block")
    //            $(".destory-sticky-1").css("display", "block")
    //        }
    //    }, {
    //        offset: '40%'
    //    });
    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
        //        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 150) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });
</script>
<script>

    //    $(function () {
    //        $('.carousel-custom').carousel-custom({
    //            pause: true, // init without autoplay (optional)
    //            interval: false, // do not autoplay after sliding (optional)
    //            wrap: false // do not loop
    //        });
    //        // left control hide
    //        //$('.carousel').children('.left.carousel-control').hide();
    //    });
    //    $('.carousel-custom').on('slid.bs.carousel', function () {
    //        var carouselData = $(this).data('bs.carousel');
    //        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
    //        $(this).children('.carousel-control-custom').show();
    //        if (currentIndex == 0) {
    //            $(this).children('.left.carousel-control-custom').fadeOut();
    //        } else if (currentIndex + 1 == carouselData.$items.length) {
    //            $(this).children('.right.carousel-control-custom').fadeOut();
    //        }
    //    });

    $('.carousel-custom .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>
@stop