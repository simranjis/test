@extends('layouts.default')
@section('content')

<style>
    .feat-price a img{
        cursor: pointer;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        width: 100%;
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-0px,0,0);
        transform: translate3d(-0px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect a, .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    .carousel-inner .active.left { left: -25%; }
    .carousel-inner .active.right { left: 25%; }
    .carousel-inner .next        { left:  25%; }
    .carousel-inner .prev		 { left: -25%; }



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        opacity: 0;
        z-index: 1;
    }
    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 262px;
        z-index: -26;
    }
    .feat-price-tag h2 {
        padding: 27px 0 17px 11px !important;
    }
    .feat-price-tag p {
        padding: 15px 12px 0 0;
    }
    .castle-on-image img {
        height: 57px;
        left: 10px;
        position: absolute;
        top: 98px;
    }
    .castle-on-image {
        position: relative;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .pill-dest li a {
            font-size: 11px;
            padding: 16px 10px;
        }
        .nav-pills-custom > li {
            float: left;
        }
        .activity-breadcrumbs {
            background-repeat: no-repeat;
        }
        .tab-destination {
            margin-top: 0px;
        }
        .advance-search {
            top: -12px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .stuck {
            width: 217px;
        }
        .castle-on-image img {
            top: 68px;
        }
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 173px;
        }
        .castle-on-image img {
            top: 68px;
        }
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
        }
    }
</style>
<div class="activity-breadcrumbs">
    <div class="container">
        <h2 class="text-center">Hong Kong</h2>
    </div>
</div>
<div class="search-position">
    <div class="advance-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group margin-top-search" id="adv-search">
                        <!--                        <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default dropdown-toggle search-custom-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </div>-->
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs" placeholder="Where's Your Next Adventure?" />
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-lg hidden-md hidden-sm" placeholder="Where To?" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-destination-1" id="adv-search-dropdown" style="display:none">
        <div class="container-custom">        
            <div class="row">
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-all-dest">
                                <h2 class="text-uppercase">Browse by</h2>
                            </div>                    
                        </div>                
                    </div>
                    <ul class="nav nav-pills nav-stacked pill-dest">            
                        <li class="active"><a href="#tab_a" data-toggle="pill">ASIA</a></li>
                        <li><a href="#tab_b" data-toggle="pill">EUROPE</a></li>
                        <li><a href="#tab_c" data-toggle="pill">NORTH AMERICA</a></li>
                        <li><a href="#tab_d" data-toggle="pill">SOUTH AMERICA</a></li>
                        <li><a href="#tab_e" data-toggle="pill">AFRICA</a></li>
                        <li><a href="#tab_f" data-toggle="pill">AUSTRALIA & NEW ZEALAND</a></li>
                        <li><a href="#tab_g" data-toggle="pill">UAE & MIDDLE EAST </a></li>
                    </ul>
                </div>
                <div class="col-sm-9">                
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_a">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in Asia</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">BANGKOK</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PHUKET</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">CHIANG MAI</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">KUALA LUMPUR</h2>
                                                <p>
                                                    MALAYSIA
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">MANILA</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_b">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in EUROPE</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_c">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in NORTH AMERICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_d">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in SOUTH AMERICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_e">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in AFRICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_f">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in AUSTRALIA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_g">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in UAE & MIDDLE EAST </h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- tab content -->
                </div>
            </div>
        </div><!-- end of container -->
    </div>
</div>
<div class="tab-destination">
    <div class="container">        
        <div class="row">
            <div class="col-sm-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Browse by</h2>
                        </div>                    
                    </div>                
                </div>
                <div class="destory-sticky">
                    <ul class="nav nav-pills nav-stacked pill-dest basic-dist-fix nav-pills-custom">            
                        <li class="active"><a href="#tab_a" data-toggle="pill">AUTHENTIC EXPERIENCES</a></li>
                        <li><a href="#tab_b" data-toggle="pill">POPULAR ACTIVITIES</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">                
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_a">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">AUTHENTIC EXPERIENCES IN Hong Kong</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                        <div class="hovereffect">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        </div>
                                        <div class="castle-on-image">
                                            <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                                        </div>
                                    </a>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                        <div class="hovereffect">

                                            <img src="images/eg.png" class="img-responsive center-block" alt="">

                                        </div>
                                        <div class="castle-on-image">
                                            <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                                        </div>
                                    </a>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <a href="{{ url('destinations/destination/pointsofinterest') }}">
                                        <div class="hovereffect">

                                            <img src="images/eg.png" class="img-responsive center-block" alt="">

                                        </div>
                                        <div class="castle-on-image">
                                            <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                                        </div>
                                    </a>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_b">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">POPULAR ACTIVITIES IN Hong Kong</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="castle-on-image">
                                        <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="castle-on-image">
                                        <img src="images/castle-3.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="feat-price">
                                    <div class="hovereffect">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                    </div>
                                    <div class="row">
                                        <div class="feat-price-tag">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <h2 class="text-left">Hawker Food trip</h2>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <p class="text-right"><del><span class="small-font-text">$4000</span></del><br/>$3000</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- tab content -->
            </div>
        </div>
    </div><!-- end of container -->
</div>
<div id="sira-end"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })
</script>
<script>

    $(function () {
        $('.carousel').carousel({
            pause: true, // init without autoplay (optional)
            interval: false, // do not autoplay after sliding (optional)
            wrap: false // do not loop
        });
        // left control hide
        //$('.carousel').children('.left.carousel-control').hide();
    });
    $('.carousel').on('slid.bs.carousel', function () {
        var carouselData = $(this).data('bs.carousel');
        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
        $(this).children('.carousel-control').show();
        if (currentIndex == 0) {
            $(this).children('.left.carousel-control').fadeOut();
        } else if (currentIndex + 1 == carouselData.$items.length) {
            $(this).children('.right.carousel-control').fadeOut();
        }
    });

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 200) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });

    $('.carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>


@stop