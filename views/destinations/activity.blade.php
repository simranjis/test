@extends('layouts.default')
@section('content')
<style>
    .checkout-table td {
        white-space: nowrap;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #fff;
        font-size: 14px;
    }

    .submenu li {
        border-bottom: 1px solid #35D5E7;
    }
    .submenu li {
        color: #000;
        padding: 9px 19px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);
    }
    .add-to-cart-box-amount {
        padding: 12px 6px;
    }
    .select-package p {
        margin: 0 0 17px 0;
    }

    a:focus, a:hover {
        text-decoration: none;
        color: #e9004c !important; 
    }

    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
        .btn_block_cart {
            width: 100%;
            overflow: hidden;
            margin: 15px 0px 13px -7px;
            width: 100%;
        }
        .well-white {
            padding: 20px 11px;
        }


    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
        .stuck {
            width: 294px;
        }
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .destory-sticky-1{
            display: block !important;
        }
        .carousel-control.left {
            left: -26px;
        }
        .carousel-control.right {
            right: -26px;
        }
        .hovereffect img {
            width: 100%;
        }
    }
</style>
<script type="text/javascript">

    // open gallery image
//    function openGalleryImage(filename){
//        $('#modal-title').html('Gallery');
//        $('#modal-body').html('<img src="uploads/gallery/'+filename+'" width="575px" />');
//    }

    // update bookings
    function updateBookings() {
    $('#activitiesSelectionForm').submit();
    }

    $(function(){
    $('#activitiesSelectionForm').bind('submit', function(e){

    var data = JSON.parse(JSON.stringify($(this).serializeArray()));
    var dataarr = [];
    var dataarr1 = [];
    var dataarr2 = [];
    var dataarr3 = [];
    var dataarr4 = [];
    var dataarr5 = [];
    var dataarr6 = [];
    var dataarr7 = [];
    $.each(data, function(index, value) {

    var splitted = value.name.split("-");
    splitted_key = splitted[0];
    splitted_value = splitted[1];
    if (splitted_key == 'price') {
    dataarr1[splitted_value] = value.value;
    dataarr.price = dataarr1;
    } else if (splitted_key == 'pricefor') {
    dataarr2[splitted_value] = value.value;
    dataarr.pricefor = dataarr2;
    } else if (splitted_key == 'quantity') {
    dataarr3[splitted_value] = value.value;
    dataarr.quantity = dataarr3;
    } else if (splitted_key == 'title') {
    dataarr4[splitted_value] = value.value;
    dataarr.title = dataarr4;
    } else if (splitted_key == 'type') {
    dataarr5[splitted_value] = value.value;
    dataarr.type = dataarr5;
    } else if (splitted_key == 'price_display') {
    dataarr6[splitted_value] = value.value;
    dataarr.price_display = dataarr6;
    } else if (splitted_key == 'min_traveller') {
    dataarr7[splitted_value] = value.value;
    dataarr.min_traveller = dataarr7;
    } else {
    dataarr[value.name] = value.value;
    }

    });
    //check for minimum traveller
//    $.each(dataarr.quantity,function(index,value){
//        if(parseInt(dataarr.quantity[index]) < parseInt(dataarr.min_traveller[index])){
//            bootbox.alert('You must select minimum traveller to proceed',function(){
//                return false;
//            });
//        }
//    });
    var build_html;
    build_html = '';
    build_html += '<table class="table table-condensed">';
    build_html += '<thead>';
    build_html += '<tr>';
    build_html += '<td class="text-left"><strong>User</strong></td>';
    build_html += '<td class="text-center"><strong>Quantity</strong></td>';
    build_html += '<td class="text-center"><strong>Price</strong></td>';
    build_html += '<td class="text-center"><strong>Totals</strong></td>';
    build_html += '</tr>';
    build_html += '</thead>';
    build_html += '<tbody>';
    $('.checkout-table').first().html('');
    $('.checkout-button').html('');
    $("#booking-date").html(data[1].value);
    var total_persons = 0;
    var total = 0;
    var total_qty = 0;
    $.each(dataarr.quantity, function(index, value) {
    //value

    if (value > 0) {
    if (parseInt(dataarr.quantity[index]) < parseInt(dataarr.min_traveller[index])){
    bootbox.alert('You must select minimum traveller in "' + dataarr.pricefor[index] + '(' + dataarr.min_traveller[index] + ')" to proceed!!!');
    return false;
    }
    build_html += '<tr>';
    if (dataarr.type[index] == 'package') {
    build_html += '<td class="text-left">' + dataarr.title[index] + '</td>';
    } else {
    switch (dataarr.pricefor[index]) {
    case 'all':
            build_html += '<td class="text-left">' + dataarr.title[index] + '</td>';
    break;
    case 'adult':
            build_html += '<td class="text-left">' + dataarr.title[index] + '</td>';
    break;
    case 'child':
            build_html += '<td class="text-left">' + dataarr.title[index] + '</td>';
    break;
    }
    total_persons += parseInt(value);
    }

    build_html += '<input type="hidden" name="quantity[]" value="' + value + '" class="text-center">';
    build_html += '<input type="hidden" name="price[]" value="' + dataarr.price[index] + '" class="text-center">';
    build_html += '<input type="hidden" name="pricefor[]" value="' + dataarr.pricefor[index] + '" class="text-center">';
    build_html += '<input type="hidden" name="id[]" value="' + index + '" class="text-center">';
    build_html += '<input type="hidden" name="title[]" value="' + dataarr.title[index] + '" class="text-center">';
    build_html += '<input type="hidden" name="type[]" value="' + dataarr.type[index] + '" class="text-center">';
    build_html += '<input type="hidden" name="date" value="' + dataarr.date + '">';
    build_html += '<td class="text-center">' + value + '</td>';
    build_html += '<td class="text-center"> {{ Pricing::getSymbol() }} ' + dataarr.price_display[index] + '</td>';
    //QTY
    var price_value = parseInt(value);
    var price_display = parseFloat(
            dataarr.price_display[index].replace(/,/g, "")
            ).toFixed(2);
    build_html += '<td  class="text-center"> {{ Pricing::getSymbol() }} ' + (price_value * price_display).toFixed(2) + '</td>';
    build_html += '</tr>';
    total += value * price_display;
    total_qty += parseInt(value);
    }

    });
    if (total_qty > 0) {
    @if ($activity['servicefee_status'] == 'yes')

            var service_fee_type = "{{ $activity['servicefee_type'] }}";
    var service_fee = "{{ Pricing::getActivityPrice($activity['service_fee'],$activity['id']) }}";
    var service_fee_total = 0;
    service_fee = parseFloat(
            service_fee.replace(/,/g, "")
            ).toFixed(2);
    if (service_fee_type == 'person') {

    service_fee_total = service_fee * total_persons;
    // service_fee_total = parseFloat(service_fee_total).toFixed(2);

    } else {
    service_fee_total = service_fee;
    total_persons = 1;
    }
    if (total_persons != 0){
    build_html += '<tr>';
    build_html += '<td class="no-line text-left"><strong>Service Fees</strong></td>';
    build_html += '<td class="no-line text-center">' + total_persons + '</td>';
    build_html += '<td class="no-line text-center"> {{ Pricing::getSymbol() }} ' + (service_fee) + '</td>';
    build_html += '<td class="no-line text-center"> {{ Pricing::getSymbol() }} ' + parseFloat(service_fee_total).toFixed(2) + '</td>';
    build_html += '</tr>';
    total += parseFloat(service_fee_total);
    }
    @endif

            build_html += '<tr>';
    build_html += '<td class="no-line"></td>';
    build_html += '<td class="no-line"></td>';
    build_html += '<td class="no-line text-center"><strong>Total</strong></td>';
    build_html += '<td class="no-line text-center"> {{ Pricing::getSymbol() }} ' + total.toFixed(2) + '</td>';
    build_html += '</tr>';
    build_html += '<tbody>';
    build_html += '</table>';
    $('.checkout-table').first().html(build_html);
    var buttons_html = '';
    buttons_html += '<div class="row">';
    buttons_html += '<div class=" col-md-6 col-sm-12 col-xs-12 text-center">';
    buttons_html += '<input type="button" onclick="addToCart();" class="text-center black_btn_checkout text-uppercase btn_block_cart" value="Add To Cart" >';
    buttons_html += '</div>';
    buttons_html += '<div class="col-md-6 col-sm-12 col-xs-12 text-center">';
    buttons_html += '<input type="submit" class="text-center pink_btn_checkout btn_block_cart  text-uppercase fixed-at-bottom" value="Book Now">';
    buttons_html += '</div>';
    buttons_html += '</div>';
    }
    //Add to Cart Buttons
    //.parent().parent()
    $('.checkout-button').html(buttons_html);
    var due_total;
    /// Localguide block
<?php if ($activity['addedbytype'] === 'localguide') { ?>
        var commision_value = $('#commision_value').val();
        $('#commision').html(commision_value + '% + Service Fee');
        var commision_amount = ((parseFloat(total) - parseFloat(service_fee_total)) * (parseFloat(commision_value) / 100)) + parseFloat(service_fee_total);
        due_total = parseFloat(total) - parseFloat(commision_amount);
        $('#amount_due').html(due_total.toFixed(2));
        $('#commision_amount').html(parseFloat(commision_amount).toFixed(2));
        $('#localguide_area').removeClass('hide');
<?php } ?>

    e.preventDefault();
    });
    $('#activitiesOrderForm').bind('submit', function(e){

    @if (Session::has('traveller'))

    @else
            if ($('input[name=feel]') == 'free') {
    } else {
    $('#travellerloginForm').append('<input type="hidden" name="checkout" value="1">');
    $('#travellerLoginModal').modal("show");
    e.preventDefault();
    }
    @endif


    });
    });</script>
<script type='text/javascript'>



    function substract(button) {

    var input = $(button).parent().parent().find("input");
    var initial = input.val();
    if (initial > 0) {
    input.val(parseInt(parseInt(initial) - parseInt(1)));
    } else {
    $(button).attr("disabled", "disabled");
    }

    }

    function add(button) {

    var input = $(button).parent().parent().find("input");
    var initial = input.val();
    input.val(parseInt(parseInt(initial) + parseInt(1)));
    input.val(parseInt(parseInt(initial) + parseInt(1)));
    $(".change-qty").removeAttr("disabled");
    }


    $(function() {
    $('.pink_btn_checkout').click(function(e){
    $(".datepicker").datepicker('show');
    });
    var unAvailableDates = '';
<?php if (!empty($activity['availability'])) { ?>
        unAvailableDates = <?php echo json_encode($activity['availability']); ?>;
<?php } ?>
    var d = new Date();
    var default_date = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
    sendRequest(default_date);
    $(".datepicker").datepicker({
    beforeShowDay: function(d) {
    var dmy = d.getDay();
    if ($.inArray(dmy, unAvailableDates) != - 1) {
    return [false, "", "Unavailable"];
    } else{
    return [true, "", "Available"];
    }
    },
            minDate: new Date('<?php echo date('Y-m-d', strtotime($activity['start_date'])); ?>'),
            maxDate: new Date('<?php echo date('Y-m-d', strtotime($activity['activity_end_date'])); ?>'),
            onSelect : function() {
            $("#booking-date").html($(this).val());
            sendRequest($(this).val());
            }
    });
    });
    function addToCart() {

    //Lets Send this to Ajax.
    //Check if the Customer is Logged In
    @if (Session::has('traveller'))
            $.ajax({
            url: 'destinations/activities/submit',
                    type: 'post',
                    data: $('#activitiesOrderForm').serialize(),
                    async: false,
                    beforeSend: function () {
                    //Can we add anything here.
                    },
                    cache: true,
                    dataType: 'json',
                    crossDomain: true,
                    success: function (response) {
                    if (response.code == 1){
                    swal({
                    title: "Success",
                            text: response.message,
                            type: "success"
                    }, function() {
                    window.location = "cart/";
                    });
                    }
                    },
                    error: function (data) {
                    console.log('Error:', data);
                    }
            });
    @else

            if ($('input[name=feel]') == 'free') {
    } else {

    $('#travellerLoginModal').modal("show");
    $('#travellerloginForm').append('<input type="hidden" name="checkout" value="3">');
    }

    @endif
    }

    function sendRequest(selected_date) {

    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
    })

            $.ajax({
            url: 'destinations/getactivitypricing',
                    type: 'post',
                    data: { 'date' : selected_date, 'activities_id' : {{ $activity['id'] }} },
                    async: true,
                    beforeSend: function () {
                    //Can we add anything here.
                    },
                    cache: true,
                    dataType: 'json',
                    crossDomain: true,
                    success: function (response) {
                    if (response.code == 1) {
                    //console.log(response.data);
                    var response_html = '';
                    $('.activity-information').html('<p>No Pricing Available for the date</p>');
                    //Lets Construct The HTML
                    if (Object.keys(response.data).length > 0) {
                    //console.log(Object.keys(response.data).length);
                    $('.activity-information').html('');
                    $.each(response.data, function(index, value) {

                    response_html += '<hr>';
                    response_html += '<div class="row">';
                    response_html += '<div class="col-xs-4">';
                    response_html += '<h2>{{ $activity['title'] }}</h2>';
                    response_html += '</div>';
                    response_html += '<div class="col-xs-4">';
                    response_html += '<p class="text-center"><br><strong>&nbsp;</strong></p>';
                    response_html += '</div>';
                    response_html += '<div class="col-xs-4 text-right">';
                    response_html += '<button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample_' + index + '" aria-expanded="false" aria-controls="collapseExample">Select</button>';
                    response_html += '</div>';
                    response_html += '</div>';
                    // if(index == 'package'){
                    //     response_html += '<div class="row">';
                    //     response_html += '<div class="col-xs-4">';
                    //     response_html += '<a href="javascript:;" data-toggle="collapse" data-target="#demo">Show Detail</a>';
                    //     response_html += '</div>';
                    //     response_html += '<div class="col-xs-4">';
                    //     response_html += '<p class="text-center"><br><strong>&nbsp;</strong></p>';
                    //     response_html += '</div>';
                    //     response_html += '<div class="col-xs-4 text-right">';
                    //     response_html += '<div id="demo" class="collapse">Lorem ipsum dolor text....</div>';
                    //     response_html += '</div>';
                    //     response_html += '</div>';  
                    // }


                    //Lets Concat the HTML


                    response_html += '<div class="row">';
                    response_html += '<div class="col-md-12">';
                    response_html += '<div class="collapse" id="collapseExample_' + index + '" aria-expanded="true" style=""> ';
                    response_html += '<div class="well well-white">';
                    response_html += '<div class="row">';
                    $.each(value, function(key, subvalue) {

                    //response_html += '<input type="hidden" name="priceid[]" value="'+subvalue.id+'">';

                    response_html += '<div class="col-xs-12">';
                    response_html += '<input type="hidden" name="type-' + subvalue.id + '" value="' + index + '">';
                    if (subvalue.pricefor == 'all') {
                    response_html += '<p>Package</p><input type="hidden" name="pricefor-' + subvalue.id + '" value="' + subvalue.pricefor + '">';
                    } else {
                    var pricefortext;
                    if (subvalue.pricefor == 'adult') {
                    pricefortext = 'Adult';
                    } else {
                    pricefortext = 'Child';
                    }
                    response_html += '<p>' + pricefortext + '</p><input type="hidden" name="pricefor-' + subvalue.id + '" value="' + subvalue.pricefor + '">';
                    }
                    response_html += '<div class="select-package">';
                    response_html += '<div class="row">';
                    response_html += '<div class="col-xs-12 col-sm-4">';
                    response_html += '<h2>' + subvalue.title + ' (Min Traveller : ' + subvalue.min_traveller + ')</h2><input type="hidden" name="title-' + subvalue.id + '" value="' + subvalue.title + '"><input type="hidden" name="min_traveller-' + subvalue.id + '" value="' + subvalue.min_traveller + '"/>';
                    response_html += '</div>';
                    response_html += '<div class="col-xs-12 col-sm-4">';
                    response_html += '<p class="text-center">';
                    if (subvalue.original_price != 0){
                    response_html += '<strike class="small-font-text">{{ Pricing::getSymbol() }} ' + subvalue.original_price + '</strike><br/> ';
                    }
                    response_html += '{{ Pricing::getSymbol() }} ' + subvalue.price_display + '</p><input type="hidden" name="price_display-' + subvalue.id + '" value="' + subvalue.price_display + '"><input type="hidden" name="price-' + subvalue.id + '" value="' + subvalue.price + '">';
                    response_html += '</div>';
                    response_html += '<div class="col-xs-12 text-right col-sm-4">';
                    response_html += '<div class="quantity-width">';
                    response_html += '<div class="input-group">';
                    response_html += '<span class="input-group-btn">';
                    response_html += '<button type="button" class="btn btn-black btn-number change-qty" disabled="disabled" data-type="minus" onclick="substract(this)" data-field="quantity[]">';
                    response_html += '<span class="glyphicon glyphicon-minus"></span>';
                    response_html += '</button>';
                    response_html += '</span>';
                    response_html += '<input type="text" name="quantity-' + subvalue.id + '" class="form-control input-number text-center qty-box" value="0" min="1" max="10">';
                    response_html += '<span class="input-group-btn">';
                    response_html += '<button type="button" class="btn btn-pink btn-number change-qty" data-type="plus" onclick="add(this)" data-field="quantity[]">';
                    response_html += '<span class="glyphicon glyphicon-plus"></span>';
                    response_html += '</button>';
                    response_html += '</span>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    if (index == 'package'){
                    //response_html += '<div class="row">';
                    //response_html += '<div class="col-xs-12">';
                    response_html += '<div class="col-xs-12">';
                    response_html += '<a href="javascript:;" data-toggle="collapse" data-target="#' + subvalue.id + '">Show Detail <i class="fa fa-angle-down"></i></a>';
                    response_html += '</div>';
                    response_html += '<div class="col-xs-12">';
                    response_html += '<div id="' + subvalue.id + '" class="collapse">' + subvalue.description + '</div>';
                    response_html += '</br></div>';
                    //response_html += '</div>';  
                    //response_html += '</div>';
                    }


                    });
                    response_html += '<div class="mt10 col-xs-12">';
                    response_html += '<button type="button" class=" pink_btn_checkout text-uppercase new_checkout_btn" data-toggle="collapse_' + index + '" data-target="#collapseExample_' + index + '" aria-expanded="false" onclick="updateBookings();" aria-controls="collapseExample">Ok</button>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '</div>';
                    response_html += '';
                    });
                    $('.activity-information').html(response_html);
                    }
                    }
                    },
                    error: function (data) {
                    console.log('Error:', data);
                    }
            });
    }

</script>  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Body -->
<input type="hidden" name="activities_id" value="{{ $activity['id'] }}">


<?php
if (!empty($activity['banner_image'])) {
    ?>
    <style>
        .activity-pay-breadcrumbs{       
            min-height: 480px;
            padding: 170px 0;
            background-attachment: fixed;
            background-image: url("uploads/banner/<?php echo $activity['banner_image'] ?>");
            background-repeat: no-repeat;
            background-size:cover;
        }
    </style>
    <?php
}
?>
<div class="activity-pay-breadcrumbs">
    <div class="container">
        <h2 class="text-center">{{ $activity['title'] }}</h2>
    </div>
</div>
<div class="container">    
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="{{url('/destinations/'.$activity['destinationID'])}}">Back</a></li>
    </ul>
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li><a href="{{url('/destinations/'.$activity['destinationID'])}}">{{$activity['destination']}}</a></li>
        <li class="active">{{$activity['title']}}</li>
    </ul>
</div>
<div class="activity-pay-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all-dest">
                    <h2 class="text-uppercase">{{ $activity['title'] }}</h2>
                </div>                    
            </div>                
        </div>
        <div class="row">            
            <div class="col-sm-8">
                <hr>            
                <!-- Localguide Activity -->
                @include('destinations.partials.quickInfo')

                <!-- Localguide Activity -->
                @include('destinations.partials.bookingSummary')

            </div>
        </div><!-- end of container -->
        <div id="sira-end"></div>
        <div id="destroy-gallery-upperbox">
            <div class="container hidden-xs">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all">
                            <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
                        </div>                    
                    </div>                
                </div>
                <div class="carousel carousel-custom slide margin-top-40" id="myCarousel1" >
                    <div class="carousel-inner" >
                        @if($activity['gallery'])
                        @foreach($activity['gallery'] as $key => $value)
                        <div onclick="openGalleryImage('<?php echo $value['image'] ?>')" class="item @if($key == 0) active @endif"  data-toggle="modal" data-target="#galleryModal">
                            <div class="col-md-12 col-sm-12">
                                <div class="hovereffect">
                                    <a href="javascript:void(0)" >
                                        <img  class="img-responsive" src="uploads/gallery/{{ $value['image'] }}" title="<?php echo $value['title'] ?>" alt="<?php echo $value['tags'] ?>" style="width:1200px; height:350px;">
                                    </a>
                                    <div class="overlay">
                                        <h2 class="feat-heading">{{ $value['title'] }}</h2>
                                        <p>
                                            {{ $value['title'] }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div> 
                    <a data-slide="prev" href="#myCarousel1" class="left carousel-control carousel-control-custom">‹</a>
                    <a data-slide="next" href="#myCarousel1" class="right carousel-control carousel-control-custom">›</a>
                </div>
            </div>
        </div>

        <!-- Localguide Activity -->
        @include('destinations.partials.localguide_activity')

    </div>
    <!--<mbile slider>-->
    <div class="container hidden-lg hidden-md hidden-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all">
                    <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
                </div>                    
            </div>                
        </div>
        <div class="carousel slide margin-top-40" id="myCarousel123">
            <div class="carousel-inner">
                @if($activity['gallery'])
                @foreach($activity['gallery'] as $value)
                <div class="item @if($key == 0) active @endif">
                    <div class="col-md-3 col-sm-3">
                        <div class="hovereffect">
                            <a href="#">
                                <img class="img-responsive" src="uploads/gallery/{{ $value['image'] }}" alt="">
                            </a>
                            <div class="overlay">
                                <h2 class="feat-heading">{{ $value['title'] }}</h2>
                                <p>
                                    {{ $value['title'] }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div> 
            <a data-slide="prev" href="#myCarousel123" class="left carousel-control">�</a>
            <a data-slide="next" href="#myCarousel123" class="right carousel-control">�</a>
        </div>
    </div>

</div>
<!--mobile slider end-->
<div class="accordion-well-background">
    <div class="container">
        <div class="row">            
            <div class="col-sm-12">
                <div class="accordion-well ">

                    @if(count($activity['faqs']))
                    <ul id="accordion" class="accordion list-unstyled">

                        <!-- Additional Info -->
                        <?php
                        if (!empty($activity['faqs'][1])) {
                            $additionalInfoArr = $activity['faqs'][1];
                            $title = $additionalInfoArr['title'];
                            $description = $additionalInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-info-circle" aria-hidden="true"></i>{{ strtoupper($title) }}<i class="fa fa-chevron-down"></i></div>

                                <?php
                                $description = str_replace('<ul>', '<ul class="submenu list-unstyled">', $description);
                                ?>

                                {!! $description !!}                                    

                            </li>
                        <?php } ?>
                        <!-- Additional Info - END -->


                        <!-- Inclusion / Exclusion Info -->
                        <?php
                        if (!empty($activity['faqs'][2]) && !empty($activity['faqs'][7]) && ($activity['faqs'][2]['show'] == 1)) {
                            $inclusionInfoArr = $activity['faqs'][2];
                            $inclusion_title = $inclusionInfoArr['title'];
                            $inclusion_description = $inclusionInfoArr['description'];

                            $exclusionInfoArr = $activity['faqs'][7];
                            $exclusion_title = $exclusionInfoArr['title'];
                            $exclusion_description = $exclusionInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-database" aria-hidden="true"></i>INCLUSIONS / EXCLUSIONS<i class="fa fa-chevron-down"></i></div>

                                <?php
                                $inclusion_exclusion_description = str_replace('<ul>', '<ul class="submenu list-unstyled">
                                    <li class="li-heading">
                                    ' . $inclusion_title . '
                                    </li>', $inclusion_description);

                                $inclusion_exclusion_description = str_replace('</ul>', '', $inclusion_exclusion_description);

                                $inclusion_exclusion_description .= '<li class="li-heading">' . $exclusion_title . '</li>';

                                $exclusion_description = str_replace('<ul>', '', $exclusion_description);

                                $exclusion_description = str_replace('</ul>', '', $exclusion_description);

                                $inclusion_exclusion_description .= $exclusion_description;
                                $inclusion_exclusion_description .= '</ul>';
                                ?>

                                {!! $inclusion_exclusion_description !!}                                    

                            </li>
                        <?php } ?>
                        <!-- Inclusion / Exclusion Info - END -->


                        <!-- Itinerary Info -->
                        <?php if (count($activity['activityItineryArr']) > 0) {
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-check-square-o" aria-hidden="true"></i>ITINERARY<i class="fa fa-chevron-down"></i></div>
                                <ul class="submenu list-unstyled" style="display: none;">
                                    @foreach($activity['activityItineryArr'] as $itinery)
                                    <li> <div class="li-heading">{{$itinery->title}}
                                        </div>
                                        <div>{!! $itinery->description !!}</div>  
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        <?php } ?>

                        <!-- Cancellation -->
                        <?php
                        if (!empty($activity['faqs'][5])) {
                            $cancellationInfoArr = $activity['faqs'][5];
                            $title = $cancellationInfoArr['title'];
                            $title = str_replace('{{FOLLOWING_DAYS}}', $cancellationInfoArr['days'], $title);
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-ban" aria-hidden="true"></i>CANCELLATION<i class="fa fa-chevron-down"></i></div>

                                <ul class="submenu list-unstyled">
                                    <li>{{$title}}</li>
                                </ul>
                            </li>
                        <?php } ?>
                        <!-- Cancellation - END -->


                        <!-- Payment Protection -->
                        <?php
                        if (!empty($activity['faqs'][6])) {
                            $paymentProtectionInfoArr = $activity['faqs'][6];
                            $title = $paymentProtectionInfoArr['title'];
                            $description = $paymentProtectionInfoArr['description'];
                            ?>
                            <li>
                                <div class="link"><i class="fa fa-money" aria-hidden="true"></i>{{ strtoupper($title) }}<i class="fa fa-chevron-down"></i></div>

                                <ul class="submenu list-unstyled">
                                    <?php
                                    $description = str_replace('<ul>', '', $description);
                                    $description = str_replace('</ul>', '', $description);
                                    $description = str_replace('<p>', '<li class="li-heading">', $description);
                                    $description = str_replace('</p>', '</li>', $description);
                                    ?>

                                    {!! $description !!}    
                                </ul>                                
                            </li>
                        <?php } ?>
                        <!-- Payment Protection - END -->

                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('destinations.partials.galleryPopup')
<!-- What Other Say -->
@include('destinations.partials.whatOtherSay')

<!-- Body - END -->

<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    $(function () {
    var Accordion = function (el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;
// Variables privadas
    var links = this.el.find('.link');
// Evento
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function (e) {
    var $el = e.data.el;
    $this = $(this),
            $next = $this.next();
    $next.slideToggle();
    $this.parent().toggleClass('open');
    if (!e.data.multiple) {
    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    }
    ;
    }

    var accordion = new Accordion($('#accordion'), false);
    });</script>
<script>
    var sticky = new Waypoint.Sticky({
    element: $('.basic-dist-fix')[0]
    })
</script>
<script>
            $(window).scroll(function (e) {
    var offset = $("#sira-end").offset().top;
    if (($(window).scrollTop() + 325) > offset) {
    $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
    } else {
    $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
    }
    });
</script>

@stop
