@extends('layouts.default')
@section('content')

<style>
    body{
        overflow-x: hidden;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    .carousel-inner .active.left { left: -25%; }
    .carousel-inner .active.right { left: 25%; }
    .carousel-inner .next        { left:  25%; }
    .carousel-inner .prev		 { left: -25%; }



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        opacity: 0;
        z-index: 1;
    }
    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 262px;
        z-index: 1;
    }
    .city-text-heading .overlay h2{
        padding: 0px;
    }
    .city-text-heading .overlay{
        padding: 1em;
    }
    .city-text-heading{
        margin: 15px 0px;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .carousel-control.left {
            left: -26px;
        }
        .carousel-control.right {
            right: -26px;
        }
        .hovereffect img {
            width: 100%;
        }
        .breadcrumbs-banner{
            min-height: 243px;
        }
        .advance-search {
            top: -12px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .stuck {
            width: 217px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
    }   
</style>
<div class="destination-breadcrumbs">
</div>
<div class="search-position">
    <div class="advance-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group margin-top-search" id="adv-search">
                        <!--                        <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default dropdown-toggle search-custom-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </div>-->
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs" placeholder="Where's Your Next Adventure?" />
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-lg hidden-md hidden-sm" placeholder="Where To?" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-destination-1" id="adv-search-dropdown" style="display:none">
        <div class="container-custom">        
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-all-dest">
                                <h2 class="text-uppercase">Browse by</h2>
                            </div>                    
                        </div>                
                    </div>
                    <ul class="nav nav-pills nav-stacked pill-dest">            
                        <li class="active"><a href="#tab_a" data-toggle="pill">ASIA</a></li>
                        <li><a href="#tab_b" data-toggle="pill">EUROPE</a></li>
                        <li><a href="#tab_c" data-toggle="pill">NORTH AMERICA</a></li>
                        <li><a href="#tab_d" data-toggle="pill">SOUTH AMERICA</a></li>
                        <li><a href="#tab_e" data-toggle="pill">AFRICA</a></li>
                        <li><a href="#tab_f" data-toggle="pill">AUSTRALIA & NEW ZEALAND</a></li>
                        <li><a href="#tab_g" data-toggle="pill">UAE & MIDDLE EAST </a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-md-9">                
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_a">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in Asia</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">BANGKOK</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PHUKET</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">CHIANG MAI</h2>
                                                <p>
                                                    THAILAND
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">KUALA LUMPUR</h2>
                                                <p>
                                                    MALAYSIA
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">MANILA</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class=" city-text-heading-1">
                                        <a href="{{ url('destinations/destination') }}">
                                            <img src="images/eg.png" class="img-responsive center-block" alt="">
                                            <div class="overlay">
                                                <h2 class="feat-heading">PALAWAN</h2>
                                                <p>
                                                    PHILIPPINES
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_b">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in EUROPE</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_c">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in NORTH AMERICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_d">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in SOUTH AMERICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_e">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in AFRICA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_f">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in AUSTRALIA</h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_g">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in UAE & MIDDLE EAST </h2>
                                    </div>                    
                                </div>                
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>FRANCE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>PARIS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ENGLAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>LONDON</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>AUSTRIA</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>VIENNA</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ICELAND</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>REYKJAVIK</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>SOUTH ICELAND</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GERMANY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>MUNICH</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>GREECE</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>ATHENS</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>THE NETHERLANDS</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <p>AMSTERDAM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tab-under-tab-text">
                                    <div class="col-sm-3">
                                        <h2>ITALY</h2>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p>FLORENCE</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>MILAN</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>ROME</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>NAPLES</p> 
                                            </div>
                                            <div class="col-sm-2">
                                                <p>VENICE</p> 
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- tab content -->
                </div>
            </div>
        </div><!-- end of container -->
    </div>
</div>
<!--mobiledevices-->
<div class="container hidden-lg hidden-md hidden-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase"><span class="divider"></span>Popular Destinations</h2>
            </div>                    
        </div>                
    </div>
    <div class="carousel slide margin-top-40" id="myCarousel1">
        <div class="carousel-inner">
            <div class="item active">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/nepal.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">South Iceland</h2>
                            <p>
                                South Iceland
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/phuket.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Phuket</h2>
                            <p>
                                Phuket
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/thailand.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Bali</h2>
                            <p>
                                Bali
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Chile</h2>
                            <p>
                                Chile
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/argentina.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Argentina</h2>
                            <p>
                                Argentina
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Nairobi</h2>
                            <p>
                                Nairobi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/paris.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Paris</h2>
                            <p>
                                Paris
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/dubai-2.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Dubai</h2>
                            <p>
                                Dubai
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <a data-slide="prev" href="#myCarousel1" class="left carousel-control">‹</a>

        <a data-slide="next" href="#myCarousel1" class="right carousel-control">›</a>
    </div>
</div>
<!--mobiledevicesend-->
<div class="container hidden-xs">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase"><span class="divider"></span>Popular Destinations</h2>
            </div>                    
        </div>                
    </div>
    <div class="carousel slide margin-top-40" id="myCarousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/nepal.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">South Iceland</h2>
                            <p>
                                South Iceland
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/phuket.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Phuket</h2>
                            <p>
                                Phuket
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/thailand.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Bali</h2>
                            <p>
                                Bali
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Chile</h2>
                            <p>
                                Chile
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/argentina.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Argentina</h2>
                            <p>
                                Argentina
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Nairobi</h2>
                            <p>
                                Nairobi
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/paris.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Paris</h2>
                            <p>
                                Paris
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/dubai-2.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Dubai</h2>
                            <p>
                                Dubai
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <a data-slide="prev" href="#myCarousel" class="left carousel-control">‹</a>

        <a data-slide="next" href="#myCarousel" class="right carousel-control">›</a>
    </div>
</div>
<!--mobile tab bar-->
<div class="mobile-collapse">
    <div class="container hidden-lg hidden-md hidden-sm">
        <div class="text-center">
            <button type="button" class="text-center black_btn text-uppercase pad-button" data-toggle="modal" data-target=".bs-example-modal-lg">Browse by</button>
        </div>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myLargeModalLabel">Browse By</h4>
                    </div>
                    <div class="panel-group panel-padding" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        ASIA
                                    </a>
                                </h4>
                            </div>
                            
                            
                            
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body panel-padding-removed">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        
                        
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        EUROPE
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        NORTH AMERICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        SOUTH AMERICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        AFRICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        AUSTRALIA & NEW ZEALAND
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        UAE & MIDDLE EAST 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--mobile tab bar end-->
<div class="tab-destination">
    <div class="container">        
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Browse by</h2>
                        </div>                    
                    </div>                
                </div>
                <div class="destory-sticky">
                    <ul class="nav nav-pills nav-stacked pill-dest basic-dist-fix">            
                        <li class="active"><a href="#tab_1" data-toggle="pill">ASIA</a></li>
                        <li><a href="#tab_2" data-toggle="pill">EUROPE</a></li>
                        <li><a href="#tab_3" data-toggle="pill">NORTH AMERICA</a></li>
                        <li><a href="#tab_4" data-toggle="pill">SOUTH AMERICA</a></li>
                        <li><a href="#tab_5" data-toggle="pill">AFRICA</a></li>
                        <li><a href="#tab_6" data-toggle="pill">AUSTRALIA & NEW ZEALAND</a></li>
                        <li><a href="#tab_7" data-toggle="pill">UAE & MIDDLE EAST </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">                
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in Asia</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in EUROPE</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/paris-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PARIS</h2>
                                            <p>
                                                FRANCE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/england.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LONDON</h2>
                                            <p>
                                                ENGLAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/vienna.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">VIENNA</h2>
                                            <p>
                                                AUSTRIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in NORTH AMERICA</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in SOUTH AMERICA</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in AFRICA</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in AUSTRALIA & NEW ZEALAND</h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_7">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-all-dest">
                                    <h2 class="text-uppercase">Destinations in UAE & MIDDLE EAST </h2>
                                </div>                    
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/bankok.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BANGKOK</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/phuket-1.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PHUKET</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/changmai.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CHIANG MAI</h2>
                                            <p>
                                                THAILAND
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">KUALA LUMPUR</h2>
                                            <p>
                                                MALAYSIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MANILA</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">PALAWAN</h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">CEBU </h2>
                                            <p>
                                                PHILIPPINES
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BOHOL</h2>
                                            <p>
                                                Nairobi
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">SINGAPORE</h2>
                                            <p>
                                                SINGAPORE
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HONG KONG</h2>
                                            <p>
                                                HONG KONG
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">MACAU</h2>
                                            <p>
                                                MACAU
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HO CHI MINH</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HANOI </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HUE</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">HOI AN </h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">DANANG</h2>
                                            <p>
                                                VIETNAM
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">BALI</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="hovereffect city-text-heading">
                                    <a href="{{ url('destinations/destination') }}">
                                        <img src="images/eg.png" class="img-responsive center-block" alt="">
                                        <div class="overlay">
                                            <h2 class="feat-heading">LOMBOK</h2>
                                            <p>
                                                INDONESIA
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- tab content -->
            </div>
        </div>
    </div><!-- end of container -->
</div> <div id="sira-end"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 420) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });

</script>
<script>
    //    $("#adv_search_txt").on('blur', function (event) {
    //            $("#adv-search-dropdown").css({'display': 'none'});
    //    });


    $(function () {
        $('.carousel').carousel({
            pause: true, // init without autoplay (optional)
            interval: false, // do not autoplay after sliding (optional)
            wrap: true // do not loop
        });
        // left control hide
        //$('.carousel').children('.left.carousel-control').hide();
    });
//    $('.carousel').on('slid.bs.carousel', function () {
//        var carouselData = $(this).data('bs.carousel');
//        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
//        $(this).children('.carousel-control').show();
//        if (currentIndex == 0) {
//            $(this).children('.left.carousel-control').fadeOut();
//        } else if (currentIndex + 1 == carouselData.$items.length) {
//            $(this).children('.right.carousel-control').fadeOut();
//        }
//    });


    //
    //    $('.carousel .item').each(function () {
    //        var next = $(this).next();
    //        if (!next.length) {
    //            next = $(this).siblings(':first');
    //        }
    //        next.children(':first-child').clone().appendTo($(this));
    //
    //        for (var i = 0; i < 2; i++) {
    //            next = next.next();
    //            if (!next.length) {
    //                next = $(this).siblings(':first');
    //            }
    //
    //            next.children(':first-child').clone().appendTo($(this));
    //        }
    //    });


</script>


@stop
