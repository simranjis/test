@extends('layouts.default')
@section('content')
<style>
    .checkout-table td {
        white-space: nowrap;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #fff;
        font-size: 14px;
    }

    .submenu li {
        border-bottom: 1px solid #35D5E7;
    }
    .submenu li {
        color: #000;
        padding: 9px 19px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);;
    }
    .add-to-cart-box-amount {
        padding: 12px 6px;
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
        .stuck {
            width: 294px;
        }
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .destory-sticky-1{
            display: block !important;
        }
        .carousel-control.left {
            left: -26px;
        }
        .carousel-control.right {
            right: -26px;
        }
        .hovereffect img {
            width: 100%;
        }
    }
</style>

<div class="activity-pay-breadcrumbs">
    <div class="container">
        <h2 class="text-center">{{ $activity['destination'] }}</h2>
    </div>
</div>
<div class="container">
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Cities</a></li>
        <li class="active">Bali</li>
    </ul>
</div>
<div class="activity-pay-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all-dest">
                    <h2 class="text-uppercase">{{ $activity['title'] }}</h2>
                </div>                    
            </div>                
        </div>
        <div class="row">            
            <div class="col-sm-8">
                <hr>
                <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="color:#000" class="margin-top-0">QUICK INFO</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-user-o"  aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">{{ $activity['quickinfo1'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">{{ $activity['quickinfo2'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">{{ $activity['quickinfo3'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">{{ $activity['quickinfo4'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">{{ $activity['quickinfo5'] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            @if($activity['addedbytype'] === 'localguide')
                                            <div class="">
                                                <div class="text-center">
                                                    <img src="images/castle-3.png" style="height:35px;">
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <!--<p class="margin-top-0">{{ $activity['quickinfo6'] }}</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="city-description">
                            <h2>HIGHLIGHTS</h2>
                            <p>{{ $activity['hightlights'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="package-options">
                                <div class="btn-group">
                                    <button type="button" class="pink_btn_checkout dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <button type="button" class="pink_btn_checkout">Please select a booking date </button>
                                </div>
                                @if(count($activity['pricing']) > 0)
                                    @foreach($activity['pricing'] as $key => $value)
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h2>{{ $value['title'] }}</h2>
                                            </div>
                                            <div class="col-xs-4">
                                                <p class="text-center"><br><strong>${{ $value['price'] }}</strong></p>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample_{{ $value['id'] }}" aria-expanded="false" aria-controls="collapseExample">Select</button>
                                            </div>                                    
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="collapse" id="collapseExample_{{ $value['id'] }}" aria-expanded="true" style=""> 
                                                    <div class="well well-white">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <p>{{ $value['pricefor'] }}</p>
                                                                <div class="select-package">
                                                                    <div class="row">
                                                                        
                                                                        <div class="col-xs-12 col-sm-4">
                                                                            <h2>{{ $activity['title'] }}</h2>
                                                                        </div>
                                                                        
                                                                        <div class="col-xs-12 col-sm-4">
                                                                            <p class="text-center">${{ $value['price'] }}</p>
                                                                        </div>
                                                                        
                                                                        <div class="col-xs-12 text-right col-sm-4">
                                                                            <div class="quantity-width">                                                                            
                                                                                <div class="input-group">
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button" class="btn btn-black btn-number change-qty" disabled="disabled" data-type="minus" data-field="quantity[]">
                                                                                            <span class="glyphicon glyphicon-minus"></span>
                                                                                        </button>
                                                                                    </span>
                                                                                    <input type="text" name="quantity[]" class="form-control input-number text-center qty-box" value="1" min="1" max="10">
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button" class="btn btn-pink btn-number change-qty" data-type="plus" data-field="quantity[]">
                                                                                            <span class="glyphicon glyphicon-plus"></span>
                                                                                        </button>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="mt10">
                                                                    <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse_{{ $value['id'] }}" data-target="#collapseExample_{{ $value['id'] }}" aria-expanded="false" aria-controls="collapseExample">Ok</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    
                                    
                                    
                                    
<script type="text/javascript">

    $(function(){
       $('.change-qty').bind('click' , function(e){
           e.preventDefault();
           var datatype = $(this).attr("data-type");
           var input = $(this).parent().parent().find('input');
           var initial = input.val();
           
           console.log(initial);
           if(datatype === 'plus') {
               input.val(parseInt(parseInt(initial) + parseInt(1)));
               $(".change-qty").removeAttr("disabled");           
           } else {
                if(initial > 1) {
                    input.val(parseInt(parseInt(initial) - parseInt(1)));
                } else {
                    $(this).attr("disabled" , 'disabled');
                }
           }
       });
       
       
       $('.qty-box').bind('change' , function(e){
           if($(this).val() < 1) {
               $(this).val(1);
           }
       });
       
       
    });

</script>                                    
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="destory-sticky-1">
                    <div class="sticky-wrapper">
                        <div class="basic-dist-fix">
                            <div class="add-to-cart-box">
                                <h2>
                                    Booking Summary
                                </h2>
                                <p>Marine Park</p>
                                <p>26/12/2017</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left"><strong>User</strong></td>
                                                        <td class="text-center"><strong>Quantity</strong></td>
                                                        <td class="text-center"><strong>Price</strong></td>
                                                        <td class="text-right"><strong>Totals</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr>
                                                        <td>Adult</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center"><del><span class="small-font-text">$4000</span></del></td>
                                                        <td class="text-right">$685.99</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Child</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center"><del><span class="small-font-text">$4000</span></del></td>
                                                        <td class="text-right">$685.99</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="no-line text-center"><strong>Service Fees</strong></td>
                                                        <td class="no-line">$3/Person</td>
                                                        <td class="no-line text-center">2 Person</td>                                                        
                                                        <td class="no-line text-right">$6.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="no-line"></td>
                                                        <td class="no-line"></td>
                                                        <td class="no-line text-center"><strong>Total</strong></td>
                                                        <td class="no-line text-right">$691.99</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="checkout-button">
                                            <div class="row">
                                                <div class=" col-md-6 col-sm-12 col-xs-12 text-center">
                                                    <a href="{{ url('cart/add') }}" button="" type="button" class="text-center black_btn_checkout text-uppercase">Add To Cart</a>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 text-center">
                                                    <a href="{{ url('cart/checkout') }}" button="" type="button" class="text-center pink_btn_checkout  text-uppercase fixed-at-bottom">Book Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-to-cart-box mt10 add-to-cart-box-amount">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr>
                                                        <td class="text-left"><strong>20% Deposit</strong></td>
                                                        <td class="text-right pay-ammount"><a href="{{ url('cart/checkout') }}" button="" type="button" class="text-center pink_btn_checkout pink_btn_checkout_copy  text-uppercase fixed-at-bottom"> Pay $220</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                            <tr>
                                                                <td class="text-left">Amount Due</td>
                                                                <td class="text-right"><div class="pay-ammount">$4000</div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p>Balance to be Paid Directly to the Local Guide (in cash)</p>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end of container -->
    <div id="sira-end"></div>
    <div id="destroy-gallery-upperbox">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-all">
                        <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
                    </div>                    
                </div>                
            </div>
            <div class="carousel carousel-custom slide margin-top-40" id="myCarousel1">
                <div class="carousel-inner">
                    
                    @if($activity['gallery'])
                        @foreach($activity['gallery'] as $key => $value)
                            <div class="item @if($key == 0) active @endif">
                                <div class="col-md-3 col-sm-3">
                                    <div class="hovereffect">
                                        <a href="#">
                                            <img class="img-responsive" src="uploads/{{ $value['image'] }}" alt="">
                                        </a>
                                        <div class="overlay">
                                            <h2 class="feat-heading">{{ $value['title'] }}</h2>
                                            <p>
                                                {{ $value['title'] }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         
                        @endforeach
                    @endif
                </div> 
                <a data-slide="prev" href="#myCarousel1" class="left carousel-control carousel-control-custom">‹</a>

                <a data-slide="next" href="#myCarousel1" class="right carousel-control carousel-control-custom">›</a>
            </div>
        </div>
    </div>
    <div class="local-guide-host mt20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-all">
                        <h2 class="text-center"><span class="divider"></span>LOCAL GUIDE HOSTS</h2>
                    </div>                    
                </div>                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/host-pay-cash.gif" class="img-responsive">
                        <p>Book NOW! Pay only a deposit to confirm this booking and the balance, you can pay directly to your Local Guide in cash!</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-2.gif" class="img-responsive">
                        <p>Worry free booking! You will only be charged the deposit once the booking is CONFIRMED by the Local Guide and it’s AFTER the FREE cancelation period</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-3.gif" class="img-responsive">
                        <p> Directly contact your Local Guide! Once your booking is 100% confirmed, you can access your Local Guide’s contact details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<mbile slider>-->
<div class="container hidden-lg hidden-md hidden-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
            </div>                    
        </div>                
    </div>
    <div class="carousel slide margin-top-40" id="myCarousel123">
        <div class="carousel-inner">
                   @if($activity['gallery'])
                        @foreach($activity['gallery'] as $value)
                            <div class="item @if($key == 0) active @endif">
                                <div class="col-md-3 col-sm-3">
                                    <div class="hovereffect">
                                        <a href="#">
                                            <img class="img-responsive" src="uploads/{{ $value['image'] }}" alt="">
                                        </a>
                                        <div class="overlay">
                                            <h2 class="feat-heading">{{ $value['title'] }}</h2>
                                            <p>
                                                {{ $value['title'] }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
        </div> 
        <a data-slide="prev" href="#myCarousel123" class="left carousel-control">‹</a>
        <a data-slide="next" href="#myCarousel123" class="right carousel-control">›</a>
    </div>
</div>
<!--mobile slider end-->
<div class="accordion-well-background">
    <div class="container">
        <div class="row">            
            <div class="col-sm-12">
                <div class="accordion-well ">
                @if(count($activity['faqs']))
                    <ul id="accordion" class="accordion list-unstyled">
                        @foreach($activity['faqs'] as $value)
                            <li>
                                <div class="link"><i class="fa fa-info-circle" aria-hidden="true"></i>{{ $value['title'] }}<i class="fa fa-chevron-down"></i></div>
                                <div class="submenu list-unstyled">
                                    {{ $value['description'] }}
                                </div>
                            </li>
                        @endforeach    
                    </ul>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">What Others Say</h2>
            </div>                    
        </div>                
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="Carousel12" class="carousel slide" data-ride="carousel" data-interval="4000">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                </div><!--.carousel-inner-->
            </div><!--.Carousel-->
        </div>
    </div>
</div><!--.container-->
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                    $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            }
            ;
        }

        var accordion = new Accordion($('#accordion'), false);
    });

</script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })
</script>
<script>
//    var waypoints = $('#destroy-gallery-upperbox').waypoint(function (direction) {
//        if (direction == "down") {
//            $(".destory-sticky ul").css("display", "none")
//            $(".destory-sticky-1").css("display", "none")
//        } else {
//            $(".destory-sticky ul").css("display", "block")
//            $(".destory-sticky-1").css("display", "block")
//        }
//    }, {
//        offset: '40%'
//    });

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 200) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });
</script>
<script>

//    $(function () {
//        $('.carousel-custom').carousel-custom({
//            pause: true, // init without autoplay (optional)
//            interval: false, // do not autoplay after sliding (optional)
//            wrap: false // do not loop
//        });
//        // left control hide
//        //$('.carousel').children('.left.carousel-control').hide();
//    });
//    $('.carousel-custom').on('slid.bs.carousel', function () {
//        var carouselData = $(this).data('bs.carousel');
//        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
//        $(this).children('.carousel-control-custom').show();
//        if (currentIndex == 0) {
//            $(this).children('.left.carousel-control-custom').fadeOut();
//        } else if (currentIndex + 1 == carouselData.$items.length) {
//            $(this).children('.right.carousel-control-custom').fadeOut();
//        }
//    });

    $('.carousel-custom .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>

@stop