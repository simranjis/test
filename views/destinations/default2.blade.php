@extends('layouts.default')
@section('content')
<script type="text/javascript">

    $('.advanced-search-text').click(function(e){
        console.log('Why');
    })

</script>
<style>
    body{
        overflow-x: hidden;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    .carousel-inner .active.left { left: -25%; }
    .carousel-inner .active.right { left: 25%; }
    .carousel-inner .next        { left:  25%; }
    .carousel-inner .prev		 { left: -25%; }



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        opacity: 0;
        z-index: 1;
    }
    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 262px;
        z-index: 1;
    }
    .city-text-heading .overlay h2{
        padding: 0px;
    }
    .city-text-heading .overlay{
        padding: 1em;
    }
    .city-text-heading{
        margin: 15px 0px;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .carousel-control.left {
            left: -26px;
        }
        .carousel-control.right {
            right: -26px;
        }
        .hovereffect img {
            width: 100%;
        }
        .breadcrumbs-banner{
            min-height: 243px;
        }
        .advance-search {
            top: -12px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .stuck {
            width: 217px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
    }   
</style>
<div class="destination-breadcrumbs">
</div>
<div class="search-position">
    <div class="advance-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group margin-top-search" id="adv-search">
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs advanced-search-text" placeholder="Where's Your Next Adventure?" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Box 
    @include('pages.search.default')
    <!-- Search Box -->
</div>

<!--mobile tab bar-->
<div class="mobile-collapse">
    <div class="container hidden-lg hidden-md hidden-sm">
        <div class="text-center">
            <button type="button" class="text-center black_btn text-uppercase pad-button" data-toggle="modal" data-target=".bs-example-modal-lg">Browse by</button>
        </div>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myLargeModalLabel">Browse By</h4>
                    </div>
                    <div class="panel-group panel-padding" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        ASIA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body panel-padding-removed">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        EUROPE
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        NORTH AMERICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        SOUTH AMERICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        AFRICA
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        AUSTRALIA & NEW ZEALAND
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        UAE & MIDDLE EAST 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item list-head-title">Asia</li>
                                        <li class="list-group-item">THAILAND
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BANGKOK</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PHUKET</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHIANG MAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MALAYSIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  KUALA LUMPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PENANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">PHILIPPINES
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANILA</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  PALAWAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CEBU </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BOHOL </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item"><a href="#">SINGAPORE</a></li>
                                        <li class="list-group-item"><a href="#">HONG KONG</a></li>
                                        <li class="list-group-item"><a href="#">MACAU</a></li>
                                        <li class="list-group-item">VIETNAM
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HO CHI MINH</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HANOI </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HUE </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  HOI AN  </a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DANANG  </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDONESIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BALI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LOMBOK </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">BHUTAN
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  THIMPU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CHINA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BEIJING</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SHANGHAI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  CHENGDU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">MYANMAR
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  YANGON</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  BAGAN</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MANDALAY</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">CAMBODIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SIEM REAP</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">LAOS
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  LUANG PRABANG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">SOUTH KOREA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  SEOUL</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JEJU</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-group-item">INDIA
                                            <ul class="country-dropdown">
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  DELHI</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  JAIPUR</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>  MUMBAI</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--mobile tab bar end-->
<div class="tab-destination">
    <div class="container">        
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Browse by</h2>
                        </div>                    
                    </div>                
                </div>
                <div class="destory-sticky">
                    <ul class="nav nav-pills nav-stacked pill-dest basic-dist-fix">
                        @foreach($regions as $key => $value)
                            <li @if($value['id'] == 1) class="active" @endif><a href="#tab_{{ $value['id'] }}" data-toggle="pill">{{ $value['title'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">                
                <div class="tab-content">
                    @foreach($regions as $key => $value)
                        <div class="tab-pane @if($value['id'] == 1) active @endif" id="tab_{{ $value['id'] }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">Destinations in {{ $value['title'] }}</h2>
                                    </div>                    
                                </div>                
                            </div>
                           <div class="row">
                            <?php 
                            if(array_key_exists( $value['id'] , $destinations)){ ?>
                                <?php foreach($destinations[$value['id']] as $destination){ ?>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="hovereffect city-text-heading">
                                            <a href="{{ url('destinations/') }}/{{ $destination['id'] }}">
                                                
                                                <?php if($destination['image']){ ?>
                                                <img src="uploads/{{ $destination['image'] }}" class="img-responsive center-block" style="height:180px;width:360px" alt="Test Image">
                                                <?php }else{ ?>
                                                    <img src="images/missing-image-640x360-1-360x180.png" class="img-responsive center-block" alt="">
                                                <?php } ?>
                                                <div class="overlay">
                                                    <h2 class="feat-heading">{{ $destination['title'] }}</h2>
                                                    <p>
                                                        {{ $destination['countries_id'] }}
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            </div>
                        </div>
                    @endforeach
                </div><!-- tab content -->
            </div>
        </div>
    </div><!-- end of container -->
</div> 
<div id="sira-end"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 420) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });

</script>
<script>

    $(function () {
        $('.carousel').carousel({
            pause: true, // init without autoplay (optional)
            interval: false, // do not autoplay after sliding (optional)
            wrap: true // do not loop
        });
    });


</script>


@stop

