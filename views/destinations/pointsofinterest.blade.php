@extends('layouts.default')
@section('content')
<style>
    .checkout-table td {
        white-space: nowrap;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-40px,0,0);
        transform: translate3d(-40px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    /*    .carousel-inner .active.left { left: -25%; }
        .carousel-inner .active.right { left: 25%; }
        .carousel-inner .next        { left:  25%; }
        .carousel-inner .prev		 { left: -25%; }*/



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    /*    .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }*/
    /*    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }*/

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 363px;
        z-index: 1;
    }
    .accordion {
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .accordion-well {
        margin:30px 0px;
    }

    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #e2e2e2;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li:last-child .link { border-bottom: 0; }

    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }

    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }

    .accordion li.open .link { color: #b63b4d; }

    .accordion li.open i { color: #b63b4d; }

    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    /**
     * Submenu
     -----------------------------*/


    .submenu {
        display: none;
        background: #fff;
        font-size: 14px;
    }

    .submenu li {
        border-bottom: 1px solid #35D5E7;
    }
    .submenu li {
        color: #000;
        padding: 9px 19px;
    }

    .submenu a {
        display: block;
        text-decoration: none;
        color: #d9d9d9;
        padding: 12px;
        padding-left: 42px;
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
    }

    .submenu a:hover {
        background: #b63b4d;
        color: #FFF;
    }
    .well {
        border: none;
        border-radius: 0px;
    }
    .well-shadow{
        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0);;
    }
    .add-to-cart-box-amount {
        padding: 12px 6px;
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 221px;
        }
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .checkout-button a {
            display: block;
            margin: 6px 0;
            overflow: hidden;
        }
        .stuck {
            width: 294px;
        }
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .destory-sticky-1{
            display: block !important;
        }
        .carousel-control.left {
            left: -26px;
        }
        .carousel-control.right {
            right: -26px;
        }
        .hovereffect img {
            width: 100%;
        }
    }
</style>
<div class="activity-pay-breadcrumbs">
    <div class="container">
        <h2 class="text-center">Bali</h2>
    </div>
</div>
<div class="container">
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Cities</a></li>
        <li class="active">Bali</li>
    </ul>
</div>
<div class="activity-pay-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all-dest">
                    <h2 class="text-uppercase">Bali Safari and Marine Park</h2>
                </div>                    
            </div>                
        </div>
        <div class="row">            
            <div class="col-sm-8">
                <hr>
                <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="color:#000" class="margin-top-0">QUICK INFO</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-user-o"  aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Minimum 2 Travelers To Book</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Confirmation: 48H</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Duration: 6 days</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-user" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Tour Type: Private</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="marin-box">
                                                <div class="text-center">
                                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Redemption: Show mobile e-voucher</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 ovrflw-hidden">
                            <div class="well-text">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-5 col-md-4">
                                        <div class="marine_park-box">
                                            <div class="">
                                                <div class="text-center">
                                                    <img src="images/castle-3.png" style="height:35px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7 col-md-8">
                                        <p class="margin-top-0">Local Guide Hosts</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="city-description">
                            <h2>HIGHLIGHTS</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed libero sagittis, tempus mauris et, tincidunt purus. In posuere metus vitae dolor ultrices commodo. Vivamus imperdiet nisl lorem, eu elementum lectus viverra at. Quisque consequat mollis tortor, sit amet placerat tellus suscipit quis. Fusce sagittis massa at libero gravida eleifend. Morbi eleifend volutpat est, sed malesuada tortor varius a. Sed magna est, pulvinar vitae lacinia mollis, hendrerit sed nunc. Aliquam sed dui gravida diam fermentum iaculis. Ut lacinia rutrum est nec blandit. In hac habitasse platea dictumst. Quisque condimentum tortor id nisi finibus, eget dapibus elit maximus.</p>
                        </div>
                    </div>
                </div>
                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="package-options">
                                <div class="btn-group">
                                    <button type="button" class="pink_btn_checkout dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <button type="button" class="pink_btn_checkout">Please select a booking date </button>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2>FAMILY FUN PACKAGE </h2>
                                    </div>
                                    <div class="col-xs-4">
                                        <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Select</button>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="collapse" id="collapseExample" aria-expanded="true" style=""> 
                                            <div class="well well-white">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <p>Child</p>
                                                        <div class="select-package">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <h2>1 DAY PARK HOPPER</h2>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                                                </div>
                                                                <div class="col-xs-12 text-right col-sm-4">
                                                                    <div class="quantity-width">                                                                            
                                                                        <div class="input-group">
                                                                            <span class="input-group-btn">
                                                                                <button type="button" class="btn btn-black btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                                </button>
                                                                            </span>
                                                                            <input type="text" name="quant[1]" class="form-control input-number text-center" value="1" min="1" max="10">
                                                                            <span class="input-group-btn">
                                                                                <button type="button" class="btn btn-pink btn-number" data-type="plus" data-field="quant[1]">
                                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                                </button>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt10">
                                                            <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Ok</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2>1 DAY PARK HOPPER</h2>
                                    </div>
                                    <div class="col-xs-4">
                                        <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">Select</button>
                                    </div>                                   
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="collapse" id="collapseExample1" aria-expanded="true" style=""> 
                                            <div class="well well-white">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <p>Adult</p>
                                                            <div class="select-package">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4">
                                                                        <h2>1 DAY PARK HOPPER</h2>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4">
                                                                        <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 text-right">
                                                                        <div class="quantity-width">                                                                            
                                                                            <div class="input-group">
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-black btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                                                        <span class="glyphicon glyphicon-minus"></span>
                                                                                    </button>
                                                                                </span>
                                                                                <input type="text" name="quant[1]" class="form-control input-number text-center" value="1" min="1" max="10">
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-pink btn-number" data-type="plus" data-field="quant[1]">
                                                                                        <span class="glyphicon glyphicon-plus"></span>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-xs-12">
                                                            <p>Child</p>
                                                            <div class="select-package">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4">
                                                                        <h2>1 DAY PARK HOPPER</h2>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4">
                                                                        <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 text-right">
                                                                        <div class="quantity-width">                                                                            
                                                                            <div class="input-group">
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-black btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                                                        <span class="glyphicon glyphicon-minus"></span>
                                                                                    </button>
                                                                                </span>
                                                                                <input type="text" name="quant[1]" class="form-control input-number text-center" value="1" min="1" max="10">
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-pink btn-number" data-type="plus" data-field="quant[1]">
                                                                                        <span class="glyphicon glyphicon-plus"></span>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mt10">
                                                                <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Ok</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2>Package</h2>
                                    </div>
                                    <div class="col-xs-4">
                                        <p class="text-center"><del><span class="small-font-text">$4000</span></del><br><strong>$3000</strong></p>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <button type="button" class=" pink_btn_checkout text-uppercase" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Select</button>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="collapse" id="collapseExample" aria-expanded="true" style=""> 
                                            <div class="well"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="destory-sticky-1">
                    <div class="sticky-wrapper">
                        <div class="basic-dist-fix">
                            <div class="add-to-cart-box">
                                <h2>
                                    Booking Summary
                                </h2>
                                <p>Marine Park</p>
                                <p>26/12/2017</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left"><strong>User</strong></td>
                                                        <td class="text-center"><strong>Quantity</strong></td>
                                                        <td class="text-center"><strong>Price</strong></td>
                                                        <td class="text-right"><strong>Totals</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr>
                                                        <td>Adult</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center"><del><span class="small-font-text">$4000</span></del></td>
                                                        <td class="text-right">$685.99</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Child</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center"><del><span class="small-font-text">$4000</span></del></td>
                                                        <td class="text-right">$685.99</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="no-line text-center"><strong>Service Fees</strong></td>
                                                        <td class="no-line">$3/Person</td>
                                                        <td class="no-line text-center">2 Person</td>                                                        
                                                        <td class="no-line text-right">$6.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="no-line"></td>
                                                        <td class="no-line"></td>
                                                        <td class="no-line text-center"><strong>Total</strong></td>
                                                        <td class="no-line text-right">$691.99</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="checkout-button">
                                            <div class="row">
                                                <div class=" col-md-6 col-sm-12 col-xs-12 text-center">
                                                    <a href="{{ url('cart/add') }}" button="" type="button" class="text-center black_btn_checkout text-uppercase">Add To Cart</a>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 text-center">
                                                    <a href="{{ url('cart/checkout') }}" button="" type="button" class="text-center pink_btn_checkout  text-uppercase fixed-at-bottom">Book Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-to-cart-box mt10 add-to-cart-box-amount">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive checkout-table">
                                            <table class="table table-condensed">
                                                <thead>
                                                    <tr>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                    <tr>
                                                        <td class="text-left"><strong>20% Deposit</strong></td>
                                                        <td class="text-right pay-ammount"><a href="{{ url('cart/checkout') }}" button="" type="button" class="text-center pink_btn_checkout pink_btn_checkout_copy  text-uppercase fixed-at-bottom"> Pay $220</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                            <tr>
                                                                <td class="text-left">Amount Due</td>
                                                                <td class="text-right"><div class="pay-ammount">$4000</div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p>Balance to be Paid Directly to the Local Guide (in cash)</p>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end of container -->
    <div id="sira-end"></div>
    <div id="destroy-gallery-upperbox">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-all">
                        <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
                    </div>                    
                </div>                
            </div>
            <div class="carousel carousel-custom slide margin-top-40" id="myCarousel1">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/nepal.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">South Iceland</h2>
                                    <p>
                                        South Iceland
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/phuket.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Phuket</h2>
                                    <p>
                                        Phuket
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/thailand.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Bali</h2>
                                    <p>
                                        Bali
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/philp.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Chile</h2>
                                    <p>
                                        Chile
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination/booking') }}">
                                    <img class="img-responsive" src="images/argentina.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Argentina</h2>
                                    <p>
                                        Argentina
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/philp.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Nairobi</h2>
                                    <p>
                                        Nairobi
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/paris.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Paris</h2>
                                    <p>
                                        Paris
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-3 col-sm-3">
                            <div class="hovereffect">
                                <a href="{{ url('destinations/destination') }}">
                                    <img class="img-responsive" src="images/dubai-2.png" alt="">
                                </a>
                                <div class="overlay">
                                    <h2 class="feat-heading">Dubai</h2>
                                    <p>
                                        Dubai
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <a data-slide="prev" href="#myCarousel1" class="left carousel-control carousel-control-custom">‹</a>

                <a data-slide="next" href="#myCarousel1" class="right carousel-control carousel-control-custom">›</a>
            </div>
        </div>
    </div>
    <div class="local-guide-host mt20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-all">
                        <h2 class="text-center"><span class="divider"></span>LOCAL GUIDE HOSTS</h2>
                    </div>                    
                </div>                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/host-pay-cash.gif" class="img-responsive">
                        <p>Book NOW! Pay only a deposit to confirm this booking and the balance, you can pay directly to your Local Guide in cash!</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-2.gif" class="img-responsive">
                        <p>Worry free booking! You will only be charged the deposit once the booking is CONFIRMED by the Local Guide and it’s AFTER the FREE cancelation period</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="local-host-content">
                        <img src="images/local-guide-3.gif" class="img-responsive">
                        <p> Directly contact your Local Guide! Once your booking is 100% confirmed, you can access your Local Guide’s contact details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<mbile slider>-->
<div class="container hidden-lg hidden-md hidden-sm">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase"><span class="divider"></span>GALLERY</h2>
            </div>                    
        </div>                
    </div>
    <div class="carousel slide margin-top-40" id="myCarousel123">
        <div class="carousel-inner">
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/nepal.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">South Iceland</h2>
                            <p>
                                South Iceland
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item active">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/phuket.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Phuket</h2>
                            <p>
                                Phuket
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/thailand.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Bali</h2>
                            <p>
                                Bali
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Chile</h2>
                            <p>
                                Chile
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/argentina.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Argentina</h2>
                            <p>
                                Argentina
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/philp.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Nairobi</h2>
                            <p>
                                Nairobi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/paris.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Paris</h2>
                            <p>
                                Paris
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-3 col-sm-3">
                    <div class="hovereffect">
                        <a href="{{ url('destinations/destination') }}">
                            <img class="img-responsive" src="images/dubai-2.png" alt="">
                        </a>
                        <div class="overlay">
                            <h2 class="feat-heading">Dubai</h2>
                            <p>
                                Dubai
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <a data-slide="prev" href="#myCarousel123" class="left carousel-control">‹</a>

        <a data-slide="next" href="#myCarousel123" class="right carousel-control">›</a>
    </div>
</div>
<!--mobile slider end-->
<div class="accordion-well-background">
    <div class="container">
        <div class="row">            
            <div class="col-sm-12">
                <div class="accordion-well ">
                    <ul id="accordion" class="accordion list-unstyled">
                        <li>
                            <div class="link"><i class="fa fa-info-circle" aria-hidden="true"></i>ADDITIONAL INFORMATION<i class="fa fa-chevron-down"></i></div>
                            <ul class="submenu list-unstyled">
                                <li>You will receive a confirmation email and voucher within 2 business working days.</li>
                                <li>Once confirmed, we will send you the voucher via email. In the event that you do not receive an email from us, please check your Spam folder or notify us via email</li>
                                <li>With six whole days to explore Bhutan, you get an immersive insight into the country, with all food, accommodation and transport included</li>
                                <li>Discover the natural beauty of Thimphu, Wangle and Punakha valleys</li>
                                <li>Don't miss key attractions like Chime Lhakhang Fertility Temple and 
                                    Punakha Dzong, the Fortress of Great Bliss
                                </li>
                                <li>The tour includes car transport between sights and some moderate hiking but can be full customized to your preferences</li>
                            </ul>
                        </li>
                        <li>
                            <div class="link"><i class="fa fa-database"></i>INCLUSIONS/EXCLUSIONS<i class="fa fa-chevron-down"></i></div>
                            <ul class="submenu list-unstyled">
                                <li class="li-heading">Inclusive of:</li>
                                <li>Everything you need for the entire trip duration in Bhutan</li>
                                <li>5 nights accommodation in standard 3 star hotels</li>
                                <li>All meals including bottled water throughout the trip</li>
                                <li>English speaking guide, other languages (Chinese, Japanese, French, Spanish and German) require a surcharge</li>
                                <li>Visa fees</li>
                                <li>Local SIM card (upon request)</li>
                                <li>Roundtrip airport transfers</li>
                                <li class="li-heading">Not Inclusive Of:</li>
                                <li>International airfare</li>
                                <li>Alcoholic beverages</li>
                                <li>Tips/gratuities</li>
                                <li>Expenses of personal nature and any other expenses not mentioned in the above cost</li>
                            </ul>
                        </li>
                        <li>
                            <div class="link"><i class="fa fa-check-square-o" aria-hidden="true"></i> ITINERARY<i class="fa fa-chevron-down"></i></div>
                            <ul class="submenu list-unstyled">
                                <li>Pick up from airport</li>
                                <li>Dungtse Lhakhang (temple)</li>
                                <li>Paro Dazong (Fortress of Heaped Jewels)</li>
                                <li>National Museum</li>
                                <li>Drive one hour journey to Thimphu</li>
                                <li>Stroll through local markets</li>
                                <li class="li-heading">Day 2 Thimphu Valley</li>
                                <li>Memorial Chorten/Stupa</li>
                                <li>Buddha Dordenma</li>
                                <li>Bhutan Postal Museum</li>
                                <li>Simply Bhutan Museum</li>
                                <li>Weekend Centenary Farmers Market</li>
                                <li>Takin Preserve</li>
                                <li>Tashi Chho Dzong (Fortress of Splendid Religion)</li>
                                <li>Dragon King of Bhutan</li>
                                <li>Visit local bazaars</li>
                            </ul>
                        </li>
                        <li>
                            <div class="link"><i class="fa fa-binoculars" aria-hidden="true"></i>HOW TO USE AND HOW TO GET THERE<i class="fa fa-chevron-down"></i></div>
                            <ul class="submenu list-unstyled">
                                <li class="li-heading">HOW TO USE</li>
                                <li>You can present either a printed or a mobile voucher for this activity</li>
                                <li>Our land operator will coordinate further on the required documents and flight confirmation</li>
                                <li>Please plan your flights before booking</li>
                                <li class="li-heading">HOW TO GET THERE</li>
                                <li>You can present either a printed or a mobile voucher for this activity</li>
                                <li>Our land operator will coordinate further on the required documents and flight confirmation</li>
                                <li>Please plan your flights before booking</li>
                            </ul>
                        </li>
                        <li>
                            <div class="link"><i class="fa fa-ban" aria-hidden="true"></i>CANCELLATION<i class="fa fa-chevron-down"></i></div>
                            <ul class="submenu list-unstyled">
                                <li>Full refunds will be issued for cancelations made at least 40 days prior to the activity</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-all">
                <h2 class="text-uppercase text-center">What Others Say</h2>
            </div>                    
        </div>                
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="Carousel12" class="carousel slide" data-ride="carousel" data-interval="4000">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-12 margin-top-40">
                                <img src="images/profile-pic-4.jpg" alt="John" class="img-responsive center-block img-circle">
                            </div>
                            <div class="col-sm-12">
                                <div class="testimonial margin-top-40">
                                    <h4 class="text-center"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></h4>
                                    <p class="text-center">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.</p>
                                    <p class="name-testimonial text-right">-Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div><!--.item-->
                </div><!--.carousel-inner-->
            </div><!--.Carousel-->
        </div>
    </div>
</div><!--.container-->
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                    $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            }
            ;
        }

        var accordion = new Accordion($('#accordion'), false);
    });

</script>
<script>
    var sticky = new Waypoint.Sticky({
        element: $('.basic-dist-fix')[0]
    })
</script>
<script>
//    var waypoints = $('#destroy-gallery-upperbox').waypoint(function (direction) {
//        if (direction == "down") {
//            $(".destory-sticky ul").css("display", "none")
//            $(".destory-sticky-1").css("display", "none")
//        } else {
//            $(".destory-sticky ul").css("display", "block")
//            $(".destory-sticky-1").css("display", "block")
//        }
//    }, {
//        offset: '40%'
//    });

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 200) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });
</script>
<script>

//    $(function () {
//        $('.carousel-custom').carousel-custom({
//            pause: true, // init without autoplay (optional)
//            interval: false, // do not autoplay after sliding (optional)
//            wrap: false // do not loop
//        });
//        // left control hide
//        //$('.carousel').children('.left.carousel-control').hide();
//    });
//    $('.carousel-custom').on('slid.bs.carousel', function () {
//        var carouselData = $(this).data('bs.carousel');
//        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
//        $(this).children('.carousel-control-custom').show();
//        if (currentIndex == 0) {
//            $(this).children('.left.carousel-control-custom').fadeOut();
//        } else if (currentIndex + 1 == carouselData.$items.length) {
//            $(this).children('.right.carousel-control-custom').fadeOut();
//        }
//    });

    $('.carousel-custom .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>

@stop