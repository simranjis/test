@extends('layouts.default')
@section('content')

<style>
    .feat-price a img{
        cursor: pointer;
    }
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
        background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
        background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
    }

    .hovereffect .overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        padding: 3em;
        text-align: left;
    }

    .hovereffect img {
        width: 100%;
        display: block;
        position: relative;
        max-width: none;
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-0px,0,0);
        transform: translate3d(-0px,0,0);
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        position: relative;
        font-size: 17px;
        background-color: transparent;
        padding: 65px 0 0 0;
        text-align: center;
    }

    .hovereffect .overlay:before {
        position: absolute;
        top: 20px;
        right: 20px;
        bottom: 20px;
        left: 20px;
        border: 1px solid #fff;
        content: '';
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-20px,0,0);
        transform: translate3d(-20px,0,0);
    }

    .hovereffect a, .hovereffect p {
        color: #FFF;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: opacity 0.35s, -webkit-transform 0.45s;
        transition: opacity 0.35s, transform 0.45s;
        -webkit-transform: translate3d(-10px,0,0);
        transform: translate3d(-10px,0,0);
        text-align: center;
    }

    .hovereffect:hover img {
        opacity: 0.6;
        filter: alpha(opacity=60);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover .overlay:before,
    .hovereffect:hover a, .hovereffect:hover p {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }
    .carousel-inner .active.left { left: -25%; }
    .carousel-inner .active.right { left: 25%; }
    .carousel-inner .next        { left:  25%; }
    .carousel-inner .prev		 { left: -25%; }



    .carousel-control {
        display: block;
        width: 60px;
        height: 100%;
        font-size: 100px;
        background: rgba(0, 0, 0, 0);
        font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
        font-weight: 300;
        line-height: 2;
    }

    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        opacity: 0;
        z-index: 1;
    }
    .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right { opacity: 1; }

    .carousel-fade .carousel-control { z-index: 2; }
    .carousel-control.left {
        background-image: none;
        color: #000;
        left: -42px;
        position: absolute;
        top: 45px;
    }
    .carousel-control.right {
        background-image: none;
        color: #000;
        right: -42px;
        position: absolute;
        top: 45px;
    }
    .stuck {
        background: #fff none repeat scroll 0 0;
        position: fixed;
        top: 50px;
        width: 262px;
        z-index: -26;
    }
    .feat-price-tag h2 {
        padding: 27px 0 17px 11px !important;
    }
    .feat-price-tag p {
        padding: 15px 12px 0 0;
    }
    .castle-on-image img {
        height: 57px;
        left: 10px;
        position: absolute;
        top: 98px;
    }
    .castle-on-image {
        position: relative;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
            transition: opacity 0.35s ease 0s, transform 0.45s ease 0s;
        }
        .stuck {
            position: inherit;
            width: auto;
        }
        .pill-dest li a {
            font-size: 11px;
            padding: 16px 10px;
        }
        .nav-pills-custom > li {
            float: left;
        }
        .activity-breadcrumbs {
            background-repeat: no-repeat;
        }
        .tab-destination {
            margin-top: 0px;
        }
        .advance-search {
            top: -12px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:landscape){
        .stuck {
            width: 217px;
        }
        .castle-on-image img {
            top: 68px;
        }
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px) and (orientation:portrait){
        .stuck {
            width: 173px;
        }
        .castle-on-image img {
            top: 68px;
        }
        .hovereffect img {
            transform: translate3d(0px, 0px, 0px);
        }
    }
    .img-inline-direction img{
        width:100% !important;
        height:450px !important;
    }
</style>
<div class="contact-breadcrumbs img-inline-direction">
    @if(is_file(public_path('/uploads/'.$destinations['image'])))
    <img src="{{url('/uploads/'.$destinations['image'])}}" class="img-responsive" alt="">
    @else
    <img src="<?php echo url('/images/activity.png'); ?>" class="img-responsive" alt="">
    @endif
    <div class="container img-inline-direction-absolute">
        <h2 class="text-center">{{ $destinations['title'] }}</h2>
    </div>
</div>
<div class="search-position">
    <div class="advance-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group margin-top-search" id="adv-search">
                        <!--                        <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default dropdown-toggle search-custom-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </div>-->
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs" placeholder="Where's Your Next Adventure?" />
                        <!--<input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-lg hidden-md hidden-sm" placeholder="Where To?" />-->
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Box -->
    @include('pages.search.default')
    <!-- Search Box -->
</div>
@if(count($activities))
<div class="tab-destination margin-top-0">
    <div class="container">        
        <div class="row">
            <div class="col-sm-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase">Browse by</h2>
                        </div>                    
                    </div>                
                </div>
                <div class="destory-sticky">
                    <ul class="nav nav-pills nav-stacked pill-dest basic-dist-fix nav-pills-custom">         
                        <!--<li class="active"><a href="#tab_a" data-toggle="pill">AUTHENTIC EXPERIENCES</a></li>
                        <li><a href="#tab_b" data-toggle="pill">POPULAR ACTIVITIES</a></li>-->
                        <?php
                        foreach ($activities as $key => $value) {

                            $tab_index = ($key == 1) ? "a" : "b";
                            ?>
                            <li @if($key == '1') class="active" @endif><a href="#tab_{{ $tab_index }}" data-toggle="pill">{{ $categories[$key] }}</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <div class="col-sm-9">                
                <div class="tab-content">
                    <?php
                    foreach ($activities as $key => $value) {
                        $tab_index = ($key == 1) ? "a" : "b";
                        ?>

                        <div class="tab-pane @if($key == '1') active @endif" id="tab_{{ $tab_index }}">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title-all-dest">
                                        <h2 class="text-uppercase">{{ $categories[$key] }} IN {{ $destinations['title'] }}</h2>
                                    </div>                    
                                </div>                
                            </div>

                            <div class="row">

                                @foreach($value as $activity)
                                <div class="col-md-4 col-sm-6">
                                    <div class="feat-price">

                                        <a href="{{ url('destinations') }}/{{ $destinations['id'] }}/{{ $activity['id'] }}">
                                            <div class="hovereffect">

                                                @if($activity['image'])
                                                <img src="uploads/{{ $activity['image'] }}" class="img-responsive center-block" alt="" style="width: 360px; height: 130px;" >
                                                @else
                                                <img src="images/missing-image-640x360-1-360x180.png" class="img-responsive center-block" alt=""  style="width: 360px; height: 130px;" >
                                                @endif
                                            </div>
                                            <div class="castle-on-image">
                                                @if($activity['addedbytype'] === 'localguide')
                                                <img src="images/castle-3.png" class="img-responsive center-block" alt="" >
                                                @endif
                                            </div>
                                        </a>

                                        <div class="row">
                                            <div class="feat-price-tag">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <h2 class="text-left">
                                                                <?php
                                                                $activity_title = $activity['title'];
                                                                if (strlen($activity_title) > 30) {
                                                                    $activity_title = substr($activity_title, 0, 30) . '...';
                                                                }
                                                                echo $activity_title;
                                                                ?>
                                                            </h2>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <p class="text-right"><del><span class="small-font-text">{{$activity['currency'].' '.number_format($activity['originalPrice'],2)}}</span></del><br/>{{$activity['currency'].' '.number_format($activity['minPrice'],2)}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                @endforeach

                            </div>
                        </div>

                    <?php } ?>


                </div><!-- tab content -->
            </div>

        </div>
    </div><!-- end of container -->
</div>
@else
<div class="row">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4 text-center">
        <h1>No Activities Found</h1>
    </div>
    <div class="col-sm-4">
    </div>
</div>
@endif
<div id="sira-end"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js"></script>
<script>
var sticky = new Waypoint.Sticky({
    element: $('.basic-dist-fix')[0]
})
</script>
<script>

    $(function () {
        $('.carousel').carousel({
            pause: true, // init without autoplay (optional)
            interval: false, // do not autoplay after sliding (optional)
            wrap: false // do not loop
        });
        // left control hide
        //$('.carousel').children('.left.carousel-control').hide();
    });
    $('.carousel').on('slid.bs.carousel', function () {
        var carouselData = $(this).data('bs.carousel');
        var currentIndex = carouselData.getItemIndex(carouselData.$element.find('.item.active'));
        $(this).children('.carousel-control').show();
        if (currentIndex == 0) {
            $(this).children('.left.carousel-control').fadeOut();
        } else if (currentIndex + 1 == carouselData.$items.length) {
            $(this).children('.right.carousel-control').fadeOut();
        }
    });

    $(window).scroll(function (e) {
        var offset = $("#sira-end").offset().top;
//        alert($(window).scrollTop() + ' ' + offset);
        if (($(window).scrollTop() + 200) > offset) {
            $(".destory-sticky ul").css("display", "none")
            $(".destory-sticky-1").css("display", "none")
        } else {
            $(".destory-sticky ul").css("display", "block")
            $(".destory-sticky-1").css("display", "block")
        }
    });

    $('.carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


</script>


@stop