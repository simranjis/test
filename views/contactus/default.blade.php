@extends('layouts.default')
@section('content')

<script type="text/javascript">



    $(function () {
        var number1 = Math.floor((Math.random() * 10) + 1);
        var number2 = Math.floor((Math.random() * 30) + 1);
        var theirsum = number1 + number2;
        $("#firstInt").text(number1);
        $("#secondInt").text(number2);
        $("#hiddenipthing").val(theirsum);
    });


</script>

<style>
    .social {
        position: relative;
        height: 3em;
        width: 13.5em;
        margin: 76px auto;
    }

    .social li {
        display: block;
        height: 4em;
        line-height: 4em;
        margin: -2.2em;
        position: absolute;
        -webkit-transition: -webkit-transform .7s;
        -moz-transition: -moz-transform .7s;
        -ms-transition: -ms-transform .7s;
        -o-transition: -o-transform .7s;
        transition: transform .7s;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        text-align: center;
        width: 4em;

    }

    .social a {
        color: #fffdf0;
        display: block;
        height: 4em;
        line-height: 6em;
        text-align: center;
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
        width: 4em; 

    }

    .social li:hover {
        -webkit-transform: scale(1.3,1.3) rotate(45deg);
        -moz-transform: scale(1.3,1.3) rotate(45deg);
        -ms-transform: scale(1.3,1.3) rotate(45deg);
        -o-transform: scale(1.3,1.3) rotate(45deg);
        transform: scale(1.3,1.3) rotate(45deg);
    }

    .facebook {
        background: #155b9d;
        left: 0;
        top: 0%;
    }

    .twitter {
        background: #1a9ec4;
        bottom: 0;
        left: 25%;
    }
    .facebook2 {
        background: #155b9d;
        left: 50%;
        top: 0%;
    }

    .twitter2 {
        background: #1a9ec4;
        bottom: 0;
        left: 25%;
    }

    .pinterest {
        background: #e11a30;
        left: 50%;
        top: 0%;
    }
    .behance {
        background: #E11A30;
        bottom: 0;
        left: 75%;
    }
    .behance2 {
        background: #E11A30;
        top: 0%;
        left: 50%;
    }
    .behance3 {
        background: #E11A30;
        bottom: 0;
        left: 75%;
    }
    .contact-post {
        background: #591F77;
        border-radius: 92px;
    }
    .contact-post h6{
        color:#fff;
        font-size: 16px;
    }
    @media only screen and (max-width:766px) and (min-width:320px){
        .black_btn_contact {
            padding: 13px 46px;
            font-size: 15px !important;
        }
        .contact-post {
            border-radius: 0px;
            padding: 10px;
        }
    }
    @media only screen and (max-width:1024px) and (min-width:768px){
        .contact-post {
            border-radius: 0px;
            padding: 10px;
            word-wrap: break-word;
        }
    }

</style>
<div class="contact-breadcrumbs">
    <img src="images/contact-us.jpg" class="img-responsive" alt="">
</div>
<div class="container">
    <ul class="breadcrumb custom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">Contact Us</li>
    </ul>
</div>
<div class="activity-pay-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-all-dest">
                    <h2 class="text-uppercase">SAY HELLO!</h2>
                </div>                    
            </div>                
        </div>
        {!! Form::open(['url' => 'contactussubmit', 'name' => 'contactusForm' , 'class'=>'contactusForm', 'id'=>'contactusForm']) !!}            
        <div class="row">            

            <div class="col-sm-6">
                <div class="contact-post">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="images/cont.png" class="img-responsive center-block">
                        </div>
                        <div class="col-md-8">
                            <img src="images/cont-1.png" class="img-responsive center-block">
                            <h3><img src="images/smartphone.png" alt="viber">&ensp;{{ $contactDetailObj ? $contactDetailObj->contact_number_1 : "---"}}</h3>
                            <h3><img src="images/viber.png" alt="viber">&ensp;{{$contactDetailObj ? $contactDetailObj->contact_number_2 : "---"}}</h3>
                            <h3><img src="images/whatsapp.png" alt="viber">&ensp;{{$contactDetailObj ? $contactDetailObj->contact_number_3 : "---"}}</h3>
                            <h6>{{$contactDetailObj ? $contactDetailObj->email : "---"}}</h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-all-dest">
                            <h2 class="text-uppercase"><span class="divider"></span>Let's Get Social!</h2>
                        </div>                    
                    </div>                
                </div>
                <ul class="social">
                    <li class="facebook"><a href="404.php"><i class="fa fa-facebook fa-3x"></i></a></li>
                    <li class="twitter"><a href="404.php"><i class="fa fa-twitter fa-3x"></i></a></li>
                    <li class="pinterest"><a href="404.php"><i class="fa fa-instagram fa-3x"></i></a></li>
                    <li class="behance"><a href="404.php"><i class="fa fa-youtube-play fa-3x"></i></a></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <div class="well well-shadow">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label  for="inputHelpBlock" class="label-font">Name</label>
                                    <input type="text" name="contactName" class="form-control form-control-custom" placeholder="Name">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label  for="inputHelpBlock" class="label-font">Email</label>
                                    <input type="text" name="contactEmail" class="form-control form-control-custom" placeholder="Email">
                                </div>
                            </div>
                            <hr>
                            <div class="row">                            
                                <div class="col-sm-12">
                                    <label  for="inputHelpBlock" class="label-font">Message</label>
                                    <textarea class="form-control" name="contactMessage" rows="5" id="comment"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="hidden" name="captache" id="hiddenipthing" value="">

                                    <label  for="inputHelpBlock" class="label-font">Captcha:</label>
                                    <span id="firstInt"></span> + <span id="secondInt"></span> = <input type="text" name="captacha" class="form-control form-control-custom" placeholder="12121">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center mt20">
                                        <button id="contactusForm-submit-button" type="button" class="text-center black_btn text-uppercase black_btn_contact">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div><!-- end of container -->
</div>


@stop