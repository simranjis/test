@extends('traveller.layouts.defaultsidebar')
@section('content')

<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Rewards
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Rewards</li>
        </ol>
    </section>


    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Rewards</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        
                                    <table border="1" class="table table-hover table-bordered">
                                        <tr class="bg-gray">
                                            <th>Title</th>
                                            <th>Discount</th>
                                            <th>Quantity</th>
                                            <th>Valid From</th>
                                            <th>Valid To</th>
                                        </tr>
                                        <?php if(count($promos)){ ?>           
                                            <?php foreach($promos as $key => $value) { ?>
                                                <tr>
                                                    <td>
                                                        <a onclick="getPromoCode('<?php echo $value->id; ?>', '<?php echo $value->promocode; ?>')" role="button" data-toggle="modal" data-target="#modal-default-4">
                                                            {{$value->title}}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{$value->discount}}
                                                        <?php
                                                            if(!empty($value->discount_type)){
                                                                echo ' %';
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        {{$value->qty}}

                                                        <?php
                                                            if($value->qty_type=='day'){
                                                                echo ' Per Day';
                                                            }else{
                                                                echo ' Total';
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo date('d F Y', strtotime($value->startdate)) ?>
                                                    </td>
                                                    <td>
                                                        <?php echo date('d F Y', strtotime($value->enddate)) ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php }else{ ?>    
                                            <tr>
                                                <td colspan="5">No Promos Available</td>
                                            </tr>
                                        <?php } ?>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

    <!-- all local guides -->
    @include('traveller.pages.partials.promoCode')

</div>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script type="text/javascript">
    function getPromoCode(id, promocode){
        $('#promotion_code').html(promocode);
    }

</script>

@stop