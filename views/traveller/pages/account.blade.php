@extends('traveller.layouts.defaultsidebar')
@section('content')



<script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css" type="text/css" />


<script type="text/javascript">



$(function(){

    // mobile number
    var country_code = $('#country_code').val();
    $("#mobilenumber").intlTelInput({
        separateDialCode: true,
        initialCountry: country_code,
    });

    $("#mobilenumber").on("countrychange", function(e, countryData) {
      $('#country_code').val(countryData.iso2);
    });
    // mobile number


    // contact mobile number
    var contactmobilenumber_country_code = $('#contactmobilenumber_country_code').val();
    $("#contactmobilenumber").intlTelInput({
        separateDialCode: true,
        initialCountry: contactmobilenumber_country_code,
    });

    $("#contactmobilenumber").on("countrychange", function(e, countryData) {
      $('#contactmobilenumber_country_code').val(countryData.iso2);
    });
    // contact mobile number


    if ($('#InputImageFile').val() != '') {
        $('#InputImageFile').val('');
    }
    
        
    $('#InputImageFile').on('change', function() {

        var ext = $(this).val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            
            if ($('#preview').data('Jcrop')) {
                // if need destroy jcrop or its created dom element here
                $('#preview').data('Jcrop').destroy();
                $('#preview').attr('src', 'http://placehold.it/250x250');
                $("#preview").removeAttr("style");
            }
            
            $(this).val('');
            alert('invalid extension!');
        } else {
            var file_name = $(this).val();
            if(file_name.length > 0) {
              addJcrop(this);
            }
        }
    });
    
    
    var addJcrop = function(input) {
        if ($('#preview').data('Jcrop')) {
          // if need destroy jcrop or its created dom element here
          $('#preview').data('Jcrop').destroy();
        }

        file = input.files && input.files[0];
        var img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = function () {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            if(width < 250 || width > 600 || height < 250) {
                $('#InputImageFile').val('');
                $("#preview").removeAttr("style");
                alert("Image size should be between 250x250 and 600px");
                $('#preview').attr('src', 'http://placehold.it/250x250');
            } else {

                // this will add the src in image_prev as uploaded
                //if (input.files && input.files[0]) {
                if(file) {
                  var reader = new FileReader();
                  reader.onload = function (e) {
                    $("#preview").attr('src', e.target.result);
                    var box_width = $('#image_prev_container').width();
                    $("#preview").removeAttr("style");
                    $('#preview').Jcrop({
                      setSelect: [0, 0, 250, 250],
                      minSize: [250, 250],
                      maxSize: [250, 250],
                      aspectRatio: 0,
                      allowResize: true,
                      onSelect: getCoordinates,
                      onChange: getCoordinates,
                     });image
                    }
                    reader.readAsDataURL(input.files[0]);
                }

                var getCoordinates = function(c){
                 $('#cropx').val(c.x);
                 $('#cropy').val(c.y);
                 $('#cropw').val(c.w);
                 $('#croph').val(c.h);
                };
   
            }
        }
    }
});



</script>

<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }


      
</style>

<script type="text/javascript">

    $(function (e) {
        $('#manageAccount').bind('submit', function (e) {
            var error = false;

            
            if ($('input[name=previous_traveller_image]').val() == '' && $('#InputImageFile').val() == '') {
                $('#InputImageFile').focus();
                alert("Please Upload your profile photo");
                error = true;
            } else if ($('input[name=cropw]').val() > 250) {
                $('#InputImageFile').focus();
                alert("Crop photo size should not be greater than 250px");
                error = true;
            } else if ($('input[name=croph]').val() > 250) {
                $('#InputImageFile').focus();
                alert("Crop photo size should not be greater than 250px");
                error = true;
            } else if ($('input[name=firstname]').val() === '') {
                $('input[name=firstname]').focus();
                alert("Please Enter your firstname");
                error = true;
            } else if ($('input[name=lastname]').val() === '') {
                $('input[name=lastname]').focus();
                alert("Please Enter your lastname");
                error = true;
            } else if ($('input[name=mobilenumber]').val() === '') {
                $('input[name=mobilenumber]').focus();
                alert("Please Enter your Mobile Number");
                error = true;
            } else if (!phoneNumber($('input[name=mobilenumber]').val())) {
                $('input[name=mobilenumber]').focus();
                alert("Please Enter Valid Mobile Number along with country code.");
                error = true;
            } else if ($('input[name=passport]').val() === '') {
                $('input[name=passport]').focus();
                alert("Please Enter your Passport Number");
                error = true;
            } else if (!checkAlphaNumeric($('input[name=passport]').val())) {
                $('input[name=passport]').focus();
                alert("Please Enter Valid Passport Number");
                error = true;
            } else if ($('#country-select option:selected').val() == 0) {
                $('#country-select').focus();
                alert("Please select your country");
                error = true;

            }


            if (error) {
                e.preventDefault();
            }
        });


        $('#manageguideAccount').bind('submit', function (e) {
            var error = false;
            //e.preventDefault();
            if ($("input[name=contactperson]").val() === '') {
                $('input[name=contactperson]').focus();
                alert("Please enter contact person name");
                error = true;

            } else if ($("input[name=contactmobilenumber]").val() === '') {
                $('input[name=contactmobilenumber]').focus();
                alert("Please enter your mobile number");
                error = true;

            } else if (!phoneNumber($("input[name=contactmobilenumber]").val())) {
                $('input[name=contactmobilenumber]').focus();
                alert("Please enter valid mobile number");
                error = true;

            }

            if (error) {
                e.preventDefault();
            }
        });

    });


//Miscelaneous Functions
    function validateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true;
        }
        return false;
    }

    function phoneNumber(inputtxt) {
        var phoneno = /^\d+$/;
        if ((phoneno.test(inputtxt))) {
            return true;
        }
        return false;
    }

    function checkAlphaNumeric(value) {
        var regex = /^[a-zA-Z0-9]+$/;
        if (regex.test(value)) {
            return true;
        }
        return false;
    }

</script>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Account
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Account</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                {!! Form::open(['url' => 'traveller/account/update', 'name' => 'manageAccount' , 'class'=>'manageAccount', 'id'=>'manageAccount', 'files'=>'true']) !!}                                
                <input type="hidden" name="traveller_id" value="{{ $traveller['id'] }}">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title text-bold text-black">My Account Information</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        @if(Request::get('message'))
                        <span style="width:100%; background: green; color:white; float: left; padding: 5px 4px;">{{ Request::get('message') }}</span>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    @if($traveller['image'])
                                    <img style="padding: 10px 0;" src="uploads/{{ $traveller['image'] }}">
                                    <br />
                                    {{ $traveller['image'] }}
                                    @else
                                    
                                    @endif
                                    

                                    
                                        <input type="hidden" name="cropx" id="cropx" value="0" />
                                        <input type="hidden" name="cropy" id="cropy" value="0" />
                                        <input type="hidden" name="cropw" id="cropw" value="0" />
                                        <input type="hidden" name="croph" id="croph" value="0" />
                                        <input type="hidden" name="country_code" id="country_code" value="{{ $traveller['country_code'] }}">

                                        <!--<img src="adminpanel/dist/img/360.png" class="img-reponsive">-->
                                        <div id="image_prev_container">
                                            <img id="preview" src="http://placehold.it/250x250" alt="your image" />
                                        </div>
                                        <div class="form-group margin">
                                            <label for="exampleInputFile">File input</label>
                                            <input type='file' id="InputImageFile" name="traveller_image" />

                                            <p class="help-block">Attach Image.</p>
                                        </div>

                                    <input name="previous_traveller_image" value="{{ $traveller['image'] }}" type="hidden">


                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <label for="inputHelpBlock" class="label-font">Title</label>
                                        <select name="title" class="form-control">
                                            @if($traveller['title'] === 0)
                                            <option selected="selected" value="0">Mr</option>
                                            @else
                                            <option value="0">Mr</option>
                                            @endif
                                            @if($traveller['title'] === 1)
                                            <option selected="selected" value="1">Mrs</option>
                                            @else
                                            <option value="1">Mrs</option>
                                            @endif
                                            @if($traveller['title'] === 2)
                                            <option selected="selected" value="2">Miss</option>
                                            @else
                                            <option value="2">Miss</option>
                                            @endif
                                        </select>

                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <label for="inputHelpBlock" name="firstname" class="label-font">First Name</label>
                                        <input class="form-control form-control-custom" name="firstname" value="{{ $traveller['firstname'] }}" type="text">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <label for="inputHelpBlock" class="label-font">Last Name</label>
                                        <input class="form-control form-control-custom" name="lastname" value="{{ $traveller['lastname'] }}" type="text">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <label for="inputHelpBlock" class="label-font">Mobile Number</label>
                                        <input class="form-control form-control-custom phone-number" name="mobilenumber" id="mobilenumber" value="{{ $traveller['mobilenumber'] }}" type="text">
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <label for="inputHelpBlock" class="label-font">Passport Number</label>
                                        <input class="form-control form-control-custom" name="passport" value="{{ $traveller['passport'] }}" type="text">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="inputHelpBlock" name="country" class="label-font">Country</label>
                                        <select id="country-select" name="countries_id" class="form-control">
                                            <option value="0">Country</option>
                                            @foreach ($countries as $key => $value)
                                            @if($value->countries_id == $traveller['countries_id'])
                                            <option value="{{ $value->countries_id }}" selected="selected">{{ $value->countries_name }}</option>
                                            @else
                                            <option value="{{ $value->countries_id }}">{{ $value->countries_name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <label for="inputHelpBlock"   class="label-font">Email Address (your voucher will be sent here, make sure it is correct)</label>
                                        <input class="form-control form-control-custom" name="email" value="{{ $traveller['email'] }}" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" id="manageAccount-submit" class="btn bg-maroon btn-flat margin" name="submit" value="Save">
                    </div>
                </div>
                {!! Form::close() !!}
                {!! Form::open(['url' => 'traveller/account/updateguide', 'name' => 'manageguideAccount' , 'class'=>'manageguideAccount', 'id'=>'manageguideAccount']) !!}                                
                <input type="hidden" name="traveller_id" value="{{ $traveller['id'] }}">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title text-black text-bold">For Localguide Access&nbsp;&nbsp;<img src="travelleradmin/dist/img/castle-3.png" style="height: 32px;"> <i class="fa fa-question-circle text-aqua" aria-hidden="true" class="top" title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="When you book an activity with a Local Guide Badge (put the actual picture of the Local Guide Badge) once you're Booking is already paid, your Local Guide can access these contact information to directly contact you"></i></h3>
                    </div>
                    @if(Request::get('message2'))
                    <span style="width:98%; background: green; color:white; float: left; margin:5px 15px 0 10px; padding: 5px 4px;">{{ Request::get('message2') }}</span>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <label for="inputHelpBlock" name="firstname" class="label-font">Contact Person</label>
                                        <input class="form-control form-control-custom" name="contactperson" value="{{ $traveller['contactperson'] }}" type="text" placeholder="Contact Person">

                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4" id="contactmobilenumber_block">
                                        <label for="inputHelpBlock" class="label-font">Mobile Number</label>
                                        <input class="form-control form-control-custom phone-number" name="contactmobilenumber" id="contactmobilenumber" value="{{ $traveller['contactmobilenumber'] }}" type="text">
                                        <input type="hidden" name="contactmobilenumber_country_code" id="contactmobilenumber_country_code" value="{{ $traveller['contactmobilenumber_country_code'] }}">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <h4>How Do You Want to be Contacted?</h4>
                                        <div class="form-group">
                                            <div class="radio">
                                                <label>
                                                    @if($traveller['contactmethod'] === 'whatsapp')
                                                    <input name="contactmethod" id="optionsRadios1" value="whatsapp" checked="checked" type="radio">
                                                    @else
                                                    <input name="contactmethod" id="optionsRadios1" value="whatsapp" type="radio">
                                                    @endif
                                                    WhatsApp
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    @if($traveller['contactmethod'] === 'viber')
                                                    <input name="contactmethod" id="optionsRadios2" value="viber" checked="checked" type="radio">
                                                    @else
                                                    <input name="contactmethod" id="optionsRadios2" value="viber" type="radio">
                                                    @endif
                                                    Viber
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                       
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" id="manageAccount-submit" class="btn bg-maroon btn-flat margin" name="submit" value="Save" >
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
        <link rel="stylesheet" href="int-tel/build/css/intlTelInput.css">
        <script src="int-tel/build/js/intlTelInput.js"></script>

@stop