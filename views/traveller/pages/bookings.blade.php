@extends('traveller.layouts.defaultsidebar')
@section('content')

<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }
    table {
        white-space: pre-line;
    }
    .castle-on-image {
        position: absolute;
        top: 6px;
        right: 3px;
    }
    .castle-on-image img {
        height: 33px;
    }
    .admin-image-uper {
        position: relative;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
    /*    .castle-on-image img {
            height: 57px;
            left: 10px;
            position: absolute;
            top: 98px;
        }
        .castle-on-image {
            position: relative;
        }*/

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Bookings
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->

                    @if(count($bookings))
                    @foreach($bookings as $key => $value)

                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Booking Reference Number :{{ $value['bookings_reference'] }}</h3>
                                        <p class="">Booking Date {{ date('d/m/Y', strtotime($value['created_at'])) }}</p>
                                        <div class="box-tools">
                                            <a class="text-white" role="button" data-toggle="collapse" href="#collapse_{{ $key }}" aria-expanded="true" aria-controls="collapse_{{ $key }}">
                                                <i class="fa fa-plus-circle fa-2x margin" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body padding-box-body-none">
                                        <div class="collapse" id="collapse_{{ $key }}" aria-expanded="true" style="">
                                            <div class="table-responsive">

                                                @if(count($value['activities']))
                                                <table class="table table-hover table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>Activity</th>
                                                            <th>Description</th>
                                                            <th>Activity Date</th>
                                                            <th>Units</th>
                                                            <th>SubTotal</th>
                                                            <th>Status</th>
                                                            <th>Note</th>
                                                            <th>Mobile E-Voucher</th>
                                                            <th>Local Guide Contact Detail</th>
                                                        </tr>

                                                        @foreach($value['activities'] as $index => $activities)


                                                        <tr>
                                                            <td>
                                                                <div class="admin-image-uper">
                                                                    <img class="img-responsive" src="uploads/{{ $activities['activities']['image'] }}" alt="{{ $activities['activities']['title'] }}" style="width: 223px">
                                                                    @if($activities['localguide_id'] > 0)
                                                                    <div class="castle-on-image">
                                                                        <img src="{{url('images/castle-3.png')}}" class="img-responsive center-block" alt="">
                                                                    </div>

                                                                    @endif
                                                                </div>
                                                            </td>
                                                            <td>{{ $activities['activities']['title'] }}</td>
                                                            <td>{{ date('m/d/Y' , strtotime($activities['bookingdate'])) }}</td>
                                                            <td>{{ $activities['total_quantity'] }} X Package</td>
                                                            <td>{{ Pricing::getCurrency($activities['currencies_id'])['code'].' '.$activities['total_price'] }}</td>
                                                            @if($activities['payment_status'] == '1')
                                                            <td><span class="label label-success">CONFIRMED</span> <span class="label label-success">PAID</span></td>
                                                            @else
                                                            <td><span class="label label-info">{{ $status[$activities['status']] }}</span>
                                                                @if($activities['payment_status'] == '0' && $activities['status'] == '1')
                                                                <a href='{{url('/traveller/pay/'.$activities['voucher_hash'])}}'><span class="label label-success">PAY</span></a>
                                                                @endif
                                                            </td>
                                                            @endif
                                                            <td> 
                                                                @if($activities['status'] == '0')
                                                                <?php echo str_replace('{{FOLLOWING_DAYS}}', $activities['cancellationArray']['days'], $activities['cancellationArray']['title']); ?>
                                                                @else
                                                                You cannot cancel this activity OR
                                                                You can cancel for FREE until Jan 01, 2018 OR
                                                                Full refunds will be issued if cancelled 45 days prior to the activity
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($activities['payment_status'] == '1')
                                                                <a target="_blank" href='{{url('/traveller/booking-order-form-pdf/'.$value['id'])}}'><span class="label label-success">View Mobile E-Voucher</span></a>
                                                                @else
                                                                <span class="label label-default">View Mobile E-Voucher</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($activities['payment_status'] == '1' && $activities['localguide_id'] > 0)
                                                                {{$activities['localguide_name']}}
                                                                {{$activities['localguide_email']}}
                                                                {{$activities['localguide_mobile']}}
                                                                {{$activities['localguide_mobileP']}}
                                                                {{$activities['localguide_location']}}

                                                                @endif
                                                            </td>
                                                        </tr>                                                                


                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>                                    <!-- /.box-header -->

                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>

                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


@stop
