@extends($user_type.'.layouts.defaultsidebar')
@section('content')

<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Messages
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </section>



    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Messages</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        {!! Form::open(['url' => $user_type . '/mymessages', 'id'=>'contactFeedForm']) !!}
                                        <div class="form-group">
                                            <label for="inputHelpBlock" class="label-font">Subject</label>
                                            <input class="form-control form-control-custom" name="contact_feed_subject" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputHelpBlock" class="label-font">Message</label>
                                            <textarea class="form-control form-control-custom" name="contact_feed_message" rows="6"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input class="btn bg-maroon btn-flat margin" name="submit" value="Submit" type="submit" id="send_message_btn">
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('#contactFeedForm').bind('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: '<?php echo $user_type; ?>/mymessages',
            type: 'post',
            data: $(this).serialize(),
            async: false,
            beforeSend: function () {
                //Can we add anything here.
            },
            cache: true,
            dataType: 'json',
            crossDomain: true,
            success: function (data) {
                if (data.code == 1) {
                    swal({
                        title: "Success",
                        text: data.message,
                        type: "success"
                    }, function () {
                        window.location = "<?php echo $user_type; ?>/mymessages";
                    });
                } else {
                    bootbox.alert(data.message);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });



    });
</script>
@stop