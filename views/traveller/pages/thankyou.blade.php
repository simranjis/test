@extends('traveller.layouts.defaultsidebar')
@section('content')
<style>
    .font-lg-lg{
        font-size: 38px;
    }
    .margin-top-50{
        margin-top:50px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <?php if ($status == 'success') { ?>
                    <h1  class="text-center">Payment Successful.</h1>
                    <div class="margin-top-50"></div>
                    <img src="{{url('/images/confirmed.png')}}" class="img-responsive center-block" style="width:150px" alt="" class="img-reponsive center-block"/> 
                    <div class="margin-top-30"></div>
                <?php } else { ?>
                    <h1  class="text-center">Payment Unsuccessful.</h1>
                    <div class="margin-top-50"></div>
                    <img src="{{url('/images/failed.png')}}" class="img-responsive center-block" style="width:150px" alt="" class="img-reponsive center-block"/> 
                    <div class="margin-top-30"></div>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
<script>
$(window).block({
    'message': '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>',
    'css': {
        border: '0',
        padding: '0',
        backgroundColor: 'none',
        marginTop: '5%',
        zIndex: '10600'
    },
    overlayCSS: {backgroundColor: '#555', opacity: 0.3, cursor: '', zIndex: '10600'},
});

setTimeout(function () {
    window.location.href = '{{url("/traveller/mybookings")}}';
}, 3000);
</script>
@stop
