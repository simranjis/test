@extends('traveller.layouts.defaultsidebar')
@section('content')


<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Settings
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Settings</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::open(['url' => 'traveller/account/changepassword', 'name' => 'changePassword' , 'class'=>'changePassword', 'id'=>'changePassword', 'files'=>'true']) !!}
                                    <input type="hidden" name="traveller_id" value="{{ $traveller_info->id }}">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <label for="inputHelpBlock" class="label-font">Current Password</label>
                                            <input class="form-control form-control-custom" name="current_password" placeholder="Current Password" type="password">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <label for="inputHelpBlock" class="label-font">New Password</label>
                                            <input class="form-control form-control-custom" name="password" placeholder="Enter New Password" type="password">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <label for="inputHelpBlock" class="label-font">Confirm Password</label>
                                            <input class="form-control form-control-custom" name="confirm_password" placeholder="Confirm Password" type="password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="submit" class="btn bg-maroon btn-flat margin" name="submit" value="Submit">
                                        </div>
                                    </div>
                                    <hr>
                                    {!! Form::close() !!}
                                    {!! Form::open(['url' => 'traveller/account/changelangcur', 'name' => 'changeLangCur' , 'class'=>'changeLangCur', 'id'=>'changeLangCur', 'files'=>'true']) !!}
                                    <input type="hidden" name="traveller_id" value="{{ $traveller_info->id }}">
                                    <h3 class="box-title">Language & Currency</h3>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <label for="inputHelpBlock" class="label-font">Default Currency</label>
                                            <select name="currencies_id" class="form-control">
                                                @foreach($currencies as $key => $value)
                                                    <option @if($traveller_info->currencies_id == $key) selected="selected" @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="inputHelpBlock" class="label-font">Default Language</label>
                                            <select name="languages_id" class="form-control">
                                                @foreach($languages as $key => $value)
                                                    <option @if($traveller_info->languages_id == $key) selected="selected" @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                        <div>
                                            <input type="submit" class="btn bg-maroon btn-flat margin" name="submit" value="Submit">
                                        </div>
                                    
                                    {!! Form::close() !!}
                                    
                                </div>
                            </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



@stop
