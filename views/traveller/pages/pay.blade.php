@extends('traveller.layouts.defaultsidebar')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'traveller/payForActivity', 'name' => 'payForActivity' , 'class'=>'payForActivity', 'id'=>'payForActivity']) !!}
                <!-- general form elements -->
                @if(strtotime(date('Y-m-d H:i:s')) <= strtotime($bookingActivityDetailsArr['booking_accept_date']." +48 hours"))
                <div class="box box-pri                    mary">
                    <div class="box-header with-borde                        r bg-maroon">
                        <h3 class="box-title text-bold text-white"><?php echo $bookingActivityDetailsArr->activity->title ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="text-center">
                            <img src="uploads/{{ $bookingActivityDetailsArr->activity->image }}" style="width: 360px; height: 130px;"><br><br>
                        </div>
                        <h4><strong>Pricing: </strong></h4>
                        <table border="1" width="100%">
                            <tr>
                                <th class="col-sm-2">Type</th>
                                <th class="col-sm-2">Price For</th>
                                <th class="col-sm-2">Title</th>
                                <th class="col-sm-2">Price</th>
                                <th class="col-sm-2">Quantity</th>
                                <th class="col-sm-2">Total</th>
                            </tr>
                            <?php
                            $total = 0;
                            foreach ($bookingActivityDetailsArr->bookings_items as $key => $value) {
                                $total += ($value->price * $value->quantity);
                                ?>
                                <tr>
                                    <td class="col-sm-2"><?php echo $value->type ?></td>
                                    <td class="col-sm-2"><?php echo ucwords($value->pricefor); ?></td>
                                    <td class="col-sm-2"><?php echo $value->title ?></td>
                                    <td class="col-sm-2"><?php echo $value->price ?></td>
                                    <td class="col-sm-2"><?php echo $value->quantity ?></td>
                                    <td class="col-sm-2"><?php echo number_format($value->price * $value->quantity, 2) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"></td>
                                <td class="col-sm-2"><strong>Subtotal: </strong></td>
                                <td class="col-sm-2"><?php echo number_format($total, 2) ?></td>
                            </tr>
                            <?php if ($bookingActivityDetailsArr->tax != 0.00) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4">
                                    <td class="col-sm-2">
                                        <strong>Tax / Fee: </strong>
                                    </td>
                                    <td class="col-sm-2"><?php echo number_format($bookingActivityDetailsArr->tax, 2) ?>
                                        <?php $total += $bookingActivityDetailsArr->tax; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if (!empty($bookingActivityDetailsArr->promotion_id)) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Promo (<?php echo $bookingActivityDetailsArr->promotion->promocode ?>): 
                                        </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php echo number_format($bookingActivityDetailsArr->promotion->discount, 2); ?>
                                        <?php
                                        $discount = $bookingActivityDetailsArr->promotion->discount;
                                        if ($bookingActivityDetailsArr->promotion->discount_type == 2) {
                                            echo ' %';
                                            $discount = ($total * ($bookingActivityDetailsArr->promotion->discount / 100));
                                        }
                                        $total = $total - $discount;
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                <td class="col-sm-2">
                                    <strong>Grand Total: </strong>
                                </td>
                                <td class="col-sm-2">
                                    <?php echo number_format($total, 2); ?>
                                </td>
                            </tr>
                            <?php if (!empty($bookingActivityDetailsArr->localguide_id)) { ?>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Payable Amount: </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php
                                        if (!empty($bookingActivityDetailsArr->localguide)) {
                                            $commision_amount = $total * ($bookingActivityDetailsArr->localguide->commision / 100);
                                            echo number_format($commision_amount, 2) . '&nbsp;';
                                            echo '( ' . $bookingActivityDetailsArr->localguide->commision . ' % )';
                                            $total = $total - $commision_amount;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-sm-2" style="padding: 5px;" colspan="4"></td>
                                    <td class="col-sm-2">
                                        <strong>Left amount will paid to localguide in:  </strong>
                                    </td>
                                    <td class="col-sm-2">
                                        <?php echo number_format($total, 2); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="text-center">
                        <button type="submit" href="javascript:void(0)" class="btn bg-maroon btn-flat margin" role="button">
                            Pay
                        </button>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border bg-maroon">
                                <h3 class="box-title text-bold text-white">The link has been expired.</h3>
                            </div>
                            <div class="box-body">
                                <div class="text-center">
                                    The link has been expired. You can contact to administrator if any query to <a href="mailto:info@seeglobalexperiencelocal.com">info@seeglobalexperiencelocal.com</a>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <input type="hidden" name="activity_voucher_hash" id="activity_voucher_hash" value="{{ $bookingActivityDetailsArr->voucher_hash }}" />
                {!! Form::close() !!}
            </div>
        </div>
    </section>
</div>
@stop