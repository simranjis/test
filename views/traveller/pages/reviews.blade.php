@extends('traveller.layouts.defaultsidebar')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script type="text/javascript">


/* Javascript */
 
//Make sure that the dom is ready
$(function () {
 
  $(".rateYo").rateYo({
     onSet: function (rating, rateYoInstance) {
      //alert("Rating is set to: " + rating);
      //alert($(this).parent().parent().html());
      $('input[name=rating]').val(rating);
      //$(this).parent().parent().append('');
    },
    fullStar: true,
    rating:1
  });
 
    $('.reviewInsertForm').bind('submit' , function(e) {
    
            $.ajax({
                url: 'traveller/myreviews/submit',
                type: 'post',
                data: $(this).serialize(),
                async: false,
                beforeSend: function () {
                    //Can we add anything here.
                },
                cache: true,
                dataType: 'json',
                crossDomain: true,
                success: function (data) {
                    if (data.code == 1) {
                        swal({
                            title: "Success",
                            text: data.message,
                            type: "success"
                        }, function() {
                            window.location = "traveller/myreviews";
                        });
                    } else {
                        bootbox.alert(data.message);
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
            
            e.preventDefault();

    });
 
});

</script>
<style>
    .activity-feed {
        padding: 15px;
    }
    .activity-feed .feed-item {
        position: relative;
        padding-bottom: 20px;
        padding-left: 30px;
        border-left: 2px solid #e4e8eb;
    }
    .activity-feed .feed-item:last-child {
        border-color: transparent;
    }
    .activity-feed .feed-item:after {
        content: "";
        display: block;
        position: absolute;
        top: 0;
        left: -6px;
        width: 10px;
        height: 10px;
        border-radius: 6px;
        background: #fff;
        border: 1px solid #f37167;
    }
    .activity-feed .feed-item .date {
        position: relative;
        top: -5px;
        color: #8c96a3;
        text-transform: uppercase;
        font-size: 13px;
    }
    .activity-feed .feed-item .text {
        position: relative;
        top: -3px;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Reviews
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Reviews</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Rating!</h4>
                    Submit a review and earn Rewards for your next purchase!
                </div>
                
                
               
                
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        
                        @if(count($bookings))
                            @foreach($bookings as $key => $value)
                        
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <div class="box-header with-border box-header-color">
                                                <h3 class="box-title sbold">Booking Reference Number: {{ $value['id'] }}<!--Booking Reference Number:2051546458--></h3>
                                                <p class="">Booking Date {{ Carbon\Carbon::parse($value['created_at'])->format('d-m-Y') }}</p>
                                                <div class="box-tools">
                                                    <div class="input-group input-group-sm" style="width: 150px;">
                                                        <!--<input name="table_search" class="form-control pull-right" placeholder="Search" type="text">

                                                        <div class="input-group-btn">
                                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                        </div>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>Activity</th>
                                                            <th>Description</th>
                                                            <th>Activity Date </th>
                                                            <th><!--Status--></th>
                                                        </tr>
                                                        
                                                        @foreach($value['activities'] as $subkey => $subvalue)
                                                        
                                                        <tr>
                                                            <td>
                                                                <div class="local-guide">
                                                                    <img class="img-responsive" src="uploads/{{ $subvalue['activities']['image'] }}" alt="User profile picture" style="height: 80px">
                                                                    <div class="local-guide-icon">
                                                                        <img class="img-responsive" src="travelleradmin/dist/img/local-guide.png" alt="User profile picture" style="height: 80px">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>{{ $subvalue['activities']['title'] }}</td>
                                                            <td>{{ Carbon\Carbon::parse($subvalue['bookingdate'])->format('d-m-Y') }}</td>
                                                            <td>
                                                                
                                                                @if($subvalue['rating'])
                                                                <div class="stars starrr" data-rating="0">
                                                                    <!--<h4>Rating</h4>-->
                                                                    @for ($i = 0; $i < $subvalue['rating']; $i++)
                                                                        <span class="glyphicon glyphicon-star"></span>
                                                                    @endfor
                                                                    
                                                                </div>
                                                                @else
                                                                
                                                                <a class="btn bg-maroon btn-flat margin" role="button" data-toggle="collapse" href="#collapse_{{ $key }}_{{ $subkey }}" aria-expanded="true" aria-controls="collapse_{{ $key }}_{{ $subkey }}">
                                                                    Write a Review
                                                                </a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td colspan="6">
                                                                {!! Form::open(['url' => 'traveller/myreviews/submit', 'name' => 'reviewInsertForm' , 'class'=>'reviewInsertForm', 'id'=>'reviewInsertForm', 'files' => true]) !!}   
                                                                <input type="hidden" name="activities_id" value="{{ $subvalue['activities_id'] }}" >
                                                                <input type="hidden" name="rating" value="1" >
                                                                <input type="hidden" name="activities_booking_id" value="{{ $subvalue['id'] }}">
                                                                <input type="hidden" name="traveller_id" value="{{ Session::get('traveller')['id'] }}">
                                                                <input type="hidden" name="localguide_id" value="{{ $subvalue['localguide_id'] }}">
                                                                <div class="collapse" id="collapse_{{ $key }}_{{ $subkey }}" aria-expanded="true">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="text-left">
                                                                                <div class="row">
                                                                                    <div class="col-md-6 col-md-offset-3">
                                                                                        <h4>Please Rate</h4>
                                                                                        <div class="rateYo"></div>
                                                                                        <div class="form-group">
                                                                                            
                                                                                            <!--<label>Comment</label>--><br>
                                                                                            <textarea class="form-control" name="descrption" rows="3" placeholder="Enter ..."></textarea>
                                                                                        </div>
                                                                                        <input class="btn bg-maroon btn-flat margin" type="submit" name="submit" value="Submit" >
                                                                                            
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {!! Form::close() !!}
                                                            </td>
                                                        </tr>
                                                        
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                        
                            @endforeach
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


@stop
