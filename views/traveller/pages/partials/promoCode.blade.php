<div class="modal fade" id="modal-default-4">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-maroon">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Promo Code</h4>
                </div>
                <div class="modal-body">
                    <p id="promotion_code" class="text-center" style="font-size:100px; border: 1px solid #ccc"></p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>