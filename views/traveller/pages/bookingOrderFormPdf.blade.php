<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Booking Order Form</title>
            <style type="text/css">
                body {
                    padding-top: 0 !important;
                    padding-bottom: 0 !important;
                    padding-top: 0 !important;
                    padding-bottom: 0 !important;
                    margin:0 !important;
                    width: 100% !important;
                    -webkit-text-size-adjust: 100% !important;
                    -ms-text-size-adjust: 100% !important;
                    -webkit-font-smoothing: antialiased !important;
                }
                .tableContent img {
                    border: 0 !important;
                    display: block !important;
                    outline: none !important;
                }

                p, h2{
                    margin:0;
                }

                div,p,ul,h2,h2{
                    margin:0;
                }

                h2.bigger,h2.bigger{
                    font-size: 32px;
                    font-weight: normal;
                }

                h2.big,h2.big{
                    font-size: 21px;
                    font-weight: normal;
                }

                a.link1{
                    color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
                }

                a.link2{
                    padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
                }

                a.link3{
                    background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
                }
                .bgBody{
                    background: #F6F6F6;
                }
                .bgItem{
                    background: #ffffff;
                }

                @media only screen and (max-width:480px)

                {

                    table[class="MainContainer"], td[class="cell"] 
                    {
                        width: 100% !important;
                        height:auto !important; 
                    }
                    td[class="specbundle"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;

                    }
                    td[class="specbundle1"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;
                        padding-bottom:20px !important;

                    }	
                    td[class="specbundle2"] 
                    {
                        width:90% !important;
                        float:left !important;
                        font-size:14px !important;
                        line-height:18px !important;
                        display:block !important;
                        padding-left:5% !important;
                        padding-right:5% !important;
                    }
                    td[class="specbundle3"] 
                    {
                        width:90% !important;
                        float:left !important;
                        font-size:14px !important;
                        line-height:18px !important;
                        display:block !important;
                        padding-left:5% !important;
                        padding-right:5% !important;
                        padding-bottom:20px !important;
                        text-align:center !important;
                    }
                    td[class="specbundle4"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;
                        padding-bottom:20px !important;
                        text-align:center !important;

                    }

                    td[class="spechide"] 
                    {
                        display:none !important;
                    }
                    img[class="banner"] 
                    {
                        width: 100% !important;
                        height: auto !important;
                    }
                    td[class="left_pad"] 
                    {
                        padding-left:15px !important;
                        padding-right:15px !important;
                    }

                }

                @media only screen and (max-width:540px) 

                {

                    table[class="MainContainer"], td[class="cell"] 
                    {
                        width: 100% !important;
                        height:auto !important; 
                    }
                    td[class="specbundle"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;

                    }
                    td[class="specbundle1"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;
                        padding-bottom:20px !important;

                    }		
                    td[class="specbundle2"] 
                    {
                        width:90% !important;
                        float:left !important;
                        font-size:14px !important;
                        line-height:18px !important;
                        display:block !important;
                        padding-left:5% !important;
                        padding-right:5% !important;
                    }
                    td[class="specbundle3"] 
                    {
                        width:90% !important;
                        float:left !important;
                        font-size:14px !important;
                        line-height:18px !important;
                        display:block !important;
                        padding-left:5% !important;
                        padding-right:5% !important;
                        padding-bottom:20px !important;
                        text-align:center !important;
                    }
                    td[class="specbundle4"] 
                    {
                        width: 100% !important;
                        float:left !important;
                        font-size:13px !important;
                        line-height:17px !important;
                        display:block !important;
                        padding-bottom:20px !important;
                        text-align:center !important;

                    }

                    td[class="spechide"] 
                    {
                        display:none !important;
                    }
                    img[class="banner"] 
                    {
                        width: 100% !important;
                        height: auto !important;
                    }
                    td[class="left_pad"] 
                    {
                        padding-left:15px !important;
                        padding-right:15px !important;
                    }

                    .font{
                        font-size:15px !important;
                        line-height:19px !important;

                    }
                }
                @media print{
                    #printButton{
                        display:none;
                    } 
                }


            </style>
            <script type="colorScheme" class="swatch active">
                {
                "name":"Default",
                "bgBody":"F6F6F6",
                "link":"62A9D2",
                "color":"999999",
                "bgItem":"ffffff",
                "title":"555555"
                }
            </script>

    </head>
    <body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" cz-shortcut-listen="true" id="content"><div style="text-align:right"><button type="button" id="printButton" onclick="window.print()">Print</button></div>
        <table class="tableContent bgBody" style="font-family:helvetica, sans-serif;" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td colspan="3" bgcolor="#333e48" height="25"></td>
                </tr>
                <tr>
                    <td><table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="spechide" valign="top">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="600" valign="top"><table class="MainContainer" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="600" align="center">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class="movableContentContainer">
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td bgcolor="#333e48" valign="top">
                                                                            <table valign="top" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="middle" align="center">
                                                                                            <div class="contentEditableContainer contentImageEditable">
                                                                                                <div class="contentEditable">
                                                                                                    <img src="{{ url('/images/logo-email.png') }}" alt="Company logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>


                                                        <!-- middle content -->


                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
                                                                <tbody><tr><td height="25" bgcolor="#333e48"></td></tr>

                                                                    <tr><td height="5" bgcolor="#333e48"></td></tr>

                                                                    <tr><td height="12'" bgcolor="#333e48"></td></tr>
                                                                    <tr><td height="20'" bgcolor="#fff"></td></tr>
                                                                    <tr><td valign="top" align="center"><p style="font-weight:bold;">MOBILE E-VOUCHER</p></td></tr>
                                                                </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold; line-height: 20px; border-left: 4px solid #f59926; padding: 0 15px;">
                                                                                                                    <p class="big">Booking Reference #: {{ $data_array->bookings_reference }}   </p>
                                                                                                                    <p>Contact Person: {{ $data_array->firstname }} {{ $data_array->lastname }}</p>
                                                                                                                    <p>Contact Number: {{ $data_array->mobilenumber }}</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr><td height="16"></td></tr>
                                                                                                    <tr><td height="16"></td></tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody></table>
                                                                        </td>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>

                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                                                                                    <p class="big">Booking Date: {{ date('d/m/Y',strtotime($data_array->bookingDate)) }}</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative; background-color: #f37022;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="border:2px dotted #fff; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#fff;font-size:15px;line-height:25px;">
                                                                                                    <p>{{ $data_array->title }}</p>


                                                                                                    <p>Activity Date: {{ date('d/m/Y',strtotime($data_array->bookingdate)) }}</p>
                                                                                                    <p>Activity Duration: {{ $data_array->quickinfo3 }}</p>
                                                                                                   <!--<p>Quantity: 3</p>-->
                                                                                                    <?php $qty = 0; ?>
                                                                                                    @if(count($data_array->activityBookingItemArray))
                                                                                                    @foreach($data_array->activityBookingItemArray as $items => $pricing)
                                                                                                    <p style="margin-left:20px">{{ ucwords($pricing->pricefor) }}: {{ $pricing->quantity }}</p>
                                                                                                    <?php $qty += $pricing->quantity; ?>
                                                                                                    @endforeach
                                                                                                    @endif                                                                                                    
                                                                                                    <p>Quantity: {{ $qty }}</p>
                                                                                                    <p>Pick-up Time: See your Activity Settings. Please arrive at least 15-20 mins early at the pick-

                                                                                                        up location.</p>

                                                                                                    <p> Additional Details: 
                                                                                                        {{$data_array->extra_details}}
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                                                                                    <h2 class="big" style="font-weight:bold;">Your Booking Summary:</h2>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p>{{ $data_array->activity->title }}</p>


                                                                                                    @if(count($data_array->activityBookingItemArray))
                                                                                                    @foreach($data_array->activityBookingItemArray as $items => $pricing)
                                                                                                    <p>{{ ucwords($pricing->price_for) }} ({{ $data_array->code }} {{ $pricing->price }} per {{ $pricing->type }}) x {{ $pricing->quantity}} = {{ $data_array->code }} {{ $pricing->price * $pricing->quantity }}</p>
                                                                                                    @endforeach
                                                                                                    @endif

                                                                                                    <p class="big" style="font-weight:bold; margin-top:20px;"> Total Tour Price: {{ $data_array->code }} {{ $data_array->total }}</p>

                                                                                                    <!--<p class="big" style="font-weight:bold;">Total Amount Paid: $349.40 MasterCard (****5599)</p>-->
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <p>Balance to be Paid Directly to the Local Guide (in cash):</p>
                                                                                            <p style="color:#acacac; font-style: italic;">Local Guide prefers to be paid in 

                                                                                                their local currency</p>
                                                                                        </td>
                                                                                        <td style="padding-bottom:8px;" valign="middle" align="right">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable">
                                                                                                    <a target="_blank" class="link2" href="#" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">{{ $data_array->booking->currency->code }} {{ $data_array->localguide_amount }}</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentImageEditable">
                                                                                <div class="contentEditable">
                                                                                    <img class="banner" src="{{ url('/images/ab.jpg') }}" alt="What we do" data-default="placeholder" data-max-width="600" width="600" height="139">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>


                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" class="" style="background:#333e48;">

                                                                <tbody><tr><td height="28"></td></tr>

                                                                    <tr>
                                                                        <td valign="top" align="center">
                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                <div class="contentEditable" style="color:#A8B0B6; font-size:13px;line-height: 16px;">
                                                                                    <img src="{{ url('images/logo-email.png') }}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr><td height="28"></td></tr>
                                                                </tbody></table>
                                                        </div>

                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style=" padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p style="font-weight:bold; font-size: 17px;background: #00afef;width: 184px;color: #fff;padding: 10px; margin-bottom:20px;">Important Information:</p>

                                                                                                    <p>• You will only be charged the deposit AFTER the FREE cancellation period.</p>

                                                                                                    <p> •

                                                                                                        You can present this Mobile E-Voucher to the Local Guide on the day of your activity.</p>

                                                                                                    <p>•

                                                                                                        The Local Guide may request for additional verification such as your passport.</p>

                                                                                                    <p>•
                                                                                                        You can view your Local Guide’s contact number after the FREE cancellation period in your Destination Seeker 

                                                                                                        Account ‘My Activities’.</p>

                                                                                                    <p>•

                                                                                                        Your Local Guide may also contact you through the mobile number you’ve provided (via WhatsApp). Rest assured 

                                                                                                        your other personal and sensitive information will not be given. See our Privacy Policy. </p>

                                                                                                    <p> •

                                                                                                        You may also discuss and verify your itinerary details directly to your Local Guide. For further information please see Destination Seeker Payment Protection Policy. </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;">
                                                                                                                    <p style="font-weight:bold; font-size: 17px;background: #f89b20;width: 184px;color: #fff;padding: 10px; margin-bottom:20px;">Price Inclusion:</p>
                                                                                                                    <p>Hotel Pick-up and Drop Off</p>

                                                                                                                    <p> @if(count($data_array->bookingActivityFAQ) > 0)
                                                                                                                        @foreach($data_array->bookingActivityFAQ as $key => $FAQ)
                                                                                                                        @if($FAQ->title == 'Inclusion')
                                                                                                                        {!! $FAQ->description !!}
                                                                                                                        @endif
                                                                                                                        @endforeach
                                                                                                                        @endif                                                                                                                </p>
                                                                                                                    <p style="font-weight:bold; font-size: 17px;background: #00afef;width: 184px;color: #fff;padding: 10px; margin-bottom:20px; margin-top:20px;">Itinerary</p>

                                                                                                                    @if(count($data_array['bookingActivityItenary']) > 0)
                                                                                                                    <ul>
                                                                                                                        @foreach($data_array['bookingActivityItenary'] as $itenary)
                                                                                                                        <li><b>{{$itenary->title}} :</b> {!! $itenary->description !!}</li>
                                                                                                                        @endforeach
                                                                                                                    </ul>
                                                                                                                    @endif                                                                                                            </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr><td height="16"></td></tr>
                                                                                                    <tr><td height="16"></td></tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody></table>
                                                                        </td>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>

                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;">
                                                                                                                    <p style="font-weight:bold; font-size: 17px;background: #f89b20;width: 184px;color: #fff;padding: 10px; margin-bottom:20px;">Price Exclusion:</p>

                                                                                                                    <p> 
                                                                                                                        @if(count($data_array->bookingActivityFAQ) > 0)
                                                                                                                        @foreach($data_array->bookingActivityFAQ as $key => $FAQ)
                                                                                                                        @if($FAQ->title == 'Exclusion')
                                                                                                                        {!! $FAQ->description !!}
                                                                                                                        @endif
                                                                                                                        @endforeach
                                                                                                                        @endif                                                                                                                </p>

                                                                                                                </div>
                                                                                                            </div></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative; background-color: red;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="border:2px dotted #fff; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#fff;font-size:15px;line-height:25px;">
                                                                                                    <p style="font-weight: bold">Note: The Itinerary is subject to change due to weather conditions and under the Local Guide’s discretion for your utmost safety 

                                                                                                        and protection. You may also further discuss your itinerary details directly with your Local Guide after the contact details have 

                                                                                                        been released to your Destination Seeker Account.</p>
                                                                                                    <p style="margin-top:20px;font-weight: bold">For further information, see Destination Seeker Payment Protection Policy.</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- Bottom Part -->


                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="spechide" valign="top"><table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td bgcolor="#333e48" height="130">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>  
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>
        <script>
//$(window).on('load', function () {
//    var specialElementHandlers =
//            function (element, renderer) {
//                return true;
//            }
//    var doc = new jsPDF();
//    doc.fromHTML($('#content').html(), 15, 15, {
//        'width': 170,
//        'elementHandlers': specialElementHandlers
//    }, function () {
//        doc.output('datauri');
//        doc.save('abc.pdf');
//    });
//    let options = {
//        pagesplit: true
//    };
//    var pdf = new jsPDF('p', 'px', 'letter');
//    pdf.addHTML(document.body, 10, 15, options, function () {
//        pdf.save('bookingDetails.pdf');
//    });
//});


        </script>
    </body></html>


