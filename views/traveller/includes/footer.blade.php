
<!-- Bootstrap 3.3.6 -->
<script src="travelleradmin/dist/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="travelleradmin/dist/js/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="travelleradmin/dist/js/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="travelleradmin/dist/js/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!--<script src="travelleradmin/plugins/morris/morris.min.js"></script>-->
<!-- Sparkline -->
<script src="travelleradmin/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="travelleradmin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="travelleradmin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="travelleradmin/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="travelleradmin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="travelleradmin/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="travelleradmin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="travelleradmin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="travelleradmin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="travelleradmin/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="travelleradmin/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="travelleradmin/dist/js/demo.js"></script>
