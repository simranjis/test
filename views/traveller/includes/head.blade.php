<?php
        header("Cache-Control: no-store, must-revalidate, max-age=0");
        header("Pragma: no-cache");
?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Destination Seeker</title>
        <base href="{{ url("/")}}/">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="travelleradmin/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="travelleradmin/dist/css/custom.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="travelleradmin/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="travelleradmin/dist/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="travelleradmin/dist/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="travelleradmin/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="travelleradmin/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="travelleradmin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="travelleradmin/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="travelleradmin/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="travelleradmin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!--Jquery CDN Added -->
        <!--<script
            src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
        crossorigin="anonymous"></script>   -->     
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="travelleradmin/bootstrap/jquery.uploadPreview.min.js" type="text/javascript" /></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script> 
        <style>
            .navbar-nav > .user-menu > .dropdown-menu {
                width: 164px;
            }
            .skin-blue-light .main-header .navbar {
                background-color: #000000;
            }
            .skin-blue-light .main-header .logo {
                background-color: #3C3C3D;
            } 
            .iti-flag {background-image: url("int-tel/build/img/flags.png");}

            @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
              .iti-flag {background-image: url("int-tel/build/img/flags@2x.png");}
            }
        </style>
        <script src="js/destination-seeker.js" type="text/javascript"></script> 


<script>
    var base_url = '{{url("/")}}';
</script>