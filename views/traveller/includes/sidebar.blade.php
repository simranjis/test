<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!--<div class="user-panel">
            <div class="pull-left image">
                <img src="travelleradmin/dist/img/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a href="dashboard"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>-->
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- <li class="active treeview">
                <a href="{{  url('traveller/') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li> -->          
            <li class="treeview">
                <a href="{{  url('traveller') }}">
                    <i class="fa fa-list"></i>
                    <span>My Account</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/mybookings') }}">
                    <i class="fa fa-list"></i>
                    <span>My Bookings</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/myreviews') }}">
                    <i class="fa fa-list"></i>
                    <span>My Reviews</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/myrewards') }}">
                    <i class="fa fa-list"></i>
                    <span>My Rewards</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/mymessages') }}">
                    <i class="fa fa-list"></i>
                    <span>Messages</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/mysettings') }}">
                    <i class="fa fa-list"></i>
                    <span>Settings</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{  url('traveller/logout') }}">
                    <i class="fa fa-list"></i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
