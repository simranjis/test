           <header class="main-header">
                <!-- Logo -->
                <a href="{{  url('/') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <img src="travelleradmin/dist/img/logo.png" alt="" class="img-responsive">
                    </span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown tasks-menu">
                            <li class="dropdown user user-menu">
                                <a href="{{  url('traveller') }}" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    @if(Session::get('traveller')['image'])
                                        <img src="uploads/{{ (Session::get('traveller')['image']) }}" class="user-image" alt="User Image">
                                    @else
                                        <img src="travelleradmin/dist/img/avatar.png" class="user-image" alt="User Image">
                                    @endif
                                    <span class="hidden-xs">{{ (Session::get('traveller')['firstname']) }} {{ (Session::get('traveller')['lastname']) }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="{{  url('traveller') }}">My Account</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="{{  url('traveller/mybookings') }}">My Bookings</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="{{  url('traveller/myreviews') }}">My Reviews</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="{{  url('traveller/myrewards') }}">My Rewards</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="{{  url('traveller/mysettings') }}">Settings</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <!-- Menu Footer-->
                                    <!--<li class="user-footer">
                                        <div class="text-center">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>-->
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="{{  url('traveller/logout') }}">
                                    <span class="hidden-xs"> <i class="fa fa-sign-out" aria-hidden="true"></i> Signout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>

