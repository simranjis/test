@extends('layouts.defaultplain')
@section('content')
<style>
    .form-control {
        width: 100%;
        padding: 10px 0;
        border: none;
        border-bottom: 1px solid;
        border-color: #a0a9b4;
        color: #868e97;
        font-size: 14px;
        margin-bottom: 30px;
        border-radius: 0 !important;
        border-top: 0px !important;
        box-shadow: none !important;
        height: 38px;
    }
</style>
<div class="user-login-5">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="row bs-reset">
                <div class="col-md-12 login-container bs-reset">
                    <a href="{{ url('/') }}">
                        <img class="login-logo login-6" src="images/logo.png" style="height:115px" class="img-responsive center-block">
                    </a>
                    <div class="login-content">
                        <h1>Reset Password</h1>
                        {!! Form::open(['url' => ['traveller/resetpassword', $hash], 'name' => 'travellerResetPasswordForm' , 'class'=>'travellerResetPasswordForm', 'id'=>'travellerResetPasswordForm']) !!}                                
                            <div class="form-group form-md-line-input">
                                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="New Password" name="password" /> 
                            </div>
                            <div class="form-group form-md-line-input">
                                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" name="confirm_password" /> 
                            </div>
                            <div class="form-actions">
                                <a href="{{ url('/') }}"><button type="button" id="back-btn" class="real_black_btn_login black_btn_contactpull-right">Back</button></a>
                                <button type="submit" id="travellerResetPasswordForm-submit-button" class="black_btn_login black_btn_contact">Reset</button>
                            </div>
                            {{ Form::hidden('reset_password_hash', $hash, array('id' => 'reset_password_hash')) }}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="js/traveller/resetPassword.js"></script>

@stop