<!DOCTYPE html>
<html>
    <head>
        @include('traveller.includes.head')
    </head>
<body class="hold-transition skin-blue-light sidebar-mini layout-boxed">
    <div class="wrapper">
        @include('traveller.includes.header')
        @include('traveller.includes.sidebar')
        
        
            @yield('content')

        @include('traveller.includes.footer')
    </div>
</body>
</html>

