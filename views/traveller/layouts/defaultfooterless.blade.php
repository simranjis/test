<!DOCTYPE html>
<html>
    <head>
        @include('traveller.includes.head')
    </head>
<body class="hold-transition skin-blue sidebar-mini layout-boxed">
    <div class="wrapper">
        @include('traveller.includes.header')
        
            @yield('content')

    </div>
</body>
</html>

