<!DOCTYPE html>
<html>
    <head>
        @include('traveller.includes.head')
    </head>
<body class="hold-transition skin-blue sidebar-mini layout-boxed">
    <div class="wrapper">
        
            @yield('content')

        @include('traveller.includes.footer')
    </div>
</body>
</html>

