 

@extends('layouts.default')
@section('content')
@php
    if(App\Setting::isPageDisplay('display_blog_page'))
    {
@endphp  

<style>
    .grid-item
    {
        width: 280px; 
        float: left;
    }
    .grid {
        margin: 0 auto;
    }
    /* clearfix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }
    .navbar-default .navbar-nav > li > a {
        color: #7f7f7f;
    }
</style>
<div class="blog-breadcrumbs">
    <div class="container">
        <h2 class="text-center">BE INSPIRED</h2>
    </div>
</div>
<div class="search-position">
    <div class="advance-search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group margin-top-search" id="adv-search">
                        <!--                        <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default dropdown-toggle search-custom-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </div>-->
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-xs" placeholder="Where's Your Next Adventure?" />
                        <input type="text" id="adv_search_txt" class="form-control search-custom search-custom-top search-custom-top-1 hidden-lg hidden-md hidden-sm" placeholder="Where To?" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle black_btn" data-toggle="dropdown" aria-expanded="false">Let's GO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="grid js-masonry" id="blog-width">
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
        <div class="grid-item">
            <div class="blog">
                <div class="blog-img">
                    <img src="images/BARCELONA.png" class="img-responsive center-block" alt="">
                </div>
                <div class="blog-content">
                    <h2 class="text-center">My First Blog</h2>
                    <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
            </div>
        </div>
    </div>
</div>
<script>
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 280,
        isFitWidth: true,
        "gutter": 10
    });
</script>

@php
    }
@endphp

@stop


