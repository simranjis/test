@extends('localguide.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Account
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Account</li>
        </ol>
    </section>

    {!! Form::open(['route' => ['localguide.myaccount'], 'name' => 'updateAccountForm' , 'class'=>'updateAccountForm', 'id'=>'updateAccountForm', 'files' => true]) !!}  

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('profile_image', 'Profile Image', array('class'=>'label-font')) }}
                                            <div class="clearfix"></div>
                                            <input type="hidden" name="cropx" id="cropx" value="0" />
                                            <input type="hidden" name="cropy" id="cropy" value="0" />
                                            <input type="hidden" name="cropw" id="cropw" value="0" />
                                            <input type="hidden" name="croph" id="croph" value="0" />
                                            <input type="hidden" name="country_code" id="country_code" value="<?php echo $localGuideInfo->country_code ?>" />

                                            <?php if(!empty($localGuideInfo->profile_image)){ ?>
                                                <div id="profile_image_div" >
                                                    <img style="padding: 10px 0;" src="uploads/{{ $localGuideInfo->profile_image }}">
                                                    <div class="clearfix"></div>
                                                    <h4>
                                                        <a href="javascript:void(0)" onclick="return editProfilePic()">
                                                            <i class="fa fa-edit"></i> Edit
                                                        </a>
                                                    </h4>
                                                </div>
                                            <?php } ?>
                                            <?php 
                                                $profile_image_hide = '';
                                                if(!empty($localGuideInfo->profile_image)){ 
                                                    $profile_image_hide = 'hide';
                                                }
                                            ?>
                                            <div id="file_uploader_div" class="{{$profile_image_hide}}">
                                                <img id="preview" src="http://placehold.it/360x180" alt="your image" />
                                                <br><br>
                                                <input type="file" name="profile_image" id="profile_image" />
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('business_name', 'Business Name', array('class'=>'label-font')) }}

                                            {{ Form::text('business_name', $localGuideInfo->business_name, array('class' => 'form-control form-control-custom', 'id'=>'business_name', 'placeholder'=>'Business Name')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('business_address', 'Business Address', array('class'=>'label-font')) }}
                                            
                                            {{ Form::textarea('business_address', $localGuideInfo->business_address, array('class' => 'form-control form-control-custom', 'rows'=>6, 'id'=>'business_address', 'placeholder'=>'Business Address')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('business_nature', 'Business Nature', array('class'=>'label-font')) }}

                                            <?php 
                                            ### businnes nature checkbox
                                            $businessNatureArr = explode(',', $localGuideInfo->business_nature);

                                            $tour_operators_checked = $transport_services_checked = $activity_providers_checked = null;
                                            
                                            if(in_array('tour_operators', $businessNatureArr)){
                                                $tour_operators_checked = true;
                                            }
                                            if(in_array('transport_services', $businessNatureArr)){
                                                $transport_services_checked = true;
                                            }
                                            if(in_array('activity_providers', $businessNatureArr)){
                                                $activity_providers_checked = true;
                                            }
                                            ?>

                                            <div class="form-group">
                                                <div class="radio">
                                                    <label>
                                                        {{ Form::checkbox('business_nature[]', 'tour_operators', $tour_operators_checked, array('id' => 'businessNatureTourOperator')) }} Tour Operator (Day Tours, Multi-Country Tours,
                                                        Long-Hauled Tours)
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        {{ Form::checkbox('business_nature[]', 'transport_services', $transport_services_checked, array('id' => 'businessNatureTransportServices')) }}
                                                        Transport Services (Car Hire with driver, Airport Transfers)
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        {{ Form::checkbox('business_nature[]', 'activity_providers', $activity_providers_checked, array('id' => 'businessNatureActivityProviders')) }}
                                                        Establishment/Activity Providers (Jungle Safari, Paragliding,
                                                        White Water Rafting, Universal Studios, etc.)
                                                    </label>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('fullname', 'Main Contact Person', array('class'=>'label-font')) }}
                                            <span class="text-danger">*</span>

                                            {{ Form::text('fullname', $localGuideInfo->fullname, array('class' => 'form-control form-control-custom', 'id'=>'fullname', 'placeholder'=>'Contact Person')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('designation', 'Designation', array('class'=>'label-font')) }}

                                            {{ Form::text('designation', $localGuideInfo->designation, array('class' => 'form-control form-control-custom', 'id'=>'designation', 'placeholder'=>'Designation')) }}
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            {{ Form::label('commision', 'Commision Rate (in %)', array('class'=>'label-font')) }}
                                            {{ Form::label('commision_rate', $localGuideInfo->commision, array('class' => 'form-control form-control-custom','readonly'=>true)) }}                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('mobilenumber', 'Mobile Number', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        {{ Form::text('mobilenumber', $localGuideInfo->mobilenumber, array('class' => 'form-control form-control-custom phone-number', 'id'=>'mobilenumber', 'placeholder'=>'Mobile Number')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('preferred_mobile_contact', 'How do you prefer to be contacted:', array('class'=>'label-font')) }}


                                        <?php 
                                        ### businnes nature checkbox
                                        $preferredMobileContactArr = explode(',', $localGuideInfo->preferred_mobile_contact);

                                        $viber_checked = $whatsup_checked = $sms_checked = null;
                                        
                                        if(in_array('viber', $preferredMobileContactArr)){
                                            $viber_checked = true;
                                        }
                                        if(in_array('whatsup', $preferredMobileContactArr)){
                                            $whatsup_checked = true;
                                        }
                                        if(in_array('sms', $preferredMobileContactArr)){
                                            $sms_checked = true;
                                        }
                                        ?>

                                        <div class="form-group">
                                            <div class="radio">
                                                <label>
                                                    {{ Form::checkbox('preferred_mobile_contact[]', 'viber', $viber_checked, array('id' => 'preferredMobileContactViber')) }} Viber
                                                </label>
                                                <label>
                                                    {{ Form::checkbox('preferred_mobile_contact[]', 'whatsup', $whatsup_checked, array('id' => 'preferredMobileContactWhatsup')) }} WhatsApp
                                                </label>
                                                <label>
                                                    {{ Form::checkbox('preferred_mobile_contact[]', 'sms', $sms_checked, array('id' => 'preferredMobileContactSms')) }} SMS
                                                </label>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('', 'Email', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        {{ Form::text('email', $localGuideInfo->email, array('class' => 'form-control form-control-custom', 'id'=>'email', 'placeholder'=>'Email')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('country_id', 'Country', array('class'=>'label-font')) }}

                                        {{ Form::select('country_id', $countriesListArr,  $localGuideInfo->country_id, array('class'=>'form-control', 'id'=>'country_id')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('city', 'City', array('class'=>'label-font')) }}
                                        <span class="text-danger">*</span>

                                        {{ Form::text('city', $localGuideInfo->city, array('class' => 'form-control form-control-custom', 'id'=>'city', 'placeholder'=>'City')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('websitelink', 'Website', array('class'=>'label-font')) }}

                                        {{ Form::text('websitelink', $localGuideInfo->websitelink, array('class' => 'form-control form-control-custom', 'id'=>'websitelink', 'placeholder'=>'Website')) }}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        {{ Form::label('about', 'Any Additional Information', array('class'=>'label-font')) }}

                                        {{ Form::textarea('about', $localGuideInfo->about, array('class' => 'form-control form-control-custom', 'rows'=>4, 'id'=>'about', 'placeholder'=>'Any Additional Information')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        {{ Form::button('Save', array('id'=>'changeCurrencyButton', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return saveAccountDetails()')) }}

                        {{ Form::button('Cancel', array('id'=>'changeCurrencyButton', 'class'=>'btn bg-grey btn-flat margin', 'onclick'=>'return changeCurrency()')) }}
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{ Form::close() }}
</div>

<!-- /.content-wrapper -->

<!-- ./wrapper -->
@stop

@section('page_scripts')
<script src="js/localguide/account.js"></script>
@stop