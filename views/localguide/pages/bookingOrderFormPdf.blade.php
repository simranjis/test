
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>BOOKING ORDER FORM</title>
        <style type="text/css">
            body {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin:0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            p, h2{
                margin:0;
            }

            div,p,ul,h2,h2{
                margin:0;
            }

            h2.bigger,h2.bigger{
                font-size: 32px;
                font-weight: normal;
            }

            h2.big,h2.big{
                font-size: 21px;
                font-weight: normal;
            }

            a.link1{
                color:#62A9D2;font-size:13px;font-weight:bold;text-decoration:none;
            }

            a.link2{
                padding:8px;background:#62A9D2;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;
            }

            a.link3{
                background:#62A9D2; color:#ffffff; padding:8px 10px;text-decoration:none;font-size:13px;
            }
            .bgBody{
                background: #F6F6F6;
            }
            .bgItem{
                background: #ffffff;
            }

            @media only screen and (max-width:480px)

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }	
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

            }

            @media only screen and (max-width:540px) 

            {

                table[class="MainContainer"], td[class="cell"] 
                {
                    width: 100% !important;
                    height:auto !important; 
                }
                td[class="specbundle"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;

                }
                td[class="specbundle1"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;

                }		
                td[class="specbundle2"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                }
                td[class="specbundle3"] 
                {
                    width:90% !important;
                    float:left !important;
                    font-size:14px !important;
                    line-height:18px !important;
                    display:block !important;
                    padding-left:5% !important;
                    padding-right:5% !important;
                    padding-bottom:20px !important;
                    text-align:center !important;
                }
                td[class="specbundle4"] 
                {
                    width: 100% !important;
                    float:left !important;
                    font-size:13px !important;
                    line-height:17px !important;
                    display:block !important;
                    padding-bottom:20px !important;
                    text-align:center !important;

                }

                td[class="spechide"] 
                {
                    display:none !important;
                }
                img[class="banner"] 
                {
                    width: 100% !important;
                    height: auto !important;
                }
                td[class="left_pad"] 
                {
                    padding-left:15px !important;
                    padding-right:15px !important;
                }

                .font{
                    font-size:15px !important;
                    line-height:19px !important;

                }
            }
            @media print{
                #printButton{
                    display:none;
                } 
            }

        </style>
        <script type="colorScheme" class="swatch active">
            {
            "name":"Default",
            "bgBody":"F6F6F6",
            "link":"62A9D2",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"555555"
            }
        </script>

    </head>
    <body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0" style="margin-left:5px; margin-right:5px; margin-top:0px; margin-bottom:0px;">
        <div style="text-align:right"><button type="button" id="printButton" onclick="window.print()">Print</button></div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center"  style='font-family:helvetica, sans-serif;'>
            <!--  =========================== The header ===========================  -->
            <tbody>
                <tr>
                    <td height='25' bgcolor='#333e48' colspan='3'></td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" width="600"><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer" bgcolor="#ffffff">
                                            <tbody>
                                                <!--  =========================== The body ===========================  -->   
                                                <tr>
                                                    <td class='movableContentContainer'>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td bgcolor='#333e48' valign='top'>
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                            <tr>
                                                                                <td align='center' valign='middle' >
                                                                                    <div class="contentEditableContainer contentImageEditable">
                                                                                        <div class="contentEditable" >
                                                                                            <img src="{{url('/images/logo-email.png')}}" alt='Compagnie logo' data-default="placeholder" data-max-width="300" width='350'>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='25' bgcolor='#333e48'></td></tr>

                                                                <tr><td height='5' bgcolor='#333e48'></td></tr>

                                                                <tr><td height=12' bgcolor="#333e48"></td></tr>
                                                                <tr><td height=20' bgcolor="#fff"></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p style="text-align:center; font-weight: bold; font-size: 18px;">BOOKING ORDER FORM</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                                                                                    <p class="big">Booking Date: <?php echo date('m/d/Y', strtotime($data_array->created_at)); ?>   </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody></table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                </tbody></table>
                                                                        </td>

                                                                        <td class="specbundle2" width="18" valign="top">&nbsp;</td>

                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td width="20"></td>
                                                                                        <td width="251">
                                                                                            <table valign="top" width="251" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                <tbody><tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold;">
                                                                                                                    <p class="big">Reference Number: {{$data_array->bookings_reference}}</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="specbundle2" width="291" valign="top">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody><tr><td colspan="3" height="15"></td></tr>

                                                                                    <tr>
                                                                                        <td>
                                                                                            <table valign="center" cellspacing="0" cellpadding="0" border="0" align="right">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div class="contentEditableContainer contentTextEditable" style="margin:0px auto; text-align: center">
                                                                                                                <div class="contentEditable" style="color:#555555;font-size:14px;font-weight:bold; text-align: center;">
                                                                                                                    <img src="{{url('images/confirm.png')}}" alt="confirm"/>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                    </tr>

                                                                                    <tr><td colspan="3" height="15"></td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>

                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:16px;line-height:28px; font-weight: normal; border-left: 4px solid #00afef; padding: 0 15px;">
                                                                                                    <p>Activity Name: {{$data_array->title}}</p>

                                                                                                    <p>Activity Date: 31st October – {{date('m/d/Y',strtotime($data_array->bookingdate))}}</p>


                                                                                                    @foreach($data_array['activityBookingItemArray'] as $item)
                                                                                                    <p style="margin-left:20px">{{$item->pricefor}}: {{$item->quantity}}</p>

                                                                                                    @endforeach
                                                                                                    <p>Pick-up Location Address: {{$data_array['destinationArray']->title.' '.$data_array['destinationArray']->countries_name}}</p>

                                                                                                    <p>Pick-up Time: {{date('m/d/Y h:i A',strtotime($data_array->bookingdate))}} Please arrive at least 15-20 mins early at the pick-

                                                                                                        up location.</p>
                                                                                      @if($data_array->extra_details !='')
                                                                                                    <p> Additional Details: 
                                                                                                        {{$data_array->extra_details}}
                                                                                                    </p>
                                                                                      @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border:2px solid #9b9b9b; padding: 13px 15px;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#555555; font-size:21px;font-weight:bold;">
                                                                                                    <h2 class="big" style="font-weight:bold;">Booking Summary:</h2>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p>{{$data_array->title}}</p>

                                                                                                    @foreach($data_array['activityBookingItemArray'] as $item)

                                                                                                    <p>{{$item->pricefor}} x {{$item->quantity}} = {{$data_array->code.' '.number_format($item->price,2)}}</p>
                                                                                                    @endforeach

                                                                                                    <p>Service Fee : {{$data_array->code.' '.number_format($data_array->tax,2)}}</p>
                                                                                                    <p> Total Tour Price: {{$data_array->code.' '.number_format($data_array->total,2)}}</p>

                                                                                                    <p>{{$data_array->commision}}% Deposit paid to Destination Seeker: {{$data_array->code.' '.$data_array->commision_amount}}</p>
                                                                                                    <p>Amount to be paid by the traveler directly in cash.  </p>
                                                                                                    <p>Local Guide prefers to be paid in their local currency</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <p>Amount to be paid by the traveler directly in cash:</p>
                                                                                            <p style="color:#acacac; font-style: italic;">Local Guide prefers to be paid in their local currency</p>
                                                                                        </td>
                                                                                        <td style="padding-bottom:8px;" valign="middle" align="right">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable">
                                                                                                    <a target="_blank" class="link2" style="pointer-events:none" style="padding:12px;background:#e9164c;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;">{{$data_array->code.' '.$data_array->localguide_amount}}</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" height="16">&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr><td height='20'></td></tr>
                                                            </table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top'>
                                                                <tr>
                                                                    <td>
                                                                        <div class='contentEditableContainer contentImageEditable'>
                                                                            <div class="contentEditable">
                                                                                <img class="banner" src="{{url('/images/ab.jpg')}}" alt='What we do' data-default="placeholder" data-max-width="600" width='600' height='139' >
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <!--                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                                                                                    <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                                        <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                                                                            <tr>
                                                                                                                                <td class="spechide" width="16">&nbsp;</td>
                                                                                                                                <td class="specbundle2" width="568">
                                                                                                                                    <table valign="top" style="border:2px solid #9b9b9b; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                                                                        <tbody>
                                                                                                                                            <tr><td height="16">&nbsp;</td></tr>
                                                                                                                                            <tr>
                                                                                                                                                <td valign="top" align="left">
                                                                                                                                                    <div class="contentEditableContainer contentTextEditable">
                                                                                                                                                        <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                                                                            <p style="font-weight:bold; font-size: 17px">Extra Details:</p>
                                                        
                                                                                                                                                            <p style="margin-left:20px">Shoe Size</p>
                                                        
                                                                                                                                                            <p style="margin-left:20px"> Adult 1: 42</p>
                                                        
                                                                                                                                                            <p style="margin-left:20px">Adult 2: 37</p>
                                                        
                                                                                                                                                            <p style="margin-left:20px"> Child: 6-9 y.o</p>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr><td height="20">&nbsp;</td></tr>
                                                                                                                                        </tbody></table>
                                                                                                                                </td>
                                                                                                                                <td class="spechide" width="16">&nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                                                                        </tbody></table>
                                                                                                                </div>-->
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p> 
                                                                                                        <span style="font-weight:bold; font-size: 17px">Tour Inclusions/Exclusions:</span>

                                                                                                        <p> @if(count($data_array->bookingActivityFAQ) > 0)
                                                                                                            @foreach($data_array->bookingActivityFAQ as $key => $FAQ)
                                                                                                            @if($FAQ->title == 'Inclusion')
                                                                                                            {!! $FAQ->description !!}
                                                                                                            @endif
                                                                                                            @endforeach
                                                                                                            @endif                                                                                                                </p>
                                                                                                        @if(count($data_array->bookingActivityFAQ) > 0)
                                                                                                        @foreach($data_array->bookingActivityFAQ as $key => $FAQ)
                                                                                                        @if($FAQ->title == 'Exclusion')
                                                                                                        {!! $FAQ->description !!}
                                                                                                        @endif
                                                                                                        @endforeach
                                                                                                        @endif 
                                                                                                    </p>
                                                                                                    <p> <span style="font-weight:bold; font-size: 17px">Itinerary: </span>
                                                                                                        @if(count($data_array['bookingActivityItenary']) > 0)
                                                                                                        <ul>
                                                                                                            @foreach($data_array['bookingActivityItenary'] as $itenary)
                                                                                                            <li><b>{{$itenary->title}} :</b> {!! $itenary->description !!}</li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                        @endif </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                    <tr>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                        <td class="specbundle2" width="568">
                                                                            <table valign="top" style="border:2px solid red; padding: 13px 15px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                                <tbody>
                                                                                    <tr><td height="16">&nbsp;</td></tr>
                                                                                    <tr>
                                                                                        <td valign="top" align="left">
                                                                                            <div class="contentEditableContainer contentTextEditable">
                                                                                                <div class="contentEditable" style="color:#565656;font-size:15px;line-height:25px;">
                                                                                                    <p style="font-weight:bold; font-size: 17px;background: red;width: 184px;color: #fff;padding: 10px;margin:-57px 0 20px 0">Important Information:</p>

                                                                                                    <p>• The Local Guide shall accept the mobile e-voucher presented by the Traveler on the Activity 

                                                                                                        Day.</p>

                                                                                                    <p> •

                                                                                                        You can save this Booking Order Form and match it with the Traveler’s Mobile E-Voucher.</p>

                                                                                                    <p>•

                                                                                                        You may also request for additional verification from the Traveler (e.g Passport)</p>

                                                                                                    <p>•

                                                                                                        You may directly contact your Traveler via Phone call, Viber, or WhatsApp after the FREE 

                                                                                                        cancellation period you specified. See ‘My Bookings’.</p>

                                                                                                    <p>•

                                                                                                        If you wish to request your cash settlement in your local currency, you can directly arrange 

                                                                                                        this with your Traveler or specify it in your activity’s ‘Additional Information’.</p>

                                                                                                    <p> •

                                                                                                        If there are any changes with the inclusions/exclusions and itinerary posted on your Desti

                                                                                                        -

                                                                                                        nation Seeker activity listing, it is the Local Guide’s sole responsibility to directly inform the 

                                                                                                        Traveler regarding these changes. </p>

                                                                                                    <p>•

                                                                                                        The Traveler’s overall experience is under the Local Guide’s responsibility.</p>

                                                                                                    <p> •

                                                                                                        The Traveler may leave and you may also request for a review after your activity date. See 

                                                                                                        ‘My Bookings’  ‘Request for a Review’ </p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr><td height="20">&nbsp;</td></tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                        <td class="spechide" width="16">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td colspan="3" height="16">&nbsp;</td></tr>
                                                                </tbody></table>
                                                        </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table valign="top" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                                                <tbody><tr>
                                                                        <td>
                                                                            <div class="contentEditableContainer contentImageEditable">
                                                                                <div class="contentEditable">
                                                                                    <img class="banner" src="{{url('images/ab.jpg')}}" alt="What we do" data-default="placeholder" data-max-width="600" width="600" height="139">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                            </div>
                                                        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign='top' class='' style="background:#333e48;" >

                                                                <tr><td height='28'></td></tr>

                                                                <tr>
                                                            <td valign='top' align='center'>
                                                                <div class="contentEditableContainer conten                                                                                    tTextEditable">
                                                                            <div class="contentEditable" style='color:#A8B0B6; font-size:13px;line-                                                                                        height: 16px;'>
                                                                                        <img src="{{url('/images/logo-email.png')}}" alt="Compagnie logo" data-default="placeholder" data-max-width="300" width="350">
                                                                                                        </div>
                                                                                                        </div>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        <tr><td height='28'></td></tr>
                                                                                                        </table>
                                                                                                        </div>

                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                        </table>
                                                                                                        </td>
                                                                                                        <td valign="top" class="spechide"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td height='130' bgcolor='#333e48'>&nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>&nbsp;</td>
                                                                                                                    </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                        </table>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                        </table>   
                                                                                                        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
                                                                                                        <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
                                                                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

                                                                                                        <script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>
                                                                                                        <script>
                                                                                                //$(window).on('load', function () {
                                                                                                //    var specialElementHandlers =
                                                                                                //            function (element, renderer) {
                                                                                                //                return true;
                                                                                                //            }
                                                                                                //    var doc = new jsPDF();
                                                                                                //    doc.fromHTML($('#content').html(), 15, 15, {
                                                                                                //        'width': 170,
                                                                                                //        'elementHandlers': specialElementHandlers
                                                                                                //    }, function () {
                                                                                                //        doc.output('datauri');
                                                                                                //        doc.save('abc.pdf');
                                                                                                //    });
                                                                                                //    let options = {
                                                                                                //        pagesplit: true
                                                                                                //    };
                                                                                                //    var pdf = new jsPDF('p', 'px', 'letter');
                                                                                                //    pdf.addHTML(document.body, 10, 15, options, function () {
                                                                                                //        pdf.save('bookingDetails.pdf');
                                                                                                //    });
                                                                                                //});


                                                                                                        </script>
                                                                                                        </body>
                                                                                                        </html>
