@extends('localguide.layouts.defaultsidebar')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Settings
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="box-title">Change Password</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        {{ Form::label('current_password', 'Current Password', array('class'=>'label-font')) }}

                                        {{ Form::password('current_password', array('class' => 'form-control form-control-custom', 'id'=>'current_password', 'placeholder'=>'Current Password')) }}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        {{ Form::label('password', 'New Password', array('class'=>'label-font')) }}

                                        {{ Form::password('password', array('class' => 'form-control form-control-custom', 'id'=>'password', 'placeholder'=>'New Password')) }}
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        {{ Form::label('confirm_password', 'Confirm Password', array('class'=>'label-font')) }}

                                        {{ Form::password('confirm_password', array('class' => 'form-control form-control-custom', 'id'=>'confirm_password', 'placeholder'=>'Confirm Password')) }}
                                    </div>                                        
                                </div>
                                <div class="row">
                                    &nbsp;
                                   {{ Form::button('Change', array('id'=>'changePasswordButton', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return changePassword()')) }}
                                </div>
                                <hr>
                                <h3 class="box-title">Currency</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        {{ Form::label('confirm_password', 'Your Preferred Payment Currency', array('class'=>'label-font')) }}
                                        
                                        {{ Form::select('currency_id', array_merge(['' => '-- Select Currency --'], $currencyListArr), $currency_id, array('class'=>'form-control', 'id'=>'currency_id')) }}

                                        <span class="help-block">
                                            All prices will be converted in standard USD however if you prefer to be paid in your local currency, this will be reflected in the Traveler's Mobile E-Voucher and you can also directly arrange this with the traveler once the activity has been paid the deposit. 
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    &nbsp;{{ Form::button('Change', array('id'=>'changeCurrencyButton', 'class'=>'btn bg-maroon btn-flat margin', 'onclick'=>'return changeCurrency()')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ./wrapper -->
@stop

@section('page_scripts')
<script src="js/localguide/settings.js"></script>
@stop