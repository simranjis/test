<div class="row" style="border: 1px solid #ccc; padding: 5px;">
	<div class="col-sm-12" id="{{$gallery_id}}">
		<div class="col-sm-3" id="{{$gallery_id}}">
		    <div class="text-center">
		    	<img src="uploads/gallery/{{ $filename }}" class="img-reponsive" width="200px" height="150px" >
		    </div>
	    </div>
	    <div class="col-sm-7">
		    <label>Title: </label>
		    <input type="title[{{$gallery_id}}]" name="title[{{$gallery_id}}]" id="title{{$gallery_id}}" class="form-control" value="" />
		    <br>
		    <label>Tags: </label><br>
		    <input type="tags[{{$gallery_id}}]" name="tags[{{$gallery_id}}]" id="tags{{$gallery_id}}" class="form-control tags col-sm-12" value="" />
	    </div>
	    <div class="col-sm-1">
            <a href="javascript:void(0)" style="vertical-align: middle;" class="btn bg-maroon btn-flat" onclick="deleteGalleryImage(<?php echo $gallery_id; ?>)">
                <i class="fa fa-trash"></i> DELETE
            </a>
        </div>
	</div>
</div>
<br>