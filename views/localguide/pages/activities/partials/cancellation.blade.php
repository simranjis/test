<div id="CancellationInfo" class="tabcontent <?php echo $cancellationInfoHidden; ?>">
     {!! Form::open(['url' => 'localguide/insertCancellationInfo', 'name' => 'insertCancellationInfo' , 'class'=>'insertCancellationInfo', 'id'=>'insertCancellationInfo']) !!}   
     <?php
        $title = $description = $cancellation_activity_faq_id = $days = '';
        $is_cancellation_info = 0;
        if(!empty($activity->activitity_faqs)){
            foreach ($activity->activitity_faqs as $key => $value) {
                if($value->faq_id==5){
                    $title = $value->title;
                    $days = $value->days;
                    $description = $value->description;
                    $cancellation_activity_faq_id = $value->id;
                    $is_cancellation_info = 1;
                }
            }
        }

        if(empty($is_cancellation_info)){
            $title = $FaqsArr[5]['title'];
            $description = $FaqsArr[5]['description'];
        }

    ?> 
 	<input type="hidden" name="id" value="{{ $id }}">
 	<input type="hidden" name="faq_id[5]" value="5">
    <input type="hidden" name="activity_faq_id[5]" value="{{$cancellation_activity_faq_id}}">
 		<div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title5', 'Title: ', array('class'=>'label-font')) }}

                                        <?php 

                                        $cancellationOptionsArr['Once booked, no cancellations can be made.'] = 'Once booked, no cancellations can be made.';

                                        /*if($activity->addedbytype=='admin'){ 
                                            $cancellationOptionsArr['Full refunds will be issued for cancellations made at least {{FOLLOWING_DAYS}} days prior to the activity.'] = "Full refunds will be issued for cancellations made at least following days prior to the activity.";

                                         }else{ */
                                            $cancellationOptionsArr['You can cancel for FREE at least {{FOLLOWING_DAYS}} days prior to the activity.'] = "You can cancel for FREE at least following days prior to the activity.";

                                         //} 

                                         ?>
                                        
                                        {{ Form::select('title[5]', $cancellationOptionsArr,  $title, array('class'=>'form-control', 'id'=>'title5')) }}

                                    </div>
                                </div>
                            </div>
                            <?php 

                            $days_hide = 'hide';
                            if($title == "You can cancel for FREE at least {{FOLLOWING_DAYS}} days prior to the activity."){
                                $days_hide = '';
                            }
                            ?>


                            <div class="row {{$days_hide}}" id="refund_days">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('days5', 'Days: ', array('class'=>'label-font')) }}

                                        {{ Form::text('days[5]', $days, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'days5', 'placeholder'=>'Days')) }}

                                    </div>
                                </div>
                            </div>
                            
                            <div class="row hide">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description5', 'Description: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[5]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description5', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
     {!! Form::close() !!}
</div>