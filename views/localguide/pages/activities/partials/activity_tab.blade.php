<div id="Activity" class="tabcontent <?php echo $activityHidden; ?>">
      {!! Form::open(['url' => 'localguide/insertActivity', 'name' => 'insertActivitiesForm' , 'class'=>'insertActivitiesForm', 'id'=>'insertActivitiesForm', 'files' => true]) !!}                                
        <div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('destinations_id', 'City: ', array('class'=>'label-font')) }}

                                        <?php
                                        $destinations_id = '';
                                        if($activity->destinations_id){
                                            $destinations_id = $activity->destinations_id;
                                        }
                                        ?>

                                        {{ Form::select('destinations_id',  $destinationsListArr, $destinations_id, array('class'=>'form-control', 'id'=>'destinations_id')) }}
                                    </div>
                                </div>
                            </div>

                            <!-- Category  -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('categories_id', 'For: ', array('class'=>'label-font')) }}

                                        <?php 
                                            $categoriesArr = array(
                                                            1=>'AUTHENTIC EXPERIENCES',
                                                            2=>'POPULAR ACTIVITIES'
                                                        ); 
                                        ?>

                                        <?php
                                        $categories_id = '';
                                        if($activity->categories_id){
                                            $categories_id = $activity->categories_id;
                                        }
                                        ?>

                                        {{ Form::select('categories_id',  $categoriesArr, $categories_id, array('class'=>'form-control', 'id'=>'categories_id')) }}
                                    </div>
                                </div>
                            </div>

                            <!-- Title -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title', 'Title: ', array('class'=>'label-font')) }}

                                        <?php
                                        $title = '';
                                        if($activity->title){
                                            $title = $activity->title;
                                        }
                                        ?>

                                        {{ Form::text('title', $title, array('class' => 'form-control', 'id'=>'title', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <!-- Activity Cover Photo -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" name="cropx" id="cropx" value="0" />
                                    <input type="hidden" name="cropy" id="cropy" value="0" />
                                    <input type="hidden" name="cropw" id="cropw" value="0" />
                                    <input type="hidden" name="croph" id="croph" value="0" />
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <strong>Cover Photo</strong><br>
                                        <hr>
                                        <?php if(!empty($activity->image)){ ?>
                                            <div id="cover_image_div" >
                                                <img style="padding: 10px 0;" src="uploads/{{ $activity->image }}" width="360px" height="180px">
                                                <div class="clearfix"></div>
                                                <h4>
                                                    <a href="javascript:void(0)" onclick="return editCoverImage()">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </h4>
                                            </div>
                                        <?php } ?>
                                        <?php 
                                            $cover_image_hide = '';
                                            if(!empty($activity->image)){ 
                                                $cover_image_hide = 'hide';
                                            }
                                        ?>
                                        
                                        <div id="file_uploader_div" class="{{$cover_image_hide}}">
                                            <img id="preview" src="http://placehold.it/360x180" alt="your image" width="360px" height="180px" />
                                            <br><br>
                                            <input type="file" name="image" id="activity-image" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                </div>    
                            </div>


                            <!-- Activity Banner Photo -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-4">
                                        <strong>Banner Image</strong><br>
                                        <hr>
                                        <?php if(!empty($activity->banner_image)){ ?>
                                            <div id="banner_image_div" >
                                                <img style="padding: 10px 0;" src="uploads/banner/{{ $activity->banner_image }}" width="360px" height="180px">
                                                <div class="clearfix"></div>
                                                <h4>
                                                    <a href="javascript:void(0)" onclick="return editBannerImage()">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </h4>
                                            </div>
                                        <?php } ?>
                                        <?php 
                                            $banner_image_hide = '';
                                            if(!empty($activity->banner_image)){ 
                                                $banner_image_hide = 'hide';
                                            }
                                        ?>
                                        
                                        <div id="banner_file_uploader_div" class="{{$banner_image_hide}}">
                                            <img id="banner_image_preview" src="http://placehold.it/1920x480" alt="your image" width="360px" height="180px" />
                                            <br><br>
                                            <input type="file" name="banner_image" id="activity_banner_image" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">&nbsp;</div>
                                </div>    
                            </div>
                            <div class="row">
			    <div class="col-sm-12">
				<div class="form-group">
				    {{ Form::label('meta_title', 'Meta Title: ', array('class'=>'label-font')) }}

				    <?php
				    $meta_title = '';
				    if ($activity->meta_title) {
					$meta_title = $activity->meta_title;
				    }
				    ?>

				    {{ Form::text('meta_title', $meta_title, array('class' => 'form-control', 'id'=>'meta_title', 'placeholder'=>'Meta Title')) }}
				</div>
			    </div>
			</div>
			<div class="row">
			    <div class="col-sm-12">
				<div class="form-group">
				    {{ Form::label('meta_keyword', 'Meta Keyword: ', array('class'=>'label-font')) }}

				    <?php
				    $meta_keyword = '';
				    if ($activity->meta_keyword) {
					$meta_keyword = $activity->meta_keyword;
				    }
				    ?>

				    {{ Form::textarea('meta_keyword', $meta_keyword, array('class' => 'form-control', 'id'=>'meta_keyword', 'placeholder'=>'Meta Keyword','rows'=>4,)) }}
				</div>
			    </div>
			</div>	    <div class="row">
			    <div class="col-sm-12">
				<div class="form-group">
				    {{ Form::label('meta_description', 'Meta Description: ', array('class'=>'label-font')) }}

				    <?php
				    $meta_description = '';
				    if ($activity->meta_description) {
					$meta_description = $activity->meta_description;
				    }
				    ?>

				    {{ Form::textarea('meta_description', $meta_description, array('class' => 'form-control', 'id'=>'meta_description', 'placeholder'=>'Meta Description' ,'rows'=>4,)) }}
				</div>
			    </div>
			</div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>    
        <input type="button" onclick="return validateActivityForm()" name="Submit" value="Submit" class="btn bg-maroon btn-flat margin">
        <input type="hidden" name="id" id="id" value="{{$id}}" />
      {!! Form::close() !!}
</div>