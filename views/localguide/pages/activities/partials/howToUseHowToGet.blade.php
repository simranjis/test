<div id="HowToUseHowToGetInfo" class="tabcontent <?php echo $howToUseHowToGetInfoHidden; ?>">
     {!! Form::open(['url' => 'localguide/insertHowToUseHowToGetInfo', 'name' => 'insertHowToUseHowToGetInfo' , 'class'=>'insertHowToUseHowToGetInfo', 'id'=>'insertHowToUseHowToGetInfo']) !!}   

     <?php
        $title = $description = $howtouseandhowtoget_info_activity_faq_id = '';
        $is_howtouseandhowtoget_info = 0;
        if(!empty($activity->activitity_faqs)){
            foreach ($activity->activitity_faqs as $key => $value) {
                if($value->faq_id==4){
                    $title = $value->title;
                    $description = $value->description;
                    $howtouseandhowtoget_info_activity_faq_id = $value->id;
                    $is_howtouseandhowtoget_info = 1;
                }
            }
        }

        if(empty($is_howtouseandhowtoget_info)){
            $title = $FaqsArr[4]['title'];
            $description = $FaqsArr[4]['description'];
        }
    ?> 
 	<input type="hidden" name="id" value="{{ $id }}">
 	<input type="hidden" name="faq_id[4]" value="4">
    <input type="hidden" name="activity_faq_id[4]" value="{{$howtouseandhowtoget_info_activity_faq_id}}">
 		<div class="">
            <div class="row">
                <div class="col-xs-12">                                
                    <div class="">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('title4', 'How to Use ', array('class'=>'label-font')) }}
                                        <br>
                                        <?php
                                        $howToGet1 = $howToGet2 = '';
                                        if($title == 'You must present a printed voucher for this activity.'){
                                            $howToGet2 = "checked='checked'";
                                        }
                                        if($title == 'You can either present a printed or a mobile e-voucher for this activity.'){
                                            $howToGet1 = "checked='checked'";
                                        }
                                        ?>
                                        <input type="radio" name="howToGet" id="howToGet1" value="You can either present a printed or a mobile e-voucher for this activity." <?php echo $howToGet1; ?> />
                                        <label for="howToGet1">
                                            You can either present a printed or a mobile e-voucher for this activity.
                                        </label>
                                        <br>
                                        <input type="radio" name="howToGet" id="howToGet2" value="You must present a printed voucher for this activity." <?php echo $howToGet2; ?> />
                                        <label for="howToGet2">
                                            You must present a printed voucher for this activity.
                                        </label>
                                        {{ Form::hidden('title[4]', $title,  array('class'=>'form-control', 'id'=>'title4', 'placeholder'=>'Title')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        {{ Form::label('description4', 'How to Get There: ', array('class'=>'label-font')) }}

                                        {{ Form::textarea('description[4]', $description, array('class' => 'form-control ckeditor form-control-custom', 'rows'=>15, 'id'=>'description4', 'placeholder'=>'Description')) }}

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" name="Submit" value="Update" class="btn bg-maroon btn-flat margin">
     {!! Form::close() !!}
</div>