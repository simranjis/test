@extends('localguide.layouts.defaultsidebar')
@section('content')


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Activities
            <small>My Activities</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                            <br>
                            <a href="{{ url('localguide/addActivity') }}" type="button" class="btn bg-maroon btn-flat margin">New Activity</a>
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Activities</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Cover Photo</th>
                                                    <th>Title </th>
                                                    <th>Continent </th>
                                                    <th>Country</th>
                                                    <th>City</th>
                                                    <th>Status</th>
                                                    <th>Call to action</th>
                                                </tr>
                                                <?php foreach ($activities as $key => $value) { 
                                                    $destinationObj = $value->destination;
                                                    $region_title = '';
                                                    if(!empty($destinationObj->region)){
                                                        $region_title = $destinationObj->region->title;
                                                    }
                                                    $country_title = '';
                                                    if(!empty($destinationObj->country)){
                                                        $country_title = $destinationObj->country->countries_name;
                                                    }

                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php if($value->image){ ?>
                                                                <img class="img-responsive" src="uploads/{{$value->image}}" alt="User profile picture" style="height: 80px;">
                                                            <?php } else { ?>
                                                                <img class="img-responsive" src="images/missing-image-640x360-1-360x180.png" alt="User profile picture" style="height: 80px; width:80px;">
                                                            <?php } ?>
                                                        </td>
                                                        <td>{{$value->title}}</td>
                                                        <td>{{$region_title}}</td>
                                                        <td>{{$country_title}}</td>
                                                        <td>{{$value->destination['title']}}</td>
                                                        <td>
                                                            <?php
                                                                if($value->status==1){
                                                                    echo 'Active';
                                                                } elseif($value->status==2) {
                                                                    echo 'Declined';
                                                                }else{
                                                                    echo 'Pending';
                                                                }

                                                            ?>
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('localguide/addActivity/'.$value->id) }}" class="btn bg-navy btn-flat margin collapsed" role="button">
                                                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ $activities->links() }}
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ./wrapper -->


@stop