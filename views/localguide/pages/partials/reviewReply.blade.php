<div class="modal fade" id="modal-default-4">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-maroon">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Reply</h4>
                </div>
                <div class="modal-body">
                	<div class="row">
                		<div class="col-sm-12">
                			{{ Form::textarea('reply', null, array('class' => 'form-control form-control-custom', 'rows'=>10, 'id'=>'reply', 'placeholder'=>'Reply')) }}
            			</div>
                	</div>
            		<hr>
                </div>
                <div class="modal-footer bg-gray text-center">
                    <a class="btn bg-maroon btn-flat margin" onclick="return replyReviewAction()" role="button">
                        Reply
                    </a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" name="review_id" id="review_id" value="">
</div>