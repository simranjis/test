<div class="modal fade" id="modal-default-4">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Traveller Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <img style="padding: 10px 0; width:150px;" src="" id="traveller_image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('full_name', 'Name', array('class'=>'label-font col-md-6 text-center')) }}

                            <label id="traveller_name"></label>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('email', 'Email', array('class'=>'label-font col-md-6 text-center')) }}

                            <label id="traveller_email"></label>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('mobilenumber', 'Mobile', array('class'=>'label-font col-md-6 text-center')) }}

                            <label id="traveller_mobilenumber"></label>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('passport', 'Passport', array('class'=>'label-font col-md-6 text-center')) }}

                            <label id="traveller_passport"></label>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('country', 'Country', array('class'=>'label-font col-md-6 text-center')) }}

                            <label id="traveller_country_name"></label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>