@extends('localguide.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Review
            <small>My Review</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Review</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Reviews</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Booking Ref. No.</th>
                                                    <th>Activity Photo</th>
                                                    <th>Activity Title</th>
                                                    <th>Rating</th>
                                                    <th>Review</th>
                                                    <th>Reply</th>
                                                    <th>Call to Action</th>
                                                </tr>
                                                <?php foreach ($activityReviewResultObj as $key => $activityReviewObj) { 

                                                    $bookingObj = $activityReviewObj->booking;
                                                    $activityObj = $activityReviewObj->activity;
                                                   // dd($activityObj);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                        {{$bookingObj->bookings_reference}}
                                                        </td>
                                                        <td>
                                                            <img class="img-responsive" src="uploads/{{$activityObj->image}}" alt="User profile picture" style="height: 80px">
                                                        </td>
                                                        <td>{{$activityObj->title}}</td>
                                                        <td>
                                                            <?php if(!empty($activityReviewObj->review)){ 
                                                                $reviewObj = $activityReviewObj->review;
                                                                ?>
                                                                <input type="hidden" name="rate<?php echo $reviewObj->id; ?>" id="rate<?php echo $reviewObj->id; ?>" value="<?php echo $reviewObj->rating; ?>">

                                                                <div class="rateYo" rate-attr="<?php echo $reviewObj->id; ?>" id="rateYo<?php echo $reviewObj->id; ?>"></div>
                                                            <?php }else{ ?>
                                                                -- Not Available --
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <?php if(!empty($activityReviewObj->review)){ 
                                                                ?>
                                                                {{$reviewObj->descrption}}
                                                            <?php }else{ ?>
                                                                -- Not Available --
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <?php if(!empty($activityReviewObj->review)){ 
                                                                ?>
                                                                {{$reviewObj->reply}}
                                                            <?php }else{ ?>
                                                                -- Not Available --
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <?php if(!empty($activityReviewObj->review) && (empty($activityReviewObj->review->reply))){ ?>
                                                                <a class="btn bg-maroon btn-flat margin" onclick="replyReview(<?php echo $reviewObj->id ?>)" role="button" data-toggle="modal" data-target="#modal-default-4">
                                                                    Reply
                                                                </a>
                                                            <?php } ?>
                                                            <?php if(empty($activityReviewObj->review) && ($activityReviewObj->request_review_status != 1)){ ?>
                                                                <a class="btn bg-maroon btn-flat margin" onclick="requestForReview(<?php echo $activityReviewObj->id ?>)" role="button" data-toggle="modal" data-target="#modal-default-5">
                                                                    Request For Review
                                                                </a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>

<!-- all local guides -->
@include('localguide.pages.partials.reviewReply')

<!-- /.content-wrapper -->
<!-- ./wrapper -->
@stop

@section('page_scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script src="js/localguide/reviews.js"></script>
@stop




