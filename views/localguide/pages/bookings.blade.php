@extends('localguide.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Bookings
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
        @if(Session::has('activityBookingSuccess'))
        <br/>
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{\Session::get('activityBookingSuccess')}}
                    <?php \Session::forget('activityBookingSuccess'); ?>
                </div>
            </div>
        </div>
        @endif
        @if(\Session::has('activityBookingError'))
        <br/>
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{Session::get('activityBookingError')}}
                    <?php \Session::forget('activityBookingError'); ?>
                </div>
            </div>
        </div>
        @endif
    </section>

    <!-- Main content -->
    <section class="content">
        <?php
        if (!empty($bookingResultArr['Bookings'])) {
            ?>
            <?php
            foreach ($bookingResultArr['Bookings'] as $booking_id => $bookingArr) {
                $traveller_id = $bookingArr['traveller_id'];
                $bookings_reference = $bookingArr['bookings_reference'];
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <div class="box-header with-border box-header-color">
                                                <h3 class="box-title sbold">Booking Reference Number: #{{$bookings_reference}}</h3>
                                                <p class="">Booking Date 
                                                    <?php echo date('d-M-Y', strtotime($bookingArr['created_at'])); ?></p>
                                            </div>
                                            <!-- /.box-header -->

                                            <div class="text-right">
                                                <a href="{{ url('localguide/bookings/view',  $bookingArr['id']) }}" class="btn bg-blue btn-flat margin" role="button">
                                                    Booking Details
                                                </a>

                                            </div>
                                            <div class="box-body table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Cover Photo</th>
                                                            <th>Title</th>
                                                            <th>Time</th>
                                                            <th>Units</th>
                                                            <th>Total Price</th>
                                                            <th>Status</th>
                                                            <th>Call to action</th>
                                                            <th>Booking order form</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        foreach ($bookingArr['BookingActivity'] as $booking_activity_id => $bookedActivityArr) {
                                                            $bookingItemsArr = $bookedActivityArr['BookingItems'];
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php if ($bookedActivityArr['activity_image']) { ?>
                                                                        <img class="img-responsive" src="uploads/{{$bookedActivityArr['activity_image']}}" alt="User profile picture" style="height: 80px;">
                                                                    <?php } else { ?>
                                                                        <img class="img-responsive" src="images/missing-image-640x360-1-360x180.png" alt="User profile picture" style="height: 80px; width:80px;">
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    {{$bookedActivityArr['activity_title']}}
                                                                </td>
                                                                <td>
                                                                    <?php echo date('d-M-Y', strtotime($bookedActivityArr['booking_activity_date'])); ?>
                                                                </td>
                                                                <td>
                                                                    <ul>
                                                                        <?php foreach ($bookingItemsArr as $bookingItemArr) { ?>
                                                                            <li>
                                                                                <?php
                                                                                if ($bookingItemArr['bookings_item_type'] == 'package') {
                                                                                    echo $bookingItemArr['bookings_item_quantity'] . ' x Package';
                                                                                } else {
                                                                                    echo $bookingItemArr['bookings_item_quantity'] . ' x ' . ucwords($bookingItemArr['bookings_item_pricefor']);
                                                                                }
                                                                                ?>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $price = 0;
                                                                    foreach ($bookingItemsArr as $bookingItemArr) {
                                                                        $price += $bookingItemArr['bookings_item_price'];
                                                                    }
                                                                    echo number_format($price, 2);
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $status = '';
                                                                    $paidStatus = '';
                                                                    if (empty($bookedActivityArr['status']) || ($bookedActivityArr['booking_activity_status'] == 0)) {
                                                                        $status = 'New';
                                                                    }
                                                                    if ($bookedActivityArr['booking_activity_status'] == 1) {
                                                                        $status = 'Pending';
                                                                        if($bookedActivityArr['payment_status'] == '1'){
                                                                            $paidStatus = '<span class="label label-success">PAID</span> ';
                                                                            $status = 'Confirmed';
                                                                        }else{
                                                                            $status = 'Pending';  
                                                                            $paidStatus = '<span class="label label-info">Not Paid </label>';  
                                                                        }
                                                                    }
                                                                    if ($bookedActivityArr['booking_activity_status'] == 2) {
                                                                        $status = 'Declined';
                                                                    }
                                                                    echo $status.'<br/>'.$paidStatus;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($bookedActivityArr['booking_activity_status'] == 0) { ?>
                                                                        <a href="javascript:void(0)" onclick="return changeBookingActivtyStatus(<?php echo $bookedActivityArr['booking_activity_id'] ?>, 1)" class="btn bg-green btn-flat margin" role="button">
                                                                            Accept
                                                                        </a>
                                                                        <a href="javascript:void(0)" onclick="return changeBookingActivtyStatus(<?php echo $bookedActivityArr['booking_activity_id'] ?>, 2)" class="btn bg-red btn-flat margin" role="button">
                                                                            Decline
                                                                        </a>
                                                                    <?php } else { ?>
                                                                        ---
                                                                    <?php } ?>
                                                                </td>
                                                                <td>
                                                                    @if($bookedActivityArr['payment_status'] == '1')
                                                                    <a target="_blank" href='{{url('/localguide/booking-order-form/'.$bookedActivityArr['booking_activity_id'])}}'><span class="label label-success">View Booking Order Form</span></a>
                                                                    @else
                                                                    <span class="label label-default">View Booking Order Form</span>
                                                                    @endif
                                                                    <br/>
                                                                    
                                                <a  @if($bookedActivityArr['payment_status'] == '0') style="pointer-events:none" @endif class="btn bg-green btn-sm margin" onclick="displayTravellerDetails(<?php echo $traveller_id ?>)" role="button" data-toggle="modal" data-target="#modal-default-4">
                                                    Traveller Details
                                                </a>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- all local guides -->
            <?php } ?>
        <?php } else { ?>
            <h2 class="text-center"> -- Bookings not available -- </h2>
        <?php } ?>
        @include('localguide.pages.partials.travellerDetails')
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- ./wrapper -->
@stop
@section('page_scripts')
<script src="js/localguide/bookings.js"></script>
@stop