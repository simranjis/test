@extends('localguide.layouts.defaultsidebar')
@section('content')


<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        vertical-align: middle;
    }
    .box-header > .box-tools {
        position: absolute;
        right: 10px;
        top: 14px;
    }
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars
    {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
    .book-activity {
        text-align: center;
        margin: 30px 0;
    }
    .book-activity  .fa.fa-flag-checkered{
        font-size: 45px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            My Activity
            <small>My Activity</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Activity</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="book-activity">
                            <i class="fa fa-flag-checkered" aria-hidden="true"></i>
                            <br>
                            <a href="{{ url('localguide/addActivity') }}" type="button" class="btn bg-maroon btn-flat margin">New Activity</a>
                        </div>
                    </div>
                </div>
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">                                
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">My Activities</h3>
                                        <p class="">Booking Date 7 Jan 2018</p>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Activity</th>
                                                    <th>Activity Title </th>
                                                    <th>Last Updated </th>
                                                    <th>Empty</th>
                                                    <th>Empty</th>
                                                    <th>Call to action</th>
                                                </tr>
                                                <tr>
                                                    <td><img class="img-responsive" src="../admin_destination_traveler/dist/img/c.png" alt="User profile picture" style="height: 80px"></td>
                                                    <td>ocean park hongkong</td>
                                                    <td>11-1-2018</td>
                                                    <td>1 X Package(4 Tickets)</td>
                                                    <td></td>
                                                    <td><a class="btn bg-maroon btn-flat margin" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                            Edit
                                                        </a>
                                                        <a class="btn bg-navy btn-flat margin" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                            Delete
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6">
                                                        <div class="collapse" id="collapseExample">
                                                            <div class="">
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Booking Date</strong></th>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Reference Number</strong></th>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Traveler Name</strong></th>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Traveler Contact Number</strong></th>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Acitivity Name</strong></th>
                                                                                <th><strong><i class="fa fa-book margin-r-5"></i> Acitivity Date</strong></th>
                                                                                <th><strong><i class="fa fa-map-marker margin-r-5"></i>Pick Up Location</strong></th>
                                                                                <th><strong><i class="fa fa-map-marker margin-r-5"></i>Quantity</strong></th>
                                                                                <th><strong><i class="fa fa-map-marker margin-r-5"></i>Total Price</strong></th>
                                                                                <th><strong><i class="fa fa-map-marker margin-r-5"></i>Status</strong></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><p class="text-muted">
                                                                                        22/1/2018
                                                                                    </p>
                                                                                </td>
                                                                                <td><p class="text-muted">
                                                                                        22445
                                                                                    </p>
                                                                                </td>
                                                                                <td><p class="text-muted">
                                                                                        Lorem Ipsum
                                                                                    </p>
                                                                                </td>
                                                                                <td><p class="text-muted">
                                                                                        Lorem Ipsum
                                                                                    </p> 
                                                                                </td>
                                                                                <td><p class="text-muted">
                                                                                        Lorem Ipsum
                                                                                    </p>
                                                                                </td>
                                                                                <td><p class="text-muted">
                                                                                        Lorem Ipsum
                                                                                    </p> 
                                                                                </td>
                                                                                <td> 
                                                                                    <p class="text-muted">Malibu, California</p>  
                                                                                </td>
                                                                                <td><p class="text-muted">Malibu, California</p> 
                                                                                </td>
                                                                                <td>
                                                                                    <p class="text-muted">Malibu, California</p> 
                                                                                </td>
                                                                                <td>
                                                                                    <a href="" data-toggle="modal" data-target="#modal-success"><span class="label label-success">Confirm</span></a>
                                                                                    <a href="" data-toggle="modal" data-target="#modal-success"><span class="label label-danger">Decline</span></a>
                                                                                </td>
                                                                        <div class="modal modal-success fade" id="modal-success">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span></button>
                                                                                        <h4 class="modal-title">Success Modal</h4>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <p>One fine body&hellip;</p>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                                                                        <button type="button" class="btn btn-outline">Save changes</button>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- /.modal-content -->
                                                                            </div>
                                                                            <!-- /.modal-dialog -->
                                                                        </div>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table
                                                                </div>
                                                                <!-- /.box-body -->
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ./wrapper -->


@stop

