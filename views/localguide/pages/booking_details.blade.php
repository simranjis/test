@extends('localguide.layouts.defaultsidebar')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Booking #{{ $bookingArr->bookings_reference }} 
            <small>Information</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Bookings</li>
        </ol>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                
                <div class="box box-primary">
                   <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="">
                                    <div class="box-header with-border box-header-color">
                                        <h3 class="box-title sbold">Booking #{{ $bookingArr->bookings_reference }}</h3>
                                        
                                    </div>
                                </div>    
                                <div class="box-body">
                                    
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <?php 
                                                $travellersArr = $bookingArr->traveller;
                                                $countriesArr = $travellersArr->country;
                                                $bookingCountriesArr = $bookingArr->country_details;
                                            ?>

                                            <h3>Traveler Information</h3>
                                            #{{ $travellersArr->bookings_reference }} <br />
                                            Name: <?php if($travellersArr->title == 0){
                                                        echo 'Mr.';
                                                    } elseif($travellersArr->title == 1){
                                                        echo 'Mrs.';
                                                    } elseif($travellersArr->title == 2){
                                                        echo 'Miss.';
                                                    }
                                                    ?>  
                                            {{ $travellersArr->firstname }} 
                                            {{ $travellersArr->lastname }} <br />
                                            Email: {{ $travellersArr->email }} <br />
                                            Mobile: {{ $travellersArr->mobilenumber }} <br />
                                            Country: 
                                            <?php if(!empty($countriesArr)){ ?>
                                            {{ $countriesArr->countries_name }} <br />
                                            <?php }else{ ?>
                                                ---
                                            <?php } ?>
                                            
                                            
                                            <h3>Billing Address</h3>
                                            Name: <?php if($bookingArr->title==0){
                                                            echo 'Mr.';
                                                        } elseif($travellersArr->title==1){
                                                            echo 'Mrs.';
                                                        } elseif($travellersArr->title==2){
                                                            echo 'Miss.';
                                                        }
                                                ?>  
                                            {{ $bookingArr->firstname }} 
                                            {{ $bookingArr->lastname }} <br />
                                            Email: {{ $bookingArr->email }} <br />
                                            Mobile: {{ $bookingArr->mobilenumber }} <br />
                                            Country: 
                                            <?php if(!empty($bookingCountriesArr)){ ?>
                                            {{ $bookingCountriesArr->countries_name }} <br />
                                            <?php }else{ ?>
                                                ---
                                            <?php } ?>
                                        </div>
                                        <div class="col-xs-8">
                                            <h3>Booking Information</h3>
                                            #{{ $bookingArr->bookings_reference }} <br />
                                             SubTotal: {{ $bookingArr->subtotal }} <br />
                                             Tax/Fee: {{ $bookingArr->tax }} <br />
                                             Total: {{ $bookingArr->total }} <br />
                                             Status: 
                                             <?php
                                                $status = '';
                                                if($bookingArr->status == 0){
                                                    $status = 'New';
                                                }
                                                if($bookingArr->status == 1){
                                                    $status = 'Accepted';
                                                }
                                                if($bookingArr->status == 2){
                                                    $status = 'Declined';
                                                }
                                             ?>
                                              <br />
                                             Create Date: {{ date('d/m/Y' , strtotime($bookingArr->created_at)) }} <br />
                                             <?php
                                             $bookingCurrencyArr = $bookingArr->currency;
                                             ?>
                                             Currency: {{ $bookingCurrencyArr->title  }}
                                             <hr />

                                             <h4>Activities</h4>
                                             <?php if(count($bookingArr->bookedactivities) > 0){ 
                                                foreach($bookingArr->bookedactivities as $key => $value){

                                                        $activityObj = $value->activity;
                                                        $destinationObj = $activityObj->destination;
                                                        $currencyObj = $activityObj->currency;
                                                        $bookingActivityItems = $value->booking_activity_items;
                                                        $bookingActivityOptions = $value->bookings_activities_options;                                                         
                                                        ?>  
                                                    #{{ $value->activities_id }} <br />
                                                    Activity: {{ $activityObj->title }} <br /><br />
                                                    <img src="uploads/{{ $activityObj->image }}" > <br /><br />
                                                    Destination: {{ $destinationObj->title }} <br />
                                                    Currency: 
                                                    <?php if(!empty($currencyObj)){ ?>
                                                        {{$currencyObj->title}}
                                                    <?php } ?>
                                                    <br />
                                                    Date: {{ date('d/m/Y' , strtotime($value->bookingdate)) }} <br /><br /><br />
                                                    
                                                    Pricing:
                                                    <hr />
                                                    
                                                    <?php if(count($bookingActivityItems) > 0){ ?>
                                                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Price For</th>
                                                            <th>Title</th>
                                                            <th>Price</th>
                                                            <th>Qty</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        <?php foreach($bookingActivityItems as $items){ ?>
                                                        <tr>
                                                            <td>{{ ucwords($items->type) }}</td>
                                                            <td>{{ ucwords($items->pricefor) }}</td>
                                                            <td>{{ $items->title }}</td>
                                                            <td>{{ $items->price }}</td>
                                                            <td>{{ $items->quantity }}</td>
                                                            <td>{{ $items->quantity*$items->price }}</td>
                                                        </tr>                                                        
                                                        <?php } ?>
                                                    </table>    
                                                    <?php } ?>
                                                    <br /><br />
                                                    Options:
                                                    <hr />
                                                    
                                                    <?php if(count($bookingActivityOptions) > 0) { ?>
                                                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                                                        <tr>
                                                            <th>Title</th>
                                                            <th>Description</th>
                                                            <th>Value</th>
                                                        </tr>
                                                        <?php foreach($bookingActivityOptions as $options){ ?>
                                                        <tr>
                                                            <td>{{ ucwords($options->title) }}</td>
                                                            <td>{{ ucwords($options->description ) }}</td>
                                                            <td>{{ $options->value }}</td>
                                                        </tr>                                                        
                                                        <?php } ?>
                                                    </table>    
                                                    <?php } ?>
                                                    
                                                    
                                                    <hr />
                                                    
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('localguide/mybookings') }}"><input type="button" name="Back" class="btn bg-maroon btn-flat margin" value="Back"></a>

                            </div>
                        </div>
                   </div>   
                    
                </div>
                
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<!-- ./wrapper -->
@stop