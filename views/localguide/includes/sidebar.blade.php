<?php
$settings_active_class = $my_account_active_class = $my_bookings_active_class = $my_message_active_class = '';
if(Request::segment(2) == 'settings'){
    $settings_active_class = 'active';
}

if(Request::segment(2) == 'myaccount'){
    $my_account_active_class = 'active';
}

if(Request::segment(2) == 'mybookings'){
    $my_bookings_active_class = 'active';
}

if(Request::segment(2) == 'mymessages'){
    $my_message_active_class = 'active';
}
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="localguideadmin/dist/img/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ (Session::get('localguide')['fullname']) }}</p>
                <a href="{{ url('localguide') }}"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview {{$my_bookings_active_class}}">
                <a href="{{ url('localguide/mybookings') }}">
                    <i class="fa fa-list"></i>
                    <span>My Bookings</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('localguide/myactivities') }}">
                    <i class="fa fa-list"></i>
                    <span>My Activities</span>

                </a>
            </li>
            <li class="treeview">
                <a href="{{ url('localguide/myreviews') }}">
                    <i class="fa fa-list"></i>
                    <span>My Reviews</span>

                </a>
            </li>
            <li class="treeview {{ $my_account_active_class }}">
                <a href="{{ url('localguide/myaccount') }}">
                    <i class="fa fa-list"></i>
                    <span>My Account</span>
                </a>
            </li>
            <li class="treeview {{ $my_message_active_class }}">
                <a href="{{ url('localguide/mymessages') }}">
                    <i class="fa fa-list"></i>
                    <span>Messages</span>
                </a>
            </li>
            <li class="treeview {{ $settings_active_class }}">
                <a href="{{ url('localguide/settings') }}">
                    <i class="fa fa-list"></i>
                    <span>Settings</span>

                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
