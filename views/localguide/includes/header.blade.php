            <header class="main-header">
                <!-- Logo -->
                <a href="{{ url('/') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <img src="localguideadmin/dist/img/logo.png" alt="" class="img-responsive">
                    </span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown tasks-menu">
                            <li class="dropdown user user-menu">
                                <a href="{{ url('localguide') }}" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    
                                    <span class="hidden-xs">{{ (Session::get('localguide')['fullname']) }} </span>
                                </a>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="{{ url('localguide/logout')}}">
                                    <span class="hidden-xs"> <i class="fa fa-sign-out" aria-hidden="true"></i> Signout</span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
