<!DOCTYPE html>
<html>
    <head>
        @include('localguide.includes.head')
    </head>
        <body class="hold-transition skin-blue-light sidebar-mini">
        <div class="wrapper">    

                 @yield('content')

        </div>
    </body>
</html>


