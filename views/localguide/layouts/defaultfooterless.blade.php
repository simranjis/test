<!DOCTYPE html>
<html>
    <head>
        @include('localguide.includes.head')
    </head>
        <body class="hold-transition skin-blue-light sidebar-mini">
        <div class="wrapper">    
            @include('localguide.includes.header')

                 @yield('content')

            @include('localguide.includes.footer')
        </div>
    </body>
</html>


