
<!DOCTYPE html>
<html>
    <head>
        @include('localguide.includes.head')
    </head>
        <body class="hold-transition skin-blue-light sidebar-mini">
        <div class="wrapper">    
            @include('localguide.includes.header')
            @include('localguide.includes.sidebar')

                 @yield('content')

            @include('localguide.includes.footer')
            @yield('page_scripts')
        </div>
    </body>
</html>

